﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using ERP.ViewModels.UrlResolver;
using ERP.ViewModels.GlobalResolver;

namespace ERP.Utility.Profile
{
    public class ApplicationProfile : AutoMapper.Profile
    {
        public ApplicationProfile()
        {
            #region Inventory

            CreateMap<Category, CategoryViewModel>()
                .ForMember(s => s.Url,
                    opt => opt.ResolveUsing<CategoryUrlResolver>())
                .ReverseMap();

            CreateMap<Category, CategoryCreationViewModel>()
                .ReverseMap();

            CreateMap<Category, CategoryUpdateViewModel>()
                .ReverseMap();

            CreateMap<Item, ItemViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<ItemUrlResolver>())
                .ForMember(i => i.Links, opt => opt.ResolveUsing<ItemLinksResolver>())
                .ReverseMap();

            CreateMap<Item, ItemCreationViewModel>()
                .ReverseMap();

            CreateMap<Item, ItemUpdateViewModel>()
                .ReverseMap();

            CreateMap<Supplier, SupplierViewModel>()
                .ForMember(s => s.Url, opt => opt.ResolveUsing<SupplierUrlResolver>())
                .ForMember(s => s.FullName, opt => opt.ResolveUsing(s => $"{s.FirstName} {s.LastName}"))
                .ReverseMap();
            
            CreateMap<Supplier, SupplierCreationViewModel>()
                .ReverseMap();

            CreateMap<Supplier, SupplierUpdateViewModel>()
                .ReverseMap();

            CreateMap<Inventory, InventoryViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<InventoryUrlResolver>())
                .ForMember(i => i.Links, opt => opt.ResolveUsing<InventoryLinksResolver>())
                .ReverseMap();

            CreateMap<Inventory, InventoryCreationViewModel>().ReverseMap();

            CreateMap<Inventory, InventoryUpdateViewModel>().ReverseMap();

            CreateMap<InventoryItem, InventoryItemViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<InventoryItemUrlResolver>())
                .ReverseMap();

            CreateMap<InventoryItem, InventoryItemCreationViewModel>().ReverseMap();

            CreateMap<InventoryItem, InventoryItemUpdateViewModel>().ReverseMap();

            #endregion

            #region Order

            CreateMap<Customer, CustomerViewModel>()
               .ForMember(s => s.Url, opt => opt.ResolveUsing<CustomerUrlResolver>())
               .ReverseMap();

            CreateMap<Customer, CustomerCreationViewModel>()
                .ReverseMap();

            CreateMap<Customer, CustomerUpdateViewModel>()
                .ReverseMap();

            CreateMap<Bill, BillViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<BillUrlResolver>())
                .ReverseMap();

            CreateMap<Bill, BillCreationViewModel>()
             .ReverseMap();

            CreateMap<Bill, BillUpdateViewModel>()
                .ReverseMap();

            CreateMap<Order, OrderViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<OrderUrlResolver>())
                .ForMember(i => i.PaymentStatus, opt => opt.ResolveUsing<PaymentStatusEnumResolver>())
                .ForMember(i => i.OrderStatus, opt => opt.ResolveUsing<OrderStatusEnumResolver>())
                .ReverseMap();

            CreateMap<Order, OrderCreationViewModel>()
                .ReverseMap();

            CreateMap<Order, OrderUpdateViewModel>()
                .ReverseMap();

            CreateMap<OrderItem, OrderItemViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<OrderItemUrlResolver>())
                .ReverseMap();

            CreateMap<OrderItem, OrderItemLinearViewModel>()
                .ReverseMap();


            CreateMap<OrderItem, OrderItemCreationViewModel>()
                .ReverseMap();

            CreateMap<OrderItem, OrderItemUpdateViewModel>()
                .ReverseMap();

            #endregion

            #region Invoice
            CreateMap<Invoice, InvoiceViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<InvoiceUrlResolver>())
                .ReverseMap();

            CreateMap<Invoice, InvoiceCreationViewModel>().ReverseMap();

            CreateMap<Invoice, InvoiceUpdateViewModel>().ReverseMap();
           

            CreateMap<InvoiceItem, InvoiceItemViewModel>()
                .ForMember(i => i.Url, opt => opt.ResolveUsing<InvoiceItemUrlResolver>())
                .ReverseMap();
            CreateMap<InvoiceItem, InvoiceItemLinearViewModel>().ReverseMap();
            CreateMap<InvoiceItem, InvoiceItemCreationViewModel>().ReverseMap();

            CreateMap<InvoiceItem, InvoiceItemUpdateViewModel>().ReverseMap();
            #endregion

            CreateMap<MeasurementUnit, MeasurementUnitViewModel>()
                .ForMember(s => s.Url,
                    opt => opt.ResolveUsing<MeasurementUnitUrlResolver>())
                .ReverseMap();

            CreateMap<MeasurementUnit, MeasurementUnitCreationViewModel>()
                .ReverseMap();

            CreateMap<MeasurementUnit, MeasurementUnitUpdateViewModel>()
                .ReverseMap();

            CreateMap<MeasurementUnitSetup, MeasurementUnitSetupViewModel>()
              .ForMember(s => s.Url,
                  opt => opt.ResolveUsing<MeasurementUnitSetupUrlResolver>())
              .ReverseMap();

            CreateMap<MeasurementUnitSetup, MeasurementUnitSetupCreationViewModel>()
                .ReverseMap();
           

            CreateMap<MeasurementUnitSetup, MeasurementUnitSetupUpdateViewModel>()
                .ReverseMap();

            CreateMap<Warehouse, WarehouseViewModel>()
               .ForMember(s => s.Url,
                   opt => opt.ResolveUsing<WarehouseUrlResolver>())
               .ReverseMap();

            CreateMap<Warehouse, WarehouseCreationViewModel>()
                .ReverseMap();

            CreateMap<Warehouse, WarehouseUpdateViewModel>()
                .ReverseMap();

            CreateMap<WarehouseItem, WarehouseItemViewModel>()
              .ForMember(s => s.Url,
                  opt => opt.ResolveUsing<WarehouseItemUrlResolver>())
              .ReverseMap();
            CreateMap<WarehouseItem, WarehouseItemCreationViewModel>()
                .ReverseMap();
            CreateMap<WarehouseItem, WarehouseItemUpdateViewModel>()
                .ReverseMap();
        }
    }
}
