﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Globalization;

namespace ERP.Utility.Attributes
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (!context.ModelState.IsValid)
            {
                context.Result = new UnprocessableEntityObjectResult(context.ModelState);
            }
        }
    }

    public class UnprocessableEntityObjectResult : ObjectResult
    {
        public UnprocessableEntityObjectResult(ModelStateDictionary modelState) : base(new SerializableError(modelState))
        {
            if (modelState == null)
            {
                throw new ArgumentNullException(nameof(modelState));
            }
            StatusCode = 422;
        }
    }

    public class GreaterThanAttribute : CompareAttribute
    {
        public GreaterThanAttribute(string otherProperty) : base(otherProperty) { }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo otherPropertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);
            if (otherPropertyInfo == null)
                return new ValidationResult(String.Format(CultureInfo.CurrentCulture, $"Could not find a property named {OtherProperty}."));

            object otherPropertyValue = otherPropertyInfo.GetValue(validationContext.ObjectInstance, null);

            switch (Type.GetTypeCode(otherPropertyInfo.PropertyType))
            {
                case TypeCode.DateTime:
                    if ((DateTime) value < (DateTime)otherPropertyValue)
                        return new ValidationResult(GetGreaterThanErrorMessage(validationContext, otherPropertyInfo.Name));
                    break;
                case TypeCode.Int32:
                    if ((Int32)value < (Int32)otherPropertyValue)
                        return new ValidationResult(GetGreaterThanErrorMessage(validationContext, otherPropertyInfo.Name));
                    break;
                case TypeCode.Double:
                    if ((Double)value < (Double)otherPropertyValue)
                        return new ValidationResult(GetGreaterThanErrorMessage(validationContext, otherPropertyInfo.Name));
                    break;
                case TypeCode.Decimal:
                    if ((Decimal)value < (Decimal)otherPropertyValue)
                        return new ValidationResult(GetGreaterThanErrorMessage(validationContext, otherPropertyInfo.Name));
                    break;
                default:
                    return new ValidationResult("Type doesn't support greater than operand");
            }
            return null;
        }

        private string GetGreaterThanErrorMessage(ValidationContext validationContext, string otherPropertyName)
        {
            if (string.IsNullOrEmpty(ErrorMessageString))
                return String.Format(CultureInfo.CurrentCulture, $"'{validationContext.DisplayName}' must be greater than '{otherPropertyName}'.");
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString);
        }
    }
}
