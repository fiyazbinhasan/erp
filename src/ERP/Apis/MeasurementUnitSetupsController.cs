﻿using AutoMapper;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class MeasurementUnitSetupsController : BaseController
    {
        private readonly IMeasurementUnitSetupRepository _measurementUnitSetupRepository;
        private readonly IRepository<MeasurementUnit> _measurementUnitRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<MeasurementUnitSetupsController> _logger;

        public MeasurementUnitSetupsController(IMeasurementUnitSetupRepository measurementUnitSetupRepository, 
                                               IRepository<MeasurementUnit> measurementUnitRepository,
                                               IItemRepository itemRepository,
                                               IUrlHelper iurlHelper, 
                                               IMapper mapper, 
                                               ILogger<MeasurementUnitSetupsController> logger)
        {
            _measurementUnitSetupRepository = measurementUnitSetupRepository;
            _measurementUnitRepository = measurementUnitRepository;
            _itemRepository = itemRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("api/v{version:apiVersion}/measurementUnitSetups"), MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var measurementUnitSetups = _measurementUnitSetupRepository.Get();
            return Ok(_mapper.Map<IEnumerable<MeasurementUnitSetupViewModel>>(measurementUnitSetups));
        }

        [HttpGet("api/v{version:apiVersion}/measurementUnitSetups/{id}", Name = "MeasurementUnitSetupGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var measurementUnitSetup = _measurementUnitSetupRepository.Get(id);
            if (measurementUnitSetup == null) return NotFound("Measurement Unit Setup not found.");
            return Ok(_mapper.Map<MeasurementUnitSetupViewModel>(measurementUnitSetup));
        }

        [HttpPost("api/v{version:apiVersion}/measurementUnitSetups"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] MeasurementUnitSetupCreationViewModel vm)
        {
            try
            {
                var item = _itemRepository.Get(vm.ItemId);
                if (item == null) return NotFound("Item not found.");

                var measurementUnit = _measurementUnitRepository.Get(vm.MeasurementUnitId);
                if (measurementUnit == null) return NotFound("Measurement Unit not found.");

                var measurementUnitSetup = _mapper.Map<MeasurementUnitSetup>(vm);
                
                _measurementUnitSetupRepository.Add(measurementUnitSetup);

                if (await _measurementUnitSetupRepository.SaveChangesAsync())
                {
                    var url = Url.Link("MeasurementUnitSetupGet", new { id = measurementUnitSetup.Id });
                    return Created(url, _mapper.Map<MeasurementUnitSetupViewModel>(measurementUnitSetup));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding measurement unit: {ex}");
            }
            return BadRequest("Could not add new measurement unit.");
        }

        [HttpPut("api/v{version:apiVersion}/measurementUnitSetups/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] MeasurementUnitSetupUpdateViewModel vm)
        {
            try
            {
                var item = _itemRepository.Get(vm.ItemId);
                if (item == null) return NotFound("Item not found.");

                var measurementUnit = _measurementUnitRepository.Get(vm.MeasurementUnitId);
                if (measurementUnit == null) return NotFound("measurement Unit not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(id);
                if (measurementUnitSetup == null)
                {
                    var measurementUnitSetupToAdd = Mapper.Map<MeasurementUnitSetup>(vm);
                    measurementUnitSetupToAdd.Id = id;
                    measurementUnitSetupToAdd.MeasurementUnit = measurementUnit;
                    measurementUnitSetupToAdd.Item = item;

                    _measurementUnitSetupRepository.Add(measurementUnitSetupToAdd);

                    if (await _measurementUnitSetupRepository.SaveChangesAsync())
                    {
                        return Ok(_mapper.Map<MeasurementUnitSetupViewModel>(measurementUnitSetupToAdd));
                    }
                }

                _mapper.Map(vm, measurementUnitSetup);
                _measurementUnitSetupRepository.Update(measurementUnitSetup);

                if (await _measurementUnitSetupRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<MeasurementUnitSetupViewModel>(measurementUnitSetup));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating measurement unit: {ex}");
            }

            return BadRequest("Could not update measurement unit setup.");
        }

        [HttpDelete("api/v{version:apiVersion}/measurementUnitSetups/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var measurementUnitSetup = _measurementUnitSetupRepository.Get(id);
                if (measurementUnitSetup == null) return NotFound("Measurement unit setup not found.");

                _measurementUnitSetupRepository.Delete(measurementUnitSetup);

                if (await _measurementUnitSetupRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while measurementUnitSetup speaker: {ex}");
            }
            return BadRequest("Could not delete measurementUnitSetup.");
        }

        [HttpGet("api/v{version:apiVersion}/measurementUnitSetups/{itemId}/measurementUnits"), MapToApiVersion("1.0")]
        public IActionResult GetMeasurementUnits(Guid itemId)
        {
            var measurementUnitSetups = _measurementUnitSetupRepository.GetMeasurementUnits(itemId);
            return Ok(_mapper.Map<IEnumerable<MeasurementUnitSetupViewModel>>(measurementUnitSetups));
        }

        [HttpGet("api/v{version:apiVersion}/measurementUnitSetups/{measurementUnitId}/items"), MapToApiVersion("1.0")]
        public IActionResult GetItems(Guid measurementUnitId)
        {
            var measurementUnitSetups = _measurementUnitSetupRepository.GetItems(measurementUnitId);
            return Ok(_mapper.Map<IEnumerable<MeasurementUnitSetupViewModel>>(measurementUnitSetups));
        }


        [HttpPut("api/v{version:apiVersion}/measurementUnitSetups/{id}/{conversionRatio}/{currentBaseId}"), MapToApiVersion("1.0")]
        public IActionResult BaseWiseQuantityConversion(Guid id, decimal conversionRatio, Guid currentBaseId)
        {
            var measurementUnitSetup = _measurementUnitSetupRepository.BaseWiseQuantityConversion(id, conversionRatio, currentBaseId);
            return Ok(_mapper.Map<MeasurementUnitSetupViewModel>(measurementUnitSetup));
        }
    }
}
