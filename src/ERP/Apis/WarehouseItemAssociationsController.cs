﻿using AutoMapper;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class WarehouseItemAssociationsController : BaseController
    {
        private readonly IWarehouseItemRepository _warehouseItemRepository;
        private readonly IMeasurementUnitSetupRepository _measurementUnitSetupRepository;
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<WarehouseItemAssociationsController> _logger;

        public WarehouseItemAssociationsController(IWarehouseItemRepository warehouseItemRepository, IWarehouseRepository warehouseRepository, IMeasurementUnitSetupRepository measurementUnitSetupRepository,
            ILogger<WarehouseItemAssociationsController> logger, IMapper mapper)
        {
            _warehouseItemRepository = warehouseItemRepository;
            _warehouseRepository = warehouseRepository;
            _measurementUnitSetupRepository = measurementUnitSetupRepository;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("api/v{version:apiVersion}/WarehouseItemAssociations"), MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var warehouseItemAssociations = _warehouseItemRepository.Get();
            return Ok(_mapper.Map<IEnumerable<WarehouseItemViewModel>>(warehouseItemAssociations));
        }

        [HttpGet("api/v{version:apiVersion}/WarehouseItemAssociations/{id}", Name = "WarehouseItemAssociationGet"),
         MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var warehouseItem = _warehouseItemRepository.Get(id);
            if (warehouseItem == null) return NotFound("WarehouseItem not found.");
            return Ok(_mapper.Map<WarehouseItemViewModel>(warehouseItem));
        }

        [HttpGet("api/v{version:apiVersion}/WarehouseItemAssociations/{WarehouseId}/items"), MapToApiVersion("1.0")]
        public IActionResult GetItems(Guid warehouseId)
        {
            var items = _warehouseItemRepository.GetItems(warehouseId);
            return Ok(_mapper.Map<IEnumerable<WarehouseItemViewModel>>(items));
        }

        [HttpGet("api/v{version:apiVersion}/WarehouseItemAssociations/{WarehouseId}/items/{ItemId}"),
         MapToApiVersion("1.0")]
        public IActionResult GetItem(Guid warehouseId, Guid itemId)
        {
            var item = _warehouseItemRepository.GetItem(warehouseId, itemId);
            if (item == null)
                return NoContent();
            return Ok(_mapper.Map<WarehouseItemViewModel>(item));
        }

        [HttpGet("api/v{version:apiVersion}/WarehouseItemAssociations/{itemId}/warehouses"), MapToApiVersion("1.0")]
        public IActionResult GetWarehouses(Guid itemId)
        {
            var warehouses = _warehouseItemRepository.GetWarehouses(itemId);
            return Ok(_mapper.Map<IEnumerable<WarehouseItemViewModel>>(warehouses));
        }

        [HttpPut("api/v{version:apiVersion}/WarehouseItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] WarehouseItemUpdateViewModel vm)
        {
            try
            {
                var warehouseItem = _warehouseItemRepository.Get(id);

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("measurement Unit Setup not found.");

                var warehouse = _warehouseRepository.Get(vm.WarehouseId);
                if (warehouse == null) return NotFound("warehouse not found.");

                _mapper.Map(vm, warehouseItem);

                _warehouseItemRepository.Update(warehouseItem);

                if (await _warehouseItemRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<WarehouseItemViewModel>(warehouseItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating warehouseItem: {ex}");
            }

            return BadRequest("Could not update warehouseItem.");
        }
    }
}
