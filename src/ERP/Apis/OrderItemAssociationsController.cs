﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class OrderItemAssociationsController :BaseController
    {
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IMeasurementUnitSetupRepository _measurementUnitSetupRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<OrderItemAssociationsController> _logger;

        public OrderItemAssociationsController(IOrderItemRepository orderItemRepository, 
                    IOrderRepository orderRepository, 
                    IItemRepository itemRepository,
                    IMeasurementUnitSetupRepository measurementUnitSetupRepository,
                    IUrlHelper iurlHelper, 
                    IMapper mapper, 
                    ILogger<OrderItemAssociationsController> logger)
        {
            _orderItemRepository = orderItemRepository;
            _orderRepository = orderRepository;
            _itemRepository = itemRepository;
            _measurementUnitSetupRepository = measurementUnitSetupRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
        }

        /* api/OrderItemAssociations */
        [HttpGet("api/v{version:apiVersion}/OrderItemAssociations"), MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var orderItemAssociations = _orderItemRepository.Get();
            return Ok(_mapper.Map<IEnumerable<OrderItemViewModel>>(orderItemAssociations));
        }

        /* api/OrderItemAssociations/1 */
        [HttpGet("api/v{version:apiVersion}/OrderItemAssociations/{id}", Name = "OrderItemAssociationGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var orderItem = _orderItemRepository.Get(id);
            if (orderItem == null) return NotFound();
            return Ok(_mapper.Map<OrderItemViewModel>(orderItem));
        }

        [HttpGet("api/v{version:apiVersion}/OrderItemAssociations/{orderId}/items"), MapToApiVersion("1.0")]
        public IActionResult GetItems(Guid orderId)
        {
            var orderItems = _orderItemRepository.GetItems(orderId);
            return Ok(_mapper.Map<IEnumerable<OrderItemViewModel>>(orderItems));
        }

        /* api/OrderItemAssociations */
        [HttpPost("api/v{version:apiVersion}/OrderItemAssociations"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] OrderItemCreationViewModel vm)
        {
            try
            {
                var order = _orderRepository.Get(vm.OrderId);
                if (order == null) return NotFound("Order not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("MeasurementUnitSetup not found.");

                var orderItem = _mapper.Map<OrderItem>(vm);

                _orderItemRepository.Add(orderItem);

                if (await _orderItemRepository.SaveChangesAsync())
                {
                    var url = Url.Link("OrderItemAssociationGet", new { id = orderItem.Id });
                    return Created(url, _mapper.Map<OrderItemViewModel>(orderItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding orderItem: {ex.Message}");
            }
            return BadRequest("Could not add new orderItem.");
        }

        [HttpPut("api/v{version:apiVersion}/OrderItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] OrderItemUpdateViewModel vm)
        {
            try
            {
                var orderItem = _orderItemRepository.Get(id);
                if (orderItem == null) return NotFound("OrderItem not found.");

                var order = _orderRepository.Get(vm.OrderId);
                if (order == null) return NotFound("Order not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("MeasurementUnitSetup not found.");

                _mapper.Map(vm, orderItem);

                _orderItemRepository.Update(orderItem);

                if (await _orderItemRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<OrderItemViewModel>(orderItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating order-item relation: {ex}");
            }

            return BadRequest("Could not update order-item relation");
        }

        /* api/OrderItemAssociations/2 */
        [HttpDelete("api/v{version:apiVersion}/OrderItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var orderItem = _orderItemRepository.Get(id);
                if (orderItem == null) return NotFound("OrderItem not found.");

                _orderItemRepository.Delete(orderItem);

                if (await _orderItemRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while OrderItem speaker: {ex}");
            }

            return BadRequest("Could not delete OrderItem.");
        }

        [HttpPatch("api/v{version:apiVersion}/OrderItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<OrderItemUpdateViewModel> vm)
        {
            try
            {
                var orderItem = _orderItemRepository.Get(id);
                if (orderItem == null) return NotFound("OrderItem not found.");

                var order = _orderRepository.Get(orderItem.OrderId);
                if (order == null) return NotFound("Order not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(orderItem.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("MeasurementUnitSetup unit not found.");

                var orderItemToPatch = _mapper.Map<OrderItemUpdateViewModel>(orderItem);
                vm.ApplyTo(orderItemToPatch);

                TryValidateModel(orderItemToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                _mapper.Map(orderItemToPatch, orderItem);

                if (await _orderItemRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<OrderItemViewModel>(orderItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating orderItem: {ex}");
            }
            return BadRequest("Could not update orderItem.");
        }
    }
}
