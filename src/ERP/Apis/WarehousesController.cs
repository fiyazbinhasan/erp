﻿using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class WarehousesController : BaseController
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IWarehouseItemRepository _warehouseItemRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<WarehousesController> _logger;
        private readonly ITypeHelperService _typeHelperService;

        public WarehousesController(IWarehouseRepository warehouseRepository,
                                     IWarehouseItemRepository warehouseItemRepository,
                                     IItemRepository itemRepository,
                                     IUrlHelper iurlHelper,
                                     IMapper mapper,
                                     ITypeHelperService typeHelperService,
                                     ILogger<WarehousesController> logger)
        {
            _warehouseRepository = warehouseRepository;
            _warehouseItemRepository = warehouseItemRepository;
            _itemRepository = itemRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _typeHelperService = typeHelperService;
        }
        
        [HttpGet("api/v{version:apiVersion}/warehouses", Name = "GetWarehouses"), MapToApiVersion("1.0")]
        public IActionResult Get(WarehouseResourceParameters warehouseResourceParameters)
        {

            if (!_typeHelperService.TypeHasProperty<WarehouseViewModel>
                (warehouseResourceParameters.OrderBy))
            {
                return BadRequest();
            }
            if (!_typeHelperService.TypeHasProperties<WarehouseViewModel>
                (warehouseResourceParameters.Fields))
            {
                return BadRequest();
            }
            var warehouses = _warehouseRepository.Get(warehouseResourceParameters);

            var previousPageLink = warehouses.HasPrevious
                ? CreateWarehouseResourceUri(warehouseResourceParameters, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = warehouses.HasNext
                ? CreateWarehouseResourceUri(warehouseResourceParameters, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = warehouses.TotalCount,
                pageSize = warehouses.PageSize,
                currentPage = warehouses.CurrentPage,
                totalPages = warehouses.TotalPages,
                previousPageLink,
                nextPageLink
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));
            return Ok(_mapper.Map<IEnumerable<WarehouseViewModel>>(warehouses).ShapeData(warehouseResourceParameters.Fields));
        }

        private string CreateWarehouseResourceUri(WarehouseResourceParameters warehouseResourceParameters, ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetWarehouses",
                        new
                        {
                            pageNumber = warehouseResourceParameters.PageNumber - 1,
                            pageSize = warehouseResourceParameters.PageSize,
                            orderBy = warehouseResourceParameters.OrderBy,
                            fields = warehouseResourceParameters.Fields,
                            searchQuery = warehouseResourceParameters.SearchQuery,
                            moniker = warehouseResourceParameters.Moniker
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetWarehouses",
                        new
                        {
                            pageNumber = warehouseResourceParameters.PageNumber + 1,
                            pageSize = warehouseResourceParameters.PageSize,
                            orderBy = warehouseResourceParameters.OrderBy,
                            fields = warehouseResourceParameters.Fields,
                            searchQuery = warehouseResourceParameters.SearchQuery,
                            moniker = warehouseResourceParameters.Moniker
                        });
                default:
                    return _urlHelper.Link("GetWarehouses",
                        new
                        {
                            pageNumber = warehouseResourceParameters.PageNumber,
                            pageSize = warehouseResourceParameters.PageSize,
                            orderBy = warehouseResourceParameters.OrderBy,
                            fields = warehouseResourceParameters.Fields,
                            searchQuery = warehouseResourceParameters.SearchQuery,
                            moniker = warehouseResourceParameters.Moniker
                        });
            }
        }

        [HttpGet("api/v{version:apiVersion}/warehouses/{id}", Name = "WarehouseGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var warehouse = _warehouseRepository.Get(id);
            if (warehouse == null) return NotFound("Warehouse not found.");
            return Ok(_mapper.Map<WarehouseViewModel>(warehouse));
        }

        [HttpPost("api/v{version:apiVersion}/warehouses"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] WarehouseCreationViewModel vm)
        {
            try
            {
                var warehouse = _mapper.Map<Warehouse>(vm);
                _warehouseRepository.Add(warehouse);

                if (await _warehouseRepository.SaveChangesAsync())
                {
                    var url = Url.Link("WarehouseGet", new { id = warehouse.Id });
                    return Created(url, _mapper.Map<WarehouseViewModel>(warehouse));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding warehouse: {ex}");
            }
            return BadRequest("Could not add new warehouse.");
        }


        [HttpPut("api/v{version:apiVersion}/warehouses/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid supplierId, Guid id, [FromBody] WarehouseUpdateViewModel vm)
        {
            try
            {
                var warehouse = _warehouseRepository.Get(id);
                if (warehouse == null)
                {
                    var warehouseToAdd = Mapper.Map<Warehouse>(vm);
                    warehouseToAdd.Id = id;
                    _warehouseRepository.Add(warehouseToAdd);
                    if (await _warehouseRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("WarehouseGet", new { id = id });
                        return Created(url, _mapper.Map<WarehouseViewModel>(warehouseToAdd));
                    }
                }
                _mapper.Map(vm, warehouse);
                _warehouseRepository.Update(warehouse);
                if (await _warehouseRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<WarehouseViewModel>(warehouse));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating warehouse: {ex}");
            }
            return BadRequest("Could not update warehouse.");
        }

        [HttpDelete("api/v{version:apiVersion}/warehouses/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid id)
        {
            try
            {
                var warehouse = _warehouseRepository.Get(id);
                if (warehouse == null) return NotFound("Warehouse not found.");
                _warehouseRepository.Delete(warehouse);
                if (await _warehouseRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while warehouse speaker: {ex}");
            }

            return BadRequest("Could not delete warehouse");
        }

        [HttpPatch("api/v{version:apiVersion}/warehouses/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<WarehouseUpdateViewModel> vm)
        {
            try
            {
                var warehouse = _warehouseRepository.Get(id);
                if (warehouse == null)
                {
                    var warehouseViewModel = new WarehouseUpdateViewModel();
                    vm.ApplyTo(warehouseViewModel, ModelState);
                    TryValidateModel(warehouseViewModel);
                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }
                    var warehouseToAdd = Mapper.Map<Warehouse>(vm);
                    warehouseToAdd.Id = id;
                    _warehouseRepository.Add(warehouseToAdd);
                    if (await _warehouseRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("WarehouseGet", new { id = id });
                        return Created(url, _mapper.Map<WarehouseViewModel>(warehouseToAdd));
                    }
                }
                var warehouseToPatch = _mapper.Map<WarehouseUpdateViewModel>(warehouse);
                vm.ApplyTo(warehouseToPatch, ModelState);
                TryValidateModel(warehouseToPatch);
                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }
                _mapper.Map(warehouseToPatch, warehouse);

                if (await _warehouseRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<WarehouseViewModel>(warehouse));

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating warehouse: {ex}");
            }
            return BadRequest("Could not update warehouse.");
        }
    }
}
