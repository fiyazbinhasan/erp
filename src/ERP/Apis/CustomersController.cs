﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ERP.Data.Helpers.RepositoryExtensions;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ValidateModel]
    public class CustomersController : BaseController
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ITypeHelperService _typeHelperService;
        private readonly ILogger<CustomersController> _logger;
        private readonly IMemoryCache _cache;

        public CustomersController(ICustomerRepository customerRepository, IUrlHelper iurlHelper, IMapper mapper, ILogger<CustomersController> logger, IMemoryCache cache,
            ITypeHelperService typeHelperService)
        {
            _customerRepository = customerRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _cache = cache;
            _typeHelperService = typeHelperService;
        }

        /* api/Customers?pageNumber=1&pageSize=10 */
        [HttpGet(Name = "GetCustomers"), MapToApiVersion("1.0")]
        public IActionResult Get(CustomerResourceParameters customerResourceParameters)
        {
            if (!_typeHelperService.TypeHasProperty<CustomerViewModel>
                (customerResourceParameters.OrderBy))
            {
                return BadRequest();
            }
            if (!_typeHelperService.TypeHasProperties<CustomerViewModel>
                (customerResourceParameters.Fields))
            {
                return BadRequest();
            }
            var customers = _customerRepository.Get(customerResourceParameters);

            var previousPageLink = customers.HasPrevious
                ? CreateCustomerResourceUri(customerResourceParameters, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = customers.HasNext
                ? CreateCustomerResourceUri(customerResourceParameters, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = customers.TotalCount,
                pageSize = customers.PageSize,
                currentPage = customers.CurrentPage,
                totalPages = customers.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));

            return Ok(_mapper.Map<IEnumerable<CustomerViewModel>>(customers).ShapeData(customerResourceParameters.Fields));
        }

        private string CreateCustomerResourceUri(CustomerResourceParameters customerResourceParameters,
            ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetCustomers",
                        new
                        {
                            pageNumber = customerResourceParameters.PageNumber - 1,
                            pageSize = customerResourceParameters.PageSize,
                            orderBy = customerResourceParameters.OrderBy,
                            fields = customerResourceParameters.Fields,
                            searchQuery = customerResourceParameters.SearchQuery,
                            includes = customerResourceParameters.Includes,
                            firstName = customerResourceParameters.FirstName,
                            phone = customerResourceParameters.Phone,
                            lastName = customerResourceParameters.LastName,
                            email = customerResourceParameters.Email
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetCustomers",
                        new
                        {
                            pageNumber = customerResourceParameters.PageNumber + 1,
                            pageSize = customerResourceParameters.PageSize,
                            orderBy = customerResourceParameters.OrderBy,
                            fields = customerResourceParameters.Fields,
                            searchQuery = customerResourceParameters.SearchQuery,
                            includes = customerResourceParameters.Includes,
                            firstName = customerResourceParameters.FirstName,
                            phone = customerResourceParameters.Phone,
                            lastName = customerResourceParameters.LastName,
                            email = customerResourceParameters.Email
                        });
                default:
                    return _urlHelper.Link("GetCustomers",
                        new
                        {
                            pageNumber = customerResourceParameters.PageNumber,
                            pageSize = customerResourceParameters.PageSize,
                            orderBy = customerResourceParameters.OrderBy,
                            fields = customerResourceParameters.Fields,
                            searchQuery = customerResourceParameters.SearchQuery,
                            includes = customerResourceParameters.Includes,
                            firstName = customerResourceParameters.FirstName,
                            phone = customerResourceParameters.Phone,
                            lastName = customerResourceParameters.LastName,
                            email = customerResourceParameters.Email
                        });
            }
        }

        /* api/Customers/1 */
        [HttpGet("{id:guid}", Name = "CustomerGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            if (Request.Headers.ContainsKey("If-None-Match"))
            {
                var oldETag = Request.Headers["If-None-Match"].First();
                if (_cache.Get($"Customer-{id}-{oldETag}") != null)
                {
                    return StatusCode((int) HttpStatusCode.NotModified);
                }
            }
            var customer = _customerRepository.Get(id);
            if (customer == null) return NotFound("Customer not found.");

            Response.Headers.Add("ETag", _customerRepository.AddETag(customer, _cache));

            return Ok(_mapper.Map<CustomerViewModel>(customer));
        }

        /* api/Customers */
        [HttpPost, MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] CustomerCreationViewModel vm)
        {
            try
            {
                var customer = _mapper.Map<Customer>(vm);
                
                _customerRepository.Add(customer);

                if (await _customerRepository.SaveChangesAsync())
                {
                    var url = Url.Link("CustomerGet", new { id = customer.Id });
                    return Created(url, _mapper.Map<CustomerViewModel>(customer));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding Customer: {ex}");
                return BadRequest(ex.Message);
            }
            return BadRequest("Could not add new Customer.");
        }

        /* api/Customers/2 */
        [HttpPut("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] CustomerUpdateViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(id);
                if (customer == null)
                {
                    var customerToAdd = Mapper.Map<Customer>(vm);
                    customerToAdd.Id = id;

                    _customerRepository.Add(customerToAdd);
                    
                    if (await _customerRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("CustomerGet", new { id = id });
                        return Created(url, _mapper.Map<CustomerViewModel>(customerToAdd));
                    }
                }

                if (Request.Headers.ContainsKey("If-Match"))
                {
                    var etag = Request.Headers["If-Match"].First();
                    if (etag != Convert.ToBase64String(_customerRepository.GetVersion(customer)))
                    {
                        return StatusCode((int)HttpStatusCode.PreconditionFailed);
                    }
                }

                _mapper.Map(vm, customer);

                if (await _customerRepository.SaveChangesAsync())
                {
                    Response.Headers.Add("ETag", _customerRepository.AddETag(customer, _cache));

                    return Ok(_mapper.Map<CustomerViewModel>(customer));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating Customer: {ex}");
            }

            return BadRequest("Could not update Customer.");
        }

        /* api/Customers/2 */
        [HttpDelete("{id:Guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid id)
        {
            try
            {
                var customer = _customerRepository.Get(id);
                if (customer == null) return NotFound();

                if (Request.Headers.ContainsKey("If-Match"))
                {
                    var etag = Request.Headers["If-Match"].First();
                    if (etag != Convert.ToBase64String(_customerRepository.GetVersion(customer)))
                    {
                        return StatusCode((int)HttpStatusCode.PreconditionFailed);
                    }
                }

                _customerRepository.Delete(customer);

                if (await _customerRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while Customer speaker: {ex}");
            }

            return BadRequest("Could not delete Customer");
        }
        
        [HttpPatch("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<CustomerUpdateViewModel> vm)
        {

            try
            {

                var customer = _customerRepository.Get(id);
                if (customer == null)
                {
                    var customerViewModel = new CustomerUpdateViewModel();
                    vm.ApplyTo(customerViewModel, ModelState);

                    TryValidateModel(customerViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var customerToAdd = Mapper.Map<Customer>(vm);
                    customerToAdd.Id = id;

                    _customerRepository.Add(customerToAdd);

                    if (await _customerRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("CustomerGet", new { id = id });
                        return Created(url, _mapper.Map<CustomerViewModel>(customerToAdd));
                    }
                }

                var customerToPatch = _mapper.Map<CustomerUpdateViewModel>(customer);
                vm.ApplyTo(customerToPatch, ModelState);

                TryValidateModel(customerToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(customerToPatch, customer);

                if (await _customerRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<CustomerViewModel>(customer));

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating customer: {ex}");
            }
            return BadRequest("Could not update customer.");

        }

        [HttpGet("{id}/GetCustomerView", Name = "GetCustomerView"), MapToApiVersion("1.0")]
        public IActionResult GetCustomerView(Guid id)
        {
            var customer = _customerRepository.Get(id);
            if (customer == null) return NotFound("Customer not found.");
            var reportData = _customerRepository.CustomerViewDatas(id);
            return Ok(reportData);
        }
    }
}
