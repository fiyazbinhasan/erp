﻿using ERP.Data.Helpers.ExcelGenerators;
using ERP.Data.Helpers.RepositoryExtensions;
using ERP.Data.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    public class SalesReportsController : BaseController
    {
        private readonly IItemRepository _itemRepository;
        private readonly ICustomerRepository _customerRepository;

        public SalesReportsController(IItemRepository itemRepository, ICustomerRepository customerRepository )
        {
            _itemRepository = itemRepository;
            _customerRepository = customerRepository;
        }

        [HttpGet("api/v{version:apiVersion}/salesReports/salesByItemReport/{startDate}/{endDate}"), MapToApiVersion("1.0")]
        public IActionResult GetSalesByItemReport(DateTime startDate, DateTime endDate)
        {
            var salesByItem = _itemRepository.SalesByItem(startDate, endDate);
            return Ok(salesByItem);
        }

        [HttpGet("api/v{version:apiVersion}/salesReports/salesByItemReport/{startDate}/{endDate}/{allowZero}/generateExcelSalesByItem", Name = "GenerateExcelSalesByItem"), MapToApiVersion("1.0")]
        public ActionResult GenerateExcelSalesByItem(DateTime startDate, DateTime endDate, string allowZero)
        {
            var salesByItem = _itemRepository.SalesByItem(startDate, endDate);
            SalesByItemExcel excel = new SalesByItemExcel();
            return File(excel.GenerateExcel(startDate, endDate, allowZero, salesByItem), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventoryDetailsReport");
        }

        [HttpGet("api/v{version:apiVersion}/salesReports/salesByCustomerReport/{startDate}/{endDate}"), MapToApiVersion("1.0")]
        public IActionResult GetSalesByCustomerReport(DateTime startDate, DateTime endDate)
        {
            var salesByCustomer = _customerRepository.SalesByCustomer(startDate, endDate);
            return Ok(salesByCustomer);
        }

        [HttpGet("api/v{version:apiVersion}/salesReports/salesByCustomerReport/{startDate}/{endDate}/{allowZero}/generateExcelSalesByCustomer", Name = "GenerateExcelSalesByCustomer"), MapToApiVersion("1.0")]
        public ActionResult GenerateExcelSalesByCustomer(DateTime startDate, DateTime endDate, string allowZero)
        {
            var salesByCustomer = _customerRepository.SalesByCustomer(startDate, endDate);
            SalesByCustomerExcel excel = new SalesByCustomerExcel();
            return File(excel.GenerateExcel(startDate, endDate, allowZero, salesByCustomer), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventoryDetailsReport");
        }
    }
}
