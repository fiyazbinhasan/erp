﻿using AutoMapper;
using ERP.Data.Helpers;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Utility.Attributes;
using System.Linq.Dynamic.Core;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Services.Interfaces;
using Microsoft.AspNetCore.JsonPatch;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ValidateModel]
    public class CategoriesController : BaseController
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<CategoriesController> _logger;
        private readonly ITypeHelperService _typeHelperService;

        public CategoriesController(ITypeHelperService typeHelperService,IRepository<Category> categoryRepository, IUrlHelper iurlHelper, IMapper mapper, ILogger<CategoriesController> logger)
        {
            _categoryRepository = categoryRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _typeHelperService = typeHelperService;
        }

        /* api/Categories */
        [HttpGet, MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var categories = _categoryRepository.Get();
            return Ok(_mapper.Map<IEnumerable<CategoryViewModel>>(categories));
        }

        /* api/Categories/1 */
        [HttpGet("{id:guid}", Name = "CategoryGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var category = _categoryRepository.Get(id);

            if (category == null) return NotFound();

            return Ok(_mapper.Map<CategoryViewModel>(category));
        }

        /* api/Categories */
        [HttpPost, MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] CategoryCreationViewModel vm)
        {
            try
            {
                var category = _mapper.Map<Category>(vm);

                _categoryRepository.Add(category);

                if (await _categoryRepository.SaveChangesAsync())
                {
                    var url = Url.Link("CategoryGet", new { id = category.Id });
                    return Created(url, _mapper.Map<CategoryViewModel>(category));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding category: {ex}");
            }
            return BadRequest("Could not add new category.");
        }

        /* api/Categories/2 */
        [HttpPut("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] CategoryUpdateViewModel vm)
        {
            try
            {
                var category = _categoryRepository.Get(id);
                if (category == null)
                {
                    var categoryToAdd = Mapper.Map<Category>(vm);
                    categoryToAdd.Id = id;

                    _categoryRepository.Add(categoryToAdd);

                    if (await _categoryRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("CategoryGet", new { id = id });
                        return Created(url, _mapper.Map<CategoryViewModel>(categoryToAdd));
                    }
                }

                _mapper.Map(vm, category);

                if (await _categoryRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<CategoryViewModel>(category));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating category: {ex}");
            }

            return BadRequest("Could not update category.");
        }

        /* api/Categories/2 */
        [HttpDelete("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var category = _categoryRepository.Get(id);
                if (category == null) return NotFound();

                _categoryRepository.Delete(category);

                if (await _categoryRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while category speaker: {ex}");
            }

            return BadRequest("Could not delete category.");
        }

        [HttpPatch("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<CategoryUpdateViewModel> vm)
        {
            try
            {
                var category = _categoryRepository.Get(id);
                if (category == null)
                {
                    var categoryViewModel = new CategoryUpdateViewModel();
                    vm.ApplyTo(categoryViewModel, ModelState);

                    TryValidateModel(categoryViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var categoryToAdd = Mapper.Map<Category>(vm);
                    categoryToAdd.Id = id;

                    _categoryRepository.Add(categoryToAdd);

                    if (await _categoryRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("CategoryGet", new { id = id });
                        return Created(url, _mapper.Map<CategoryViewModel>(categoryToAdd));
                    }
                }

                var categoryToPatch = _mapper.Map<CategoryUpdateViewModel>(category);

                vm.ApplyTo(categoryToPatch, ModelState);

                TryValidateModel(categoryToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(categoryToPatch, category);

                if (await _categoryRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<CategoryViewModel>(category));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating category: {ex}");
            }
            return BadRequest("Could not update category.");
        }
    }
}
