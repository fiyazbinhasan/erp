﻿using ERP.Data.Helpers.ExcelGenerators;
using ERP.Data.Helpers.RepositoryExtensions;
using ERP.Data.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.IO;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class InventoryReportsController : BaseController
    {
        private readonly IItemRepository _itemRepository;

        public InventoryReportsController(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        [HttpGet("inventoryDetailsReport/{startDate}/{endDate}"), MapToApiVersion("1.0")]
        public object GetInventoryDetailsReport(DateTime startDate, DateTime endDate)
        {
            var inventoryDetails = _itemRepository.InventoryDetails(startDate, endDate);
            return Ok(inventoryDetails);
        }

        [HttpGet("inventoryDetailsReport/{startDate}/{endDate}/{listType}/GenerateExcelInventoryDetails", Name = "GenerateExcelInventoryDetails"), MapToApiVersion("1.0")]
        public ActionResult GenerateExcelInventoryDetails(DateTime startDate, DateTime endDate, string listType)
        {
            var inventoryDetails = _itemRepository.InventoryDetails(startDate, endDate);
            InventoryDetailsReportExcel excel = new InventoryDetailsReportExcel();
            return File(excel.GenerateExcel(startDate, endDate, listType, inventoryDetails), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventoryDetailsReport");
        }

        [HttpGet("inventoryValuationSummary/{startDate}/{endDate}"), MapToApiVersion("1.0")]
        public object GetInventoryValuationSummaryReport(DateTime startDate, DateTime endDate)
        {
            var inventoryValuationSummary = _itemRepository.InventoryValuationSummaryReport(startDate, endDate);
            return Ok(inventoryValuationSummary);
        }

        [HttpGet("inventoryValuationSummary/{startDate}/{endDate}/{listType}/GenerateExcelInventoryValuationSummary", Name = "GenerateExcelInventoryValuationSummary"), MapToApiVersion("1.0")]
        public ActionResult GenerateExcelInventoryValuationSummary(DateTime startDate, DateTime endDate, string listType)
        {
            var inventoryValuationSummary = _itemRepository.InventoryValuationSummaryReport(startDate, endDate);
            InventoryValuationSummaryExcel excel = new InventoryValuationSummaryExcel();
            return File(excel.GenerateExcel(startDate, endDate, listType, inventoryValuationSummary), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventoryDetailsReport");
        }
    }
}
