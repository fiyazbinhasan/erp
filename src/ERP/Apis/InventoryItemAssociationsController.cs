﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class InventoryItemAssociationsController : BaseController
    {
        private readonly IInventoryItemRepository _inventoryItemRepository;
        private readonly IInventoryRepository _inventoryRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IMeasurementUnitSetupRepository _measurementUnitSetupRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<InventoryItemAssociationsController> _logger;

        public InventoryItemAssociationsController(IInventoryItemRepository inventoryItemRepository, 
                    IInventoryRepository inventoryRepository, 
                    IItemRepository itemRepository,
                    IWarehouseRepository warehouseRepository,
                    IMeasurementUnitSetupRepository measurementUnitSetupRepository,
                    IUrlHelper iurlHelper, 
                    IMapper mapper, 
                    ILogger<InventoryItemAssociationsController> logger)
        {
            _inventoryItemRepository = inventoryItemRepository;
            _inventoryRepository = inventoryRepository;
            _itemRepository = itemRepository;
            _measurementUnitSetupRepository = measurementUnitSetupRepository;
            _warehouseRepository = warehouseRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
        }


        /* api/InventoryItemAssociations */
        [HttpGet("api/v{version:apiVersion}/InventoryItemAssociations"), MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var inventoryItemAssociations = _inventoryItemRepository.Get();
            return Ok(_mapper.Map<IEnumerable<InventoryItemViewModel>>(inventoryItemAssociations));
        }

        /* api/InventoryItemAssociations/1 */
        [HttpGet("api/v{version:apiVersion}/InventoryItemAssociations/{id}", Name = "InventoryItemAssociationGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var inventoryItem = _inventoryItemRepository.Get(id);
            if (inventoryItem == null) return NotFound("InventoryItem not found.");
            return Ok(_mapper.Map<InventoryItemViewModel>(inventoryItem));
        }

        [HttpGet("api/v{version:apiVersion}/InventoryItemAssociations/{inventoryId}/items"), MapToApiVersion("1.0")]
        public IActionResult GetItems(Guid inventoryId)
        {
            var inventoryItems = _inventoryItemRepository.GetItems(inventoryId);
            return Ok(_mapper.Map<IEnumerable<InventoryItemViewModel>>(inventoryItems));
        }

        [HttpGet("api/v{version:apiVersion}/InventoryItemAssociations/{itemId}/inventories"), MapToApiVersion("1.0")]
        public IActionResult GetInventories(Guid itemId)
        {
            var inventories = _inventoryItemRepository.GetInventories(itemId);
            return Ok(_mapper.Map<IEnumerable<InventoryItemViewModel>>(inventories));
        }

        /* api/InventoryItemAssociations */
        [HttpPost("api/v{version:apiVersion}/InventoryItemAssociations"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] InventoryItemCreationViewModel vm)
        {
            try
            {
                var inventory = _inventoryRepository.Get(vm.InventoryId);
                if (inventory == null) return NotFound("Inventory not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("Measurement unit setup not found.");

                var warehouse = _warehouseRepository.Get(vm.WarehouseId);
                if (warehouse == null) return NotFound("Warehouse not found.");

                var inventoryItem = _mapper.Map<InventoryItem>(vm);

                _inventoryItemRepository.Add(inventoryItem);

                if (await _inventoryItemRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<InventoryViewModel>(inventory));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding InventoryItem: {ex.Message}");
            }
            return BadRequest("Could not add new InventoryItem.");
        }

        [HttpPut("api/v{version:apiVersion}/InventoryItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] InventoryItemUpdateViewModel vm)
        {
            try
            {
                var inventoryItem = _inventoryItemRepository.Get(id);
                if (inventoryItem == null) return NotFound("InventoryItem not found.");

                var inventory = _inventoryRepository.Get(vm.InventoryId);
                if (inventory == null) return NotFound("Inventory not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("Measurement unit setup not found.");

                var warehouse = _warehouseRepository.Get(vm.WarehouseId);
                if (warehouse == null) return NotFound("Warehouse not found.");

                var inventoryItemCopy = inventoryItem.Clone();
                _mapper.Map(vm, inventoryItem);

                inventoryItem.AdjustedQuantity = inventoryItem.IssuedQuantity - inventoryItemCopy.IssuedQuantity;
                inventoryItem.Warehouse = warehouse;

                _inventoryItemRepository.Update(inventoryItem, inventoryItemCopy);

                if (await _inventoryItemRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<InventoryItemViewModel>(inventoryItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating inventory-item relation: {ex}");
            }
            return BadRequest("Could not update inventory-item relation");
        }

        /* api/InventoryItemAssociations/2 */
        [HttpDelete("api/v{version:apiVersion}/InventoryItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var inventoryItem = _inventoryItemRepository.Get(id);
                if (inventoryItem == null) return NotFound("InventoryItem not found.");

                _inventoryItemRepository.Delete(inventoryItem);

                if (await _inventoryItemRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while InventoryItem speaker: {ex}");
            }

            return BadRequest("Could not delete InventoryItem.");
        }

        [HttpPatch("api/v{version:apiVersion}/InventoryItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update( Guid id, [FromBody] JsonPatchDocument<InventoryItemUpdateViewModel> vm)
        {
            try
            {
                var inventoryItem = _inventoryItemRepository.Get(id);
                if (inventoryItem == null) return NotFound("InventoryItem not found.");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(inventoryItem.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("Measurement unit setup not found.");

                var inventoryItemToPatch = _mapper.Map<InventoryItemUpdateViewModel>(inventoryItem);
                vm.ApplyTo(inventoryItemToPatch);

                TryValidateModel(inventoryItemToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(inventoryItemToPatch, inventoryItem);

                if (await _inventoryItemRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<InventoryItemViewModel>(inventoryItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating inventoryItem: {ex}");
            }

            return BadRequest("Could not update inventoryItem.");
        }
    }
}
