using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.RepositoryExtensions;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ERP.Apis
{ 
    [ApiVersion("1.0")]
    public class DashboardController : BaseController
    {
        private ILogger<DashboardController> _logger;
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IWarehouseItemRepository _warehouseItemRepository;
        private readonly ISupplierRepository _supplierRepository;
        private readonly IInventoryRepository _inventoryRepository;
        private readonly IInventoryItemRepository _inventoryItemRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IMeasurementUnitSetupRepository _measurementUnitSetupRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInvoiceItemRepository _invoiceItemRepository;
        private readonly IBillRepository _billRepository;
        private readonly IMapper _mapper;

        public DashboardController(IInventoryRepository inventoryRepository, IOrderRepository orderRepository, IInvoiceRepository invoiceRepository, IBillRepository billRepository, ILogger<DashboardController> logger, IWarehouseRepository warehouseRepository, ISupplierRepository supplierRepository, ICustomerRepository customerRepository, IItemRepository itemRepository, IOrderItemRepository orderItemRepository, IMapper mapper, IWarehouseItemRepository warehouseItemRepository, IRepository<Category> categoryRepository, IInventoryItemRepository inventoryItemRepository, IInvoiceItemRepository invoiceItemRepository, IMeasurementUnitSetupRepository measurementUnitSetupRepository)
        {
            _billRepository = billRepository;
            _invoiceRepository = invoiceRepository;
            _orderRepository = orderRepository;
            _inventoryRepository = inventoryRepository;
            _warehouseRepository = warehouseRepository;
            _supplierRepository = supplierRepository;
            _customerRepository = customerRepository;
            _itemRepository = itemRepository;
            _orderItemRepository = orderItemRepository;
            _mapper = mapper;
            _warehouseItemRepository = warehouseItemRepository;
            _categoryRepository = categoryRepository;
            _inventoryItemRepository = inventoryItemRepository;
            _invoiceItemRepository = invoiceItemRepository;
            _measurementUnitSetupRepository = measurementUnitSetupRepository;
            _logger = logger;
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/IntroInformation"), MapToApiVersion("1.0")]
        public IActionResult GetIntroInformation()
        {
            return Ok(new
            {
                WarehouseCount = _warehouseRepository.Count(),
                SupplierCount = _supplierRepository.Count(),
                InventoryCount = _inventoryRepository.Count(),
                ItemCount = _itemRepository.Count(),
                OrderCount = _orderRepository.Count(),
                PendingOrders = _orderRepository.PendingOrders(),
                PartialDeliveredOrders = _orderRepository.PartialDeliveredOrders(),
                InvoiceCount = _invoiceRepository.Count(),
                BillCount = _billRepository.Count(),
                UnpaidOrders = _orderRepository.UnpaidOrders(),
                PartialPaidOrders = _orderRepository.PartialPaidOrders(),
                CustomerCount = _customerRepository.Count()
            });
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/ProductDetails"), MapToApiVersion("1.0")]
        public IActionResult ProductDetails()
        {
            return Ok(new
            {
                LowStockItems = _warehouseItemRepository.LowStockItems(),
                CategoryCount = _categoryRepository.Count(),
                ItemCount = _itemRepository.Count()
            });
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/TopSellingItems"), MapToApiVersion("1.0")]
        public IActionResult TopSellingItems()
        {
            return Ok(_orderItemRepository.TopSellingItems());
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/ProfitMargin"), MapToApiVersion("1.0")]
        public IActionResult ProfitMargin()
        {
            var profitMarginByMonth = from purchase in _inventoryItemRepository.PurchasedAmountByMonth()
                join sale in _billRepository.SoldAmountByMonth() on purchase.Month equals sale.Month
                select new
                {
                    purchase.Month,
                    PurchasedAmount = purchase.Amount,
                    SoldAmount = sale.Amount
                };
            
            return Ok(profitMarginByMonth);
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/SoldQuantityByMonth"), MapToApiVersion("1.0")]
        public IActionResult SoldQuantityByMonth()
        {
            var soldQuantityByMonth = _invoiceItemRepository.InvoicedQuantityByMonth().Select(iq=> new {
                iq.Month,
                iq.Quantity
            });

            return Ok(soldQuantityByMonth);
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/SoldAmountByMonth"), MapToApiVersion("1.0")]
        public IActionResult SoldAmountByMonth()
        {
            var soldAmountByMonth = _billRepository.SoldAmountByMonth();
            return Ok(soldAmountByMonth);
        }

        [HttpGet("api/v{version:apiVersion}/dashboard/PurchasedQuantityByMonth"), MapToApiVersion("1.0")]
        public IActionResult PurchasedQuantityByMonth()
        {
            return Ok(_inventoryItemRepository.PurchasedQuantityByMonth());
        }
    }
}