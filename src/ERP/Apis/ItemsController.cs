﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ERP.Data.Helpers.RepositoryExtensions;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class ItemsController : BaseController
    {
        private readonly IItemRepository _itemRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<ItemsController> _logger;
        private readonly ITypeHelperService _typeHelperService;

        public ItemsController(ITypeHelperService typeHelperService,IItemRepository itemRepository, IRepository<Category> categoryRepository, IUrlHelper iurlHelper, IMapper mapper, ILogger<ItemsController> logger)
        {
            _itemRepository = itemRepository;
            _categoryRepository = categoryRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _typeHelperService = typeHelperService;
        }
        
        [HttpGet("api/v{version:apiVersion}/categories/{categoryName}/items", Name = "GetItems"), MapToApiVersion("1.0")]
        public IActionResult Get(string categoryName, ItemResourceParameters itemResourceParameters)
        {
            if (!_typeHelperService.TypeHasProperty<ItemViewModel>(itemResourceParameters.OrderBy))
                return BadRequest();
            if (!_typeHelperService.TypeHasProperties<ItemViewModel>(itemResourceParameters.Fields))
                return BadRequest();

            var items = _itemRepository.Get(itemResourceParameters, categoryName);

            var previousPageLink = items.HasPrevious
                ? CreateItemResourceUri(itemResourceParameters, categoryName, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = items.HasNext
                ? CreateItemResourceUri(itemResourceParameters, categoryName, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = items.TotalCount,
                pageSize = items.PageSize,
                currentPage = items.CurrentPage,
                totalPages = items.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));
            return Ok(_mapper.Map<IEnumerable<ItemViewModel>>(items).ShapeData(itemResourceParameters.Fields));

        }

        private string CreateItemResourceUri(ItemResourceParameters itemResourceParameters, string categoryName, ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetItems",
                        new
                        {
                            pageNumber = itemResourceParameters.PageNumber - 1,
                            pageSize = itemResourceParameters.PageSize,
                            orderBy = itemResourceParameters.OrderBy,
                            fields = itemResourceParameters.Fields,
                            searchQuery = itemResourceParameters.SearchQuery,
                            includes = itemResourceParameters.Includes,
                            name = itemResourceParameters.Name,
                            category= itemResourceParameters.Category,
                            categoryName
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetItems",
                        new
                        {
                            pageNumber = itemResourceParameters.PageNumber + 1,
                            pageSize = itemResourceParameters.PageSize,
                            orderBy = itemResourceParameters.OrderBy,
                            fields = itemResourceParameters.Fields,
                            searchQuery = itemResourceParameters.SearchQuery,
                            includes = itemResourceParameters.Includes,
                            name = itemResourceParameters.Name,
                            category = itemResourceParameters.Category,
                            categoryName
                        });
                default:
                    return _urlHelper.Link("GetItems",
                        new
                        {
                            pageNumber = itemResourceParameters.PageNumber,
                            pageSize = itemResourceParameters.PageSize,
                            orderBy = itemResourceParameters.OrderBy,
                            fields = itemResourceParameters.Fields,
                            searchQuery = itemResourceParameters.SearchQuery,
                            includes = itemResourceParameters.Includes,
                            name = itemResourceParameters.Name,
                            category = itemResourceParameters.Category,
                            categoryName
                        });
            }
        }

        [HttpGet("api/v{version:apiVersion}/categories/{categoryId}/items/{id}", Name = "ItemGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid categoryId, Guid id)
        {
            var item = _itemRepository.Get(id);
            if (item == null) return NotFound();
            if (item.Category.Id != categoryId) return BadRequest("Item not in specific category");

            return Ok(_mapper.Map<ItemViewModel>(item));
        }

        [HttpPost("api/v{version:apiVersion}/categories/{categoryId}/items"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post(Guid categoryId, [FromBody]ItemCreationViewModel vm)
        {
            try
            {
                var category = _categoryRepository.Get(categoryId);
                if (category == null) return BadRequest("Could not find category");

                var item = _mapper.Map<Item>(vm);

                item.Category = category;

                _itemRepository.Add(item);

                if (await _itemRepository.SaveChangesAsync())
                {
                    var url = Url.Link("ItemGet", new {categoryId = category.Id, id = item.Id});
                    return Created(url, _mapper.Map<ItemViewModel>(item));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding item: {ex}");
            }

            return BadRequest("Could not add new item");
        }

        [HttpPut("api/v{version:apiVersion}/categories/{categoryId}/items/{id}", Name = "UpdateItem"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Put(Guid categoryId, Guid id, [FromBody] ItemUpdateViewModel vm)
        {
            try
            {
                var category = _categoryRepository.Get(categoryId);
                if (category == null) return NotFound();

                var item = _itemRepository.Get(id);
                if (item == null)
                {
                    var itemToAdd = Mapper.Map<Item>(vm);
                    itemToAdd.Id = id;
                    itemToAdd.Category = category;

                    _itemRepository.Add(itemToAdd);

                    if (await _itemRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("ItemGet", new { categoryId = category.Id, id = id });
                        return Created(url, _mapper.Map<ItemViewModel>(itemToAdd));
                    }
                }

                _mapper.Map(vm, item);

                if (item != null)
                {
                    item.Category = category;
                    _itemRepository.Update(item);

                    if (await _itemRepository.SaveChangesAsync())
                    {
                        return Ok(_mapper.Map<ItemViewModel>(item));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating item: {ex}");
            }

            return BadRequest("Could not update item");
        }

        [HttpDelete("api/v{version:apiVersion}/categories/{categoryId}/items/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid categoryId, Guid id)
        {
            try
            {
                var item = _itemRepository.Get(id);
                if (item == null) return NotFound();
                if (item.Category.Id != categoryId) return BadRequest("Item and Category do not match");

                _itemRepository.Delete(item);

                if (await _itemRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while deleting item: {ex}");
            }
            return BadRequest("Could not delete item");
        }
        
        [HttpPatch("api/v{version:apiVersion}/categories/{categoryId}/items/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid categoryId, Guid id, [FromBody] JsonPatchDocument<ItemUpdateViewModel> vm)
        {

            try
            {
                var category = _categoryRepository.Get(categoryId);
                if (category == null) return NotFound();

                var item = _itemRepository.Get(id);
                if (item == null)
                {
                    var itemViewModel = new ItemUpdateViewModel();
                    vm.ApplyTo(itemViewModel, ModelState);

                    TryValidateModel(itemViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var itemToAdd = Mapper.Map<Item>(vm);
                    itemToAdd.Id = id;
                    itemToAdd.Category = category;

                    _itemRepository.Add(itemToAdd);

                    if (await _itemRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("ItemGet", new { categoryId = category.Id, id = id });
                        return Created(url, _mapper.Map<ItemViewModel>(itemToAdd));
                    }
                }

                var itemToPatch = _mapper.Map<ItemUpdateViewModel>(item);

                vm.ApplyTo(itemToPatch, ModelState);

                TryValidateModel(itemToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(itemToPatch, item);

                if (await _itemRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<ItemViewModel>(item));

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating item: {ex}");
            }
            return BadRequest("Could not update item.");

        }

        [HttpGet("api/v{version:apiVersion}/categories/{categoryId}/items/{id}/ItemDemandGraph"), MapToApiVersion("1.0")]
        public IActionResult GetItemDemandGraph(Guid id)
        {
            return Ok(_itemRepository.ItemDemandGraph(id));

        }


    }
}
