﻿using AutoMapper;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ValidateModel]
    public class MeasurementUnitsController : BaseController
    {
        private readonly IRepository<MeasurementUnit> _measurementUnitRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<MeasurementUnitsController> _logger;

        public MeasurementUnitsController(IRepository<MeasurementUnit> measurementUnitRepository, IUrlHelper iurlHelper, IMapper mapper, ILogger<MeasurementUnitsController> logger)
        {
            _measurementUnitRepository = measurementUnitRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
        }

        /* api/v1.0/MeasurementUnits */
        [HttpGet, MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var measurementUnits = _measurementUnitRepository.Get();
            return Ok(_mapper.Map<IEnumerable<MeasurementUnitViewModel>>(measurementUnits));
        }

        /* api/Categories/1 */
        [HttpGet("{id:guid}", Name = "MeasurementUnitGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var measurementUnit = _measurementUnitRepository.Get(id);
            if (measurementUnit == null) return NotFound();

            return Ok(_mapper.Map<MeasurementUnitViewModel>(measurementUnit));
        }

        /* api/Categories */
        [HttpPost, MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] MeasurementUnitCreationViewModel vm)
        {
            try
            {
                var measurementUnit = _mapper.Map<MeasurementUnit>(vm);

                _measurementUnitRepository.Add(measurementUnit);

                if (await _measurementUnitRepository.SaveChangesAsync())
                {
                    var url = Url.Link("MeasurementUnitGet", new { id = measurementUnit.Id });
                    return Created(url, _mapper.Map<MeasurementUnitViewModel>(measurementUnit));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding measurement unit: {ex}");
            }
            return BadRequest("Could not add new measurement unit.");
        }

        /* api/Categories/2 */
        [HttpPut("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] MeasurementUnitUpdateViewModel vm)
        {
            try
            {
                var measuremetUnit = _measurementUnitRepository.Get(id);
                if (measuremetUnit == null)
                {
                    var measurementUnitToAdd = Mapper.Map<MeasurementUnit>(vm);
                    measurementUnitToAdd.Id = id;

                    _measurementUnitRepository.Add(measurementUnitToAdd);

                    if (await _measurementUnitRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("MeasurementUnitGet", new { id = id });
                        return Created(url, _mapper.Map<MeasurementUnitViewModel>(measurementUnitToAdd));
                    }
                }

                _mapper.Map(vm, measuremetUnit);

                if (await _measurementUnitRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<MeasurementUnitViewModel>(measuremetUnit));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating measurement unit: {ex}");
            }

            return BadRequest("Could not update measurement unit.");
        }

        /* api/Categories/2 */
        [HttpDelete("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var measurementUnit = _measurementUnitRepository.Get(id);
                if (measurementUnit == null) return NotFound();

                _measurementUnitRepository.Delete(measurementUnit);

                if (await _measurementUnitRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while category speaker: {ex}");
            }

            return BadRequest("Could not delete category.");
        }

        [HttpPatch("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<MeasurementUnitUpdateViewModel> vm)
        {
            try
            {
                var measurementUnit = _measurementUnitRepository.Get(id);
                if (measurementUnit == null)
                {
                    var measurementUnitViewModel = new MeasurementUnitUpdateViewModel();
                    vm.ApplyTo(measurementUnitViewModel, ModelState);

                    TryValidateModel(measurementUnitViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var measurementUnitToAdd = Mapper.Map<MeasurementUnit>(vm);
                    measurementUnitToAdd.Id = id;

                    _measurementUnitRepository.Add(measurementUnitToAdd);

                    if (await _measurementUnitRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("MeasurementUnit", new { id = id });
                        return Created(url, _mapper.Map<MeasurementUnitViewModel>(measurementUnitToAdd));
                    }
                }

                var measurementUnitToPatch = _mapper.Map<MeasurementUnitUpdateViewModel>(measurementUnit);

                vm.ApplyTo(measurementUnitToPatch, ModelState);

                TryValidateModel(measurementUnitToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(measurementUnitToPatch, measurementUnit);

                if (await _measurementUnitRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<MeasurementUnitViewModel>(measurementUnit));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating measurement unit: {ex}");
            }
            return BadRequest("Could not update measurement unit.");
        }
    }
}
