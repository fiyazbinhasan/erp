﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class InvoiceItemAssociationsController :BaseController
    {

        private readonly IInvoiceItemRepository _invoiceItemRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly IMeasurementUnitSetupRepository _measurementUnitSetupRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<InvoiceItemAssociationsController> _logger;


        public InvoiceItemAssociationsController(IInvoiceItemRepository invoiceItemRepository, IInvoiceRepository invoiceRepository, IItemRepository itemRepository, IWarehouseRepository warehouseRepository, IMeasurementUnitSetupRepository measurementUnitSetupRepository, IUrlHelper iurlHelper, IMapper mapper, ILogger<InvoiceItemAssociationsController> logger)
        {
            _invoiceItemRepository = invoiceItemRepository;
            _invoiceRepository = invoiceRepository;
            _itemRepository = itemRepository;
            _warehouseRepository = warehouseRepository;
            _measurementUnitSetupRepository = measurementUnitSetupRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
        }

        /* api/InvoiceItemAssociations */
        [HttpGet("api/v{version:apiVersion}/InvoiceItemAssociations"), MapToApiVersion("1.0")]
        public IActionResult Get()
        {
            var invoiceItemAssociations = _invoiceItemRepository.Get();
            return base.Ok(_mapper.Map<IEnumerable<InvoiceItemViewModel>>(invoiceItemAssociations));
        }

        /* api/InvoiceItemAssociations/1 */
        [HttpGet("api/v{version:apiVersion}/InvoiceItemAssociations/{id}", Name = "InvoiceItemAssociationGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var invoiceItem = _invoiceItemRepository.Get(id);
            if (invoiceItem == null) return NotFound();
            return base.Ok(_mapper.Map<InvoiceItemViewModel>(invoiceItem));
        }

        [HttpGet("api/v{version:apiVersion}/InvoiceItemAssociations/{invoiceId}/items"), MapToApiVersion("1.0")]
        public IActionResult GetItems(Guid invoiceId)
        {
            var items = _invoiceItemRepository.GetItems(invoiceId);
            return Ok(_mapper.Map<IEnumerable<InvoiceItemViewModel>>(items));
        }

        [HttpGet("api/v{version:apiVersion}/InvoiceItemAssociations/{itemId}/invoices"), MapToApiVersion("1.0")]
        public IActionResult GetInvoices(Guid itemId)
        {
            var invoices = _invoiceItemRepository.GetInvoices(itemId);
            return Ok(_mapper.Map<IEnumerable<InvoiceItemViewModel>>(invoices));
        }

        /* api/InvoiceItemAssociations */
        [HttpPost("api/v{version:apiVersion}/InvoiceItemAssociations"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] InvoiceItemCreationViewModel vm)
        {
            try
            {
                var invoice = _invoiceRepository.Get(vm.InvoiceId);
                if (invoice == null) return NotFound("Invoice not found");
               
                var warehouse = _warehouseRepository.Get(vm.WarehouseId);
                if (warehouse == null) return NotFound("Warehouse not found");

                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("MeasurementUnitSetup not found");

                var invoiceItem = _mapper.Map<InvoiceItem>(vm);

                invoiceItem.Invoice = invoice;
                _invoiceItemRepository.Add(invoiceItem);

                if (await _invoiceItemRepository.SaveChangesAsync())
                {
                    var url = Url.Link("InvoiceItemAssociationGet", new { id = invoiceItem.Id });
                    return base.Created(url, _mapper.Map<InvoiceItemViewModel>(invoiceItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding InvoiceItem: {ex.Message}");
            }
            return BadRequest("Could not add new InvoiceItem.");
        }

        [HttpPut("api/v{version:apiVersion}/InvoiceItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] InvoiceItemUpdateViewModel vm)
        {
            try
            {
                var invoiceItem = _invoiceItemRepository.Get(id);
                if (invoiceItem == null) return NotFound("InvoiceItem not found");

                var invoice = _invoiceRepository.Get(vm.InvoiceId);
                if (invoice == null) return NotFound("Invoice not found");

                var warehouse = _warehouseRepository.Get(vm.WarehouseId);
                if (warehouse == null) return NotFound("Warehouse not found");
                
                var measurementUnitSetup = _measurementUnitSetupRepository.Get(vm.MeasurementUnitSetupId);
                if (measurementUnitSetup == null) return NotFound("MeasurementUnitSetup not found");

                var invoiceItemCopy = invoiceItem.Clone();
                _mapper.Map(vm, invoiceItem);

                _invoiceItemRepository.Update(invoiceItem, invoiceItemCopy);

                if (await _invoiceItemRepository.SaveChangesAsync())
                {
                    return base.Ok(_mapper.Map<InvoiceItemViewModel>(invoiceItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating invoice-item relation: {ex}");
            }

            return BadRequest("Could not update invoice-item relation");
        }

        /* api/InvoiceItemAssociations/2 */
        [HttpDelete("api/v{version:apiVersion}/InvoiceItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var invoiceItem = _invoiceItemRepository.Get(id);
                if (invoiceItem == null) return NotFound();

                _invoiceItemRepository.Delete(invoiceItem);

                if (await _invoiceItemRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while invoiceItem speaker: {ex}");
            }

            return BadRequest("Could not delete invoiceItem.");
        }

        [HttpPatch("api/v{version:apiVersion}/InvoiceItemAssociations/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<InvoiceItemUpdateViewModel> vm)
        {
            try
            {

                var invoiceItem = _invoiceItemRepository.Get(id);

                if (invoiceItem == null) return NotFound();

                var invoiceItemToPatch = _mapper.Map<InvoiceItemUpdateViewModel>(invoiceItem);
                vm.ApplyTo(invoiceItemToPatch);

                TryValidateModel(invoiceItemToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(invoiceItemToPatch, invoiceItem);

                if (await _invoiceItemRepository.SaveChangesAsync())
                {
                    return base.Ok(_mapper.Map<InvoiceItemViewModel>(invoiceItem));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating invoiceItem: {ex}");
            }

            return BadRequest("Could not update invoiceItem.");
        }

    }
}
