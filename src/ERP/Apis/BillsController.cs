﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using ERP.Data.Helpers.PdfGenerators;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class BillsController:BaseController
    {
        private readonly IBillRepository _billRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<BillsController> _logger;
        private readonly ITypeHelperService _typeHelperService;

        public BillsController(ITypeHelperService typeHelperService, 
                               IBillRepository billRepository, 
                               ICustomerRepository customerRepository, 
                               IOrderRepository orderRepository,
                               IOrderItemRepository orderItemRepository,
                               IUrlHelper iurlHelper, 
                               IMapper mapper, 
                               ILogger<BillsController> logger)
        {
            _billRepository = billRepository;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _typeHelperService = typeHelperService;
        }

        [HttpGet("api/v{version:apiVersion}/customers/{customerPhone}/orders/{orderMoniker}/bills", Name = "GetBills"), MapToApiVersion("1.0")]
        public IActionResult Get(string customerPhone, string orderMoniker, BillResourceParameters billResourceParameters)
        {
            if (!_typeHelperService.TypeHasProperty<BillViewModel>
                (billResourceParameters.OrderBy))
            {
                return BadRequest();
            }
            if (!_typeHelperService.TypeHasProperties<BillViewModel>
                (billResourceParameters.Fields))
            {
                return BadRequest();
            }
            var bills = _billRepository.Get(billResourceParameters, customerPhone, orderMoniker);

            var previousPageLink = bills.HasPrevious
                ? CreateBillResourceUri(billResourceParameters, customerPhone, orderMoniker, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = bills.HasNext
                ? CreateBillResourceUri(billResourceParameters, customerPhone, orderMoniker, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = bills.TotalCount,
                pageSize = bills.PageSize,
                currentPage = bills.CurrentPage,
                totalPages = bills.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));

            return Ok(_mapper.Map<IEnumerable<BillViewModel>>(bills).ShapeData(billResourceParameters.Fields));
        }

        private string CreateBillResourceUri(BillResourceParameters billResourceParameters, string customerPhone, string orderMoniker, ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetBills",
                        new
                        {
                            pageNumber = billResourceParameters.PageNumber - 1,
                            pageSize = billResourceParameters.PageSize,
                            orderBy = billResourceParameters.OrderBy,
                            fields = billResourceParameters.Fields,
                            searchQuery = billResourceParameters.SearchQuery,
                            includes = billResourceParameters.Includes,
                            moniker = billResourceParameters.Moniker,
                            paidAmountRanges = billResourceParameters.PaidAmountRanges,
                            paymentMethod = billResourceParameters.PaymentMethod,
                            orderNo = billResourceParameters.OrderNo,
                            customerPhone,
                            orderMoniker
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetBills",
                        new
                        {
                            pageNumber = billResourceParameters.PageNumber + 1,
                            pageSize = billResourceParameters.PageSize,
                            orderBy = billResourceParameters.OrderBy,
                            fields = billResourceParameters.Fields,
                            searchQuery = billResourceParameters.SearchQuery,
                            includes = billResourceParameters.Includes,
                            moniker = billResourceParameters.Moniker,
                            paidAmountRanges = billResourceParameters.PaidAmountRanges,
                            paymentMethod = billResourceParameters.PaymentMethod,
                            orderNo = billResourceParameters.OrderNo,
                            customerPhone,
                            orderMoniker
                        });
                default:
                    return _urlHelper.Link("GetBills",
                        new
                        {
                            pageNumber = billResourceParameters.PageNumber,
                            pageSize = billResourceParameters.PageSize,
                            orderBy = billResourceParameters.OrderBy,
                            fields = billResourceParameters.Fields,
                            searchQuery = billResourceParameters.SearchQuery,
                            includes = billResourceParameters.Includes,
                            moniker = billResourceParameters.Moniker,
                            paidAmountRanges = billResourceParameters.PaidAmountRanges,
                            paymentMethod = billResourceParameters.PaymentMethod,
                            orderNo = billResourceParameters.OrderNo,
                            customerPhone,
                            orderMoniker
                        });
            }
        }

        [HttpGet("api/v{version:apiVersion}/orders/{orderId}/bills/", Name = "GetAnOrderBills"), MapToApiVersion("1.0")]
        public IActionResult GetAnOrderBills(Guid orderId)
        {
            var order = _orderRepository.Get(orderId);
            if (order == null) return NotFound("Order not found.");
            var bills = _billRepository.GetAnOrderBills(orderId);
            return Ok(_mapper.Map<IEnumerable<BillViewModel>>(bills));
        }

        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/bills/{id}", Name = "BillGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid customerId, Guid orderId, Guid id)
        {
            var bill = _billRepository.Get(id);
            if (bill == null) return NotFound("Bill not found.");
            if (bill.Order.Id != orderId && bill.Order.Customer.Id != customerId) return BadRequest("Order not found.");
            return Ok(_mapper.Map<BillViewModel>(bill));
        }

        [HttpPost("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/bills"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post(Guid customerId, Guid orderId, [FromBody] BillCreationViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");

                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");

                if (order.Customer != customer) return NotFound("Customer not in specific order.");

                var bill = _mapper.Map<Bill>(vm);
                bill.Order = order;
                _billRepository.Add(bill);

                if (await _billRepository.SaveChangesAsync())
                {
                    var url = Url.Link("BillGet", new {orderId = order.Id, customerId = customer.Id, id = bill.Id});
                    return Created(url, _mapper.Map<BillViewModel>(bill));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding Bill: {ex}");
            }
            return BadRequest("Could not add new Bill.");
        }

        [HttpPut("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/bills/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid customerId, Guid orderId, Guid id, [FromBody] BillUpdateViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");

                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");

                if (order.Customer != customer) return NotFound("Customer not in specific order.");
               
                var bill = _billRepository.Get(id);
                if (bill == null)
                {
                    var billToAdd = Mapper.Map<Bill>(vm);
                    billToAdd.Id = id;
                    billToAdd.Order = order;
                    billToAdd.Order.Customer = customer;

                    _billRepository.Add(billToAdd);

                    if (await _billRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("BillGet", new { orderId = order.Id, customerId = customer.Id, id = id });
                        return Created(url, _mapper.Map<BillViewModel>(billToAdd));
                    }
                }

                if (order.Bills.Contains(bill) == false) return NotFound("Bill not found in order.");

                _mapper.Map(vm, bill);
                _billRepository.Update(bill);

                if (await _billRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<BillViewModel>(bill));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating bill: {ex}");
            }

            return BadRequest("Could not update bill.");
        }

        [HttpDelete("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/bills/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid customerId, Guid orderId, Guid id)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");
                
                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");

                if (order.Customer != customer) return NotFound("Customer not in specific order.");
                
                var bill = _billRepository.Get(id);
                if (bill == null) return NotFound("Bill not found.");

                if (order.Bills.Contains(bill) == false) return NotFound("Bill not found in order.");

                _billRepository.Delete(bill);

                if (await _billRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while bill speaker: {ex}");
            }
            return BadRequest("Could not delete bill");
        }

        [HttpPatch("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/bills/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid customerId, Guid orderId, Guid id, [FromBody] JsonPatchDocument<BillUpdateViewModel> vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");

                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");

                if (order.Customer != customer) return NotFound("Customer not in specific order.");

                var bill = _billRepository.Get(id);
                if (bill == null)
                {
                    var billViewModel = new BillUpdateViewModel();
                    vm.ApplyTo(billViewModel, ModelState);

                    TryValidateModel(billViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var billToAdd = Mapper.Map<Bill>(vm);
                    billToAdd.Id = id;
                    billToAdd.Order = order;
                    billToAdd.Order.Customer = customer;

                    _billRepository.Add(billToAdd);

                    if (await _billRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("BillGet", new { customerId = customer.Id, orderId = order.Id, id = id });
                        return Created(url, _mapper.Map<BillViewModel>(billToAdd));
                    }
                }

                if (order.Bills.Contains(bill) == false) return NotFound("Bill not found in order.");

                var billToPatch = _mapper.Map<BillUpdateViewModel>(bill);
                vm.ApplyTo(billToPatch, ModelState);

                TryValidateModel(billToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(vm, bill);

                if (await _billRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<BillViewModel>(bill));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating bill: {ex}");
            }

            return BadRequest("Could not update bill.");
        }

        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/bills/{id}/generateBillPdf", Name = "GenerateBillPdf"), MapToApiVersion("1.0")]
        public ActionResult GenerateBillPdf(Guid supplierId, Guid orderId, Guid id)
        {
            var bill = _billRepository.Get(id);
            BillPdfGenerator pdf = new BillPdfGenerator();
            byte[] abytes = pdf.GeneratePdf(bill);
            return File(abytes, "application/pdf");
        }
    }
}
