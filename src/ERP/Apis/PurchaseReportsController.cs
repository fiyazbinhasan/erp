﻿using ERP.Data.Helpers.ExcelGenerators;
using ERP.Data.Helpers.RepositoryExtensions;
using ERP.Data.Interface;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class PurchaseReportsController : BaseController
    {
        private readonly ISupplierRepository _supplierRepository;
        private readonly IItemRepository _itemRepository;
        public PurchaseReportsController(ISupplierRepository supplierRepository,
                                         IItemRepository itemRepository)
        {
            _supplierRepository = supplierRepository;
            _itemRepository = itemRepository; ;
        }

        [HttpGet("purchaseBySupplier/{startDate}/{endDate}"), MapToApiVersion("1.0")]
        public object GetPurchaseBySupplierReport(DateTime startDate, DateTime endDate)
        {
            var purchaseBySuppliers = _supplierRepository.PurchaseBySupplierReport(startDate, endDate);
            return Ok(purchaseBySuppliers);
        }

        [HttpGet("purchaseBySupplier/{startDate}/{endDate}/{listType}/GenerateExcelPurchaseBySupplier", Name = "GenerateExcelPurchaseBySupplier"), MapToApiVersion("1.0")]
        public ActionResult GenerateExcelPurchaseBySupplier(DateTime startDate, DateTime endDate, string listType)
        {
            var purchaseBySuppliers = _supplierRepository.PurchaseBySupplierReport(startDate, endDate);
            PurchaseBySupplierExcel excel = new PurchaseBySupplierExcel();
            return File(excel.GenerateExcel(startDate, endDate, listType, purchaseBySuppliers), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventoryDetailsReport");
        }

        [HttpGet("purchaseByItem/{startDate}/{endDate}"), MapToApiVersion("1.0")]
        public object GetPurchaseByItemReport(DateTime startDate, DateTime endDate)
        {
            var purchaseByItems = _itemRepository.PurchaseByItemReport(startDate, endDate);
            return Ok(purchaseByItems);
        }

        [HttpGet("purchaseByItem/{startDate}/{endDate}/{listType}/GenerateExcelPurchaseByItem", Name = "GenerateExcelPurchaseByItem"), MapToApiVersion("1.0")]
        public ActionResult GenerateExcelPurchaseByItem(DateTime startDate, DateTime endDate, string listType)
        {
            var purchaseByItems = _itemRepository.PurchaseByItemReport(startDate, endDate);
            PurchaseByItemExcel excel = new PurchaseByItemExcel();
            return File(excel.GenerateExcel(startDate, endDate, listType, purchaseByItems), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InventoryDetailsReport");
        }
    }
}
