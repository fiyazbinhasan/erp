﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Utility.Attributes;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using ERP.Data.Helpers.PdfGenerators;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class InvoicesController : BaseController
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInvoiceItemRepository _invoiceItemRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<InvoicesController> _logger;

        public InvoicesController(IInvoiceRepository invoiceRepository, IInvoiceItemRepository invoiceItemRepository, ICustomerRepository customerRepository,
            IOrderRepository orderRepository, IUrlHelper iurlHelper, IMapper mapper, ILogger<InvoicesController> logger)
        {
            _invoiceRepository = invoiceRepository;
            _invoiceItemRepository = invoiceItemRepository;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
        }

       [HttpGet("api/v{version:apiVersion}/customers/{customerPhone}/orders/{orderMoniker}/invoices", Name = "GetInvoices"), MapToApiVersion("1.0")]
       public IActionResult Get(string customerPhone, string orderMoniker, InvoiceResourceParameters invoiceResourceParameters)
        {
            var invoices = _invoiceRepository.Get(invoiceResourceParameters, customerPhone, orderMoniker);

            var previousPageLink = invoices.HasPrevious
                ? CreateInvoiceResourceUri(invoiceResourceParameters, customerPhone, orderMoniker, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = invoices.HasNext
                ? CreateInvoiceResourceUri(invoiceResourceParameters, customerPhone, orderMoniker, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = invoices.TotalCount,
                pageSize = invoices.PageSize,
                currentPage = invoices.CurrentPage,
                totalPages = invoices.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));

            return Ok(_mapper.Map<IEnumerable<InvoiceViewModel>>(invoices));
        }

        private string CreateInvoiceResourceUri(InvoiceResourceParameters invoiceResourceParameters, string customerPhone, string orderMoniker, ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("Getinvoices",
                        new
                        {
                            pageNumber = invoiceResourceParameters.PageNumber - 1,
                            pageSize = invoiceResourceParameters.PageSize,
                            orderBy = invoiceResourceParameters.OrderBy,
                            fields = invoiceResourceParameters.Fields,
                            searchQuery = invoiceResourceParameters.SearchQuery,
                            includes = invoiceResourceParameters.Includes,
                            moniker = invoiceResourceParameters.Moniker,
                            customerPhone,
                            orderMoniker
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("Getinvoices",
                        new
                        {
                            pageNumber = invoiceResourceParameters.PageNumber + 1,
                            pageSize = invoiceResourceParameters.PageSize,
                            orderBy = invoiceResourceParameters.OrderBy,
                            fields = invoiceResourceParameters.Fields,
                            searchQuery = invoiceResourceParameters.SearchQuery,
                            includes = invoiceResourceParameters.Includes,
                            moniker = invoiceResourceParameters.Moniker,
                            customerPhone,
                            orderMoniker
                        });
                default:
                    return _urlHelper.Link("Getinvoices",
                        new
                        {
                            pageNumber = invoiceResourceParameters.PageNumber,
                            pageSize = invoiceResourceParameters.PageSize,
                            orderBy = invoiceResourceParameters.OrderBy,
                            fields = invoiceResourceParameters.Fields,
                            searchQuery = invoiceResourceParameters.SearchQuery,
                            includes = invoiceResourceParameters.Includes,
                            moniker = invoiceResourceParameters.Moniker,
                            customerPhone,
                            orderMoniker
                        });
            }
        }


        /* api/customers/1/orders/4/invoices/5 */
        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices/{id}", Name = "InvoiceGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid customerId, Guid orderId, Guid id)
        {
            var invoice = _invoiceRepository.Get(id);
            if (invoice == null) return NotFound("Invoice not found.");

            if (invoice.Order.Id != orderId && invoice.Order.Customer.Id != customerId) return BadRequest("Order not found.");

            return Ok(_mapper.Map<InvoiceViewModel>(invoice));
        }

        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices/all", Name = "OrderInvoices"), MapToApiVersion("1.0")]
        public IActionResult GetOrderInvoices(Guid customerId, Guid orderId)
        {
            var invoices = _invoiceRepository.GetOrderInvoices(orderId);
            return Ok(_mapper.Map<IEnumerable<InvoiceViewModel>>(invoices));
        }

        /* api/customers/1/orders/4/invoices */
        [HttpPost("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post(Guid customerId, Guid orderId, [FromBody] InvoiceCreationViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");

                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");

                if (order.Customer != customer) return NotFound("Customer not in specific order.");

                var invoice = _mapper.Map<Invoice>(vm);

                _invoiceRepository.Add(invoice);

                if (await _invoiceRepository.SaveChangesAsync())
                {
                    var url = Url.Link("invoiceGet",
                        new { orderId = order.Id, customerId = order.Customer.Id, id = invoice.Id });
                    return Created(url, _mapper.Map<InvoiceViewModel>(invoice));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding invoice: {ex}");
            }
            return BadRequest("Could not add new invoice.");
        }

        /* api/customers/1/orders/3/invoices/2 */
        [HttpPut("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices/{id}", Name = "UpdateInvoice"), MapToApiVersion("1.0")]
         public async Task<IActionResult> Update(Guid customerId, Guid orderId, Guid id, [FromBody] InvoiceUpdateViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");

                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");
               
                if (order.Customer != customer) return NotFound("Customer not in specific order.");

                var invoice = _invoiceRepository.Get(id);
                if (invoice == null) return NotFound("Invalied Invoice");

                _mapper.Map(vm, invoice);

                invoice.Order = order;
                invoice.Order.Customer = customer;

                _invoiceRepository.Update(invoice);

                if (await _invoiceRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<InvoiceViewModel>(invoice));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating invoice: {ex}");
            }

            return BadRequest("Could not update invoice.");
        }

        /* api/invoices/2 */
        [HttpDelete("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid id)
        {
            try
            {
                var invoice = _invoiceRepository.Get(id);
                if (invoice == null) return NotFound("Invoice not found.");
              
                _invoiceRepository.Delete(invoice);

                if (await _invoiceRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while invoice speaker: {ex}");
            }
            return BadRequest("Could not delete invoice.");
        }

        [HttpPatch("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid customerId, Guid orderId, Guid id, [FromBody] JsonPatchDocument<InvoiceUpdateViewModel> vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound("Customer not found.");

                var order = _orderRepository.Get(orderId);
                if (order == null) return NotFound("Order not found.");

                if (order.Customer != customer) return NotFound("Customer not in specific order.");

                var invoice = _invoiceRepository.Get(id);
                if (invoice == null)
                {
                    var invoiceViewModel = new InvoiceUpdateViewModel();
                    vm.ApplyTo(invoiceViewModel, ModelState);

                    TryValidateModel(invoiceViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var invoiceToAdd = Mapper.Map<Invoice>(vm);
                    invoiceToAdd.Id = id;
                    invoiceToAdd.Order = order;
                    invoiceToAdd.Order.Customer = customer;

                    _invoiceRepository.Add(invoiceToAdd);

                    if (await _invoiceRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("InvoiceGet",
                            new { orderId = order.Id, customerId = order.Customer.Id, id = id });
                        return Created(url, _mapper.Map<InvoiceViewModel>(invoiceToAdd));
                    }
                }

                var invoiceToPatch = _mapper.Map<InvoiceUpdateViewModel>(invoice);
                vm.ApplyTo(invoiceToPatch);

                TryValidateModel(invoiceToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(invoiceToPatch, invoice);

                if (await _invoiceRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<InvoiceViewModel>(invoice));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating invoice: {ex}");
            }
            return BadRequest("Could not update invoice.");
        }

        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{orderId}/invoices/{id}/generateInvoicePdf", Name = "GenerateInvoicePdf"), MapToApiVersion("1.0")]
        public ActionResult GenerateInvoicePdf(Guid customerId, Guid orderId, Guid id)
        {
            var invoice = _invoiceRepository.Get(id);
            invoice.InvoiceItems = _invoiceItemRepository.GetItems(id).ToList();
            InvoicePdfGenerator pdf = new InvoicePdfGenerator();
            byte[] abytes = pdf.GeneratePdf(invoice);
            return File(abytes, "application/pdf");
        }
    }
}
