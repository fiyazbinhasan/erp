﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using ERP.Data.Helpers.PdfGenerators;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class InventoriesController : BaseController
    {
        private readonly IInventoryRepository _inventoryRepository;
        private readonly ISupplierRepository _supplierRepository;
        private readonly IInventoryItemRepository _inventoryItemRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<InventoriesController> _logger;
        private readonly ITypeHelperService _typeHelperService;

        public InventoriesController(IInventoryRepository inventoryRepository,
                                     ISupplierRepository supplierRepository, 
                                     IInventoryItemRepository inventoryItemRepository, 
                                     IItemRepository itemRepository, 
                                     IUrlHelper iurlHelper, 
                                     IMapper mapper, 
                                     ITypeHelperService typeHelperService, 
                                     ILogger<InventoriesController> logger)
        {
            _inventoryRepository = inventoryRepository;
            _supplierRepository = supplierRepository;
            _inventoryItemRepository = inventoryItemRepository;
            _itemRepository = itemRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _typeHelperService = typeHelperService;
        }
        
        [HttpGet("api/v{version:apiVersion}/suppliers/{supplierPhone}/inventories", Name = "GetInventories"), MapToApiVersion("1.0")]
        public IActionResult Get(string supplierPhone, InventoryResourceParameters inventoryResourceParameters)
        {

            if (!_typeHelperService.TypeHasProperty<InventoryViewModel>
                (inventoryResourceParameters.OrderBy))
            {
                return BadRequest();
            }
            if (!_typeHelperService.TypeHasProperties<InventoryViewModel>
                (inventoryResourceParameters.Fields))
            {
                return BadRequest();
            }
            var inventories = _inventoryRepository.Get(inventoryResourceParameters, supplierPhone);

            var previousPageLink = inventories.HasPrevious
                ? CreateInventoryResourceUri(inventoryResourceParameters, supplierPhone, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = inventories.HasNext
                ? CreateInventoryResourceUri(inventoryResourceParameters, supplierPhone, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = inventories.TotalCount,
                pageSize = inventories.PageSize,
                currentPage = inventories.CurrentPage,
                totalPages = inventories.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));

            return Ok(_mapper.Map<IEnumerable<InventoryViewModel>>(inventories).ShapeData(inventoryResourceParameters.Fields));

        }

        private string CreateInventoryResourceUri(InventoryResourceParameters inventoryResourceParameters, string supplierPhone, ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetInventories",
                        new
                        {
                            pageNumber = inventoryResourceParameters.PageNumber - 1,
                            pageSize = inventoryResourceParameters.PageSize,
                            orderBy = inventoryResourceParameters.OrderBy,
                            fields = inventoryResourceParameters.Fields,
                            searchQuery = inventoryResourceParameters.SearchQuery,
                            includes = inventoryResourceParameters.Includes,
                            moniker = inventoryResourceParameters.Moniker,
                            supplierPhone = supplierPhone
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetInventories",
                        new
                        {
                            pageNumber = inventoryResourceParameters.PageNumber + 1,
                            pageSize = inventoryResourceParameters.PageSize,
                            orderBy = inventoryResourceParameters.OrderBy,
                            fields = inventoryResourceParameters.Fields,
                            searchQuery = inventoryResourceParameters.SearchQuery,
                            includes = inventoryResourceParameters.Includes,
                            moniker = inventoryResourceParameters.Moniker,
                            supplierPhone = supplierPhone
                        });
                default:
                    return _urlHelper.Link("GetInventories",
                        new
                        {
                            pageNumber = inventoryResourceParameters.PageNumber,
                            pageSize = inventoryResourceParameters.PageSize,
                            orderBy = inventoryResourceParameters.OrderBy,
                            fields = inventoryResourceParameters.Fields,
                            searchQuery = inventoryResourceParameters.SearchQuery,
                            includes = inventoryResourceParameters.Includes,
                            moniker = inventoryResourceParameters.Moniker,
                            supplierPhone = supplierPhone
                        });
            }
        }

        [HttpGet("api/v{version:apiVersion}/suppliers/{supplierId}/inventories/{id}", Name = "InventoryGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid supplierId, Guid id)
        {
            var inventory = _inventoryRepository.Get(id);
            if (inventory == null) return NotFound();
            if (inventory.Supplier.Id != supplierId) return BadRequest("Inventory not in specific supplier");
            
            return Ok(_mapper.Map<InventoryViewModel>(inventory));
        }

        [HttpPost("api/v{version:apiVersion}/suppliers/{supplierId}/inventories"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post(Guid supplierId, [FromBody] InventoryCreationViewModel vm)
        {
            try
            {
                var supplier = _supplierRepository.Get(supplierId);
                if (supplier == null) return BadRequest("Could not find supplier");

                var inventory = _mapper.Map<Inventory>(vm);

                inventory.Supplier = supplier;
                
                _inventoryRepository.Add(inventory);

                if (await _inventoryRepository.SaveChangesAsync())
                {
                    var url = Url.Link("InventoryGet", new {id = inventory.Id});
                    return Created(url, _mapper.Map<InventoryViewModel>(inventory));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding inventory: {ex}");
            }

            return BadRequest("Could not add new inventory.");
        }
        
        [HttpPut("api/v{version:apiVersion}/suppliers/{supplierId}/inventories/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid supplierId, Guid id, [FromBody] InventoryUpdateViewModel vm)
        {
            try
            {
                var supplier = _supplierRepository.Get(supplierId);
                if (supplier == null) return NotFound();

                var inventory = _inventoryRepository.Get(id);
                if (inventory == null)
                {
                    var inventoryToAdd = Mapper.Map<Inventory>(vm);
                    inventoryToAdd.Id = id;
                    inventoryToAdd.Supplier = supplier;

                    _inventoryRepository.Add(inventoryToAdd);

                    if (await _inventoryRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("InventoryGet", new { supplierId = supplier.Id, id = id });
                        return Created(url, _mapper.Map<InventoryViewModel>(inventoryToAdd));
                    }
                }
                
                _mapper.Map(vm, inventory);
                inventory.Supplier = supplier;

                _inventoryRepository.Update(inventory);

                if (await _inventoryRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<InventoryViewModel>(inventory));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating inventory: {ex}");
            }

            return BadRequest("Could not update inventory.");
        }

        [HttpDelete("api/v{version:apiVersion}/suppliers/{supplierId}/inventories/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid supplierId, Guid id)
        {
            try
            {
                var inventory = _inventoryRepository.Get(id);
                if (inventory == null) return NotFound("No inventory found.");
                if (inventory.Supplier.Id != supplierId) return BadRequest("Inventory and Supplier do not match");

                _inventoryRepository.Delete(inventory);

                if (await _inventoryRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while inventory speaker: {ex}");
            }

            return BadRequest("Could not delete inventory");
        }

        [HttpPatch("api/v{version:apiVersion}/suppliers/{supplierId}/inventories/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid supplierId, Guid id, [FromBody] JsonPatchDocument<InventoryUpdateViewModel> vm)
        {
            try
            {
                var supplier = _supplierRepository.Get(supplierId);
                if (supplier == null) return NotFound();

                var inventory = _inventoryRepository.Get(id);
                if (inventory == null)
                {
                    var inventoryViewModel = new InventoryUpdateViewModel();
                    vm.ApplyTo(inventoryViewModel, ModelState);

                    TryValidateModel(inventoryViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var inventoryToAdd = Mapper.Map<Inventory>(vm);
                    inventoryToAdd.Id = id;
                    inventoryToAdd.Supplier = supplier;

                    _inventoryRepository.Add(inventoryToAdd);

                    if (await _inventoryRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("InventoryGet", new { supplierId = supplier.Id, id = id });
                        return Created(url, _mapper.Map<InventoryViewModel>(inventoryToAdd));
                    }
                }

                var inventoryToPatch = _mapper.Map<InventoryUpdateViewModel>(inventory);

                vm.ApplyTo(inventoryToPatch, ModelState);

                TryValidateModel(inventoryToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(inventoryToPatch, inventory);

                if (await _inventoryRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<InventoryViewModel>(inventory));

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating inventory: {ex}");
            }
            return BadRequest("Could not update inventory.");

        }

        [HttpGet("api/v{version:apiVersion}/suppliers/{supplierId}/inventories/{id}/generateInventoryPdf", Name = "GenerateInventoryPdf"), MapToApiVersion("1.0")]
        public ActionResult GenerateInventoryPdf(Guid supplierId, Guid id)
        {
            var inventory = _inventoryRepository.Get(id);
            inventory.InventoryItems = _inventoryItemRepository.GetItems(id);
            InventoryPdfGenerator pdf = new InventoryPdfGenerator();
            byte[] abytes = pdf.GeneratePdf(inventory);
            return File(abytes, "application/pdf");
        }
    }
}
