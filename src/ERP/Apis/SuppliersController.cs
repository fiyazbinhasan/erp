﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ERP.ViewModels.Inventory;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using Microsoft.AspNetCore.JsonPatch;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using Newtonsoft.Json;
using ERP.Data.Helpers.RepositoryExtensions;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ValidateModel]
    public class SuppliersController : BaseController
    {
        private readonly ISupplierRepository _supplierRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<SuppliersController> _logger;
        private readonly ITypeHelperService _typeHelperService;
        
        public SuppliersController(ISupplierRepository supplierRepository, IUrlHelper iurlHelper, IMapper mapper, ITypeHelperService typeHelperService, ILogger<SuppliersController> logger)
        {
            _supplierRepository = supplierRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
           _typeHelperService = typeHelperService;
        }

        /* api/Suppliers?pageNumber=1&pageSize=10 */
        [HttpGet(Name = "GetSuppliers"), MapToApiVersion("1.0")]
        public IActionResult Get(SupplierResourceParameters supplierResourceParameters)
        {
            if (!_typeHelperService.TypeHasProperty<SupplierViewModel>
                (supplierResourceParameters.OrderBy))
            {
                return BadRequest();
            }

            if (!_typeHelperService.TypeHasProperties<SupplierViewModel>
                (supplierResourceParameters.Fields))
            {
                return BadRequest();
            }

            var suppliers = _supplierRepository.Get(supplierResourceParameters);

            var previousPageLink = suppliers.HasPrevious
                ? CreateSupplierResourceUri(supplierResourceParameters, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = suppliers.HasNext
                ? CreateSupplierResourceUri(supplierResourceParameters, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = suppliers.TotalCount,
                pageSize = suppliers.PageSize,
                currentPage = suppliers.CurrentPage,
                totalPages = suppliers.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));

            return Ok(_mapper.Map<IEnumerable<SupplierViewModel>>(suppliers).ShapeData(supplierResourceParameters.Fields));
        }

        private string CreateSupplierResourceUri(SupplierResourceParameters supplierResourceParameters, ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetSuppliers",
                        new
                        {
                            pageNumber = supplierResourceParameters.PageNumber - 1,
                            pageSize = supplierResourceParameters.PageSize,
                            orderBy = supplierResourceParameters.OrderBy,
                            fields = supplierResourceParameters.Fields,
                            searchQuery = supplierResourceParameters.SearchQuery,
                            includes = supplierResourceParameters.Includes,
                            firstName = supplierResourceParameters.FirstName,
                            lastName = supplierResourceParameters.LastName,
                            phone = supplierResourceParameters.Phone,
                            email = supplierResourceParameters.Email,
                            country = supplierResourceParameters.Country,
                            city = supplierResourceParameters.City
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetSuppliers",
                        new
                        {
                            pageNumber = supplierResourceParameters.PageNumber + 1,
                            pageSize = supplierResourceParameters.PageSize,
                            orderBy = supplierResourceParameters.OrderBy,
                            fields = supplierResourceParameters.Fields,
                            searchQuery = supplierResourceParameters.SearchQuery,
                            includes = supplierResourceParameters.Includes,
                            firstName = supplierResourceParameters.FirstName,
                            lastName = supplierResourceParameters.LastName,
                            phone = supplierResourceParameters.Phone,
                            email = supplierResourceParameters.Email,
                            country = supplierResourceParameters.Country,
                            city = supplierResourceParameters.City
                        });
                default:
                    return _urlHelper.Link("GetSuppliers",
                        new
                        {
                            pageNumber = supplierResourceParameters.PageNumber,
                            pageSize = supplierResourceParameters.PageSize,
                            orderBy = supplierResourceParameters.OrderBy,
                            fields = supplierResourceParameters.Fields,
                            searchQuery = supplierResourceParameters.SearchQuery,
                            includes = supplierResourceParameters.Includes,
                            firstName = supplierResourceParameters.FirstName,
                            lastName = supplierResourceParameters.LastName,
                            phone = supplierResourceParameters.Phone,
                            email = supplierResourceParameters.Email,
                            country = supplierResourceParameters.Country,
                            city = supplierResourceParameters.City
                        });
            }
        }

        /* api/Suppliers/1 */
        [HttpGet("{id:guid}", Name = "SupplierGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid id)
        {
            var supplier = _supplierRepository.Get(id);
            if (supplier == null) return NotFound();
            return Ok(_mapper.Map<SupplierViewModel>(supplier));
        }

        /* api/Suppliers */
        [HttpPost, MapToApiVersion("1.0")]
        public async Task<IActionResult> Post([FromBody] SupplierCreationViewModel vm)
        {
            try
            {
                var supplier = _mapper.Map<Supplier>(vm);

                _supplierRepository.Add(supplier);

                if (await _supplierRepository.SaveChangesAsync())
                {
                    var url = Url.Link("SupplierGet", new { id = supplier.Id });
                    return Created(url, _mapper.Map<SupplierViewModel>(supplier));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding supplier: {ex}");
            }
            return BadRequest("Could not add new supplier.");
        }

        /* api/Suppliers/2 */
        [HttpPut("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] SupplierUpdateViewModel vm)
        {
            try
            {
                var supplier = _supplierRepository.Get(id);
                if (supplier == null)
                {
                    var supplierToAdd = Mapper.Map<Supplier>(vm);
                    supplierToAdd.Id = id;

                    _supplierRepository.Add(supplierToAdd);

                    if (await _supplierRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("SupplierGet", new { id = id });
                        return Created(url, _mapper.Map<SupplierViewModel>(supplierToAdd));
                    }
                }
                _mapper.Map(vm, supplier);

                if (await _supplierRepository.SaveChangesAsync())
                {
                    return Ok(_mapper.Map<SupplierViewModel>(supplier));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating supplier: {ex}");
            }

            return BadRequest("Could not update supplier.");
        }

        /* api/Suppliers/2 */
        [HttpDelete("{id:Guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid id)
        {
            try
            {
                var supplier = _supplierRepository.Get(id);
                if (supplier == null) return NotFound();

                _supplierRepository.Delete(supplier);

                if (await _supplierRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while supplier: {ex}");
            }
            return BadRequest("Could not delete supplier");
        }

        [HttpPatch("{id:guid}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid id, [FromBody] JsonPatchDocument<SupplierUpdateViewModel> vm)
        {

            try
            {
                var supplier = _supplierRepository.Get(id);
                if (supplier == null)
                {
                    var supplierViewModel = new SupplierUpdateViewModel();
                    vm.ApplyTo(supplierViewModel, ModelState);

                    TryValidateModel(supplierViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }

                    var supplierToAdd = Mapper.Map<Supplier>(vm);
                    supplierToAdd.Id = id;

                    _supplierRepository.Add(supplierToAdd);

                    if (await _supplierRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("SupplierGet", new { id = id });
                        return Created(url, _mapper.Map<SupplierViewModel>(supplierToAdd));
                    }
                }

                var supplierToPatch = _mapper.Map<SupplierUpdateViewModel>(supplier);

                vm.ApplyTo(supplierToPatch, ModelState);

                TryValidateModel(supplierToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(supplierToPatch, supplier);

                if (await _supplierRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<SupplierViewModel>(supplier));

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating supplier: {ex}");
            }
            return BadRequest("Could not update supplier.");
        }

        [HttpGet("{id}/GetSupplierView", Name = "GetSupplierView"), MapToApiVersion("1.0")]
        public IActionResult GetSupplierView(Guid id)
        {
            var supplier = _supplierRepository.Get(id);
            if (supplier == null) return NotFound("Supplier not found.");
            var reportData = _supplierRepository.SupplierViewDatas(id);
            return Ok(reportData);
        }
    }
}
