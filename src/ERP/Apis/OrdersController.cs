﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using ERP.Services.Interfaces;
using ERP.Utility.Attributes;
using ERP.Utility.Extensions;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq;
using ERP.Data.Helpers.PdfGenerators;

namespace ERP.Apis
{
    [ApiVersion("1.0")]
    [ValidateModel]
    public class OrdersController : BaseController
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IBillRepository _billRepository;
        private readonly IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<OrdersController> _logger;
        private readonly ITypeHelperService _typeHelperService;

        public OrdersController(ITypeHelperService typeHelperService,
                        IOrderRepository orderRepository,
                        IOrderItemRepository orderItemRepository,
                        ICustomerRepository customerRepository,
                        IBillRepository billRepository,
                        IUrlHelper iurlHelper, 
                        IMapper mapper, 
                        ILogger<OrdersController> logger)
        {
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _customerRepository = customerRepository;
            _billRepository = billRepository;
            _urlHelper = iurlHelper;
            _mapper = mapper;
            _logger = logger;
            _typeHelperService = typeHelperService;
        }

        /* api/Customers/2/Orders */
        [HttpGet("api/v{version:apiVersion}/customers/{customerPhone}/orders", Name = "GetOrders"), MapToApiVersion("1.0")]
        public IActionResult Get(string customerPhone, OrderResourceParameters orderResourceParameters)
        {
            if (!_typeHelperService.TypeHasProperty<OrderViewModel>
                (orderResourceParameters.OrderBy))
            {
                return BadRequest();
            }
            if (!_typeHelperService.TypeHasProperties<OrderViewModel>
                (orderResourceParameters.Fields))
            {
                return BadRequest();
            }
            var orders = _orderRepository.Get(orderResourceParameters, customerPhone);

            var previousPageLink = orders.HasPrevious
                ? CreateOrderResourceUri(orderResourceParameters, customerPhone, ApplicationEnums.ResourceUriType.Previous)
                : null;
            var nextPageLink = orders.HasNext
                ? CreateOrderResourceUri(orderResourceParameters, customerPhone, ApplicationEnums.ResourceUriType.Next)
                : null;

            var paginationMataData = new
            {
                totalCount = orders.TotalCount,
                pageSize = orders.PageSize,
                currentPage = orders.CurrentPage,
                totalPages = orders.TotalPages,
                previousPageLink,
                nextPageLink
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMataData));

            return Ok(_mapper.Map<IEnumerable<OrderViewModel>>(orders).ShapeData(orderResourceParameters.Fields));
        }

        private string CreateOrderResourceUri(OrderResourceParameters orderResourceParameters, string customerPhone,
            ApplicationEnums.ResourceUriType enumResourceUriType)
        {
            switch (enumResourceUriType)
            {
                case ApplicationEnums.ResourceUriType.Previous:
                    return _urlHelper.Link("GetOrders",
                        new
                        {
                            pageNumber = orderResourceParameters.PageNumber - 1,
                            pageSize = orderResourceParameters.PageSize,
                            orderBy = orderResourceParameters.OrderBy,
                            fields = orderResourceParameters.Fields,
                            searchQuery = orderResourceParameters.SearchQuery,
                            includes = orderResourceParameters.Includes,
                            moniker = orderResourceParameters.Moniker,
                            paymentStatus = orderResourceParameters.PaymentStatus,
                            orderStatus = orderResourceParameters.OrderStatus,
                            customerPhone
                        });
                case ApplicationEnums.ResourceUriType.Next:
                    return _urlHelper.Link("GetOrders",
                        new
                        {
                            pageNumber = orderResourceParameters.PageNumber + 1,
                            pageSize = orderResourceParameters.PageSize,
                            orderBy = orderResourceParameters.OrderBy,
                            fields = orderResourceParameters.Fields,
                            searchQuery = orderResourceParameters.SearchQuery,
                            includes = orderResourceParameters.Includes,
                            moniker = orderResourceParameters.Moniker,
                            paymentStatus = orderResourceParameters.PaymentStatus,
                            orderStatus = orderResourceParameters.OrderStatus,
                            customerPhone
                        });
                default:
                    return _urlHelper.Link("GetOrders",
                        new
                        {
                            pageNumber = orderResourceParameters.PageNumber,
                            pageSize = orderResourceParameters.PageSize,
                            orderBy = orderResourceParameters.OrderBy,
                            fields = orderResourceParameters.Fields,
                            searchQuery = orderResourceParameters.SearchQuery,
                            includes = orderResourceParameters.Includes,
                            moniker = orderResourceParameters.Moniker,
                            paymentStatus = orderResourceParameters.PaymentStatus,
                            orderStatus = orderResourceParameters.OrderStatus,
                            customerPhone
                        });
            }
        }

        /* api/Customers/2/Orders/1 */
        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{id}", Name = "OrderGet"), MapToApiVersion("1.0")]
        public IActionResult Get(Guid customerId, Guid id)
        {
            var order = _orderRepository.Get(id);
            if (order == null) return NotFound("Order not found.");
            if (order.Customer.Id != customerId) return BadRequest("Order not in specific customer.");
            return Ok(_mapper.Map<OrderViewModel>(order));
        }

        [HttpGet("api/v{version:apiVersion}/orders/{moniker}", Name = "SearchByMoniker"), MapToApiVersion("1.0")]
        public IActionResult SearchByMoniker(string moniker)
        {
            var order = _orderRepository.SearchByMoniker(moniker);
            if (order == null) return NoContent();
            return Ok(_mapper.Map<OrderViewModel>(order));
        }

        /* api/Customers/2/Orders */
        [HttpPost("api/v{version:apiVersion}/customers/{customerId}/orders"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Post(Guid customerId, [FromBody] OrderCreationViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return BadRequest("Could not find customer");

                var order = _mapper.Map<Order>(vm);

                order.Customer = customer;

                _orderRepository.Add(order);

                if (await _orderRepository.SaveChangesAsync())
                {
                    var url = Url.Link("OrderGet", new {id = order.Id});
                    return Created(url, _mapper.Map<OrderViewModel>(order));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while adding order: {ex}");
            }
            return BadRequest("Could not add new order.");
        }

        [HttpPut("api/v{version:apiVersion}/customers/{customerId}/orders/{id}", Name = "UpdateOrder"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid customerId, Guid id, [FromBody] OrderUpdateViewModel vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound();

                var order = _orderRepository.Get(id);
                if (order == null)
                {
                    var orderToAdd = Mapper.Map<Order>(vm);
                    orderToAdd.Id = id;
                    orderToAdd.Customer = customer;

                    _orderRepository.Add(orderToAdd);

                    if (await _orderRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("OrderGet", new {customerId = customer.Id, id = id});
                        return Created(url, _mapper.Map<OrderViewModel>(orderToAdd));
                    }
                }

                _mapper.Map(vm, order);

                if (order != null)
                {
                    order.Customer = customer;
                    
                    if (_orderRepository.ValidateOrderStatus(order) && _orderRepository.ValidatePaymentStatus(order))
                        return BadRequest("Order could not be updated because it is " + order.OrderStatus + " and " + order.PaymentStatus);

                    _orderRepository.Update(order);

                    if (await _orderRepository.SaveChangesAsync())
                    {
                        return Ok(_mapper.Map<OrderViewModel>(order));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating order: {ex}");
            }
            return BadRequest("Could not update order.");
        }
        
        [HttpDelete("api/v{version:apiVersion}/customers/{customerId}/orders/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveAsync(Guid customerId, Guid id)
        {
            try
            {
                var order = _orderRepository.Get(id);
                if (order == null) return NotFound();
                if (order.Customer.Id != customerId) return BadRequest("Order and Customer do not match");

                if (_orderRepository.ValidateOrderStatus(order) && _orderRepository.ValidatePaymentStatus(order))
                    return BadRequest("Order could not be deleted because it is " + order.OrderStatus + " and " + order.PaymentStatus);

                _orderRepository.Delete(order);

                if (await _orderRepository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while order speaker: {ex}");
            }
            return BadRequest("Could not delete order");
        }

        [HttpPatch("api/v{version:apiVersion}/customers/{customerId}/orders/{id}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> Update(Guid customerId,Guid id,[FromBody] JsonPatchDocument<OrderUpdateViewModel> vm)
        {
            try
            {
                var customer = _customerRepository.Get(customerId);
                if (customer == null) return NotFound();

                var order = _orderRepository.Get(id);
                if (order == null)
                {
                    var orderViewModel = new OrderUpdateViewModel();
                    vm.ApplyTo(orderViewModel,ModelState);

                    TryValidateModel(orderViewModel);

                    if (!ModelState.IsValid)
                    {
                        return new UnprocessableEntityObjectResult(ModelState);
                    }
                    var orderToAdd = Mapper.Map<Order>(orderViewModel);
                    orderToAdd.Id = id;
                    orderToAdd.Customer = customer;

                    _orderRepository.Add(orderToAdd);

                    if (await _orderRepository.SaveChangesAsync())
                    {
                        var url = Url.Link("OrderGet", new {customerId = customer.Id, id = id});
                        return Created(url, _mapper.Map<OrderViewModel>(orderToAdd));
                    }
                }

                if (_orderRepository.ValidateOrderStatus(order) && _orderRepository.ValidatePaymentStatus(order))
                    return BadRequest("Order could not be Updated because it is " + order.OrderStatus + " and " + order.PaymentStatus);

                var orderToPatch = _mapper.Map<OrderUpdateViewModel>(order);

                vm.ApplyTo(orderToPatch, ModelState);

                TryValidateModel(orderToPatch);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                _mapper.Map(orderToPatch, order);
                
                if (await _orderRepository.SaveChangesAsync())
                    return Ok(_mapper.Map<OrderViewModel>(order));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception thrown while updating order: {ex}");
            }
            return BadRequest("Could not update order.");
        }

        [HttpGet("api/v{version:apiVersion}/customers/{customerId}/orders/{id}/generateOrderPdf", Name = "GenerateOrderPdf"), MapToApiVersion("1.0")]
        public ActionResult GenerateOrderPdf(Guid customerId, Guid id)
        {
            var order = _orderRepository.Get(id);
            order.OrderItems = _orderItemRepository.GetItems(id).ToList();
            OrderPdfGenerator pdf = new OrderPdfGenerator();
            byte[] abytes = pdf.GeneratePdf(order);
            return File(abytes, "application/pdf");
        }
    }
}
