﻿using System;
using AutoMapper;
using ERP.Apis;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class InventoryUrlResolver : IValueResolver<Model.Inventory.Inventory, InventoryViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InventoryUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(Model.Inventory.Inventory source, InventoryViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];

            return url.Link("InventoryGet", new { supplierId = source.Supplier.Id, id = source.Id });

        }
    }
}
