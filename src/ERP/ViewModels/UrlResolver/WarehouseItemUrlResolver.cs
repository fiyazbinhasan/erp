﻿using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class WarehouseItemUrlResolver : IValueResolver<WarehouseItem, WarehouseItemViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WarehouseItemUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(WarehouseItem source, WarehouseItemViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("WarehouseItemAssociationGet", new { id = source.Id });
        }
    }
}
