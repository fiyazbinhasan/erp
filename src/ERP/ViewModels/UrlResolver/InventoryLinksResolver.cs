﻿using System.Collections.Generic;
using AutoMapper;
using ERP.Apis;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class InventoryLinksResolver : IValueResolver<Model.Inventory.Inventory, InventoryViewModel, IEnumerable<LinkViewModel>>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InventoryLinksResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<LinkViewModel> Resolve(Model.Inventory.Inventory source, InventoryViewModel destination, IEnumerable<LinkViewModel> destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];

            return new List<LinkViewModel>()
            {
                new LinkViewModel()
                {
                    Rel = "Self",
                    Href = url.Link("InventoryGet", new {supplierId = source.Supplier.Id, id = source.Id})
                },
                new LinkViewModel()
                {
                    Rel = "Update",
                    Href = url.Link("UpdateInventory", new {supplierId = source.Supplier.Id, id = source.Id}),
                    Verb = "PUT"
                },
                new LinkViewModel()
                {
                    Rel = "Supplier",
                    Href = url.Link("SupplierGet", new {id = source.Supplier.Id})
                }
            };
        }
    }
}
