﻿using AutoMapper;
using ERP.Apis;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class InventoryItemUrlResolver : IValueResolver<Model.Inventory.InventoryItem, InventoryItemViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InventoryItemUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(Model.Inventory.InventoryItem source, InventoryItemViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("InventoryItemAssociationGet", new { id = source.Id });
        }
    }
}
