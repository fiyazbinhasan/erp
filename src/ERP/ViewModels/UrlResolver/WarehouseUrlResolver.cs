﻿using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class WarehouseUrlResolver : IValueResolver<Warehouse, WarehouseViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WarehouseUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(Warehouse source, WarehouseViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("WarehouseGet", new { id = source.Id });
        }
    }
}
