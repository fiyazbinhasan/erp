﻿using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class MeasurementUnitUrlResolver : IValueResolver<MeasurementUnit, MeasurementUnitViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MeasurementUnitUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(MeasurementUnit source, MeasurementUnitViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("MeasurementUnitGet", new { id = source.Id });

        }
    }
}
