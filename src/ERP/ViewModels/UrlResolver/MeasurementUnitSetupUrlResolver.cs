﻿using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class MeasurementUnitSetupUrlResolver : IValueResolver<MeasurementUnitSetup, MeasurementUnitSetupViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MeasurementUnitSetupUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(MeasurementUnitSetup source, MeasurementUnitSetupViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("MeasurementUnitSetupGet", new { id = source.Id });
        }
    }
}
