﻿using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class InvoiceItemUrlResolver : IValueResolver<InvoiceItem, InvoiceItemViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InvoiceItemUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(InvoiceItem source, InvoiceItemViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("invoiceItemAssociationGet", new { id = source.Id });
        }
    }
}
