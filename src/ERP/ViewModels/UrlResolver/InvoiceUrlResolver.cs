﻿using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class InvoiceUrlResolver : IValueResolver<Invoice, InvoiceViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InvoiceUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(Invoice source, InvoiceViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("InvoiceGet", new { id = source.Id });

        }
    }
}
