﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class CategoryUrlResolver : IValueResolver<Category, CategoryViewModel, string>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CategoryUrlResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Resolve(Category source, CategoryViewModel destination, string destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];
            return url.Link("CategoryGet", new { id = source.Id });

        }
    }
}
