﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Apis;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ERP.ViewModels.UrlResolver
{
    public class ItemLinksResolver : IValueResolver<Item, ItemViewModel, IEnumerable<LinkViewModel>>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ItemLinksResolver(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<LinkViewModel> Resolve(Item source, ItemViewModel destination, IEnumerable<LinkViewModel> destMember, ResolutionContext context)
        {
            var url = (IUrlHelper)_httpContextAccessor.HttpContext.Items[BaseController.Urlhelper];

            return new List<LinkViewModel>()
            {
                new LinkViewModel()
                {
                    Rel = "Self",
                    Href = url.Link("ItemGet", new { categoryId = source.Category.Id, id = source.Id })
                },
                new LinkViewModel()
                {
                    Rel = "Update",
                    Href = url.Link("UpdateItem", new { categoryId = source.Category.Id, id = source.Id }),
                    Verb = "PUT"
                },
                new LinkViewModel()
                {
                    Rel = "Category",
                    Href = url.Link("CategoryGet", new { id = source.Category.Id })
                }
            };
        }
    }
}
