﻿using AutoMapper;
using ERP.Model.Inventory;
using ERP.ViewModels.Inventory;
using static ERP.Model.Common.ApplicationEnums;

namespace ERP.ViewModels.GlobalResolver
{
    public class PaymentStatusEnumResolver : IValueResolver<Order, OrderViewModel, string>
    {
        public string Resolve(Order source, OrderViewModel destination, string destMember, ResolutionContext context)
        {
            switch (source.PaymentStatus)
            {
                case PaymentStatus.Unpaid:
                    return "Unpaid";
                case PaymentStatus.PartialPaid:
                    return "Partial Paid";
                case PaymentStatus.Paid:
                    return "Paid";
                default:
                    return "Unpaid";
            }
        }
    }

    public class OrderStatusEnumResolver : IValueResolver<Order, OrderViewModel, string>
    {
        public string Resolve(Order source, OrderViewModel destination, string destMember, ResolutionContext context)
        {
            switch (source.OrderStatus)
            {
                case OrderStatus.Pending:
                    return "Pending";
                case OrderStatus.PartialDelivered:
                    return "Partial Delivered";
                case OrderStatus.DeliveryCompleted:
                    return "Delivery Completed";
                default:
                    return "Pending";
            }
        }
    }
}
