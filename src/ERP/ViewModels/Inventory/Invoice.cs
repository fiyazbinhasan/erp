﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ERP.Model.Common;

namespace ERP.ViewModels.Inventory
{
    public class InvoiceViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Moniker { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Description { get; set; }
        public OrderViewModel Order { get; set; }
        public IEnumerable<InvoiceItemLinearViewModel> InvoiceItems { get; set; }
    }

    public class InvoiceCreationViewModel
    {
        public string Moniker { get; set; }
        [Required]
        public DateTime InvoiceDate { get; set; } = DateTime.Now;
        public string Description { get; set; }
        public Guid OrderId { get; set; }

    }

    public class InvoiceUpdateViewModel
    {
        public string Moniker { get; set; }
        [Required]
        public DateTime InvoiceDate { get; set; }
        public string Description { get; set; }
      //  public OrderViewModel Order { get; set; }
        public Guid OrderId { get; set; }
    }
}
