﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class InventoryItemViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public int IssuedQuantity { get; set; }
        public decimal CostPerUnit { get; set; }
        public string Remarks { get; set; }

        public InventoryViewModel Inventory { get; set; }
        public Guid InventoryId { get; set; }

        public MeasurementUnitSetupViewModel MeasurementUnitSetup { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }

        public WarehouseViewModel Warehouse { get; set; }
        public Guid WarehouseId { get; set; }
    }

    public class InventoryItemCreationViewModel
    {
        [Required]
        public int IssuedQuantity { get; set; }
        [Required]
        public decimal CostPerUnit { get; set; }
        public string Remarks { get; set; }
        public Guid InventoryId { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
        public Guid WarehouseId { get; set; }
    }

    public class InventoryItemUpdateViewModel
    {
        [Required]
        public int IssuedQuantity { get; set; }
        [Required]
        public decimal CostPerUnit { get; set; }
        public string Remarks { get; set; }
        public Guid InventoryId { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
        public Guid WarehouseId { get; set; }
    }
}
