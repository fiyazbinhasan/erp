﻿using ERP.Model.Inventory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.ViewModels.Inventory
{
    public class MeasurementUnitSetupViewModel
    {
        public string Url { get; set; }
        public Guid Id { get; set; }
        public ItemViewModel Item { get; set; }
        public Guid ItemId { get; set; }
        public MeasurementUnitViewModel MeasurementUnit { get; set; }
        public Guid MeasurementUnitId { get; set; }
        public bool IsBase { get; set; }
        public decimal ConversionRatio { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public bool AllowDiscount { get; set; }
        public bool IsUsed { get; set; }
    }

    public class MeasurementUnitSetupCreationViewModel
    {
        public Guid ItemId { get; set; }
        public Guid MeasurementUnitId { get; set; }
        public bool IsBase { get; set; }

        [Required]
        public decimal ConversionRatio { get; set; }

        public decimal SellingPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public bool AllowDiscount { get; set; }
    }

    public class MeasurementUnitSetupUpdateViewModel
    {
        public Guid ItemId { get; set; }
        public Guid MeasurementUnitId { get; set; }
        public bool IsBase { get; set; }

        [Required]
        public decimal ConversionRatio { get; set; }

        public decimal SellingPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public bool AllowDiscount { get; set; }
    }



}
