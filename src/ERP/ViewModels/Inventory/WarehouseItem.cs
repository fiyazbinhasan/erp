﻿using System;

namespace ERP.ViewModels.Inventory
{
    public class WarehouseItemViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public Guid WarehouseId { get; set; }
        public decimal Quantity { get; set; }
        public WarehouseViewModel Warehouse { get; set; }
        public MeasurementUnitSetupViewModel MeasurementUnitSetup { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }
    public class WarehouseItemCreationViewModel
    {
        public Guid WarehouseId { get; set; }
        public decimal Quantity { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }
    public class WarehouseItemUpdateViewModel
    {
       
        public Guid WarehouseId { get; set; }
        public decimal Quantity { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }
}
