﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class CategoryViewModel
    {
        public string Url { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
    public class CategoryCreationViewModel
    {
        [Required(ErrorMessage = "Provide a valid and unique name for the category")]
        public string Name { get; set; }
        [MaxLength(500, ErrorMessage = "Description shouldn't have more than 500 characters")]
        public string Description { get; set; }
    }
    public class CategoryUpdateViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
