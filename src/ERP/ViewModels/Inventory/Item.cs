﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class ItemViewModel
    {
        public string Url { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public CategoryViewModel Category { get; set; }
        public IEnumerable<LinkViewModel> Links { get; set; }
    }

    public class ItemCreationViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
    public class ItemUpdateViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
       
    }
}
