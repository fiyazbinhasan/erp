﻿using System;
using System.ComponentModel.DataAnnotations;
using ERP.Model.Inventory;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ERP.ViewModels.Inventory
{
    public class OrderItemViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public int Quantity { get; set; }
        public int LeftQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public OrderViewModel Order { get; set; }
        public MeasurementUnitSetupViewModel MeasurementUnitSetup { get; set; }
        public Guid OrderId { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }

    public class OrderItemCreationViewModel
    {
        public int Quantity { get; set; }
        public int LeftQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
        public Guid OrderId { get; set; }
    }

    public class OrderItemUpdateViewModel
    {
        public int Quantity { get; set; }
        public int LeftQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public Guid OrderId { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }

    public class OrderItemLinearViewModel
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }
        public int LeftQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public MeasurementUnitSetupViewModel MeasurementUnitSetup { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }
}
