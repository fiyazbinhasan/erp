﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.ViewModels.Inventory
{
    public class MeasurementUnitViewModel
    {
        public string Url { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }
    }
    public class MeasurementUnitCreationViewModel
    {
        [Required(ErrorMessage = "Provide a valid and unique name for the measurement unit")]
        public string Name { get; set; }
        [MaxLength(500, ErrorMessage = "Remarks shouldn't have more than 500 characters")]
        public string Remarks { get; set; }
    }
    public class MeasurementUnitUpdateViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Remarks { get; set; }
    }
}
