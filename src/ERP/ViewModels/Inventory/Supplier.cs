﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class SupplierViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AlternativePhone { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
    }

    public class SupplierCreationViewModel
    {
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AlternativePhone { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
    }

    public class SupplierUpdateViewModel
    {
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AlternativePhone { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
    }
}
