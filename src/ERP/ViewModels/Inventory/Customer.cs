﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class CustomerViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AlternativePhone { get; set; }
        public string Email { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Website { get; set; }
        public string PhoneWithName => this.Phone + " (" + this.FirstName + " " + this.LastName + ")";
    }
    public class CustomerCreationViewModel
    {
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AlternativePhone { get; set; }
        public string Email { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Website { get; set; }
    }

    public class CustomerUpdateViewModel
    {
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string AlternativePhone { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
    }
}
