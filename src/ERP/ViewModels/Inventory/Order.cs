﻿using ERP.Utility.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ERP.Model.Inventory;

namespace ERP.ViewModels.Inventory
{
    public class OrderViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Moniker { get; set; }
        public DateTime OrderDate { get; set; }

        public DateTime ExpectedDeliveryDate { get; set; }
        public string PaymentStatus { get; set; }
        public string OrderStatus { get; set; }
        public CustomerViewModel Customer { get; set; }

        public IEnumerable<OrderItemLinearViewModel> OrderItems { get; set; }
  
    }
    public class OrderCreationViewModel
    {
        public string Moniker { get; set; }
        public DateTime OrderDate { get; set; }
        [GreaterThan("OrderDate", ErrorMessage = "The order date must be greater than the delivery date.")]
        public DateTime ExpectedDeliveryDate { get; set; }
    }
    public class OrderUpdateViewModel
    {
        public string Moniker { get; set; }
        public DateTime OrderDate { get; set; }
        [GreaterThan("OrderDate", ErrorMessage = "The order date must be greater than the delivery date.")]
        public DateTime ExpectedDeliveryDate { get; set; }
    }
}
