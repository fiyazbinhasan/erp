﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ERP.Model.Inventory;
using ERP.Model.Common;

namespace ERP.ViewModels.Inventory
{
    public class InventoryViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Moniker { get; set; }
        public int PriorityLevel { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Description { get; set; }
        public SupplierViewModel Supplier { get; set; }
        public IEnumerable<LinkViewModel> Links { get; set; }
        
    }

    public class InventoryCreationViewModel
    {
        public string Moniker { get; set; }
        public int PriorityLevel { get; set; }
        [Required]
        public DateTime PurchaseDate { get; set; }
        public string Description { get; set; }
    }

    public class InventoryUpdateViewModel
    {
        public string Moniker { get; set; }
        public int PriorityLevel { get; set; }
        [Required]
        public DateTime PurchaseDate { get; set; }
        public string Description { get; set; }
    }
}
