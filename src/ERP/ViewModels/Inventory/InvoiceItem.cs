﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ERP.Model.Inventory;

namespace ERP.ViewModels.Inventory
{
   
    public class InvoiceItemViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public InvoiceViewModel Invoice { get; set; }
        public Guid InvoiceId { get; set; }
        public WarehouseViewModel Warehouse { get; set; }
        public Guid WarehouseId { get; set; }
        public MeasurementUnitSetupViewModel MeasurementUnitSetup { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }

    public class InvoiceItemCreationViewModel
    {
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
        public Guid InvoiceId { get; set; }
    }

    public class InvoiceItemUpdateViewModel
    {
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid InvoiceId { get; set; }
    }

    public class InvoiceItemLinearViewModel
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }
        public Guid InvoiceId { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }
    }
}
