﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class WarehouseViewModel
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Moniker { get; set; }
        public string Address { get; set; }
    }
    public class WarehouseCreationViewModel
    {
        [Required]
        public string Moniker { get; set; }
        [Required]
        public string Address { get; set; }
    }
    public class WarehouseUpdateViewModel
    {
        [Required]
        public string Moniker { get; set; }
        [Required]
        public string Address { get; set; }
    }
}
