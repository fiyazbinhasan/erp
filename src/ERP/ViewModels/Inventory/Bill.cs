﻿using System;
using static ERP.Model.Common.ApplicationEnums;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.Inventory
{
    public class BillViewModel
    {
        public string Url { get; set; }
        public Guid Id { get; set; }
        public string Moniker { get; set; }
        public decimal PaidAmount { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime BillDate { get; set; } = DateTime.Now;
        public OrderViewModel Order { get; set; }
    }
    public class BillCreationViewModel
    {
        public string Moniker { get; set; }
        public decimal PaidAmount { get; set; }
        public PaymentMethod PaymentMethod { get; set; } = PaymentMethod.Cash;
        public DateTime BillDate { get; set; } = DateTime.Now;
    }
    public class BillUpdateViewModel
    {
        public string Moniker { get; set; }
        public decimal PaidAmount { get; set; }
        public PaymentMethod PaymentMethod { get; set; } = PaymentMethod.Cash;
        public DateTime BillDate { get; set; } = DateTime.Now;
    }
}
