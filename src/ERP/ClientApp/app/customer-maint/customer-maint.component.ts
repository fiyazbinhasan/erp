﻿import { Component, OnInit, Output, AfterViewInit, OnDestroy, ViewChildren, ElementRef, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { GenericValidator } from '../shared/generic-validator';

import { ICustomer } from '../view-models/customer';
import { CustomerDataService } from '../services/customer-data.service';
import { SelectItem } from '../shared/select-item'

@Component({
    selector: 'customer-maint',
    templateUrl: 'customer-maint.component.html',
    styleUrls: ['./customer-maint.component.css']
})

export class CustomerMaintComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    errorMessage: string;
    public customerForm: FormGroup;

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    headline: string = 'Edit customer';
    paragraph: string = 'Use this form to edit the existing customer';

    customer: ICustomer;
    private sub: Subscription;

    countries: Array<SelectItem> = [];

    constructor(private fb: FormBuilder,
        private customerDataService: CustomerDataService,
        private route: ActivatedRoute,
        private router: Router) {
        this.validationMessages = {
            firstName: {
                required: 'First name is required.',
                minlength: 'First name must be at least three characters.',
                maxlength: 'First name cannot exceed 50 characters.'
            },
            lastName: {
                required: 'Last name is required.',
                minlength: 'Last name must be at least three characters.',
                maxlength: 'Last name cannot exceed 50 characters.'
            },
            phone: {
                required: 'Phone number is required.'
            },
            address1: {
                required: 'Address is required.'
            },
            email: {
                pattern: 'Provide a valid email address'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.customerForm = this.fb.group({
            firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            lastName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            companyName : '',
            phone: ['', [Validators.required]],
            alternativePhone: '',
            address1: ['', [Validators.required]],
            address2: '',
            city: '',
            country: '',
            postalCode: '',
            email: ['', [Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]],
            website: ''
        });

        this.countries.push.apply(this.countries, this.customerDataService.countries);

        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getCustomer(id);
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.customerForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.customerForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveCustomer(): void {
        if (this.customerForm.dirty && this.customerForm.valid) {
            let s = Object.assign({}, this.customer, this.customerForm.value);

            this.customerDataService.saveCustomer(s)
                .subscribe(() => {
                    this.onSaveComplete();
                },
                (error: any) => this.errorMessage = <any>error);

        } else if (!this.customerForm.dirty) {
            this.onSaveComplete();
        }
    }

    getCustomer(id: string): void {
        if (id === undefined) {
            this.headline = 'Add customer';
            this.paragraph = `Use this form to add a new customer`;
        } else {
            this.customerDataService.getCustomer(id)
                .subscribe(
                (customer: ICustomer) => this.onCustomerRetrieved(customer),
                (error: any) => this.errorMessage = <any>error
                );
        }
    }

    onCustomerRetrieved(customer: ICustomer): void {
        if (this.customerForm) {
            this.customerForm.reset();
        }

        this.customer = customer;

        this.customerForm.setValue({
            firstName: this.customer.firstName,
            lastName: this.customer.lastName,
            companyName: this.customer.companyName,
            phone: this.customer.phone,
            alternativePhone: this.customer.alternativePhone,
            email: this.customer.email,
            address1: this.customer.address1,
            address2: this.customer.address2,
            city: this.customer.city,
            country: this.customer.country,
            postalCode: this.customer.postalCode,
            website: this.customer.website
        });
    }

    onSaveComplete(): void {
        this.customerForm.reset();
        this.router.navigate(["/customer-list"]);
    }
}