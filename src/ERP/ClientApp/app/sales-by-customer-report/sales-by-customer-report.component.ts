﻿import { Component, OnInit } from '@angular/core';
import { SalesReportDataService } from '../services/sales-report.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'sales-by-customer-report',
    templateUrl: 'sales-by-customer-report.component.html',
    styleUrls: ['./sales-by-customer-report.component.css']
})
export class SalesByCustomerReportComponent implements OnInit {
    loading: boolean;
    stacked: boolean;
    cols: any[];
    errorMessage: string;
    salesByCustomers = [];
    salesByCustomersCopy = [];
    dateRange: Date[];
    allowZero: any = false;
    totalInvoice: number = 0;
    totalInvoicedAmount: number = 0;

    constructor(private salesReportDataService: SalesReportDataService) {}
    ngOnInit(): void {
        var date = new Date();
        this.dateRange = [new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)]

        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'totalInvoice', header: 'Total Invoice' },
            { field: 'totalInvoicedAmount', header: 'Total Amount' },
        ];
        this.generateReport();
    }

    generateReport() {
        this.totalInvoice = 0;
        this.totalInvoicedAmount = 0;
        let date = new Date();
        let startDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.dateRange[0]);
        let endDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.dateRange[1]);

        this.salesReportDataService.getSalesByCustomerReport(startDate, endDate).subscribe(salesByCustomers => {
            this.salesByCustomers = salesByCustomers
            _.each(salesByCustomers, (salesByCustomer) => {
                this.totalInvoice += salesByCustomer.totalInvoice;
                this.totalInvoicedAmount += salesByCustomer.totalInvoicedAmount;
            });
            this.salesByCustomersCopy = this.salesByCustomers;
            this.toggleList();
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + monthIndex + '-' + day;
    }

    toggleList(): void {
        this.salesByCustomers = this.salesByCustomersCopy;
        if (this.allowZero) {
            this.salesByCustomers = [];
            this.totalInvoice = 0;
            this.totalInvoicedAmount = 0;
            _.each(this.salesByCustomersCopy, (salesByCustomer) => {
                if (salesByCustomer.totalInvoice == 0 &&
                    salesByCustomer.totalInvoicedAmount == 0) {
                    this.salesByCustomers.push(salesByCustomer);
                    this.totalInvoice += salesByCustomer.totalInvoice;
                    this.totalInvoicedAmount += salesByCustomer.totalInvoicedAmount;
                }
            });
        }
        else if (!this.allowZero && this.allowZero != null) {
            this.salesByCustomers = [];
            this.totalInvoice = 0;
            this.totalInvoicedAmount = 0;
            _.each(this.salesByCustomersCopy, (salesByCustomer) => {
                if (salesByCustomer.totalInvoice > 0 ||
                    salesByCustomer.totalInvoicedAmount > 0) {
                    this.salesByCustomers.push(salesByCustomer);
                    this.totalInvoice += salesByCustomer.totalInvoice;
                    this.totalInvoicedAmount += salesByCustomer.totalInvoicedAmount;
                }
            });
        }
    }

    generateExcel() {
        var date = new Date();
        var startDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.dateRange[0]);
        var endDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.dateRange[1]);
        this.salesReportDataService.generateExcelSalesByCustomer(startDate, endDate, this.allowZero)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/xlsx' });
                FileSaver.saveAs(blob, "Sales by customer.xlsx");
            }, err => console.error(err));
    }
}