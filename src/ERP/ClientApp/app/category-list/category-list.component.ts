﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { ICategory } from '../view-models/category'
import { CategoryDataService } from '../services/category-data.service'
import { SelectItem} from '../shared/select-item'
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'category-list',
    templateUrl: 'category-list.component.html',
    styleUrls: ['./category-list.component.css'],
    providers: [ConfirmationService]
})

export class CategoryListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    categories: Array<ICategory>;
    loading: boolean;
    stacked: boolean;

    constructor(private categoryDataService: CategoryDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Name', filter: true },
            { field: 'description', header: 'Description', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols.slice(0, 5);

        this.loadCategoriesLazy();
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }
    
    private loadCategoriesLazy() {
        this.loading = true;

        this.categoryDataService.getCategories().subscribe(categories => {
            this.categories = categories;
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editCategory(category: ICategory) {
        this.router.navigate(['/category-maint', category.id]);
    }

    viewCategory(category: ICategory) {
        this.router.navigate(['/category-view', category.id]);
    }

    navigateToCategoryAddPage() {
        this.router.navigate(['/category-maint']);
    }

    deleteCategory(category: ICategory): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.categoryDataService.deleteCategory(category.id)
                    .subscribe(
                    () => this.refreshDataTable(),
                        (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadCategoriesLazy();
    }
}