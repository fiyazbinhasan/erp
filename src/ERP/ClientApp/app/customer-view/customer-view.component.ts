﻿import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CustomerDataService } from '../services/customer-data.service';
import { OrderDataService } from '../services/order-data.service';
import { InvoiceDataService } from '../services/invoice-data.service';
import { BillDataService } from '../services/bill-data.service';

import { IOrder } from '../view-models/order';
import { IInvoice } from '../view-models/invoice';
import { IBill } from '../view-models/bill';

import { LazyLoadEvent } from 'primeng/primeng';

import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

@Component({
    moduleId: 'AppModule',
    selector: 'app-customer-view',
    templateUrl: 'customer-view.component.html',
    styleUrls: ['./customer-view.component.css']
})

export class CustomerViewComponent implements OnInit {
    totalOrders: number = 0;
    totalPendingOrders: number = 0;
    totalInvoices: number = 0;
    totalBills: number = 0;
    errorMessage: string;
    totalBillAmount: number = 0;
    totalDueAmount: number = 0;
    totalOrderedItems: number = 0;
    totalInvoicedItems: number = 0;
    totalItemToBeInvoiced: number = 0;
    phone = "";

    customer: any = {
        id: '',
        firstName: '',
        lastName: '',
        phone: '',
        email: '',
        address1: '',
        address2: '',
        city: '',
        country: ''
    }

    orders: Array<IOrder>;
    pendingOrders: Array<IOrder>;
    invoices: Array<IInvoice>;
    bills: Array<IBill>;

    dtOrderCols: any[];
    dtInvoiceCols: any[];
    dtBillCols: any[];

    lazyLoadEvent: LazyLoadEvent;
    loadingOrder: boolean;
    loadingInvoice: boolean;
    loadingBill: boolean;
    loadingPendingOrder: boolean;

    constructor(private customerDataService: CustomerDataService,
        private orderDataService: OrderDataService,
        private invoiceDataService: InvoiceDataService,
        private billDataService: BillDataService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.dtOrderCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'orderDate', header: 'Order Date' },
            { field: 'expectedDeliveryDate', header: 'Expected Delivery Date' },
            { field: 'paymentStatus', header: 'Payment Status' }
        ];

        this.dtInvoiceCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'invoiceDate', header: 'Invoice Date' },
            { field: 'description', header: 'Description' }
        ];

        this.dtBillCols = [
            { field: 'moniker', header: 'Bill No', filter: true },
            { field: 'billDate', header: 'Bill Date' },
            { field: 'paidAmount', header: 'Amount' },
            { field: 'paymentMethod', header: 'Payment Method' }
        ];

        this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getCustomer(id);
            }
        );
    }

    getCustomer(customerId: string): void {
        this.customerDataService.getCustomer(customerId)
            .subscribe(customer => {
                this.customer = customer;
                this.customerDataService.getCustomerViewData(customerId)
                    .subscribe(result => {
                        this.totalBillAmount = result[0].totalBillAmount;
                        this.totalDueAmount = result[0].totalOrderAmount - result[0].totalBillAmount;
                        this.totalOrderedItems = result[0].totalOrderedItems;
                        this.totalInvoicedItems = result[0].totalInvoicedItems;
                        this.totalItemToBeInvoiced = result[0].totalOrderedItems - result[0].totalInvoicedItems;
                    });
            });
    }

    loadOrdersLazy(event: LazyLoadEvent) {
        this.loadingOrder = true;
        this.orderDataService.getPagedOrders(event, this.customer.phone)
            .subscribe(orders => {
                this.totalOrders = orders['headers'].totalCount;
                this.orders = orders['body'];
                _.each(this.orders, order => {
                    order.orderDate = new Date(order.orderDate).toDateString();
                    order.expectedDeliveryDate = new Date(order.expectedDeliveryDate).toDateString();
                });
                this.loadingOrder = false;
            }, (error: any) => this.errorMessage = <any>error);
    }

    loadPendingOrdersLazy(event: LazyLoadEvent) {
        this.loadingPendingOrder = true;
        event.filters = {
            orderStatus : {
                value: "pending"
            }
        };
        this.orderDataService.getPagedOrders(event, this.customer.phone)
            .subscribe(orders => {
                this.totalPendingOrders = orders['headers'].totalCount;
                this.pendingOrders = orders['body'];
                _.each(this.pendingOrders, order => {
                    order.orderDate = new Date(order.orderDate).toDateString();
                    order.expectedDeliveryDate = new Date(order.expectedDeliveryDate).toDateString();
                });
                this.loadingPendingOrder = false;
            }, (error: any) => this.errorMessage = <any>error);
    }

    loadInvoicesLazy(event: LazyLoadEvent) {
        this.loadingInvoice = true;
        this.invoiceDataService.getPagedInvoices(event, this.customer.phone).subscribe(invoices => {
            this.totalInvoices = invoices['headers'].totalCount;
            this.invoices = invoices['body'];
            _.each(this.invoices, invoice => {
                invoice.invoiceDate = new Date(invoice.invoiceDate).toDateString();
            });
            this.loadingInvoice = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    loadBillsLazy(event: LazyLoadEvent) {
        this.loadingBill = true;
        this.billDataService.getPagedBills(event, this.customer.phone).subscribe(bills => {
            this.totalBills = bills['headers'].totalCount;
            this.bills = bills['body'];
            this.loadingBill = false;
        }, (error: any) => this.errorMessage = <any>error);
    }
}