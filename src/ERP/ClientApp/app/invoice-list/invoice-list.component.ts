﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InvoiceDataService } from '../services/invoice-data.service';
import { InvoiceItemDataService } from '../services/invoice-item-data.service';


import { SelectItem } from '../shared/select-item'
import { IInvoice } from '../view-models/invoice'

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';

@Component({
    moduleId: 'AppModule',
    selector: 'app-invoice-list',
    templateUrl: 'invoice-list.component.html',
    styleUrls: ['./invoice-list.component.css'],
    providers: [ConfirmationService]
})
export class InvoiceListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    invoices: Array<IInvoice>;
    totalRecords: number;
    loading: boolean;
    stacked: boolean;

    lazyLoadEvent: LazyLoadEvent;

    constructor(private invoiceDataService: InvoiceDataService,
        private invoiceItemDataService: InvoiceItemDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {

        this.dtCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'invoiceDate', header: 'Invoice Date', filter: false },
            { field: 'description', header: 'Description', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols.slice(0, 3);
    }
    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }
        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadInvoicesLazy(event: LazyLoadEvent) {
        this.lazyLoadEvent = event;
        this.loading = true;

        this.invoiceDataService.getPagedInvoices(event).subscribe(invoices => {
            this.totalRecords = invoices['headers'].totalCount;
            this.invoices = invoices['body'];
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    navigateToInvoiceAddPage() {
        this.router.navigate(['/invoice-maint']);
    }

    editInvoice(invoice: IInvoice) {
        this.router.navigate(['/invoice-maint', invoice.order.customer.id, invoice.order.id, invoice.id]);
    }

    viewInvoice(invoice: IInvoice) {
        this.router.navigate(['/invoice-view', invoice.order.customer.id, invoice.order.id, invoice.id]);
    }

    deleteInvoice(invoice: IInvoice): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.invoiceItemDataService.getItems(invoice.id)
                    .subscribe(invoiceItems => {
                        let observables = [];
                        observables = _.map(invoiceItems, invoiceItem => {
                            return this.invoiceItemDataService.deleteInvoiceItem(invoiceItem.id);
                        });
                        observables.push(this.invoiceDataService.deleteInvoice(invoice.order.customer.id, invoice.order.id, invoice.id));
                        Observable.concat(...observables).subscribe(() => {
                            this.refreshDataTable();
                        }, (error: any) =>
                            { this.errorMessage = <any>error });

                    }, (error: any) => this.errorMessage = <any>error);
            }
        });
    }

    refreshDataTable() {
        this.loadInvoicesLazy(this.lazyLoadEvent);
    }
}