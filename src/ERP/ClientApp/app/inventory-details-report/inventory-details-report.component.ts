﻿import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';

import { InventoryReportDataService } from '../services/inventory-report.service';
import { SelectItem } from '../shared/select-item';

import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-inventory-details-report',
    templateUrl: 'inventory-details-report.component.html'
})

export class InventoryDetailsReportComponent implements OnInit {
    errorMessage: string;
    inventoryDetailsReport: Array<any>;
    dtCols: any[];
    columnOptions: SelectItem[];
    totalRecords: number = 0;
    loading: boolean;
    rangeDates: Date[];

    totalOrderedQuantity: number = 0;
    totalQuantityIn: number = 0;
    totalQuantityOut: number = 0;
    totalQuantityAvailable: number = 0;
    totalItems: number = 0;

    listType: any = false;

    inventoryDetails = [];

    constructor(private fb: FormBuilder,
        private inventoryReportDataService: InventoryReportDataService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Item'},
            { field: 'orderedQuantity', header: 'Ordered Quantity' },
            { field: 'inventoryQuantity', header: 'Quantity In' },
            { field: 'invoicedQuantity', header: 'Quantity Out' },
            { field: 'warehouseQuantity', header: 'Quantity Available' }
        ];
        this.loadReport();

        var date = new Date();
        this.rangeDates = [new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)];
    }

    loadReport() {
        this.totalOrderedQuantity = 0;
        this.totalQuantityIn = 0;
        this.totalQuantityOut = 0;
        this.totalQuantityAvailable = 0;
        this.loading = true;

        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);

        this.inventoryReportDataService.getInventorieDetailsReport(startDate, endDate).subscribe(inventoryDetails => {
            this.inventoryDetailsReport = inventoryDetails;
            this.inventoryDetails = inventoryDetails;
            this.toggleList();
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    toggleList(): void {
        this.inventoryDetailsReport = this.inventoryDetails;
        if (this.listType) {
            this.totalOrderedQuantity = 0;
            this.totalQuantityIn = 0;
            this.totalQuantityOut = 0;
            this.totalQuantityAvailable = 0;
            this.inventoryDetailsReport = [];
            _.each(this.inventoryDetails, inventoryDetail => {
                if (inventoryDetail.orderedQuantity == 0 &&
                    inventoryDetail.inventoryQuantity == 0 &&
                    inventoryDetail.invoicedQuantity == 0) {
                    this.inventoryDetailsReport.push(inventoryDetail);
                    this.totalQuantityAvailable += inventoryDetail.warehouseQuantity;
                }
            });
        }
        else if (!this.listType && this.listType != null) {
            this.totalOrderedQuantity = 0;
            this.totalQuantityIn = 0;
            this.totalQuantityOut = 0;
            this.totalQuantityAvailable = 0;
            this.inventoryDetailsReport = [];
            _.each(this.inventoryDetails, inventoryDetail => {
                if (inventoryDetail.orderedQuantity > 0 ||
                    inventoryDetail.inventoryQuantity > 0 ||
                    inventoryDetail.invoicedQuantity > 0) {
                    this.inventoryDetailsReport.push(inventoryDetail);

                    this.totalOrderedQuantity += inventoryDetail.orderedQuantity;
                    this.totalQuantityIn += inventoryDetail.inventoryQuantity;
                    this.totalQuantityOut += inventoryDetail.invoicedQuantity;
                    this.totalQuantityAvailable += inventoryDetail.warehouseQuantity;
                }
            });
        }
    }

    formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + monthIndex + '-' + day;
    }

    generateExcel() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);
        this.inventoryReportDataService.generateExcelInventoryDetails(startDate, endDate, this.listType)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/xlsx' });
                FileSaver.saveAs(blob, "Inventory-details.xlsx");
            }, err => console.error(err));
    }
}