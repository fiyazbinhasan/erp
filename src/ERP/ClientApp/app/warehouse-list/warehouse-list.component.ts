﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IWarehouse } from '../view-models/warehouse'
import { WarehouseDataService } from '../services/warehouse-data.service'
import { SelectItem } from '../shared/select-item'

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
    moduleId: 'AppModule',
    selector: 'app-warehouse-list',
    templateUrl: 'warehouse-list.component.html',
    styleUrls: ['./warehouse-list.component.css'],
    providers: [ConfirmationService]
})

export class WarehouseListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    warehouses: Array<IWarehouse>;
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;
    
    lazyLoadEvent: LazyLoadEvent;

    constructor(private warehouseDataService: WarehouseDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { header: 'Code', field: 'moniker', filter: true },
            { header: 'Address', field: 'address', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols;
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }
        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadWarehousesLazy(event: LazyLoadEvent) {
        this.lazyLoadEvent = event;
        this.loading = true;

        this.warehouseDataService.getPagedWarehouses(event).subscribe(items => {
            this.totalRecords = items['headers'].totalCount;
            this.warehouses = items['body'];
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editWarehouse(warehouse: IWarehouse) {
        this.router.navigate(['/warehouse-maint', warehouse.id]);
    }
    
    navigateToWarehouseAddPage() {
        this.router.navigate(['/warehouse-maint']);
    }

    viewWarehouse(warehouse: IWarehouse) {
        this.router.navigate(['/warehouse-view', warehouse.id]);
    }
    deleteWarehouse(warehouse: IWarehouse): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.warehouseDataService.deleteWarehouse(warehouse.id)
                    .subscribe(
                    () => this.refreshDataTable(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadWarehousesLazy(this.lazyLoadEvent);
    }
}