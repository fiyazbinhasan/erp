﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { ICategory } from '../view-models/category';
import { CategoryDataService } from '../services/category-data.service';

import { IItem } from '../view-models/item';
import { ItemDataService } from '../services/item-data.service';
import { LazyLoadEvent } from 'primeng/primeng';
import * as _ from 'lodash';

@Component({
    selector: 'category-view',
    templateUrl: 'category-view.component.html',
    styleUrls: ['./category-view.component.css']
})

export class CategoryViewComponent implements OnInit {
    private sub: Subscription;
    categoryName: string = "";
    categoryDescription: string = "";
    errorMessage: string;
    items: Array<IItem>;
    totalRecords: number = 0;
    dtCols: any[];
    loading: boolean;

    constructor(private categoryDataService: CategoryDataService,
        private itemDataService: ItemDataService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Name', filter: true },
            { field: 'description', header: 'Description' }
        ];
        this.route.params.subscribe(
            params => {
                let id = params['id'];
            }
        );
    }

    loadItemsLazy(event: LazyLoadEvent) {
        if (_.isEmpty(this.categoryName)) {
            this.route.params.subscribe(
                params => {
                    this.categoryDataService.getCategory(params['id'])
                        .subscribe((category: ICategory) => {
                            this.categoryName = category.name;
                            this.categoryDescription = category.description;
                            this.getPagedItems(event);
                        },
                        (error: any) => this.errorMessage = <any>error);
                }
            );
        }
        else {
            this.getPagedItems(event);
        }
    }

    getPagedItems(event): void {
        this.loading = true;
        this.itemDataService.getPagedItems(event, this.categoryName).subscribe(items => {
            this.totalRecords = items['headers'].totalCount;
            this.items = items['body'];
            this.loading = false;
        },
        (error: any) => this.errorMessage = <any>error);
    }
}   