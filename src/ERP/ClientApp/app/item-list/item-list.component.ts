import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IItem } from '../view-models/item';
import { ItemDataService } from '../services/item-data.service';
import { SelectItem } from '../shared/select-item';

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
    moduleId: 'AppModule',
    selector: 'app-item-list',
    templateUrl: 'item-list.component.html',
    styleUrls: ['./item-list.component.css'],
    providers: [ConfirmationService]
})

export class ItemListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    items: Array<IItem>;
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;

    rangeValues: number[];

    lazyLoadEvent: LazyLoadEvent;

    constructor(private itemDataService: ItemDataService, 
        private router: Router, 
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Name', filter: true, sortable: true },
            { field: 'description', header: 'Description', filter: false, sortable: true },
            { field: 'category.name', header: 'Category', filter: true, sortable: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols;
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadItemsLazy(event: LazyLoadEvent) {
        this.loading = true;
        this.lazyLoadEvent = event;

        this.itemDataService.getPagedItems(event).subscribe(items => {
                this.totalRecords = items['headers'].totalCount;
                this.items = items['body'];
                this.loading = false;
            },
            (error: any) => this.errorMessage = <any>error);
    }
    viewItem(item: IItem) {
        this.router.navigate(['/item-view', item.category.id, item.id]);
    }

    editItem(item: IItem) {
        this.router.navigate(['/item-maint', item.category.id, item.id]);
    }

    navigateToItemAddPage() {
        this.router.navigate(['/item-maint']);
    }

    deleteItem(item: IItem): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.itemDataService.deleteItem(item.category.id, item.id)
                    .subscribe(() => this.refreshDataTable(),
                        (error: any) => this.errorMessage = <any>error);
            }
        });
    }

    refreshDataTable() {
        this.loadItemsLazy(this.lazyLoadEvent);
    }
}
