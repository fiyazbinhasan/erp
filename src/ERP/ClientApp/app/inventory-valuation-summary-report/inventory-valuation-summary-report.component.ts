﻿import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';

import { InventoryReportDataService } from '../services/inventory-report.service';
import { SelectItem } from '../shared/select-item';

import * as _ from 'lodash';

import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-inventory-valuation-summary-report',
    templateUrl: 'inventory-valuation-summary-report.component.html'
})

export class InventoryValuationSummaryReportComponent implements OnInit {
    errorMessage: string;
    summaryDetails: Array<any>;
    dtCols: any[];
    columnOptions: SelectItem[];
    loading: boolean;
    totalRecords: number = 0;
    rangeDates: Date[];
    totalAsset: number = 0;
    totalAvailableQuatity: number = 0;
    inventoryDetails = [];
    listType: any = false;

    constructor(private fb: FormBuilder,
        private inventoryReportDataService: InventoryReportDataService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Item' },
            { field: 'warehouseQuantity', header: 'Quantity Available' },
            { field: 'inventoryAsset', header: 'Inventory Asset Value' }
        ];
        this.loadReport();

        var date = new Date();
        this.rangeDates = [new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)];
    }

    loadReport() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);

        this.inventoryReportDataService.getInventoryValuationSummary(startDate, endDate).subscribe(inventoryDetails => {
            this.summaryDetails = inventoryDetails;
            this.inventoryDetails = inventoryDetails;
            this.toggleList();
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    toggleList(): void {
        this.summaryDetails = this.inventoryDetails;
        if (this.listType) {
            this.totalAsset = 0;
            this.totalAvailableQuatity = 0;
            this.summaryDetails = [];
            _.each(this.inventoryDetails, inventoryDetail => {
                if (inventoryDetail.inventoryAsset == 0 &&
                    inventoryDetail.warehouseQuantity == 0) {
                    this.summaryDetails.push(inventoryDetail);
                }
            });
        }
        else if (!this.listType && this.listType != null) {
            this.totalAsset = 0;
            this.totalAvailableQuatity = 0;
            this.summaryDetails = [];
            _.each(this.inventoryDetails, inventoryDetail => {
                if (inventoryDetail.inventoryAsset > 0 ||
                    inventoryDetail.warehouseQuantity > 0) {
                    this.summaryDetails.push(inventoryDetail);

                    this.totalAsset += inventoryDetail.inventoryAsset;
                    this.totalAvailableQuatity += inventoryDetail.warehouseQuantity;
                }
            });
        }
    }

    formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + monthIndex + '-' + day;
    }

    generateExcel() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);

        this.inventoryReportDataService.generateExcelInventoryValuationSummary(startDate, endDate, this.listType)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/xlsx' });
                FileSaver.saveAs(blob, "Inventory-valuation-report.xlsx");
            }, err => console.error(err));
    }
}