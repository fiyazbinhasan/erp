import { Component, OnInit, Output, AfterViewInit, OnDestroy, ViewChildren, ElementRef, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { GenericValidator } from '../shared/generic-validator';

import { ISupplier } from '../view-models/supplier';
import { SupplierDataService } from '../services/supplier-data.service';
import { SelectItem } from '../shared/select-item'

@Component({
    selector: 'supplier-maint',
    templateUrl: 'supplier-maint.component.html',
    styleUrls: ['./supplier-maint.component.css']
})

export class SupplierMaintComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
    
    errorMessage: string;
    public supplierForm: FormGroup;

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    headline: string = 'Edit supplier';
    paragraph: string = 'Use this form to edit the existing supplier';
    
    supplier: ISupplier;
    private sub: Subscription;

    countries: Array<SelectItem> = [];

    constructor(private fb: FormBuilder,
        private supplierDataService: SupplierDataService,
        private route: ActivatedRoute,
        private router: Router) {
        this.validationMessages = {
            firstName: {
                required: 'First name is required.',
                minlength: 'First name must be at least three characters.',
                maxlength: 'First name cannot exceed 50 characters.'
            },
            lastName: {
                required: 'Last name is required.',
                minlength: 'Last name must be at least three characters.',
                maxlength: 'Last name cannot exceed 50 characters.'
            },
            phone: {
                required: 'Phone number is required.'
            },
            address1: {
                required: 'Address is required.'
            },
            email: {
                pattern: 'Provide a valid email address'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.supplierForm = this.fb.group({
            firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            lastName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            companyName: '',
            phone: ['', [Validators.required]],
            alternativePhone: '',
            address1: ['', [Validators.required]],
            address2: '',
            city: '',
            country: '',
            postalCode: '',
            email: ['', [Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]],
            website: ''
        });

        this.countries.push.apply(this.countries, this.supplierDataService.countries);

        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getSupplier(id);
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.supplierForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.supplierForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveSupplier(): void {
        if (this.supplierForm.dirty && this.supplierForm.valid) {
            let s = Object.assign({}, this.supplier, this.supplierForm.value);

            this.supplierDataService.saveSupplier(s)
                .subscribe(() => {
                    this.onSaveComplete();
                },
                (error: any) => this.errorMessage = <any>error);

        } else if (!this.supplierForm.dirty) {
            this.onSaveComplete();
        }
    }

    getSupplier(id: string): void {
        if (id === undefined) {
            this.headline = 'Add supplier';
            this.paragraph = `Use this form to add a new supplier`;
        } else {
            this.supplierDataService.getSupplier(id)
                .subscribe(
                (supplier: ISupplier) => this.onSupplierRetrieved(supplier),
                (error: any) => this.errorMessage = <any>error
                );
        }
    }

    onSupplierRetrieved(supplier: ISupplier): void {
        if (this.supplierForm) {
            this.supplierForm.reset();
        }

        this.supplier = supplier;
        // Update the data on the form
        this.supplierForm.setValue({
            firstName: this.supplier.firstName,
            lastName: this.supplier.lastName,
            companyName: this.supplier.companyName,
            phone: this.supplier.phone,
            alternativePhone: this.supplier.alternativePhone,
            address1: this.supplier.address1,
            address2: this.supplier.address2,
            city: this.supplier.city,
            country: this.supplier.country,
            postalCode: this.supplier.postalCode,
            email: this.supplier.email,
            website: this.supplier.website
        });
    }

    onSaveComplete(): void {
        // Reset the form to clear the flags
        this.supplierForm.reset();
        this.router.navigate(["/supplier-list"]);
    }
}