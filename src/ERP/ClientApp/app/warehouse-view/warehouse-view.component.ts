﻿import {Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router" 
import { IWarehouse } from '../view-models/warehouse'
import { IWarehouseItem } from '../view-models/warehouse-item'

import { WarehouseDataService } from '../services/warehouse-data.service';
import { WarehouseItemDataService } from '../services/warehouse-item-data.service';

import * as _ from "lodash";

@Component({
    selector: 'app-warehouse-view',
    templateUrl: 'warehouse-view.component.html',
    styleUrls: ['./warehouse-view.component.css']
})

export class WarehouseViewComponent implements OnInit {
    errorMessage: string;
    cols: any[];
    totalRecords: number = 0;
    loading: boolean;
    warehouse: any = {
        moniker: '',
        address: ''
    };
    warehouseItems: Array<IWarehouseItem>;
    constructor(private warehouseDataservice: WarehouseDataService, private warehouseItemDataservice: WarehouseItemDataService, private route: ActivatedRoute) {}
    ngOnInit() {
        this.cols = [
            { header: 'Name', field: 'measurementUnitSetup.item.name', filter: true },
            { header: 'Base Unit', field: 'measurementUnitSetup.measurementUnit.name' },
            { header: 'Quantity', field: 'quantity', sortable: true },
        ];
        this.loading = true;
        this.route.params.subscribe(
            params => {
                this.warehouseDataservice.getWarehouse(params['id'])
                    .subscribe((warehouse: IWarehouse) => {
                        this.warehouse = warehouse;
                        this.warehouseItemDataservice.getItems(warehouse.id)
                            .subscribe(
                            (warehouseItems: any) => {
                                this.warehouseItems = [];
                                this.totalRecords = warehouseItems.length;
                                this.warehouseItems = warehouseItems;
                                this.loading = false;
                            }
                            , (error: any) => this.errorMessage = <any>error)
                    }, (error: any) => this.errorMessage = <any>error);
            }, (error: any) => this.errorMessage = <any>error);
    }
}