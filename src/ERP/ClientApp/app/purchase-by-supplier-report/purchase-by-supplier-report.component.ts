﻿import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';

import { PurchaseReportDataService } from '../services/purchase-report.service';
import { SelectItem } from '../shared/select-item';

import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-purchase-by-supplier-report',
    templateUrl: 'purchase-by-supplier-report.component.html'
})

export class PurchaseBySupplierReportComponent implements OnInit {
    errorMessage: string;
    purchaseBySuppliers: Array<any>;
    dtCols: any[];
    columnOptions: SelectItem[];
    loading: boolean;
    totalRecords: number = 0;
    rangeDates: Date[];
    totalBill: number = 0;
    fullRecordDetails = [];
    listType: any = false;

    constructor(private fb: FormBuilder,
        private purchaseReportDataService: PurchaseReportDataService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Supplier' },
            { field: 'bill', header: 'Bill' }
        ];
        this.loadReport();

        var date = new Date();
        this.rangeDates = [new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)];
    }

    loadReport() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);

        this.purchaseReportDataService.getPurchaseBySupplier(startDate, endDate).subscribe(results => {
            this.purchaseBySuppliers = results;
            this.fullRecordDetails = results;
            this.toggleList();
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    toggleList(): void {
        this.purchaseBySuppliers = this.fullRecordDetails;
        if (this.listType) {
            this.totalBill = 0;
            this.purchaseBySuppliers = [];
            _.each(this.fullRecordDetails, result => {
                if (result.bill == 0) {
                    this.purchaseBySuppliers.push(result);
                }
            });
        }
        else if (!this.listType && this.listType != null) {
            this.totalBill = 0;
            this.purchaseBySuppliers = [];
            _.each(this.fullRecordDetails, result => {
                if (result.bill > 0) {
                    this.purchaseBySuppliers.push(result);
                    this.totalBill += result.bill;
                }
            });
        }
    }

    formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + monthIndex + '-' + day;
    }

    generateExcel() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);
        this.purchaseReportDataService.generateExcelPurchaseBySupplier(startDate, endDate, this.listType)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/xlsx' });
                FileSaver.saveAs(blob, "Purchase by supplier.xlsx");
            }, err => console.error(err));
    }
}