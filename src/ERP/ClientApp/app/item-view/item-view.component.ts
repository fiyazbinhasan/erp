﻿import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgClass } from '@angular/common';

import { IItem } from '../view-models/item';
import { ItemDataService } from '../services/item-data.service';
import { IMeasurementUnitSetup } from '../view-models/measurement-unit-setup';
import { MeasurementUnitSetupDataService } from '../services/measurement-unit-setup-data.service';
import * as _ from "lodash";

@Component({
    moduleId: 'AppModule',
    selector: 'app-item-view',
    templateUrl: 'item-view.component.html',
    styleUrls: ['./item-view.component.css']
})
export class ItemViewComponent implements OnInit {
    item: any = {
        name: '',
        description: '',
        category: {
            name: '',
        }
    };
    measurementUnitSetups: Array<IMeasurementUnitSetup>;
    errorMessage: any;
    dtCols: any[];
    itemDemandData: any;

    options = {
        scales: {
            xAxes: [{
                barPercentage: 1,
            }]
        },
        maintainAspectRatio: false
    }

    constructor(private itemDataService: ItemDataService, private measurementUnitSetupDataService:MeasurementUnitSetupDataService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        
        this.dtCols = [
            { field: 'measurementUnit.name', header: 'Unit', filter: true },
            { field: 'isBase', header: 'Base'},
            { field: 'conversionRatio', header: 'Conversion Ratio', },
            { field: 'sellingPrice', header: 'Unit Selling Price' },
            { field: 'allowDiscount', header: 'Allow Discount'},
            { field: 'discountPrice', header: 'Discount Price' }
        ];

        this.route.params.subscribe(
            params => {
                this.itemDemandGraphCalculation(params['categoryId'], params['id']);
                this.itemDataService.getItem(params['categoryId'], params['id']).subscribe(item => {
                    this.item = item;
                    this.measurementUnitSetupDataService.getMeasurementUnits(params['id']).subscribe(measurementUnitSetups => {
                        this.measurementUnitSetups = measurementUnitSetups;
                    },
                        (error: any) => this.errorMessage = <any>error);
                },
                    (error: any) => this.errorMessage = <any>error);
            },
            (error: any) => this.errorMessage = <any>error);

    }

    itemDemandGraphCalculation(categoryId: string, id: string)
    {
        this.itemDataService.getItemDemandGraph(categoryId, id).subscribe((itemDemandByMonths) => {
            this.itemDemandData = {
                labels: _.map(itemDemandByMonths, 'month'),
                datasets: [
                    {
                        type: 'bar',
                        label: 'Purchase Quantity',
                        width: '30',
                        borderWidth: 2,
                        backgroundColor: '#607D8B',
                        data:_.map(itemDemandByMonths, 'purchaseQuantity')
                    },
                    {
                        type: 'bar',
                        label: 'Ordered Quantity',
                        width: '30',
                        borderWidth: 2,
                        backgroundColor: '#9E9E9E',
                        data:_.map(itemDemandByMonths, 'orderedQuantity')
                    },
                    {
                        type: 'bar',
                        label: 'Sold Quantity',
                        width: '30',
                        borderWidth: 2,
                        backgroundColor: '#4DB6AC',
                        data: _.map(itemDemandByMonths, 'soldQuantity')
                    },
                ]
            }
        },
            (error) => this.errorMessage = <any>error);
    }
}