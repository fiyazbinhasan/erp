﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InputSwitchModule, GrowlModule, Message } from 'primeng/primeng';

import { SelectItem } from '../shared/select-item';

import { IOrder } from '../view-models/order';
import { IItem } from '../view-models/item';
import { IOrderItem, IOrderItems } from '../view-models/order-item';
import { IMeasurementUnit } from '../view-models/measurement-unit';

import { CustomerDataService } from '../services/customer-data.service';
import { ItemDataService } from '../services/item-data.service';
import { OrderDataService } from '../services/order-data.service';
import { OrderItemDataService } from '../services/order-item-data.service';
import { MeasurementUnitDataService } from '../services/measurement-unit-data.service';
import { MeasurementUnitSetupDataService } from '../services/measurement-unit-setup-data.service';
import { GenericValidator } from '../shared/generic-validator';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import * as _ from "lodash";

function fixTimeOffset(date: Date): Date {
    date.setHours(date.getHours() - date.getTimezoneOffset() / 60);
    return date;
}

@Component({
    moduleId: 'AppModule',
    selector: 'app-order-maint',
    templateUrl: 'order-maint.component.html',
    styleUrls: ['./order-maint.component.css']
})

export class OrderMaintComponent implements OnInit {
    private sub: Subscription;
    private genericValidator: GenericValidator;
    private validationMessages: { [key: string]: { [key: string]: string } };
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[]; //??

    paragraph: string = 'Use this form to edit the existing order.';
    displayMessage: { [key: string]: string } = {};
    errorMessage: string;

    paymentStatus: string = "Unpaid";
    orderStatus: string = "Pending";

    minDateValue: Date = new Date();
    expectedDeliveryDate?: Date = null;

    checked: boolean = false;
    msgs: Message[] = [];

    orderForm: FormGroup;
    customers: Array<SelectItem> = [];
    
    items: Array<IItem> = [];
    order: IOrder =  <IOrder>{};
    orderWithItems: IOrderItems = <IOrderItems>{};
    orderItemsCopy: Array<string> = [];
    
    get orderItems(): FormArray {
        return <FormArray>this.orderForm.get('orderItems');
    }

    get moniker() {
        return this.orderForm.get('moniker');
    }

    constructor(private fb: FormBuilder,
        private itemDataService: ItemDataService,
        private customerDataService: CustomerDataService,
        private orderDataService: OrderDataService,
        private orderItemDataService: OrderItemDataService,
        private measurementUnitDataService: MeasurementUnitDataService,
        private measurementUnitSetupDataService: MeasurementUnitSetupDataService,
        private route: ActivatedRoute,
        private router: Router) {}

    ngOnInit() {
        this.orderForm = this.fb.group({
            moniker: ['', [Validators.required]],
            orderDate: ['', [Validators.required]],
            expectedDeliveryDate: ['', [Validators.required]],
            customer: ['', [Validators.required]],
            orderItems: this.fb.array([])
        });

        this.validationMessages = {
            moniker: {
                required: 'Order Name is required'
            },
            orderDate: {
                required: 'Order date is required'
            },
            expectedDeliveryDate: {
                required: 'Expected delivery date is required'
            },
            customer: {
                required: 'Customer is required'
            }
        };
        
        this.genericValidator = new GenericValidator(this.validationMessages);

        this.customerDataService.getCustomers().subscribe(
            (customers) => {
                customers.map((customer) => {
                    this.customers.push(<SelectItem>{ label: customer.firstName + " " + customer.lastName, value: customer });
                });
            }
        );

        this.sub = this.route.params.subscribe(
            params => {
                let customerId = params['customerId'];
                let id = params['id'];
                this.getOrderWithItems(customerId, id);
            }
        );
    }
    
    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.orderForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.orderForm);
        });

    }

    handleChange() {
        if (this.checked) {
            this.moniker.clearValidators()
            this.moniker.setErrors(null);
        }
            
        else {
            this.moniker.setValidators(Validators.required)
        }
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    searchItems(event) {
        this.itemDataService.searchItems(event.query).subscribe(
            (items) => {
                this.items = items;
            }
        );
    }

    addItemInList(item) {
        var isAdded = _.find(this.orderItems.controls,
            orderItemControl => (<IItem>orderItemControl.get('item').value).id === item.id);

        if (isAdded)
            return;
        else {
            this.measurementUnitSetupDataService.getMeasurementUnits(item.id).subscribe(measurementUnitSetups => {
                var setupObj = {
                    measurementUnitSetups:[]
                };
                
                setupObj.measurementUnitSetups = _.map(measurementUnitSetups,
                    measurementUnitSetup => ({
                        label: measurementUnitSetup.measurementUnit.name,
                        value: measurementUnitSetup
                    })
                );

                this.orderItems.push(this.fb.group({
                    id: '',
                    quantity: 1,
                    measurementUnitSetups: [setupObj.measurementUnitSetups],
                    measurementUnitSetup: ['', [Validators.required]],
                    unitPrice: 0,
                    discount: 0,
                    amount:0,
                    remarks: 'N/A',
                    orderId: '',
                    order: this.order,
                    item: item,
                    itemId: item.id
                }));
            });
        }
          
    }

    deleteItemFromList(index: number) {
        const arrayControl = <FormArray>this.orderForm.controls['orderItems'];
        arrayControl.removeAt(index);

        this.orderForm.updateValueAndValidity();
        this.orderForm.markAsDirty();
    }

    extractOrderForManipulate(orderWithItems: any): IOrder {
        return <IOrder>{
            id: this.order.id,
            moniker: this.checked ? "":orderWithItems.moniker,
            orderDate: fixTimeOffset(orderWithItems.orderDate),
            expectedDeliveryDate: fixTimeOffset(orderWithItems.expectedDeliveryDate),
            customer: orderWithItems.customer
        };
    }

    calculateAmount(quantity: number, unitPrice: number, discount: number) {
        return quantity * (unitPrice - discount);
    }
   
    saveOrder(): void {
        if (this.orderForm.dirty && this.orderForm.valid) {

          
            let orderWithItems = Object.assign({}, this.orderWithItems, this.orderForm.value);
            let order = this.extractOrderForManipulate(orderWithItems);
            let orderItems = orderWithItems.orderItems;
            let orderItemsforDeletion = _.differenceWith(this.orderItemsCopy, _.map(orderItems, 'id'), _.isEqual);

            this.orderDataService.saveOrder(order).subscribe((order) => {
                    let observables = [];
                    this.order = order;
                   
                    observables = _.map(orderItems,
                        orderItem => {
                            return this.orderItemDataService.saveOrderItem(<IOrderItem>{
                                id: orderItem.id,
                                quantity: orderItem.quantity,
                                leftQuantity: orderItem.quantity,
                                measurementUnitSetupId: orderItem.measurementUnitSetup.id,
                                unitPrice: orderItem.unitPrice,
                                discount: orderItem.discount,
                                amount: this.calculateAmount(orderItem.quantity,
                                    orderItem.unitPrice,
                                    (orderItem.measurementUnitSetup.allowDiscount ? orderItem.discount : 0)),
                                remarks: orderItem.remarks,
                                orderId: this.order.id
                            });
                        });

                    if (orderItemsforDeletion != null && orderItemsforDeletion.length != 0)
                        _.each(orderItemsforDeletion,
                            (orderItemId) => {
                                observables.push(this.orderItemDataService.deleteOrderItem(orderItemId));
                            });

                    Observable
                        .forkJoin(observables)
                        .subscribe(() => {
                                this.msgs = [];
                                this.msgs.push({
                                    severity: 'success',
                                    summary: 'Saved SuccessFully',
                                    detail: this.order.moniker
                                });
                                this.onSaveComplete();
                            },
                            (error: any) => this.errorMessage = <any>error);
                },
                (error: any) => this.errorMessage = <any>error);

        } else if (!this.orderForm.dirty) {
            this.onSaveComplete();
        }
    }

    getOrderWithItems(customerId: string, id: string): void {
        if (id === undefined && customerId === undefined) {
            this.paragraph = `Use this form to add a new order`;
        } else {
            Observable
                .forkJoin([this.orderDataService.getOrder(customerId, id), this.orderItemDataService.getItems(id)])
                .subscribe(results => {
                    this.onOrderRetrieved(results[0]);
                    this.onOrderItemsRetrieved(results[1]);
                });
        }
    }

    onOrderRetrieved(order: any) {
        if (this.orderForm) {
            this.orderForm.reset();
        }

        this.order = order;
        this.paymentStatus = order.paymentStatus;

        this.orderForm.patchValue({
            moniker: this.order.moniker,
            orderDate: new Date(this.order.orderDate),
            expectedDeliveryDate: new Date(this.order.expectedDeliveryDate),
            customer: this.order.customer
        });

        this.setExpectedDateMinValue();
    }

    onOrderItemsRetrieved(orderItems: any): void {
        this.orderItemsCopy = _.map(orderItems, 'id');
        const orderItemsControl = <FormArray>this.orderForm.controls['orderItems'];

        _.each(orderItems,
            (orderItem => {
                this.measurementUnitSetupDataService.getMeasurementUnits(orderItem.measurementUnitSetup.itemId).subscribe(measurementUnitSetups => {
                    var setupObj = {
                        measurementUnitSetups: []
                    };

                    setupObj.measurementUnitSetups = _.map(measurementUnitSetups,
                        measurementUnitSetup => ({
                            label: measurementUnitSetup.measurementUnit.name,
                            value: measurementUnitSetup
                        }));

                orderItemsControl.push(this.fb.group({
                    id: orderItem.id,
                    quantity: orderItem.quantity,
                    unitPrice: orderItem.unitPrice,
                    discount: orderItem.discount,
                    amount: this.calculateAmount(orderItem.quantity, orderItem.unitPrice, orderItem.discount),
                    remarks: orderItem.remarks,
                    item: new FormControl(orderItem.measurementUnitSetup.item),
                    measurementUnitSetups: [setupObj.measurementUnitSetups],
                    measurementUnitSetup: new FormControl(orderItem.measurementUnitSetup),
                    measurementUnitSetupId: new FormControl(orderItem.measurementUnitSetupId),
                    order: orderItem.order,
                    orderId: orderItem.orderId
                }));
                });
            }));
    }

    onSaveComplete(): void {
        this.orderForm.reset();
        this.orderForm.setControl('orderItems', this.fb.array([]));
        this.router.navigate(["/order-list"]);
    }

    calculateRowAmount(index: number) {
        var formArray = this.orderForm.get("orderItems") as FormArray;
        formArray.at(index).patchValue({
            amount: this.calculateAmount(formArray.at(index).get('quantity').value, formArray.at(index).get('unitPrice').value, formArray.at(index).get('discount').value)
        });
    }

    changeFieldsValues(index: number) {
        var formArray = this.orderForm.get("orderItems") as FormArray;
        var unitPrice = formArray.at(index).get("measurementUnitSetup").value == 0 ? 0 : formArray.at(index).get("measurementUnitSetup").value.sellingPrice;
        var discount = formArray.at(index).get("measurementUnitSetup").value == 0 ? 0 : formArray.at(index).get("measurementUnitSetup").value.discountPrice;
        formArray.at(index).patchValue({
            unitPrice: unitPrice,
            discount: discount,
            amount: this.calculateAmount(formArray.at(index).get("quantity").value, unitPrice, discount)
        });
    }

    setExpectedDateMinValue(): void {
        var orderDate = new Date(this.orderForm.getRawValue().orderDate);
        var expectedDeliveryDate = new Date(this.orderForm.getRawValue().expectedDeliveryDate);
        this.expectedDeliveryDate = (expectedDeliveryDate < orderDate ? orderDate : expectedDeliveryDate);
        this.minDateValue.setDate(orderDate.getDate());
        this.minDateValue.setMonth(orderDate.getMonth());
        this.minDateValue.setFullYear(orderDate.getFullYear());
    }
}