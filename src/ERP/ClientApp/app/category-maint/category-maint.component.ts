﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { SelectItem } from '../shared/select-item'
import { ICategory } from '../view-models/category';
import { CategoryDataService } from '../services/category-data.service';
import { GenericValidator } from '../shared/generic-validator';

@Component({
    selector: 'category-maint',
    templateUrl: 'category-maint.component.html',
    styleUrls: ['./category-maint.component.css']
})

export class CategoryMaintComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    errorMessage: string;
    public categoryForm: FormGroup;

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    headline: string = 'Edit category';
    paragraph: string = 'Use this form to edit the existing category';

    category: ICategory;
    private sub: Subscription;

    constructor(private fb: FormBuilder,
        private categoryDataService: CategoryDataService,
        private route: ActivatedRoute,
        private router: Router) {
        this.validationMessages = {
            name: {
                required: 'Name is required.',
            }
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.categoryForm = this.fb.group({
            name: ['', [Validators.required]],
            description: ''
        });

        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getCategory(id);
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.categoryForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.categoryForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveCategory(): void {
        if (this.categoryForm.dirty && this.categoryForm.valid) {
            let category = Object.assign({}, this.category, this.categoryForm.value);

            this.categoryDataService.saveCategory(category)
                .subscribe(() => {
                        this.onSaveComplete();
                    },
                    (error: any) => this.errorMessage = <any>error);

        } else if (!this.categoryForm.dirty) {
            this.onSaveComplete();
        }
    }

    onCategoryRetrieved(category: ICategory): void {
        if (this.categoryForm) {
            this.categoryForm.reset();
        }

        this.category = category;
        // Update the data on the form
        this.categoryForm.setValue({
            name: this.category.name,
            description: this.category.description
         
        });
    }
    getCategory(id: string): void {
        if (id === undefined) {
            this.headline = 'Add Category';
            this.paragraph = `Use this form to add a new category`;
        } else {
            this.categoryDataService.getCategory(id)
                .subscribe(
                (category: ICategory) => this.onCategoryRetrieved(category),
                    (error: any) => this.errorMessage = <any>error
                );
        }
    }
    onSaveComplete(): void {
        // Reset the form to clear the flags
        this.categoryForm.reset();
        this.router.navigate(["/category-list"]);
    }
}