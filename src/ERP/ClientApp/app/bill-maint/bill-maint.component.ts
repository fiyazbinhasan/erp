﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InputSwitchModule, GrowlModule, Message } from 'primeng/primeng';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { SelectItem } from '../shared/select-item';
import { IBill } from '../view-models/bill';
import { IOrder } from '../view-models/order';
import { ICustomer } from '../view-models/customer';
import { BillDataService } from '../services/bill-data.service';
import { OrderDataService } from '../services/order-data.service';
import { OrderItemDataService } from '../services/order-item-data.service';
import { CustomerDataService } from '../services/customer-data.service';
import { GenericValidator } from '../shared/generic-validator';
import { LazyLoadEvent } from 'primeng/primeng';

import { ValidatorService } from '../services/validator.service';


import * as _ from "lodash";

@Component({
    selector: 'bill-maint',
    templateUrl: 'bill-maint.component.html',
    styleUrls: ['./bill-maint.component.css']
})

export class BillMaintComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    headline: string = 'Edit Bill';
    paragraph: string = 'Use this form to edit the existing bill.';

    errorMessage: string = null;
    public billForm: FormGroup;

    orderTotalAmount: number = 0;
    dueTotalAmount: number = 0;
    totalPaidAmount: number = 0;

    bill: IBill;
    orders: Array<IOrder> = [];
    customer: ICustomer = null;

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    private sub: Subscription;
    checked: boolean = false;
    msgs: Message[] = [];
    
    get bills(): FormArray {
        return <FormArray>this.billForm.get('bills');
    }

    get orderMoniker() {
        return this.billForm.get('orderMoniker');
    }
    get moniker() {
        return this.billForm.get('moniker');
    }

    constructor(private fb: FormBuilder,
        private billDataService: BillDataService,
        private orderDataService: OrderDataService,
        private orderItemDataService: OrderItemDataService,
        private customerDataService: CustomerDataService,
        private validatorService: ValidatorService,
        private route: ActivatedRoute,
        private router: Router) {
        this.validationMessages = {
            moniker: {
                required: 'Bill no is required.'
            },
            billDate: {
                required: 'Bill date is required.'
            },
            orderMoniker: {
                required: 'Order no is required.',
                notFound: 'Order does not exists'
            },
            paidAmount: {
                required: "Give bill amount."
            }
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.billForm = this.fb.group({
            moniker: ['', [Validators.required]],
            paidAmount: [null, [Validators.required]],
            paymentMethod: 0,
            billDate: [null, [Validators.required]],
            orderMoniker: ['', [Validators.required], [this.validatorService.notFoundOrderMoniker()]],
            order: null,
            bills: this.fb.array([])
        });

        this.sub = this.route.params.subscribe(
            params => {
                let orderId = params['orderId'];
                let customerId = params['customerId'];
                let orderMoniker = params['orderMoniker'];
                let id = params['id'];

                this.getBillAndOrderBills(customerId, orderId, id, orderMoniker);
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.billForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.billForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
    handleChange() {
        if (this.checked) {
            this.moniker.clearValidators()
            this.moniker.setErrors(null);
        }

        else {
            this.moniker.setValidators(Validators.required)
        }
    }
    saveBill(): void {
        if (this.billForm.dirty && this.billForm.valid) {
            let billData = Object.assign({}, this.bill, this.billForm.value);

            if (this.checked)
                billData.moniker = "";

            this.billDataService.saveBill(billData)
                .subscribe((bill) => {
                        this.bill = bill;
                        this.msgs = [];
                        this.msgs.push({
                            severity: 'success',
                            summary: 'Saved SuccessFully',
                            detail: this.bill.moniker
                        });
                        this.onSaveComplete();
                    },
                (error: any) => this.errorMessage = <any>error);

        } else if (!this.billForm.dirty) {
            this.onSaveComplete();
        }
    }

    getBillAndOrderBills(customerId: string, orderId: string, id: string, orderMoniker: string): void {
        if (customerId == undefined && orderId == undefined && id === undefined && orderMoniker === undefined) {
            this.paragraph = `Use this form to add a new bill`;
        } else if (orderMoniker != undefined) {
            this.billForm.get("orderMoniker").setValue(orderMoniker);
            this.billForm.get("orderMoniker").disable();
            this.searchOrders();
        }
        else {
            Observable.forkJoin([this.billDataService.getBill(customerId, orderId, id), this.billDataService.getOrderBills(orderId), this.orderItemDataService.getItems(orderId)])
                .subscribe(results => {
                    this.onBillRetrieved(results[0]);
                    this.onOrderBillsRetrieved(results[1]);
                    this.onOrderItemsRetrieved(results[2]);
                });
        }
    }

    onBillRetrieved(bill: IBill): void {
        if (this.billForm)
            this.billForm.reset();

        this.bill = bill;

        this.billForm.patchValue({
            moniker: this.bill.moniker,
            paidAmount: this.bill.paidAmount,
            paymentMethod: this.bill.paymentMethod,
            billDate: new Date(this.bill.billDate),
            order: this.bill.order,
            orderMoniker: this.bill.order.moniker
        });
    }

    onSaveComplete(): void {
        this.billForm.setControl('bills', this.fb.array([]));
        this.billForm.reset();
        this.router.navigate(["/bill-list"]);
    }

    onOrderBillsRetrieved(orderBills: any) {
        this.billForm.setControl('bills', this.fb.array([]));
        const billsControl = <FormArray>this.billForm.controls['bills'];
        
        _.each(orderBills, (bill => {
            billsControl.push(this.fb.group({
                id: bill.id,
                moniker: bill.moniker,
                billDate: new Date(bill.billDate).toDateString(),
                paidAmount: bill.paidAmount,
                paymentMethod: bill.paymentMethod
            }));
        }));

        this.totalPaidAmount = _.sumBy(orderBills, 'paidAmount');
    }

    onOrderItemsRetrieved(orderItems: any) {
        this.orderTotalAmount = _.sumBy(orderItems, (orderItem) => orderItem.quantity * (orderItem.unitPrice - orderItem.discount));
        this.dueTotalAmount = this.orderTotalAmount - this.totalPaidAmount;
    }
    
    searchOrders() {
        this.orderTotalAmount = 0;
        this.dueTotalAmount = 0;
        this.totalPaidAmount = 0;

        this.orderDataService.SearchByMoniker(this.orderMoniker.value).subscribe(
            (order) => {
                if (!_.isEmpty(order)) {
                    Observable.forkJoin([this.billDataService.getOrderBills(order.id), this.orderItemDataService.getItems(order.id)])
                        .subscribe(results => {
                            this.onOrderBillsRetrieved(results[0]);
                            this.onOrderItemsRetrieved(results[1]);
                        });

                    this.billForm.get("order").setValue(order);
                }
            }, (error: any) => this.errorMessage = <any>error
        );
    }
}