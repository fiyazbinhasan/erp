import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { SelectItem } from '../shared/select-item'
import { IMeasurementUnit } from '../view-models/measurement-unit';
import { MeasurementUnitDataService } from '../services/measurement-unit-data.service';
import { GenericValidator } from '../shared/generic-validator';

@Component({
    selector: 'measurement-unit-maint',
    templateUrl: 'measurement-unit-maint.component.html',
    styleUrls: ['./measurement-unit-maint.component.css']
})

export class MeasurementUnitMaintComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    errorMessage: string;
    public measurementUnitForm: FormGroup;

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    headline: string = 'Edit Measurement Unit';
    paragraph: string = 'Use this form to edit the existing measurement unit';

    measurementUnit: IMeasurementUnit;
    private sub: Subscription;

    constructor(private fb: FormBuilder,
        private measurementUnitDataService: MeasurementUnitDataService,
        private route: ActivatedRoute,
        private router: Router) {
        this.validationMessages = {
            name: {
                required: 'Name is required.'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.measurementUnitForm = this.fb.group({
            name: ['', [Validators.required]],
            remarks: ''
        });


        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getMeasurementUnit(id);
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.measurementUnitForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.measurementUnitForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveMeasurementUnit(): void {
        if (this.measurementUnitForm.dirty && this.measurementUnitForm.valid) {
            let measurementUnit = Object.assign({}, this.measurementUnit, this.measurementUnitForm.value);

            this.measurementUnitDataService.saveMeasurementUnit(measurementUnit)
                .subscribe(() => {
                        this.onSaveComplete();
                    },
                    (error: any) => this.errorMessage = <any>error);

        } else if (!this.measurementUnitForm.dirty) {
            this.onSaveComplete();
        }
    }

    onMeasurementUnitRetrieved(measurementUnit: IMeasurementUnit): void {
        if (this.measurementUnitForm) {
            this.measurementUnitForm.reset();
        }

        this.measurementUnit = measurementUnit;
        // Update the data on the form
        this.measurementUnitForm.setValue({
            name: this.measurementUnit.name,
            remarks: this.measurementUnit.remarks
        });
    }

    getMeasurementUnit(id: string): void {
        if (id === undefined) {
            this.headline = 'Add Measurement Unit';
            this.paragraph = `Use this form to add a new measurement unit`;
        } else {
            this.measurementUnitDataService.getMeasurementUnit(id)
                .subscribe(
                (measurementUnit: IMeasurementUnit) => this.onMeasurementUnitRetrieved(measurementUnit),
                    (error: any) => this.errorMessage = <any>error
                );
        }
    }
    onSaveComplete(): void {
        // Reset the form to clear the flags
        this.measurementUnitForm.reset();
        this.router.navigate(["/measurement-unit-list"]);
    }
}