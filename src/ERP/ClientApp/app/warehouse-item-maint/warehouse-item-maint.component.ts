﻿import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, FormControlName } from '@angular/forms';
import { Message } from 'primeng/primeng';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';

import * as _ from "lodash";

import { IWarehouse } from '../view-models/warehouse'
import { IWarehouseItem } from '../view-models/warehouse-item';
import { WarehouseItemDataService } from '../services/warehouse-item-data.service';
import { GenericValidator } from '../shared/generic-validator';
import Measurementunitsetup = require("../view-models/measurement-unit-setup");
import IMeasurementUnitSetup = Measurementunitsetup.IMeasurementUnitSetup;

@Component({
    selector: 'warehouse-item-maint',
    templateUrl: 'warehouse-item-maint.component.html',
    styleUrls: ['./warehouse-item-maint.component.css']
})
export class WarehouseItemMaintComponent implements OnInit, AfterViewInit {
    warehouseItemForm: FormGroup;
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
    @Input('warehouse') warehouse: any;

    displayMessage: { [key: string]: string } = {};
    messages: Message[];

    get warehouseItems(): FormArray {
        return <FormArray>this.warehouseItemForm.get('warehouseItems');
    }

    constructor(private fb: FormBuilder,
        private warehouseItemDataService: WarehouseItemDataService) { }

    ngOnInit(): void {
        this.warehouseItemForm = this.fb.group({
            warehouseItems: this.fb.array([])
        });

        this.validationMessages = {
            measurementUnit: {
                selection: 'Select a measurement Unit.'
            },
            conversionRatio: {
                selection: 'Provide a conversion ratio.'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);

        this.loadForm();

    }

    loadForm() {
        if (this.warehouseItemForm) {
            this.warehouseItemForm.setControl('warehouseItems', this.fb.array([]));
            this.warehouseItemForm.reset();
        }

        if (this.warehouse.id !== undefined) {
            this.warehouseItemDataService.getItems(this.warehouse.id)
                .subscribe(warehouseItems => {
                    this.onWarehouseItemsRetrieved(warehouseItems);
                });
        }
    }

    onWarehouseItemsRetrieved(warehouseItems: IWarehouseItem[]): void {

        const warehouseItemsControls =
            this.warehouseItemForm.controls['warehouseItems'] as FormArray;

        _.each(warehouseItems,
            (warehouseItem => {
                warehouseItemsControls.push(this.fb.group({
                    id: warehouseItem.id,
                    quantity: warehouseItem.quantity,
                    warehouse: warehouseItem.warehouse,
                    measurementUnitSetup: warehouseItem.measurementUnitSetup,
                    warehouseId: warehouseItem.warehouse.id,
                    measurementUnitSetupId: warehouseItem.measurementUnitSetup.id
                }));
            }));
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.warehouseItemForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.warehouseItemForm);
        });
    }

    saveWarehouseItem(): void {
        let warehouseItemControls = this.warehouseItemForm.get('warehouseItems') as FormArray;

        if (this.warehouseItemForm.dirty) {

            let observables = _.map(warehouseItemControls.getRawValue(),
                warehouseItem => {

                    return this.warehouseItemDataService.saveWarehouseItem(
                        <IWarehouseItem>{
                            id: warehouseItem.id,
                            quantity: warehouseItem.quantity,
                            warehouseId: warehouseItem.warehouse.id,
                            measurementUnitSetupId: warehouseItem.measurementUnitSetup.id

                        });

                });

            Observable
                .forkJoin(observables)
                .subscribe((results) => this.onSaveComplete(results),
                (error: any) => (error: any) => console.log(error));

        } else if (!this.warehouseItemForm.dirty) {
            this.onSaveComplete();
        }
    }

    onSaveComplete(results?: any[]): void {
        this.showMessage('success', 'Successful', 'Warehouse Items saved successfully');
        this.loadForm();
    }

    showMessage(severity, summary, details) {
        this.messages = [];
        this.messages.push({ severity: severity, summary: summary, detail: details });
    }
}