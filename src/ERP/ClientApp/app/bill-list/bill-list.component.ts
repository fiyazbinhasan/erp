﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IBill } from '../view-models/bill'
import { BillDataService } from '../services/bill-data.service';
import { SelectItem } from '../shared/select-item';

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
    moduleId: 'AppModule',
    selector: 'app-bill-list',
    templateUrl: 'bill-list.component.html',
    styleUrls: ['./bill-list.component.css'],
    providers: [ConfirmationService]
})

export class BillListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    bills: Array<IBill>;
    paymentMethodOptions: SelectItem[];
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;

    rangeValues: number[] = [0, 1000000];

    lazyLoadEvent: LazyLoadEvent;

    constructor(private billDataService: BillDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'moniker', header: 'Moniker', filter: true, sortable: true },
            { field: 'order.moniker', header: 'Order', filter: true, sortable: false },
            { field: 'paidAmount', header: 'Paid Amount', filter: false, sortable: true },
            { field: 'paymentMethod', header: 'Payment Method', filter: true, sortable: true },
            { field: 'billDate', header: 'Bill Date', filter: false, sortable: true }
        ];

        this.paymentMethodOptions = [
            { label: 'All', value: null },
            { label: 'Cash', value: 0 },
            { label: 'Card', value: 1 }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }
        this.cols = this.dtCols.slice(0, 4);
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }
        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadBillsLazy(event: LazyLoadEvent) {
        this.lazyLoadEvent = event;
        this.loading = true;

        this.billDataService.getPagedBills(event).subscribe(bills => {
            this.totalRecords = bills['headers'].totalCount;
            this.bills = bills['body'];
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editBill(bill: IBill) {
        this.router.navigate(['/bill-maint', bill.order.customer.id, bill.order.id, bill.id]);
    }

    viewBill(bill: IBill) {
        this.router.navigate(['/bill-view', bill.order.customer.id, bill.order.id, bill.id]);
    }

    navigateToBillAddPage() {
        this.router.navigate(['/bill-maint']);
    }

    deleteBill(bill: IBill): void {
        this.confirmationService.confirm({
            message: 'Confirm delete?',
            accept: () => {
                this.billDataService.deleteBill(bill.order.customer.id, bill.order.id, bill.id)
                    .subscribe(
                    () => this.refreshDataTable(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadBillsLazy(this.lazyLoadEvent);
    }
}