﻿import { Component, OnInit } from '@angular/core';
import { SalesReportDataService } from '../services/sales-report.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'sales-by-item-report',
    templateUrl: 'sales-by-item-report.component.html',
    styleUrls: ['./sales-by-item-report.component.css']
})
export class SalesByItemReportComponent implements OnInit {
    loading: boolean;
    stacked: boolean;
    cols: any[];
    errorMessage: string;
    salesByItems = [];
    salesByItemsCopy = [];
    dateRange: Date[];
    allowZero: any = false;
    totalSoldQuantity: number = 0;
    totalSoldPrice: number = 0;

    constructor(private salesReportDataService: SalesReportDataService) {}
    ngOnInit(): void {
        var date = new Date();
        this.dateRange = [new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)]
        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'soldQuantity', header: 'Soled Quantity' },
            { field: 'totalSoldPrice', header: 'Amount' },
            { field: 'averagePrice', header: 'Average Price' }
        ];
        this.generateReport();
    }

    generateReport() {
        this.totalSoldQuantity = 0;
        this.totalSoldPrice = 0;
        let date = new Date();
        let startDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.dateRange[0]);
        let endDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.dateRange[1]);

        this.salesReportDataService.getSalesByItemReport(startDate, endDate).subscribe(salesByItems => {
            this.salesByItems = _.map(salesByItems, function (salesByItem) {
                return {
                    name: salesByItem.name,
                    soldQuantity: salesByItem.soldQuantity,
                    totalSoldPrice: salesByItem.totalSoldPrice,
                    averagePrice: salesByItem.soldQuantity == 0 ? 0 : salesByItem.totalSoldPrice / salesByItem.soldQuantity
                }
            });

            _.each(salesByItems, (salesByItem) => {
                this.totalSoldQuantity += salesByItem.soldQuantity;
                this.totalSoldPrice += salesByItem.totalSoldPrice;
            });
            this.salesByItemsCopy = this.salesByItems;
            this.toggleList();
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + monthIndex + '-' + day;
    }

    toggleList(): void {
        this.salesByItems = this.salesByItemsCopy;
        if (this.allowZero) {
            this.salesByItems = [];
            this.totalSoldQuantity = 0;
            this.totalSoldPrice = 0;
            _.each(this.salesByItemsCopy, (salesByItem) => {
                if (salesByItem.soldQuantity == 0 &&
                    salesByItem.totalSoldPrice == 0 &&
                    salesByItem.averagePrice == 0) {
                    this.salesByItems.push(salesByItem);
                    this.totalSoldQuantity += salesByItem.soldQuantity;
                    this.totalSoldPrice += salesByItem.totalSoldPrice;
                }
            });
        }
        else if (!this.allowZero && this.allowZero != null) {
            this.salesByItems = [];
            this.totalSoldQuantity = 0;
            this.totalSoldPrice = 0;
            _.each(this.salesByItemsCopy, (salesByItem) => {
                if (salesByItem.soldQuantity > 0 ||
                    salesByItem.totalSoldPrice > 0 ||
                    salesByItem.averagePrice > 0) {
                    this.salesByItems.push(salesByItem);
                    this.totalSoldQuantity += salesByItem.soldQuantity;
                    this.totalSoldPrice += salesByItem.totalSoldPrice;
                }
            });
        }
    }

    generateExcel() {
        var date = new Date();
        var startDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.dateRange[0]);
        var endDate: string = this.formatDate(this.dateRange === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.dateRange[1]);
        this.salesReportDataService.generateExcelSalesByItem(startDate, endDate, this.allowZero)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/xlsx' });
                FileSaver.saveAs(blob, "Sales by item.xlsx");
            }, err => console.error(err));
    }
}