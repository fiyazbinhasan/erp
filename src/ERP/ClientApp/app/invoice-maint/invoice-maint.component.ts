﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { InputSwitchModule, GrowlModule, Message } from 'primeng/primeng';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderDataService } from '../services/order-data.service';
import { OrderItemDataService } from '../services/order-item-data.service';
import { InvoiceDataService } from '../services/invoice-data.service';
import { InvoiceItemDataService } from '../services/invoice-item-data.service';
import { WarehouseDataService } from '../services/warehouse-data.service';
import { WarehouseItemDataService } from '../services/warehouse-item-data.service';
import { MeasurementUnitDataService } from '../services/measurement-unit-data.service';
import { ValidatorService } from '../services/validator.service';

import { IInvoice } from '../view-models/invoice'
import { IInvoiceItem, IInvoiceItems } from '../view-models/invoice-Item';
import { IOrderItem } from '../view-models/order-item'

import { SelectItem } from '../shared/select-item';
import { GenericValidator } from '../shared/generic-validator';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/observable/concat';

import * as _ from "lodash";

function fixTimeOffset(date: Date): Date {
    date.setHours(date.getHours() - date.getTimezoneOffset() / 60);
    return date;
}

@Component({
    moduleId: 'AppModule',
    selector: 'app-invoice-maint',
    templateUrl: 'invoice-maint.component.html',
    styleUrls: ['./invoice-maint.component.css']
})
export class InvoiceMaintComponent implements OnInit {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
    invoiceForm: FormGroup;
    errorMessage: string;
    private validationMessages: { [key: string]: { [key: string]: string } };
    orders: Array<SelectItem> = [];
    invoiceWithItems: IInvoiceItems = <IInvoiceItems>{};
    private genericValidator: GenericValidator;
    paragraph: string = 'Use this form to edit the existing order.';
    invoiceItemsCopy: Array<string> = [];
    invoice: IInvoice = <IInvoice>{};
    warehouses: Array<SelectItem> = [];
    checked: boolean = false;
    msgs: Message[] = [];
    displayMessage: { [key: string]: string } = {};
    private sub: Subscription;

    get invoiceItems(): FormArray {
        return this.invoiceForm.get('invoiceItems') as FormArray;
    }
    get moniker() {
        return this.invoiceForm.get('moniker');
    }

    constructor(private fb: FormBuilder,
        private orderDataService: OrderDataService,
        private orderItemDataService: OrderItemDataService,
        private invoiceDataService: InvoiceDataService,
        private invoiceItemDataService: InvoiceItemDataService,
        private warehouseDataService: WarehouseDataService,
        private warehouseItemDataService: WarehouseItemDataService,
        private measurementUnitDataService: MeasurementUnitDataService,
        private validatorService: ValidatorService,
        private route: ActivatedRoute,
        private router: Router) {

        this.validationMessages = {
            moniker: {
                required: 'Invoice no is required.'
            },
            invoiceDate: {
                required: 'Invoice date is required'
            },
            orderMoniker: {
                required: 'Order is required',
                notFound: 'Order does not exists'
            }
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.invoiceForm = this.fb.group({
            moniker: ['', [Validators.required]],
            invoiceDate: ['', [Validators.required]],
            description: '',
            orderMoniker: ['', [Validators.required], [this.validatorService.notFoundOrderMoniker()]],
            order: [null, [Validators.required]],
            invoiceItems: this.fb.array([])
        });

        this.orderDataService.getOrders().subscribe(
            (orders) => {
                orders.map((order) => {
                    this.orders.push(<SelectItem>{ label: order.moniker, value: order });
                });
                this.warehouseDataService.getWarehouses().subscribe(
                    (warehouses) => {
                        warehouses.map((warehouse) => {
                            this.warehouses.push(<SelectItem>{ label: warehouse.moniker, value: warehouse });
                        });
                        this.sub = this.route.params.subscribe(
                            params => {
                                let customerId = params['customerId'];
                                let orderId = params['orderId'];
                                let orderMoniker = params['orderMoniker'];
                                let id = params['id'];
                                this.getInvoiceWithItems(customerId, orderId, id, orderMoniker);
                            }
                        );
                    });
            }
        );
    }
    handleChange() {
        if (this.checked) {
            this.moniker.clearValidators()
            this.moniker.setErrors(null);
        }

        else {
            this.moniker.setValidators(Validators.required)
        }
    }
    saveInvoice(): void {
        if (this.invoiceForm.dirty && this.invoiceForm.valid) {
            let invoiceWithItems = Object.assign({}, this.invoiceWithItems, this.invoiceForm.getRawValue());
            let invoice = this.extractInvoiceForManipulation(invoiceWithItems);
            let invoiceItems = invoiceWithItems.invoiceItems;

            let invoiceItemsforDeletion = _.differenceWith(this.invoiceItemsCopy, _.map(invoiceItems, 'id'), _.isEqual);

            this.invoiceDataService.saveInvoice(invoice)
                .subscribe((invoice) => {
                    let observables = [];
                    this.invoice = invoice;

                    observables = _.map(invoiceItems, invoiceItem => {
                        return this.invoiceItemDataService.saveInvoiceItem(<IInvoiceItem>{
                            id: invoiceItem.id,
                            quantity: invoiceItem.quantity,
                            unitPrice: invoiceItem.unitPrice,
                            discount: invoiceItem.discount,
                            remarks: invoiceItem.remarks,
                            invoiceId: this.invoice.id,
                            measurementUnitSetupId: invoiceItem.measurementUnitSetupId,
                            warehouseId: invoiceItem.warehouse.id
                        });
                    });

                    if (invoiceItemsforDeletion != null && invoiceItemsforDeletion.length !== 0)
                        _.each(invoiceItemsforDeletion, (invoiceItemId) => {
                            observables.push(this.invoiceItemDataService.deleteInvoiceItem(invoiceItemId));
                        });

                    Observable
                        .concat(...observables)
                        .subscribe(() => {

                        }, (error: any) => this.errorMessage = <any>error);
                    this.msgs = [];
                    this.msgs.push({
                        severity: 'success',
                        summary: 'Saved Successfully',
                        detail: this.invoice.moniker
                    });
                    this.onSaveComplete();
                }, (error: any) => this.errorMessage = <any>error);

        } else if (!this.invoiceForm.dirty) {
            this.onSaveComplete();
        }
    }

    extractInvoiceForManipulation(invoiceWithItems: any): IInvoice {
        return <IInvoice>{
            id: this.invoice.id,
            moniker: this.checked ? "" : invoiceWithItems.moniker,
            invoiceDate: fixTimeOffset(invoiceWithItems.invoiceDate),
            description: invoiceWithItems.description,
            order: invoiceWithItems.order,
            orderId: invoiceWithItems.order.id
        }
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.invoiceForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.invoiceForm);
        });
    }

    addOrderedItemsInList(order) {
        if (order.id !== undefined) {
            this.orderItemDataService.getItems(order.id).subscribe(
                (orderItems) => this.onOrderItemsRetrieved(orderItems)
            );
        }
    }

    onOrderItemsRetrieved(orderItems: IOrderItem[]): void {
        this.invoiceForm.setControl('invoiceItems', this.fb.array([]));
        _.each(orderItems, (orderItem => {
            if (orderItem.leftQuantity !== 0) {
                this.invoiceItems.push(this.fb.group({
                    id: new FormControl(''),
                    unitPrice: new FormControl(orderItem.unitPrice),
                    orderedQuantity: new FormControl(orderItem.quantity),
                    measurementUnit: new FormControl(orderItem.measurementUnitSetup.measurementUnit.name),
                    leftOverQuantity: new FormControl(orderItem.leftQuantity),
                    quantity: [0, Validators.minLength(1)],
                    remarks: new FormControl(''),
                    discount: new FormControl(orderItem.discount),
                    amount: new FormControl(0),
                    warehouse: ['', [Validators.required]],
                    warehouseQuantity: new FormControl(0),
                    baseUnit: new FormControl(null),
                    measurementUnitSetup: new FormControl(orderItem.measurementUnitSetup),
                    measurementUnitSetupId: new FormControl(orderItem.measurementUnitSetup.id),
                    item: new FormControl(orderItem.measurementUnitSetup.item)
                }));
            }
        }));
    }

    getInvoiceWithItems(customerId: string, orderId: string, id: string, orderMoniker: string): void {
        if (customerId === undefined && orderId === undefined && id === undefined && orderMoniker === undefined) {
            this.paragraph = `Use this form to add a new Invoice`;
        }
        else if (orderMoniker != undefined) {
            this.invoiceForm.get("orderMoniker").setValue(orderMoniker);
            this.invoiceForm.get("orderMoniker").disable();
            this.searchOrders();
        }
        else {
            Observable
                .forkJoin([
                    this.invoiceDataService.getInvoice(customerId, orderId, id),
                    this.invoiceItemDataService.getItems(id)
                ])
                .subscribe(results => {
                    this.onInvoiceRetrieved(results[0]);
                    this.onInvoiceItemsRetrieved(results[1]);
                });
        }
    }

    onInvoiceRetrieved(invoice: any) {
        if (this.invoiceForm) {
            this.invoiceForm.reset();
        }
        this.invoice = invoice;

        this.invoiceForm.patchValue({
            moniker: this.invoice.moniker,
            invoiceDate: new Date(this.invoice.invoiceDate),
            description: this.invoice.description,
            order: this.invoice.order,
            orderMoniker: this.invoice.order.moniker
        });
    }

    onInvoiceItemsRetrieved(invoiceItems: any): void {
        this.invoiceItemsCopy = _.map(invoiceItems, 'id');

        _.each(invoiceItems,
            (invoiceItem => {

                var orderItem = _.find(invoiceItem.invoice.order.orderItems,
                    (orderItem => {
                        if (invoiceItem.measurementUnitSetup.itemId === orderItem.measurementUnitSetup.itemId) {
                            return orderItem;
                        }
                        return null;
                    }));
                this.invoiceItems.push(this.fb.group({
                    id: new FormControl(invoiceItem.id),
                    unitPrice: new FormControl(invoiceItem.unitPrice),
                    orderedQuantity: new FormControl(orderItem.quantity),
                    leftOverQuantity: new FormControl(orderItem.leftQuantity + invoiceItem.quantity),
                    measurementUnit: new FormControl(invoiceItem.measurementUnitSetup.measurementUnit.name),
                    quantity: [invoiceItem.quantity, Validators.minLength(1)],
                    remarks: new FormControl(invoiceItem.remarks),
                    discount: new FormControl(invoiceItem.discount),
                    amount: new FormControl((invoiceItem.quantity * invoiceItem.unitPrice) -
                        (invoiceItem.allowDiscount ? invoiceItem.discount : 0)
                    ),
                    warehouse: invoiceItem.warehouse,
                    warehouseId: new FormControl(invoiceItem.warehouseId),
                    warehouseQuantity: new FormControl(0),
                    baseUnit: new FormControl(null),
                    measurementUnitSetup: new FormControl(invoiceItem.measurementUnitSetup),
                    measurementUnitSetupId: new FormControl(invoiceItem.measurementUnitSetup.id),
                    item: new FormControl(invoiceItem.measurementUnitSetup.item),
                    itemId: new FormControl(invoiceItem.measurementUnitSetup.itemId)
                }));
            }));
        _.each(this.invoiceItems,
            (val, i) => {
                this.getWarehouseItemQuantity(i);
            });
    }

    deleteItemFromList(index: number) {
        const arrayControl = <FormArray>this.invoiceForm.controls['invoiceItems'];
        arrayControl.removeAt(index);

        this.invoiceForm.updateValueAndValidity();
        this.invoiceForm.markAsDirty();
    }

    onSaveComplete(): void {
        this.invoiceForm.reset();
        this.invoiceForm.setControl('invoiceItems', this.fb.array([]));
        this.router.navigate(["/invoice-list"]);
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    searchOrders() {
        var orderMoniker = this.invoiceForm.get("orderMoniker").value;
        this.orderDataService.SearchByMoniker(orderMoniker).subscribe(
            (order) => {
                if (!_.isEmpty(order)) {
                    this.orderItemDataService.getItems(order.id).subscribe(
                        (orderItems) => this.onOrderItemsRetrieved(orderItems)
                    );
                    this.invoiceForm.get("order").setValue(order);
                }
            }, (error: any) => { this.errorMessage = <any>error; }
        );
    }

    getWarehouseItemQuantity(index: number) {
        var formArray = this.invoiceForm.get("invoiceItems") as FormArray;
        var itemId = formArray.at(index).get('item').value.id;
        var warehouseId = formArray.at(index).get('warehouse').value.id;
        if (warehouseId !== undefined) {
            this.warehouseItemDataService.getItem(warehouseId, itemId).subscribe(warehouseItem => {
                if (_.isEmpty(warehouseItem)) {
                    formArray.at(index).patchValue({
                        warehouseQuantity: 0,
                        baseUnit: ""
                    });
                } else {
                    formArray.at(index).patchValue({
                        warehouseQuantity: warehouseItem.quantity,
                        baseUnit: "(" + warehouseItem.measurementUnitSetup.measurementUnit.name + ")"
                    });
                }
                this.checkValidQuantity(index);
            }, (error: any) => this.errorMessage = <any>error);
        } else {
            formArray.at(index).patchValue({
                warehouseQuantity: 0,
                baseUnit: ""
            });
        }
    }

    checkValidQuantity(index: number) {
        var formArray = this.invoiceForm.get("invoiceItems") as FormArray;
        switch (true) {
            case formArray.at(index).get('quantity').value > formArray.at(index).get('leftOverQuantity').value:
                this.errorMessage = "Given invoice quantity(" + formArray.at(index).get('quantity').value + ") must be less or equal to LeftoverQuantity(" + formArray.at(index).get('leftOverQuantity').value + ") ";
                formArray.at(index).patchValue({
                    quantity: 0
                });
                break;
            case formArray.at(index).get('quantity').value * (formArray.at(index).get('measurementUnitSetup').value).conversionRatio > formArray.at(index).get('warehouseQuantity').value:
                this.errorMessage = "insufficient quantity in warehouse(" + formArray.at(index).get('warehouseQuantity').value + ")";
                formArray.at(index).patchValue({
                    quantity: 0
                });
                break;
            default:
                this.errorMessage = "";
        }
    }

    calculateRowAmount(index: number) {
        var formArray = this.invoiceForm.get("invoiceItems") as FormArray;

        formArray.at(index).patchValue({
            amount: formArray.at(index).get('quantity').value * (formArray.at(index).get('unitPrice').value - formArray.at(index).get('discount').value)
        });
        this.checkValidQuantity(index);
    }
}