﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { ICustomer } from '../view-models/customer'
import { SelectItem } from '../shared/select-item'
import { CustomerDataService } from '../services/customer-data.service'

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';

import * as _ from "lodash";

@Component({
    selector: 'customer-list',
    templateUrl: 'customer-list.component.html',
    styleUrls: ['./customer-list.component.css'],
    providers: [ConfirmationService]
})

export class CustomerListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];
    
    countries: SelectItem[];
    customers: Array<ICustomer>;
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;

    lazyLoadEvent: LazyLoadEvent;

    constructor(private customerDataService: CustomerDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { header: 'First Name', field: 'firstName',filter:true },
            { header: 'Last Name', field: 'lastName', filter: true },
            { header: 'Company', field: 'companyName', filter: false },
            { header: 'Phone', field: 'phone', filter: true},
            { header: 'Primary Address', field: 'address1', filter: false },
            { header: 'City', field: 'city', filter: true },
            { header: 'Country', field: 'country', filter: true },
            { header: 'Postal Code', field: 'postalCode', filter: false },
            { header: 'Email', field: 'email',filter: true },
            { header: 'Website', field: 'website', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols.slice(0, 5);

        this.countries = this.customerDataService.countries;
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadCustomersLazy(event: LazyLoadEvent) {
        this.lazyLoadEvent = event;
        this.loading = true;

        this.customerDataService.getPagedCustomers(event).subscribe(customers => {
            this.totalRecords = customers['headers'].totalCount;
            this.customers = customers['body'];
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editCustomer(customer: ICustomer) {
        this.router.navigate(['/customer-maint', customer.id]);
    }

    viewCustomer(customer: ICustomer) {
        this.router.navigate(['/customer-view', customer.id]);
    }

    navigateToCustomerAddPage() {
        this.router.navigate(['/customer-maint']);
    }

    deleteCustomer(customer: ICustomer): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.customerDataService.deleteCustomer(customer.id)
                    .subscribe(
                    () => this.refreshDataTable(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadCustomersLazy(this.lazyLoadEvent);
    }
}