import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IInventory } from '../view-models/inventory'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class InventoryDataService {
    constructor(private http: Http) { }

    createInventory(supplierId: string, inventory: IInventory, options: RequestOptions): Observable<any> {
        
        const url = `api/v1.0/suppliers/${supplierId}/inventories`;
        return this.http.post(url, inventory, options)
            .map(this.extractData)
            .do(data => console.log('createInventory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteInventory(supplierId: string, id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/suppliers/${supplierId}/inventories/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteInventory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInventories(): Observable<IInventory[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = 'api/v1.0/suppliers/all/inventories';
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getInventories: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPagedInventories(inventoriesResourceParameters: LazyLoadEvent, supplierPhone: string = "all"): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let pageNumber = (inventoriesResourceParameters.first / inventoriesResourceParameters.rows) + 1;
        let pageSize = inventoriesResourceParameters.rows;

        let url = `api/v1.0/suppliers/all/inventories?pageNumber=${pageNumber}&pageSize=${pageSize}`;
        
        if(inventoriesResourceParameters.sortField !== undefined && inventoriesResourceParameters.sortOrder === -1)
            url = url + `&orderby=${inventoriesResourceParameters.sortField} desc`;
        else if(inventoriesResourceParameters.sortField !== undefined && inventoriesResourceParameters.sortOrder === 1)
            url = url + `&orderby=${inventoriesResourceParameters.sortField}`;

        if (inventoriesResourceParameters.globalFilter !== undefined)
            url = url + `&searchQuery=${inventoriesResourceParameters.globalFilter}`;

        if(inventoriesResourceParameters.filters["moniker"] !== undefined)
            url = url + `&moniker=${inventoriesResourceParameters.filters["moniker"].value}`;
        

        return this.http.get(url, options)
            .map(this.extractPagedData)
            .do(data => console.log('getInventories: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInventory(supplierId: string, id: string): Observable<any> {
        const url = `api/v1.0/suppliers/${supplierId}/inventories/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getInventory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateInventory(supplierId: string, inventory: IInventory,  options: RequestOptions): Observable<any> {
        const url = `api/v1.0/suppliers/${supplierId}/inventories/${inventory.id}`;
        return this.http.put(url, inventory, options)
            .map(() => inventory)
            .do(data => console.log('updateInventory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveInventory(inventory: IInventory): Observable<IInventory> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        
        if (inventory.id === undefined) {
            return this.createInventory(inventory.supplier.id, inventory, options);
        }
        return this.updateInventory(inventory.supplier.id, inventory, options);
    }

    generatePdf(supplierId: string, id: string): any {
        const url = `api/v1.0/suppliers/${supplierId}/inventories/${id}/generateInventoryPdf`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generatePdf: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private extractPagedData(response: Response) {
        let headers = JSON.parse(response.headers.get("x-pagination"));
        let body = response.json();
        return {body: body, headers: headers} || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}