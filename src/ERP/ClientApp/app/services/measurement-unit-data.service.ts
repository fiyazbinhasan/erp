import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { IMeasurementUnit } from '../view-models/measurement-unit'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MeasurementUnitDataService {
    constructor(private http: Http) { }

    createMeasurementUnit(measurementUnit: IMeasurementUnit, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/measurementunits`;
        return this.http.post(url, measurementUnit, options)
            .map(this.extractData)
            .do(data => console.log('createMeasurementUnit: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteMeasurementUnit(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/measurementunits/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteMeasurementUnit: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getMeasurementUnits(): Observable<IMeasurementUnit[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = 'api/v1.0/measurementunits';
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getMeasurementUnits: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getMeasurementUnit(id: string): Observable<any> {
        if (id === "") {
            return Observable.of(this.initializeItem());
        };
        
        const url = `api/v1.0/measurementunits/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getMeasurementUnit: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateMeasurementUnit(measurementUnit: IMeasurementUnit, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/measurementunits/${measurementUnit.id}`;
        return this.http.put(url, measurementUnit, options)
            .map(() => measurementUnit)
            .do(data => console.log('updateMeasurementUnit: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveMeasurementUnit(measurementUnit: IMeasurementUnit): Observable<IMeasurementUnit> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (measurementUnit.id === "" || measurementUnit.id === undefined) {
            return this.createMeasurementUnit(measurementUnit, options);
        }
        return this.updateMeasurementUnit(measurementUnit, options);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    initializeItem(): IMeasurementUnit {
        // Return an initialized object
        return {
            id: "",
            name: null,
            remarks: null
        };
    }
}