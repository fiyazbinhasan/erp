﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { IOrderItem } from '../view-models/order-item'

@Injectable()
export class OrderItemDataService {
    constructor(private http: Http) { }

    getOrderItems(): Observable<IOrderItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        
        const url = `api/v1.0/OrderItemAssociations`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getOrderItmes: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getItems(orderId: string): Observable<IOrderItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });


        const url = `api/v1.0/OrderItemAssociations/${orderId}/items`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItmes: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveOrderItem(orderItem: IOrderItem): Observable<IOrderItem> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (orderItem.id === undefined || orderItem.id === '') {
            return this.createOrderItem(orderItem, options);
        }
        return this.updateOrderItem(orderItem, options);
    }

    createOrderItem(orderItem: IOrderItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/OrderItemAssociations`;
        return this.http.post(url, orderItem, options)
            .map(this.extractData)
            .do(data => console.log('createOrderItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateOrderItem(orderItem: IOrderItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/OrderItemAssociations/${orderItem.id}`;
        return this.http.put(url, orderItem, options)
            .map(() => orderItem)
            .do(data => console.log('updateOrderItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteOrderItem(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/OrderItemAssociations/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteOrderItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}