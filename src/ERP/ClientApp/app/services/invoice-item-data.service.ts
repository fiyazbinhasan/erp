﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { IInvoiceItem } from '../view-models/invoice-item'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class InvoiceItemDataService {
    constructor(private http: Http) { }

    getItems(invoiceId: string): Observable<IInvoiceItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/InvoiceItemAssociations/${invoiceId}/items`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItmes: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInvoiceItems(): Observable<IInvoiceItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/InvoiceItemAssociations`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getInvoiceItmes: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInvoiceItem(id: string): Observable<any> {
        const url = `api/v1.0/InvoiceItemAssociations/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getInvoiceItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    createInvoiceItem(invoiceItem: IInvoiceItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/InvoiceItemAssociations`;
        return this.http.post(url, invoiceItem, options)
            .map(this.extractData)
            .do(data => console.log('createInvoice: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateInvoiceItem(invoiceItem: IInvoiceItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/InvoiceItemAssociations/${invoiceItem.id}`;
        return this.http.put(url, invoiceItem, options)
            .map(() => invoiceItem)
            .do(data => console.log('updateInvoiceItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveInvoiceItem(invoiceItem: IInvoiceItem): Observable<IInvoiceItem> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (invoiceItem.id === undefined || invoiceItem.id === '') {
            return this.createInvoiceItem(invoiceItem, options);
        }
        return this.updateInvoiceItem(invoiceItem, options);
    }

    deleteInvoiceItem(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/InvoiceItemAssociations/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteInvoiceItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}