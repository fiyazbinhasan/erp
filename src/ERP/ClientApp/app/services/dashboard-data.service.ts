﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { IIntroInformation, IProductDetail, ITopSellingItem, IProfitMargin, ISoldQuantityByMonth, ISoldAmountByMonth, IPurchasedQuantityByMonth } from '../view-models/dashboard';

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DashboardDataService {
    constructor(private http: Http) { }

    getIntroInformation(): Observable<IIntroInformation> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/introinformation`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getIntroInformation: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getProductDetails(): Observable<IProductDetail> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/ProductDetails`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getProductDetails: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getTopSellingItems(): Observable<ITopSellingItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/TopSellingItems`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getTopSellingItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getProfitMargin(): Observable<IProfitMargin[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/ProfitMargin`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getProfitMargins: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getSoldQuantityByMonth(): Observable<ISoldQuantityByMonth[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/SoldQuantityByMonth`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getSoldQuantityByMonth: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getSoldAmountByMonth(): Observable<ISoldAmountByMonth[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/SoldAmountByMonth`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getSoldAmountByMonth: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPurchasedQuantityByMonth(): Observable<IPurchasedQuantityByMonth[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/dashboard/PurchasedQuantityByMonth`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getPurchasedQuantityByMonth: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}