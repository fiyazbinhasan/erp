﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class InventoryReportDataService {
    constructor(private http: Http) { }

    getInventorieDetailsReport(startDate: string, endDate: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/inventoryReports/inventoryDetailsReport/${startDate}/${endDate}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getInventorieDetailsReport : ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generateExcelInventoryDetails(startDate: string, endDate: string, listType: any): any {
        const url = `api/v1.0/inventoryReports/inventoryDetailsReport/${startDate}/${endDate}/${listType}/generateExcelInventoryDetails`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generateExcelInventoryDetails: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInventoryValuationSummary(startDate: string, endDate: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/inventoryReports/inventoryValuationSummary/${startDate}/${endDate}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getinventoryValuationSummary : ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generateExcelInventoryValuationSummary(startDate: string, endDate: string, listType: any): any {
        const url = `api/v1.0/inventoryReports/inventoryValuationSummary/${startDate}/${endDate}/${listType}/generateExcelInventoryValuationSummary`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generateExcelInventoryValuationSummary: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}