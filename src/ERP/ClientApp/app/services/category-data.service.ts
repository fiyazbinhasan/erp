import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { ICategory } from '../view-models/category'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CategoryDataService {
    constructor(private http: Http) { }

    createCategory(category: ICategory, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/categories`;
        return this.http.post(url, category, options)
            .map(this.extractData)
            .do(data => console.log('createCategory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteCategory(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/categories/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteCategory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getCategories(): Observable<ICategory[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = 'api/v1.0/categories';
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getCategories: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getCategory(id: string): Observable<any> {
        if (id === "") {
            return Observable.of(this.initializeItem());
        };
        const url = `api/v1.0/categories/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getCategory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateCategory(category: ICategory, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/categories/${category.id}`;
        return this.http.put(url, category, options)
            .map(() => category)
            .do(data => console.log('updateProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveCategory(category: ICategory): Observable<ICategory> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (category.id === "" || category.id === undefined) {
            return this.createCategory(category, options);
        }
        return this.updateCategory(category, options);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    initializeItem(): ICategory {
        // Return an initialized object
        return {
            id: "",
            name: null,
            description: null
        };
    }
}