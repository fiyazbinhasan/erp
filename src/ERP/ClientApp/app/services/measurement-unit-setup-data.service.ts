﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable'
import { EmptyObservable } from 'rxjs/Observable/EmptyObservable';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/defaultIfEmpty';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/take';

import { IMeasurementUnitSetup } from '../view-models/measurement-unit-setup'

@Injectable()
export class MeasurementUnitSetupDataService {
    constructor(private http: Http) { }
    
    getMeasurementUnitSetups(): Observable<IMeasurementUnitSetup[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/measurementUnitSetups`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getMeasurementUnitSetups: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveMeasurementUnitSetup(measurementUnitSetup: IMeasurementUnitSetup): Observable<IMeasurementUnitSetup> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (measurementUnitSetup.id === "" || measurementUnitSetup.id === undefined) {
            return this.createMeasurementUnitSetup(measurementUnitSetup, options);
        }
        return this.updateMeasurementUnitSetup(measurementUnitSetup, options);
    }

    createMeasurementUnitSetup(measurementUnitSetup: IMeasurementUnitSetup, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/measurementUnitSetups`;
        return this.http.post(url, measurementUnitSetup, options)
            .map(this.extractData)
            .do(data => console.log('createMeasurementUnitSetup: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    updateMeasurementUnitSetup(measurementUnitSetup: IMeasurementUnitSetup, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/measurementUnitSetups/${measurementUnitSetup.id}`;
        return this.http.put(url, measurementUnitSetup, options)
            .map(() => measurementUnitSetup)
            .do(data => console.log('updateMeasurementUnitSetup: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteMeasurementUnitSetup(id: string): Observable<any> {
        if (id !== undefined) {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });

            const url = `api/v1.0/measurementUnitSetups/${id}`;
            return this.http.delete(url, options)
                .do(data => console.log('deleteMeasurementUnitSetup: ' + JSON.stringify(data)))
                .catch(this.handleError);
        } else {
            return Observable.empty().defaultIfEmpty(false);
        }
    }

    getMeasurementUnits(itemId: string): Observable<IMeasurementUnitSetup[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/measurementUnitSetups/${itemId}/measurementUnits`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getMeasurementUnits: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getItems(measurementUnitId: string): Observable<IMeasurementUnitSetup[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/measurementUnitSetups/${measurementUnitId}/items`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    baseWiseQuantityConversion(id: string, conversionRatio: number, currentBaseId: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/measurementUnitSetups/${id}/${conversionRatio}/${currentBaseId}`;
        return this.http.put(url, options)
            .map(this.extractData)
            .do(data => console.log('baseWiseQuantityConversion: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}