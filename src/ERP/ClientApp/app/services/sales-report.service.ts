﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SalesReportDataService {
    constructor(private http: Http) { }

    getSalesByItemReport(startDate: string, endDate: string): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/salesReports/salesByItemReport/${startDate}/${endDate}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getSalesbyItemReport : ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generateExcelSalesByItem(startDate: string, endDate: string, allowZero: any): any {
        const url = `api/v1.0/salesReports/salesByItemReport/${startDate}/${endDate}/${allowZero}/generateExcelSalesByItem`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generateExcelSalesByItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getSalesByCustomerReport(startDate: string, endDate: string): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/salesReports/salesByCustomerReport/${startDate}/${endDate}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getSalesByCustomerReport : ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generateExcelSalesByCustomer(startDate: string, endDate: string, allowZero: any): any {
        const url = `api/v1.0/salesReports/salesByCustomerReport/${startDate}/${endDate}/${allowZero}/generateExcelSalesByCustomer`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generateExcelSalesByCustomer: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}