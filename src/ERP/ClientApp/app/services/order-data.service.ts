﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IOrder } from '../view-models/order'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class OrderDataService {
    constructor(private http: Http) { }

    createOrder(customerId: string, order: IOrder, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders`;
        return this.http.post(url, order, options)
            .map(this.extractData)
            .do(data => console.log('createOrder: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }


    getOrders(): Observable<IOrder[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/customers/all/orders`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getOrders: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPagedOrders(ordersResourceParameters: LazyLoadEvent, customerPhone: string = "all"): Observable<IOrder[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
     
        let pageNumber = (ordersResourceParameters.first / ordersResourceParameters.rows) + 1;
        let pageSize = ordersResourceParameters.rows;

        let url = `api/v1.0/customers/${customerPhone}/orders?pageNumber=${pageNumber}&pageSize=${pageSize}`;

        if (ordersResourceParameters.sortField !== undefined && ordersResourceParameters.sortOrder === -1)
            url = url + `&orderby=${ordersResourceParameters.sortField} desc`;
        else if (ordersResourceParameters.sortField !== undefined && ordersResourceParameters.sortOrder === 1)
            url = url + `&orderby=${ordersResourceParameters.sortField}`;

        if (ordersResourceParameters.globalFilter !== undefined)
            url = url + `&searchQuery=${ordersResourceParameters.globalFilter}`;
        if (ordersResourceParameters.filters["moniker"] !== undefined) 
            url = url + `&moniker=${ordersResourceParameters.filters["moniker"].value}`;
        if (ordersResourceParameters.filters["paymentStatus"] !== undefined)
            url = url + `&paymentStatus=${ordersResourceParameters.filters["paymentStatus"].value}`;
        if (ordersResourceParameters.filters["orderStatus"] !== undefined)
            url = url + `&orderStatus=${ordersResourceParameters.filters["orderStatus"].value}`;

        return this.http.get(url, options)
            .map(this.extractPagedData)
            .do(data => console.log('getOrders: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getOrder(customerId: string, id: string): Observable<IOrder> {
        const url = `api/v1.0/customers/${customerId}/orders/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getOrder: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateOrder(customerId: string, order: IOrder, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders/${order.id}`;
        return this.http.put(url, order, options)
            .map(() => order)
            .do(data => console.log('updateOrder: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveOrder(order: IOrder): Observable<IOrder> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (order.id === undefined) {
            return this.createOrder(order.customer.id, order, options);
        }
        return this.updateOrder(order.customer.id, order, options);
    }

    deleteOrder(customerId: string, id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/customers/${customerId}/orders/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteOrder: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    SearchByMoniker(moniker: string): Observable<IOrder> {
        const url = `api/v1.0/orders/${moniker}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getOrder: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generatePdf(customerId: string, id: string): any {
        const url = `api/v1.0/customers/${customerId}/orders/${id}/generateOrderPdf`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generatePdf: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractPagedData(response: Response) {
        let headers = JSON.parse(response.headers.get("x-pagination"));
        let body = response.json();
        return { body: body, headers: headers } || {};
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}