﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PurchaseReportDataService {
    constructor(private http: Http) { }

    getPurchaseBySupplier(startDate: string, endDate: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/purchaseReports/purchaseBySupplier/${startDate}/${endDate}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getPurchaseBySupplier : ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generateExcelPurchaseBySupplier(startDate: string, endDate: string, listType: any): any {
        const url = `api/v1.0/purchaseReports/purchaseBySupplier/${startDate}/${endDate}/${listType}/generateExcelPurchaseBySupplier`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generateExcelPurchaseBySupplier: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPurchaseByItem(startDate: string, endDate: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/purchaseReports/purchaseByItem/${startDate}/${endDate}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getPurchaseByItem : ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generateExcelPurchaseByItem(startDate: string, endDate: string, listType: any): any {
        const url = `api/v1.0/purchaseReports/purchaseByItem/${startDate}/${endDate}/${listType}/generateExcelPurchaseByItem`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generateExcelPurchaseByItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}