﻿import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, ValidatorFn, Validators } from '@angular/forms';
import { Observable } from 'rxjs'
import { OrderDataService } from './order-data.service'
import 'rxjs/add/operator/map';

import * as _ from "lodash";

function isEmptyInputValue(value: any): boolean {
    // we don't check for string here so it also works with arrays
    return value == null || value.length === 0;
}

@Injectable()
export class ValidatorService {
    constructor(private orderDataService: OrderDataService) {

    }

    notFoundOrderMoniker(): AsyncValidatorFn {
        return (control: AbstractControl): Promise<{ [key: string]: any } | null> | Observable<{ [key: string]: any } | null> => {
            if (isEmptyInputValue(control.value)) {
                return Observable.of(null);
            } else if (control.value.indexOf("_") !== -1) {
                /* The check for dashes is only here because we used this character as a mask character */
                return Observable.of({ 'notFound': { value: control.value } });
            }
            else {
                return this.orderDataService.SearchByMoniker(control.value).map(order => {
                    return _.isEmpty(order) ? { 'notFound': { value: control.value } } : null;
                });
            }
        };
    }
}
