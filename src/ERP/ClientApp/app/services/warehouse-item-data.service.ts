﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IItem } from '../view-models/Item'
import { IWarehouseItem } from '../view-models/warehouse-item'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WarehouseItemDataService {
    constructor(private http: Http) { }

    getItems(warehouseId: string): Observable<IWarehouseItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/WarehouseItemAssociations/${warehouseId}/items`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getItem(warehouseId: string, itemId: string): Observable<IWarehouseItem> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
    
        const url = `api/v1.0/WarehouseItemAssociations/${warehouseId}/items/${itemId}`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getWarehouseItems(): Observable<IWarehouseItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/WarehouseItemAssociations`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getWarehouseItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getWarehouseItem(id: string): Observable<any> {
        const url = `api/v1.0/WarehouseItemAssociations/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getWarehouseItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    createWarehouseItem(warehouseItem: IWarehouseItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/WarehouseItemAssociations`;
        return this.http.post(url, warehouseItem, options)
            .map(this.extractData)
            .do(data => console.log('createProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateWarehouseItem(warehouseItem: IWarehouseItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/WarehouseItemAssociations/${warehouseItem.id}`;
        return this.http.put(url, warehouseItem, options)
            .map(() => warehouseItem)
            .do(data => console.log('updateWarehouseItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveWarehouseItem(warehouseItem: IWarehouseItem): Observable<IWarehouseItem> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (warehouseItem.id === undefined || warehouseItem.id === '') {
            return this.createWarehouseItem(warehouseItem, options);
        }
        return this.updateWarehouseItem(warehouseItem, options);
    }

    deleteWarehouseItem(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/WarehouseItemAssociations/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteWarehouseItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}