﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IWarehouse } from '../view-models/warehouse'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WarehouseDataService {
   
    constructor(private http: Http) { }

    createWarehouse(warehouse: IWarehouse, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/warehouses`;
        return this.http.post(url, warehouse, options)
            .map(this.extractData)
            .do(data => console.log('createWarehouse: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteWarehouse(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/warehouses/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteWarehouse: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getWarehouses(): Observable<IWarehouse[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = 'api/v1.0/warehouses';
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getWarehouses: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPagedWarehouses(warehousesResourceParameters: LazyLoadEvent): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let pageNumber = (warehousesResourceParameters.first / warehousesResourceParameters.rows) + 1;
        let pageSize = warehousesResourceParameters.rows;

        let url = `api/v1.0/warehouses?pageNumber=${pageNumber}&pageSize=${pageSize}`;

        if (warehousesResourceParameters.sortField !== undefined && warehousesResourceParameters.sortOrder === -1)
            url = url + `&orderby=${warehousesResourceParameters.sortField} desc`;
        else if (warehousesResourceParameters.sortField !== undefined && warehousesResourceParameters.sortOrder === 1)
            url = url + `&orderby=${warehousesResourceParameters.sortField}`;

        if (warehousesResourceParameters.globalFilter !== undefined)
            url = url + `&searchQuery=${warehousesResourceParameters.globalFilter}`;
        if (warehousesResourceParameters.filters["moniker"] !== undefined)
            url = url + `&moniker=${warehousesResourceParameters.filters["moniker"].value}`;

        return this.http.get(url, options)
            .map(this.extractPagedData)
            .do(data => console.log('getWarehouses: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getWarehouse(id: string): Observable<any> {
        const url = `api/v1.0/warehouses/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getWarehouse: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateWarehouse(warehouse: IWarehouse,  options: RequestOptions): Observable<any> {
        const url = `api/v1.0/warehouses/${warehouse.id}`;
        return this.http.put(url, warehouse, options)
            .map(() => warehouse)
            .do(data => console.log('updateWarehouse: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveWarehouse(warehouse: IWarehouse): Observable<IWarehouse> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        if (warehouse.id === undefined) {
            return this.createWarehouse(warehouse, options);
        }
        return this.updateWarehouse(warehouse, options);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private extractPagedData(response: Response) {
        let headers = JSON.parse(response.headers.get("x-pagination"));
        let body = response.json();
        return {body: body, headers: headers} || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}