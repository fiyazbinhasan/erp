﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IBill } from '../view-models/bill';

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class BillDataService {
    constructor(private http: Http) { }

    getBills(customerId: string, orderId: string = "all"): Observable<IBill[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/Bills`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getBills: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPagedBills(billsResourceParameters: LazyLoadEvent, customerPhone: string = "all", orderMoniker: string = "all"): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let pageNumber = (billsResourceParameters.first / billsResourceParameters.rows) + 1;
        let pageSize = billsResourceParameters.rows;

        let url = `api/v1.0/customers/${customerPhone}/orders/${orderMoniker}/bills?pageNumber=${pageNumber}&pageSize=${pageSize}`;

        if (billsResourceParameters.sortField !== undefined && billsResourceParameters.sortOrder === -1)
            url = url + `&orderby=${billsResourceParameters.sortField} desc`;
        else if (billsResourceParameters.sortField !== undefined && billsResourceParameters.sortOrder === 1)
            url = url + `&orderby=${billsResourceParameters.sortField}`;

        if (billsResourceParameters.globalFilter !== undefined)
            url = url + `&searchQuery=${billsResourceParameters.globalFilter}`;
        if (billsResourceParameters.filters["moniker"] !== undefined)
            url = url + `&moniker=${billsResourceParameters.filters["moniker"].value}`;
        if (billsResourceParameters.filters["paidAmount"] !== undefined)
            url = url + `&paidAmountRanges=${billsResourceParameters.filters["paidAmount"].value}`;
        if (billsResourceParameters.filters["paymentMethod"] !== undefined) 
            url = url + `&paymentMethod=${billsResourceParameters.filters["paymentMethod"].value}`;
        if (billsResourceParameters.filters["order.moniker"] !== undefined)
            url = url + `&orderNo=${billsResourceParameters.filters["order.moniker"].value}`;

       
        return this.http.get(url, options)
            .map(this.extractPagedData)
            .do(data => console.log('getBills: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getBill(customerId: string, orderId: string, id: string): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/Bills/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getBill: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    searchBills(searchQuery: string): Observable<IBill[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let url = `api/v1.0/customers/all/orders/all/Bills?searchQuery=${searchQuery}` //check Ratin

        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getBills: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    createBill(customerId: string, orderId: string, bill: IBill, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/bills`;
        return this.http.post(url, bill, options)
            .map(this.extractData)
            .do(data => console.log('createBill: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateBill(customerId: string, orderId: string, bill: IBill, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/bills/${bill.id}`;
        return this.http.put(url, bill, options)
            .map(() => bill)
            .do(data => console.log('updateBill: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveBill(bill: IBill): Observable<IBill> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        if (bill.id === undefined) {
            return this.createBill(bill.order.customer.id, bill.order.id, bill, options);
        }
        return this.updateBill(bill.order.customer.id, bill.order.id, bill, options);
    }

    deleteBill(customerId: string, orderId: string,  id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/bills/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteBill: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getOrderBills(orderId: string): Observable<IBill[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/orders/${orderId}/Bills`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getBills: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generatePdf(customerId: string, orderId: string, id: string): any {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/bills/${id}/generateBillPdf`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generatePdf: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private extractPagedData(response: Response) {
        let headers = JSON.parse(response.headers.get("x-pagination"));
        let body = response.json();
        return { body: body, headers: headers } || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}