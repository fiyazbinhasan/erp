import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IItem } from '../view-models/Item'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ItemDataService {
    constructor(private http: Http) { }

    getItems(): Observable<IItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = 'api/v1.0/categories/all/items';
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPagedItems(itemsResourceParameters: LazyLoadEvent, categoryName: string = "all"): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let pageNumber = (itemsResourceParameters.first / itemsResourceParameters.rows) + 1;
        let pageSize = itemsResourceParameters.rows;

        let url = `api/v1.0/categories/${categoryName}/items?pageNumber=${pageNumber}&pageSize=${pageSize}`;

        if (itemsResourceParameters.sortField !== undefined && itemsResourceParameters.sortOrder === -1)
            url = url + `&orderby=${itemsResourceParameters.sortField} desc`;
        else if (itemsResourceParameters.sortField !== undefined && itemsResourceParameters.sortOrder === 1)
            url = url + `&orderby=${itemsResourceParameters.sortField}`;

        if (itemsResourceParameters.globalFilter !== undefined && itemsResourceParameters.globalFilter !== null)
            url = url + `&searchQuery=${itemsResourceParameters.globalFilter}`;
        if (itemsResourceParameters.filters["name"] !== undefined)
            url = url + `&name=${itemsResourceParameters.filters["name"].value}`;
        if (itemsResourceParameters.filters["category.name"] !== undefined)
            url = url + `&category=${itemsResourceParameters.filters["category.name"].value}`;

        return this.http.get(url, options)
            .map(this.extractPagedData)
            .do(data => console.log('getItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getItem(categoryId: string, id: string): Observable<any> {
        const url = `api/v1.0/categories/${categoryId}/items/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    searchItems(searchQuery: string): Observable<IItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let url = `api/v1.0/categories/all/items?searchQuery=${searchQuery}`;

        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItems: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    createItem(categoryId: string, item: IItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/categories/${categoryId}/items`;
        return this.http.post(url, item, options)
            .map(this.extractData)
            .do(data => console.log('createProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateItem(categoryId: string, item: IItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/categories/${categoryId}/items/${item.id}`;
        return this.http.put(url, item, options)
            .map(() => item)
            .do(data => console.log('updateProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveItem(item: IItem): Observable<IItem> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        if (item.id === undefined) {
            return this.createItem(item.category.id, item, options);
        }
        return this.updateItem(item.category.id, item, options);
    }

    deleteItem(categoryId: string, id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/categories/${categoryId}/items/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteProduct: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getItemDemandGraph(categoryId: string, id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/categories/${categoryId}/items/${id}/ItemDemandGraph`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItemDemandGraph: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private extractPagedData(response: Response) {
        let headers = JSON.parse(response.headers.get("x-pagination"));
        let body = response.json();
        return { body: body, headers: headers } || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

   

}