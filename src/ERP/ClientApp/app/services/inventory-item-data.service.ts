import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {LazyLoadEvent} from 'primeng/primeng';

import { IItem } from '../view-models/Item'
import { IInventoryItem } from '../view-models/inventory-item'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class InventoryItemDataService {
    constructor(private http: Http) { }

    getItems(inventoryId: string): Observable<IInventoryItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/InventoryItemAssociations/${inventoryId}/items`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getItmes: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInventoryItems(): Observable<IInventoryItem[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/InventoryItemAssociations`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getInventoryItmes: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInventoryItem(id: string): Observable<any> {
        const url = `api/v1.0/InventoryItemAssociations/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getInventoryItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    createInventoryItem(inventoryItem: IInventoryItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/InventoryItemAssociations`;
        return this.http.post(url, inventoryItem, options)
            .map(this.extractData)
            .do(data => console.log('createInventory: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateInventoryItem(inventoryItem: IInventoryItem, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/InventoryItemAssociations/${inventoryItem.id}`;
        return this.http.put(url, inventoryItem, options)
            .map(() => inventoryItem)
            .do(data => console.log('updateInventoryItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveInventoryItem(inventoryItem: IInventoryItem): Observable<IInventoryItem> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        if (inventoryItem.id === undefined || inventoryItem.id === '') {
            return this.createInventoryItem(inventoryItem, options);
        }
        return this.updateInventoryItem(inventoryItem, options);
    }

    deleteInventoryItem(id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/InventoryItemAssociations/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteInventoryItem: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }
    
    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}