﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { LazyLoadEvent } from 'primeng/primeng';

import { IInvoice } from '../view-models/invoice'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class InvoiceDataService {
    constructor(private http: Http) { }

    createInvoice(customerId: string, orderId: string, invoice: IInvoice, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/invoices`;
        return this.http.post(url, invoice, options)
            .map(this.extractData)
            .do(data => console.log('createInvoice: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInvoices(): Observable<IInvoice[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        const url = `api/v1.0/customers/all/orders/all/invoices`;
        return this.http.get(url, options)
            .map(this.extractData)
            .do(data => console.log('getInvoices: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPagedInvoices(invoiceResourceParameters: LazyLoadEvent, customerPhone: string = "all"): Observable<any[]> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        let pageNumber = (invoiceResourceParameters.first / invoiceResourceParameters.rows) + 1;
        let pageSize = invoiceResourceParameters.rows;

        let url = `api/v1.0/customers/${customerPhone}/orders/all/invoices?pageNumber=${pageNumber}&pageSize=${pageSize}`;

        if (invoiceResourceParameters.sortField !== undefined && invoiceResourceParameters.sortOrder === -1)
            url = url + `&orderby=${invoiceResourceParameters.sortField} desc`;
        else if (invoiceResourceParameters.sortField !== undefined && invoiceResourceParameters.sortOrder === 1)
            url = url + `&orderby=${invoiceResourceParameters.sortField}`;

        if (invoiceResourceParameters.globalFilter !== undefined)
            url = url + `&searchQuery=${invoiceResourceParameters.globalFilter}`;
        if (invoiceResourceParameters.filters["moniker"] !== undefined)
            url = url + `&moniker=${invoiceResourceParameters.filters["moniker"].value}`;

        return this.http.get(url, options)
            .map(this.extractPagedData)
            .do(data => console.log('getInvoices: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInvoice(customerId: string, orderId: string, id: string): Observable<IInvoice> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/invoices/${id}`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getInvoice: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getOrderInvoices(customerId: string, orderId: string): Observable<IInvoice[]> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/invoices/all`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getInvoice: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    updateInvoice(customerId: string, orderId: string, invoice: IInvoice, options: RequestOptions): Observable<any> {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/invoices/${invoice.id}`;
        return this.http.put(url, invoice, options)
            .map(() => invoice)
            .do(data => console.log('updateInvoice: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveInvoice(invoice: IInvoice): Observable<IInvoice> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        if (invoice.id === undefined) {
            return this.createInvoice(invoice.order.customer.id, invoice.order.id, invoice, options);
        }
        return this.updateInvoice(invoice.order.customer.id, invoice.order.id, invoice, options);
    }

    deleteInvoice(customerId: string, orderId: string, id: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/invoices/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteInvoice: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    generatePdf(customerId: string, orderId: string, id: string): any {
        const url = `api/v1.0/customers/${customerId}/orders/${orderId}/invoices/${id}/generateInvoicePdf`;
        return this.http.get(url, { responseType: ResponseContentType.Blob })
            .map(this.extractData)
            .do(data => console.log('generatePdf: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractPagedData(response: Response) {
        let headers = JSON.parse(response.headers.get("x-pagination"));
        let body = response.json();
        return { body: body, headers: headers } || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }
}