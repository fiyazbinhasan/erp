﻿import { IItem } from './item';
import { IMeasurementUnit } from './measurement-unit';


export interface IMeasurementUnitSetup {
    id: string;
    item: IItem;
    itemId: string;
    measurementUnit: IMeasurementUnit;
    measurementUnitId: string;
    isBase: boolean;
    conversionRatio: number;
    sellingPrice: number;
    discountPrice: number;
    allowDiscount: boolean;
    isUsed:boolean;

}

export interface IItemMeasurementUnitSetups {
    item: IItem;
    itemMeasurementUnitSetups: IMeasurementUnitSetup[];
}