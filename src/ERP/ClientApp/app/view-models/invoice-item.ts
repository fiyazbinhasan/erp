﻿import { IItem } from './item'
import { IInvoice } from './invoice'
import { IWarehouse } from './warehouse'
import { IMeasurementUnitSetup } from './measurement-unit-setup'

export interface IInvoiceItem {
    id: string;
    quantity: number;
    unitPrice: number;
    discount: number;
    remarks: string;
    invoice: IInvoice;
    invoiceId: string;
    warehouse: IWarehouse;
    warehouseId: string;
    measurementUnitSetup: IMeasurementUnitSetup;
    measurementUnitSetupId: string;
    warehouseQuantity: number;
    baseUnit : string;
}

export interface IInvoiceItems {
    invoice: IInvoice;
    invoiceItems: IInvoiceItem[];
}