export interface IMeasurementUnit {
    id: string;
    name: string;
    remarks: string;
}