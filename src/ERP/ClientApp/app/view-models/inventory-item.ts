import { IInventory } from './inventory';
import { IMeasurementUnitSetup } from './measurement-unit-setup';
import { IWarehouse } from './warehouse';

export interface IInventoryItem {
    id: string;
    issuedQuantity: number;
    costPerUnit: number;
    amount: number;
    remarks: string;

    inventory: IInventory;
    inventoryId: string;

    measurementUnitSetup: IMeasurementUnitSetup;
    measurementUnitSetupId: string;

    warehouse: IWarehouse;
    warehouseId: string;
    warehouseQuantity: number;
    baseUnit: string;
}

export interface IInventoryItems{
    inventory: IInventory;
    inventoryItems: IInventoryItem[];
}