﻿import { IMeasurementUnitSetup } from './measurement-unit-setup';
import { IWarehouse } from './warehouse';

export interface IWarehouseItem {
    id: string;
    quantity: number;
    warehouse: IWarehouse;
    measurementUnitSetup: IMeasurementUnitSetup;
    warehouseId: string;
    measurementUnitSetupId: string;
}

export interface IWarehouseItems {
    warehouse: IWarehouse;
    warehouseItems: IWarehouseItem[];
}