﻿import { IItem } from "./item";
import {IMeasurementUnit} from "./measurement-unit"

export interface IIntroInformation {
    warehouseCount?: number;
    supplierCount?: number;
    inventoryCount?: number;
    orderCount?: number;
    pendingOrders?: number;
    partialDeliveredOrders?: number;
    invoiceCount?: number;
    billCount?: number;
    unpaidOrders?: number;
    partialPaidOrders?: number;
    customerCount?: number;
    itemCount?:number;
}

export interface ITopSellingItem {
    item: IItem;
    totalOrdered: number;
    baseUnit: IMeasurementUnit;
}

export interface IProductDetail {
    lowStockItems?: number;
    itemCount?: number;
    categoryCount?: number;
}

export interface IProfitMargin {
    month?: string;
    purchasedAmount?: number;
    soldAmount?:number;
}

export interface ISoldQuantityByMonth {
    month?: string;
    quantity?: number;
}

export interface ISoldAmountByMonth {
    month?: string;
    amount?: number;
}

export interface IPurchasedQuantityByMonth {
    month?: string;
    quantity?: number;
}