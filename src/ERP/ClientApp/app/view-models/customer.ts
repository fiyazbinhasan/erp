﻿export interface ICustomer {
    id: string;
    firstName: string;
    lastName: string;
    companyName: string;
    phone: string;
    alternativePhone: string;
    email: string;
    address1: string;
    address2: string;
    city: string;
    country: string;
    postalCode: string;
    website: string;
    phoneWithName: string;
}