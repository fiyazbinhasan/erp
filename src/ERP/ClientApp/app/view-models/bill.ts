﻿import { IOrder } from "./order"

export interface IBill {
    id: string;
    moniker: string;
    paidAmount: number;
    paymentMethod: number;
    billDate?: Date;
    order: IOrder;
}