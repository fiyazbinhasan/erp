﻿import { IItem } from './item';
import { IOrder } from './order';
import { IMeasurementUnitSetup } from './measurement-unit-setup';

export interface IOrderItem {
    id: string;
    quantity: number;
    leftQuantity: number;
    unitPrice: number;
    discount: number;
    remarks: string;
    amount: number;
    order: IOrder;
    measurementUnitSetup: IMeasurementUnitSetup;
    orderId: string;
    measurementUnitSetupId: string;
}

export interface IOrderItems {
    order: IOrder;
    orderItems: IOrderItem[];
}