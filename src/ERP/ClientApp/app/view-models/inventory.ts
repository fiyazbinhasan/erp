import { ISupplier } from './supplier'

export interface IInventory {
    id: string;
    moniker: string;
    priorityLevel: number;
    purchaseDate?: Date;
    description: string;
    supplier?: ISupplier;
}