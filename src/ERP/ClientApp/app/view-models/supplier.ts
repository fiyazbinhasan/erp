export interface ISupplier {
    id: string;
    firstName: string;
    lastName: string;
    phone: string;
    alternativePhone: string;
    address1: string;
    address2: string;
    city: string;
    country: string;
    postalCode: string;
    email: string;
    website: string;
    companyName: string;
}
