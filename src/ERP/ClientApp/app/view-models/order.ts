﻿import { ICustomer } from './customer';
import { IOrderItem } from './order-item';

export interface IOrder {
    id: string;
    moniker: string;
    orderDate?: Date;
    expectedDeliveryDate?: Date;
    paymentStatus?: string;
    orderStatus?: string;
    customer?: ICustomer;

    orderItems?: IOrderItem[];
}