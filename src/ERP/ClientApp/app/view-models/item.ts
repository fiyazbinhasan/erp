﻿import { ICategory } from "./category";

export interface IItem {
    id: string;
    name: string;
    description: string;
    category: ICategory;
}
