﻿export interface IWarehouse {
    id: string;
    moniker: string;
    address: string;
}