﻿import { IOrder } from './order';
import { IInvoiceItem } from './invoice-item';

export interface IInvoice {
    id: string;
    moniker: string;
    invoiceDate: Date;
    description: string;
    order?: IOrder;
}
