import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { IMeasurementUnit } from '../view-models/measurement-unit'
import {  MeasurementUnitDataService } from '../services/measurement-unit-data.service'
import { SelectItem} from '../shared/select-item'
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'measurement-unit-list',
    templateUrl: 'measurement-unit-list.component.html',
    styleUrls: ['./measurement-unit-list.component.css'],
    providers: [ConfirmationService]
})

export class MeasurementUnitListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    measurementUnits: Array<IMeasurementUnit>;
    loading: boolean;
    stacked: boolean;

    constructor(private measurementUnitDataService: MeasurementUnitDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { header: 'Name', field: 'name', filter: true },
            { header: 'Remarks', field: 'remarks', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols.slice(0, 5);

        this.loadMeasurementUnitsLazy();
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadMeasurementUnitsLazy() {
        this.measurementUnitDataService.getMeasurementUnits().subscribe(measurementUnits => {
            this.measurementUnits = measurementUnits;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editMeasurementUnit(measurementUnit: IMeasurementUnit) {
        this.router.navigate(['/measurement-unit-maint', measurementUnit.id]);
    }

    navigateToMeasurementUnitAddPage() {
        this.router.navigate(['/measurement-unit-maint']);
    }

    deleteMeasurementUnit(measurementUnit: IMeasurementUnit): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.measurementUnitDataService.deleteMeasurementUnit(measurementUnit.id)
                    .subscribe(
                    () => this.refreshDataTable(),
                        (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadMeasurementUnitsLazy();
    }

    viewMesurementUnit(measurementUnit: IMeasurementUnit) {
        this.router.navigate(['/measurement-unit-view', measurementUnit.id]);
    }
}