﻿import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';

import { PurchaseReportDataService } from '../services/purchase-report.service';
import { SelectItem } from '../shared/select-item';

import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-purchase-by-item-report',
    templateUrl: 'purchase-by-item-report.component.html'
})

export class PurchaseByItemReportComponent implements OnInit {
    errorMessage: string;
    purchaseByItems: Array<any>;
    dtCols: any[];
    columnOptions: SelectItem[];
    loading: boolean;
    totalRecords: number = 0;
    rangeDates: Date[];
    totalQuantityPurchased: number = 0;
    totalAmount: number = 0;
    fullRecordDetails = [];
    listType: any = false;

    constructor(private fb: FormBuilder,
        private purchaseReportDataService: PurchaseReportDataService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'name', header: 'Item' },
            { field: 'purchasedQuantity', header: 'Quantity Purchased' },
            { field: 'amount', header: 'Amount' },
            { field: 'averageUnitPrice', header: 'Average Price' }
        ];
        this.loadReport();

        var date = new Date();
        this.rangeDates = [new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)];
    }

    loadReport() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);

        this.purchaseReportDataService.getPurchaseByItem(startDate, endDate).subscribe(result => {
            _.each(result, function (item) {
                item.averageUnitPrice = (parseFloat(item.amount) / parseFloat(item.purchasedQuantity));
                item.averageUnitPrice = isNaN(item.averageUnitPrice) ? 0 : parseFloat(item.averageUnitPrice).toFixed(2);
            });
            this.purchaseByItems = result;
            this.fullRecordDetails = result;
            this.toggleList();
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    toggleList(): void {
        this.purchaseByItems = this.fullRecordDetails;
        if (this.listType) {
            this.totalQuantityPurchased = 0;
            this.totalAmount = 0;
            this.purchaseByItems = [];
            _.each(this.fullRecordDetails, result => {
                if (result.purchasedQuantity == 0 &&
                    result.amount == 0) {
                    this.purchaseByItems.push(result);
                }
            });
        }
        else if (!this.listType && this.listType != null) {
            this.totalQuantityPurchased = 0;
            this.totalAmount = 0;
            this.purchaseByItems = [];
            _.each(this.fullRecordDetails, result => {
                if (result.purchasedQuantity > 0 ||
                    result.amount > 0) {
                    this.purchaseByItems.push(result);
                    this.totalQuantityPurchased += result.purchasedQuantity;
                    this.totalAmount += result.amount;
                }
            });
        }
    }

    formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + '-' + monthIndex + '-' + day;
    }

    generateExcel() {
        var date = new Date();
        var startDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth(), 1) : this.rangeDates[0]);
        var endDate: string = this.formatDate(this.rangeDates === undefined ? new Date(date.getFullYear(), date.getMonth() + 1, 0) : this.rangeDates[1]);
        this.purchaseReportDataService.generateExcelPurchaseByItem(startDate, endDate, this.listType)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/xlsx' });
                FileSaver.saveAs(blob, "Purchase by item.xlsx");
            }, err => console.error(err));
    }
}