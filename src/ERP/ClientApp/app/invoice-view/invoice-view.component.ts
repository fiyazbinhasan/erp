﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { InvoiceDataService } from '../services/invoice-data.service';
import { InvoiceItemDataService } from '../services/invoice-item-data.service';

import { IInvoice } from '../view-models/invoice';

import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-invoice-view',
    templateUrl: 'invoice-view.component.html',
    styleUrls: ['./invoice-view.component.css']
})

export class InvoiceViewComponent implements OnInit {
    invoice: any = {
        id: '',
        moniker: '',
        invoiceDate: '',
        order: {
            moniker: '',
            customer: {
                id: '',
                firstName: '',
                lastName: '',
                address1: '',
                phone: '',
                email: ''
            }
        }
    };
    invoiceItems: Array<any>;
    dtCols: any[];
    totalAmount: number = 0;
    loading: boolean;

    constructor(private invoiceDataService: InvoiceDataService,
        private invoiceItemDataService: InvoiceItemDataService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'measurementUnitSetup.item.name', header: 'Item' },
            { field: 'quantity', header: 'Quantity' },
            { field: 'measurementUnitSetup.measurementUnit.name', header: 'M.Unit' },
            { field: 'unitPrice', header: 'U.Price' },
            { field: 'discount', header: 'Discount' },
            { field: 'amount', header: 'Amount' },
            { field: 'remarks', header: 'Remarks' }
        ];

        this.route.params.subscribe(
            params => {
                let customerId = params['customerId'];
                let orderId = params['orderId'];
                let id = params['id'];
                this.getInvoiceWithItems(customerId, orderId, id);
            }
        );
    }

    getInvoiceWithItems(customerId: string, orderId: string, id: string): void {
        Observable
            .forkJoin([
                this.invoiceDataService.getInvoice(customerId, orderId, id),
                this.invoiceItemDataService.getItems(id)
            ])
            .subscribe(results => {
                this.onInvoiceRetrieved(results[0]);
                this.onInvoiceItemsRetrieved(results[1]);
            });
    }

    onInvoiceRetrieved(invoice: any) {
        this.invoice = invoice;
        this.invoice.invoiceDate = new Date(invoice.invoiceDate).toDateString();
    }

    onInvoiceItemsRetrieved(invoiceItems: any): void {
        this.loading = true;
        this.invoiceItems = invoiceItems;
        _.each(invoiceItems, invoiceItem => {
            invoiceItem.amount = invoiceItem.quantity * (invoiceItem.unitPrice - invoiceItem.discount);
            this.totalAmount += invoiceItem.amount;
        });
        this.loading = false;
    }

    generatePdf() {
        this.invoiceDataService.generatePdf(this.invoice.order.customer.id, this.invoice.order.id, this.invoice.id)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/pdf' });
                FileSaver.saveAs(blob, "Invoice.pdf");
            }, err => console.error(err));
    }

    viewOrder(){
        this.router.navigate(['/order-view', this.invoice.order.customer.id, this.invoice.order.id])
    }

    editInvoice() {
        this.router.navigate(['/invoice-maint', this.invoice.order.customer.id, this.invoice.order.id, this.invoice.id])
    }
}