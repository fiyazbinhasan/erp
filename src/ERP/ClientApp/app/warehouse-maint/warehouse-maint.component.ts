﻿import { Component, OnInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControlName } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { IWarehouse } from '../view-models/warehouse';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/mergeMap';

import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/forkJoin';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Message } from 'primeng/primeng';

import * as _ from "lodash";

import { GenericValidator } from '../shared/generic-validator';

import { WarehouseDataService } from '../services/warehouse-data.service';
import { WarehouseItemDataService } from '../services/warehouse-item-data.service';

@Component({
    moduleId: 'AppModule',
    selector: 'app-warehouse-maint',
    templateUrl: 'warehouse-maint.component.html',
    styleUrls: ['./warehouse-maint.component.css']
})

export class WarehouseMaintComponent implements OnInit {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    paragraph: string = 'Use this form to edit the existing warehouse.';
    errorMessage: string;
    messages: Message[];

    warehouseForm: FormGroup;
    warehouse: IWarehouse;
    
    get warehouseItems(): FormArray {
        return <FormArray>this.warehouseForm.get('warehouseItems');
    }

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    private sub: Subscription;
    
    constructor(private fb: FormBuilder,
        private warehouseDataService: WarehouseDataService,
        private warehouseItemDataService: WarehouseItemDataService,
        private route: ActivatedRoute,
        private router: Router) {

        this.validationMessages = {
            moniker: {
                required: 'Warehouse code is required.'
            },
            address: {
                required: 'Warehouse address is required.'
            }
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.warehouseForm = this.fb.group({
            moniker: ['', [Validators.required]],
            address: ['', [Validators.required]],
            warehouseItems: this.fb.array([])
        });

        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getWarehouse(id);
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.warehouseForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.warehouseForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveWarehouse(): void {
        if (this.warehouseForm.dirty && this.warehouseForm.valid) {
            let warehouse = Object.assign({}, this.warehouse, this.warehouseForm.value);

            this.warehouseDataService.saveWarehouse(warehouse)
                .subscribe(() => {
                    this.warehouse = warehouse;
                        this.onSaveComplete();
                    },
                (error: any) => {
                    this.showMessage('error', 'Error', `Could not save Warehouse. ${error}`);
                    
                });

        } else if (!this.warehouseForm.dirty) {
            this.onSaveComplete();
        }
    }

    onWarehouseRetrieved(warehouse: IWarehouse): void {
        if (this.warehouseForm) {
            this.warehouseForm.reset();
        }

        this.warehouse = warehouse;

        this.warehouseForm.patchValue({
            moniker: this.warehouse.moniker,
            address: this.warehouse.address
        });
    }


    getWarehouse(id: string): void {
        if (id === undefined) {
            this.paragraph = `Use this form to add a new warehouse`;
        } else {
            this.warehouseDataService.getWarehouse(id)
                .subscribe(
                    (warehouse: IWarehouse) => this.onWarehouseRetrieved(warehouse),
                    (error: any) => this.errorMessage = <any>error
                );
        }
    }

    onSaveComplete(): void {
        this.warehouseForm.reset();
        this.showMessage('success', 'Successful', 'Warehouse saved successfully');
        this.router.navigate(["/warehouse-list"]);
    }

    showMessage(severity, summary, details) {
        this.messages = [];
        this.messages.push({ severity: severity, summary: summary, detail: details });
    }
}