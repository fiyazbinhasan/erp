import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InputSwitchModule, GrowlModule, Message } from 'primeng/primeng';
import { IInventory } from '../view-models/inventory';
import { IItem } from '../view-models/item';
import { IInventoryItem, IInventoryItems } from '../view-models/inventory-item';

import { SupplierDataService } from '../services/supplier-data.service';
import { ItemDataService } from '../services/item-data.service';
import { InventoryDataService } from '../services/inventory-data.service';
import { InventoryItemDataService } from '../services/inventory-item-data.service';
import { MeasurementUnitDataService } from '../services/measurement-unit-data.service';
import { MeasurementUnitSetupDataService } from '../services/measurement-unit-setup-data.service';
import { WarehouseDataService } from '../services/warehouse-data.service';
import { WarehouseItemDataService } from '../services/warehouse-item-data.service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/mergeMap';

import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/forkJoin';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import * as _ from "lodash";

import { SelectItem } from '../shared/select-item';
import { GenericValidator } from '../shared/generic-validator';

function fixTimeOffset(date: Date) : Date {
    date.setHours(date.getHours() - date.getTimezoneOffset() / 60);
    return date;
}

@Component({
    moduleId: 'AppModule',
    selector: 'app-inventory-maint',
    templateUrl: 'inventory-maint.component.html',
    styleUrls: ['./inventory-maint.component.css']
})

export class InventoryMaintComponent implements OnInit {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    paragraph: string = 'Use this form to edit the existing inventory.';
    errorMessage: string;
    itemDisabled: boolean = true;

    inventoryItemsCopy: Array<string> = [];
    inventoryForm: FormGroup;
    inventory: IInventory = <IInventory>{};
    suppliers: Array<SelectItem> = [];
    warehouses: Array<SelectItem> = [];

    items: Array<IItem> = [];
    inventoryWithItems: IInventoryItems = <IInventoryItems>{};


    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    private sub: Subscription;
    checked: boolean = false;
    msgs: Message[] = [];

    get inventoryItems(): FormArray {
        return <FormArray>this.inventoryForm.get('inventoryItems');
    }

    get moniker() {
        return this.inventoryForm.get('moniker');
    }

    constructor(private fb: FormBuilder,
        private itemDataService: ItemDataService,
        private supplierDataService: SupplierDataService,
        private inventoryDataService: InventoryDataService,
        private inventoryItemDataService: InventoryItemDataService,
        private warehouseDataService: WarehouseDataService,
        private warehouseItemDataService: WarehouseItemDataService,
        private measurementUnitDataService: MeasurementUnitDataService,
        private measurementUnitSetupDataService: MeasurementUnitSetupDataService,
        private route: ActivatedRoute,
        private router: Router) {

        this.validationMessages = {
            moniker: {
                required: 'Inventory code is required.',
                minlength: 'Inventory code must be at least three characters.',
                maxlength: 'Inventory code cannot exceed 50 characters.'
            },
            purchaseDate: {
                required: 'Purchase date is required'
            },
            supplier: {
                required: 'Supplier is required'
            },
            warehouse: {
                required: 'Warehouse is required'
            }
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.inventoryForm = this.fb.group({
            moniker: ['', [Validators.required,Validators.minLength(3), Validators.maxLength(50)]],
            priorityLevel: 0,
            purchaseDate: ['', [Validators.required]],
            description: '',
            supplier: ['', [Validators.required]],
            warehouse: ['', [Validators.required]],
            inventoryItems: this.fb.array([])
        });

        this.supplierDataService.getSuppliers().subscribe(
            (suppliers) => {
                suppliers.map((supplier) => {
                    this.suppliers.push(<SelectItem>{ label: supplier.firstName + " " + supplier.lastName, value: supplier });
                });

                this.warehouseDataService.getWarehouses().subscribe(
                    (warehouses) => {
                        warehouses.map((warehouse) => {
                            this.warehouses.push(<SelectItem>{ label: warehouse.moniker, value: warehouse });
                        });

                        this.sub = this.route.params.subscribe(
                            params => {
                                let supplierId = params['supplierId'];
                                let id = params['id'];
                                this.getInventoryWithItems(supplierId, id);
                            }
                        );
                    }
                );
            }
        );
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.inventoryForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.inventoryForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    searchItems(event) {
        this.itemDataService.searchItems(event.query).subscribe(
            (items) => {
                this.items = items;
            }
        );
    }

    handleChange() {
        if (this.checked) {
            this.moniker.clearValidators()
            this.moniker.setErrors(null);
        }

        else {
            this.moniker.setValidators(Validators.required)
        }
    }
    addItemInList(item) {
        var isAdded = _.find(this.inventoryItems.controls,
            inventoryItemControl => (<IItem>inventoryItemControl.get('item').value).id === item.id);

        if (isAdded)
            return;
        else {
             this.measurementUnitSetupDataService.getMeasurementUnits(item.id).subscribe(measurementUnitSetups => {
                var setupObj = {
                    measurementUnitSetups:[]
                };

                setupObj.measurementUnitSetups = _.map(measurementUnitSetups,
                    measurementUnitSetup => ({
                        label: measurementUnitSetup.measurementUnit.name,
                        value: measurementUnitSetup
                    }));

                this.inventoryItems.push(this.fb.group({
                    id: '',
                    item: item,
                    issuedQuantity: 0,
                    measurementUnitSetups: [setupObj.measurementUnitSetups],
                    measurementUnitSetup: ['', [Validators.required]],
                    costPerUnit: 0,
                    amount: 0,
                    remarks: '',
                    inventory: this.inventory,
                    warehouse: this.inventoryForm.get('warehouse').value
                }));
            });
        }
    }

    extractInventoryForManipulate(inventoryWithItems: any) : IInventory{
        return <IInventory> {
            id: this.inventory.id,
            moniker: this.checked ? "" :inventoryWithItems.moniker,
            purchaseDate: fixTimeOffset(inventoryWithItems.purchaseDate),
            description: inventoryWithItems.description,
            supplier: inventoryWithItems.supplier,
            priorityLevel: inventoryWithItems.priorityLevel
        }
    }

    saveInventory(): void {
        if (this.inventoryForm.dirty && this.inventoryForm.valid) {
            let inventoryWithItems = Object.assign({}, this.inventoryWithItems, this.inventoryForm.getRawValue());
            let inventory = this.extractInventoryForManipulate(inventoryWithItems);
            let inventoryItems = inventoryWithItems.inventoryItems;
            let inventoryItemsforDeletion = _.differenceWith(this.inventoryItemsCopy, _.map(inventoryItems, 'id'), _.isEqual);

            this.inventoryDataService.saveInventory(inventory).subscribe((inventory) => {
                let observables = [];
                this.inventory = inventory;
                observables = _.map(inventoryItems, inventoryItem => {
                    return this.inventoryItemDataService.saveInventoryItem(<IInventoryItem>{
                        id: inventoryItem.id,
                        issuedQuantity: inventoryItem.issuedQuantity,
                        costPerUnit: inventoryItem.costPerUnit,
                        amount: inventoryItem.issuedQuantity * inventoryItem.costPerUnit,
                        remarks: inventoryItem.remarks,
                        inventoryId: this.inventory.id,
                        measurementUnitSetupId: inventoryItem.measurementUnitSetup.id,
                        warehouseId: inventoryItem.warehouse.id
                    });
                });

                if (inventoryItemsforDeletion != null && inventoryItemsforDeletion.length != 0)
                    _.each(inventoryItemsforDeletion, (inventoryItemId) => {
                        observables.push(this.inventoryItemDataService.deleteInventoryItem(inventoryItemId));
                    });

                Observable.concat(...observables).subscribe(() => {
                    this.onSaveComplete();
                }, (error: any) =>
                    { this.errorMessage = <any>error });
                this.msgs = [];
                this.msgs.push({
                    severity: 'success',
                    summary: 'Saved Successfully',
                    detail: this.inventory.moniker
                });
            }, (error: any) => this.errorMessage = <any>error);

        } else if (!this.inventoryForm.dirty) {
            this.onSaveComplete();
        }
    }

    getInventoryWithItems(supplierId: string, id: string): void {
        if (id === undefined && supplierId === undefined) {
            this.paragraph = `Use this form to add a new inventory`;
        } else {
            Observable
                .forkJoin([this.inventoryDataService.getInventory(supplierId, id), this.inventoryItemDataService.getItems(id)])
                .subscribe(results => {
                    this.onInventoryRetrieved(results[0]);
                    this.onInventoryItemsRetrieved(results[1]);
                });
        }
    }

    onInventoryRetrieved(inventory: any) {
        if (this.inventoryForm) {
            this.inventoryForm.reset();
        }

        this.inventory = inventory;

        this.inventoryForm.patchValue({
            moniker: this.inventory.moniker,
            purchaseDate: new Date(this.inventory.purchaseDate),
            description: this.inventory.description,
            priorityLevel: this.inventory.priorityLevel,
            supplier: this.inventory.supplier
        });
    }

    onInventoryItemsRetrieved(inventoryItems: any): void {
        this.inventoryItemsCopy = _.map(inventoryItems, 'id');
        const inventoryItemsControl = <FormArray>this.inventoryForm.controls['inventoryItems'];

        _.each(inventoryItems,
            ((inventoryItem, i) => {
                this.measurementUnitSetupDataService.getMeasurementUnits(inventoryItem.measurementUnitSetup.itemId).subscribe(measurementUnitSetups => {
                    var setupObj = {
                        measurementUnitSetups: []
                    };

                    setupObj.measurementUnitSetups = _.map(measurementUnitSetups, measurementUnitSetup => {
                        return ({
                            label: measurementUnitSetup.measurementUnit.name,
                            value: measurementUnitSetup
                        });
                    });

                    inventoryItemsControl.push(this.fb.group({
                        id: inventoryItem.id,
                        issuedQuantity: inventoryItem.issuedQuantity,
                        costPerUnit: inventoryItem.costPerUnit,
                        amount: inventoryItem.issuedQuantity * inventoryItem.costPerUnit,
                        remarks: inventoryItem.remarks,
                        item: new FormControl(inventoryItem.measurementUnitSetup.item),
                        measurementUnitSetups: [setupObj.measurementUnitSetups],
                        measurementUnitSetup: new FormControl(inventoryItem.measurementUnitSetup),
                        measurementUnitSetupId: new FormControl(inventoryItem.measurementUnitSetupId),
                        warehouse: new FormControl(inventoryItem.warehouse),
                        warehouseId: new FormControl(inventoryItem.warehouseId)
                    }));
                });
            }));
    }

    onSaveComplete(): void {
        this.inventoryForm.reset();
        this.inventoryForm.setControl('inventoryItems', this.fb.array([]));
        this.router.navigate(["/inventory-list"]);
    }

    calculateRowAmount(index: number) {
        var formArray = this.inventoryForm.get('inventoryItems') as FormArray;
        formArray.at(index).patchValue({
            amount: formArray.at(index).get('issuedQuantity').value* formArray.at(index).get('costPerUnit').value
        });
    }

    deleteItemFromList(index: number) {
        const arrayControl = <FormArray>this.inventoryForm.controls['inventoryItems'];
        arrayControl.removeAt(index);
        this.inventoryForm.updateValueAndValidity();
        this.inventoryForm.markAsDirty();
    }

    toggleItemDropdown(): void {
        this.itemDisabled = ((this.inventoryForm.get('warehouse').value).id == undefined ? true : false);
    }
}