import {Component,Input,OnInit,OnDestroy,EventEmitter,ViewChild,Inject,forwardRef} from '@angular/core';
import {trigger,state,style,transition,animate} from '@angular/animations';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {MenuItem} from 'primeng/primeng';
import {AppComponent} from '../app.component';

@Component({
    selector: 'app-menu',
    template: `
        <ul app-submenu [item]="model" root="true" class="navigation-menu" visible="true"></ul>
    `
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(@Inject(forwardRef(() => AppComponent)) public app:AppComponent) {}
    
    ngOnInit() {
        this.model = [
            { label: 'Dashboard', icon: 'fa fa-fw fa-home', routerLink: ['/'] },
            { label: 'Warehouse', icon: 'fa fa-fw fa-industry', routerLink: ['/warehouse-list'] },
            { label: 'Measurement Unit', icon: 'fa fa-fw fa-balance-scale', routerLink: ['/measurement-unit-list'] },
            { label: 'Category', icon: 'fa fa-fw fa-tags', routerLink: ['/category-list'] },
            { label: 'Item', icon: 'fa fa-fw fa-shopping-bag', routerLink: ['/item-list'] },
            { label: 'Customer', icon: 'fa fa-fw fa-user', routerLink: ['/customer-list'] },
            { label: 'Supplier', icon: 'fa fa-fw fa-truck', routerLink: ['/supplier-list'] },
            { label: 'Inventory', icon: 'fa fa-fw fa-braille', routerLink: ['/inventory-list'] },
            { label: 'Order', icon: 'fa fa-fw fa-shopping-cart', routerLink: ['/order-list'] },
            { label: 'Bill', icon: 'fa fa-fw fa-money', routerLink: ['/bill-list'] },
            { label: 'Invoice', icon: 'fa fa-fw fa-files-o', routerLink: ['/invoice-list'] },
            {
                label: 'Reports', icon: 'fa fa-fw fa-bar-chart',
                items: [
                    {
                        label: 'Inventory Reports', icon: 'fa fa-fw fa-th-large',
                        items: [
                            { label: 'Item Summary Report', icon: 'fa fa-fw fa-table', routerLink: ['/inventory-details-report']},
                            { label: 'Valuation Summary', icon: 'fa fa-fw fa-table', routerLink: ['/inventory-valuation-summary-report'] },
                        ]
                    },
                    {
                        label: 'Purchase Reports', icon: 'fa fa-fw fa-th-large',
                        items: [
                            { label: 'Purchase by Supplier', icon: 'fa fa-fw fa-table', routerLink: ['/purchase-by-supplier-report'] },
                            { label: 'Purchase by Item', icon: 'fa fa-fw fa-table', routerLink: ['/purchase-by-item-report'] }
                        ]
                    },
                    {
                        label: 'Sales Reports', icon: 'fa fa-fw fa-th-large',
                        items: [
                            { label: 'Sales by Customer', icon: 'fa fa-fw fa-table', routerLink: ['/sales-by-customer-report'] },
                            { label: 'Sales by Item', icon: 'fa fa-fw fa-table', routerLink: ['/sales-by-item-report'] }
                        ]
                    }
                ]
            }
        ];
    }

    changeTheme(theme) {
        this.app.theme = theme;
        let themeLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('theme-css');
        let layoutLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('layout-css');
        
        themeLink.href = 'assets/theme/theme-' + theme +'.css';
        layoutLink.href = 'assets/layout/css/layout-' + theme +'.css';
        
    }
}

@Component({
    selector: '[app-submenu]',
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li [ngClass]="{'active-menuitem': isActive(i)}" *ngIf="child.visible === false ? false : true">
                <a [href]="child.url||'#'" (click)="itemClick($event,child,i)" *ngIf="!child.routerLink" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                    (mouseenter)="hover=true" (mouseleave)="hover=false">
                    <i [ngClass]="child.icon"></i>
                    <span>{{child.label}}</span>
                    <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                </a>

                <a (click)="itemClick($event,child,i)" *ngIf="child.routerLink"
                    [routerLink]="child.routerLink" routerLinkActive="active-menuitem-routerlink" [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target"
                    (mouseenter)="hover=true" (mouseleave)="hover=false">
                    <i [ngClass]="child.icon"></i>
                    <span>{{child.label}}</span>
                    <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                </a>
                <ul app-submenu [item]="child" *ngIf="child.items" [@children]="isActive(i) ? 'visible' : 'hidden'" [visible]="isActive(i)"></ul>
            </li>
        </ng-template>
    `,
    animations: [
        trigger('children', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenu implements OnDestroy {

    @Input() item: MenuItem;
    
    @Input() root: boolean;
    
    @Input() visible: boolean;
        
    activeIndex: number;
    
    hover: boolean;
    
    constructor(@Inject(forwardRef(() => AppComponent)) public app:AppComponent, public router: Router, public location: Location) {}
        
    itemClick(event: Event, item: MenuItem, index: number)  {
        //avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        //activate current item and deactivate active sibling if any
        this.activeIndex = (this.activeIndex === index) ? null : index;

        //execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }

        //prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            event.preventDefault();
        }

        //hide menu
        if (!item.items && (this.app.overlay || !this.app.isDesktop())) {
            this.app.sidebarActive = false;
        }
    }
    
    isActive(index: number): boolean {
        return this.activeIndex === index;
    }
    
    unsubscribe(item: any) {
        if(item.eventEmitter) {
            item.eventEmitter.unsubscribe();
        }
        
        if(item.items) {
            for(let childItem of item.items) {
                this.unsubscribe(childItem);
            }
        }
    }
        
    ngOnDestroy() {        
        if(this.item && this.item.items) {
            for(let item of this.item.items) {
                this.unsubscribe(item);
            }
        }
    }
}