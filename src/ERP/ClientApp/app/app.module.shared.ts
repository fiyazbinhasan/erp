﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';

import { AppComponent } from './app.component';

import {AccordionModule} from 'primeng/primeng';
import {AutoCompleteModule} from 'primeng/primeng';
import {BreadcrumbModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {CarouselModule} from 'primeng/primeng';
import {ChartModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {ChipsModule} from 'primeng/primeng';
import {CodeHighlighterModule} from 'primeng/primeng';
import {ConfirmDialogModule} from 'primeng/primeng';
import {SharedModule, ColumnFilterTemplateLoader} from 'primeng/primeng';
import {ContextMenuModule} from 'primeng/primeng';
import {DataGridModule} from 'primeng/primeng';
import {DataListModule} from 'primeng/primeng';
import {DataScrollerModule} from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {DragDropModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {GalleriaModule} from 'primeng/primeng';
import {GMapModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {InputMaskModule} from 'primeng/primeng';
import {InputSwitchModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
import {LightboxModule} from 'primeng/primeng';
import {ListboxModule} from 'primeng/primeng';
import {MegaMenuModule} from 'primeng/primeng';
import {MenuModule} from 'primeng/primeng';
import {MenubarModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';
import { OrderListModule } from 'primeng/primeng';
import {OverlayPanelModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {PanelMenuModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {PickListModule} from 'primeng/primeng';
import {ProgressBarModule} from 'primeng/primeng';
import {RadioButtonModule} from 'primeng/primeng';
import {RatingModule} from 'primeng/primeng';
import {ScheduleModule} from 'primeng/primeng';
import {SelectButtonModule} from 'primeng/primeng';
import {SlideMenuModule} from 'primeng/primeng';
import {SliderModule} from 'primeng/primeng';
import {SpinnerModule} from 'primeng/primeng';
import {SplitButtonModule} from 'primeng/primeng';
import {StepsModule} from 'primeng/primeng';
import {TabMenuModule} from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';
import {TerminalModule} from 'primeng/primeng';
import {TieredMenuModule} from 'primeng/primeng';
import {ToggleButtonModule} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';
import {TooltipModule } from 'primeng/primeng';
import {TreeModule} from 'primeng/primeng';
import {TreeTableModule } from 'primeng/primeng';
import {TriStateCheckboxModule } from 'primeng/primeng';
import {BlockUIModule} from 'primeng/primeng';
//import {SidebarModule} from 'primeng/primeng';

import {AppMenuComponent,AppSubMenu}  from './skeleton/app.menu.component';
import {AppSideBarComponent} from './skeleton/app.sidebar.component';
import { AppSidebarTabContent } from './skeleton/app.sidebartabcontent.component';
import { AppTopBar } from './skeleton/app.topbar.component';
import {AppFooter}  from './skeleton/app.footer.component';

import { DashboardComponent } from './dashboard/dashboard.component'
import { InventoryListComponent } from './inventory-list/inventory-list.component'
import { InventoryMaintComponent } from './inventory-maint/inventory-maint.component'
import { InventoryViewComponent } from './inventory-view/inventory-view.component'
import { ItemListComponent } from './item-list/item-list.component'
import { ItemMaintComponent } from './item-maint/item-maint.component'
import { ItemViewComponent } from './item-view/item-view.component'
import { SupplierListComponent } from './supplier-list/supplier-list.component'
import { SupplierMaintComponent } from './supplier-maint/supplier-maint.component'
import { SupplierViewComponent } from './supplier-view/supplier-view.component'
import { CustomerListComponent } from './customer-list/customer-list.component'
import { CustomerMaintComponent } from './customer-maint/customer-maint.component'
import { CustomerViewComponent } from './customer-view/customer-view.component'
import { CategoryListComponent } from './category-list/category-list.component'
import { CategoryMaintComponent } from './category-maint/category-maint.component'
import { CategoryViewComponent } from './category-view/category-view.component'
import { OrderListComponent } from './order-list/order-list.component'
import { OrderMaintComponent } from './order-maint/order-maint.component'
import { OrderViewComponent } from './order-view/order-view.component'
import { MeasurementUnitListComponent } from './measurement-unit-list/measurement-unit-list.component'
import { MeasurementUnitMaintComponent } from './measurement-unit-maint/measurement-unit-maint.component'
import { MeasurementUnitViewComponent } from './measurement-unit-view/measurement-unit-view.component'
import { MeasurementUnitSetupMaintComponent } from './measurement-unit-setup-maint/measurement-unit-setup-maint.component'
import { WarehouseItemMaintComponent } from './warehouse-item-maint/warehouse-item-maint.component'
import { BillListComponent } from './bill-list/bill-list.component'
import { BillMaintComponent } from './bill-maint/bill-maint.component'
import { BillViewComponent } from './bill-view/bill-view.component'
import { WarehouseListComponent } from './warehouse-list/warehouse-list.component'
import { WarehouseMaintComponent } from './warehouse-maint/warehouse-maint.component'
import { InvoiceListComponent } from './invoice-list/invoice-list.component'
import { InvoiceMaintComponent } from './invoice-maint/invoice-maint.component'
import { InvoiceViewComponent } from './invoice-view/invoice-view.component'
import { SalesByItemReportComponent } from './sales-by-item-report/sales-by-item-report.component'
import { SalesByCustomerReportComponent } from './sales-by-customer-report/sales-by-customer-report.component'


import { DashboardDataService } from './services/dashboard-data.service'
import { ItemDataService } from './services/item-data.service'
import { SupplierDataService } from './services/supplier-data.service'
import { CategoryDataService } from './services/category-data.service'
import { InventoryDataService } from './services/inventory-data.service'
import { InventoryItemDataService } from './services/inventory-item-data.service'
import { CustomerDataService } from './services/customer-data.service'
import { OrderDataService } from './services/order-data.service'
import { OrderItemDataService } from './services/order-item-data.service'
import { MeasurementUnitDataService } from './services/measurement-unit-data.service'
import { MeasurementUnitSetupDataService } from './services/measurement-unit-setup-data.service'
import { BillDataService } from './services/bill-data.service'
import { WarehouseDataService } from './services/warehouse-data.service'
import { WarehouseViewComponent } from './warehouse-view/warehouse-view.component'
import { WarehouseItemDataService } from './services/warehouse-item-data.service'
import { InvoiceDataService } from './services/invoice-data.service'
import { InvoiceItemDataService } from './services/invoice-item-data.service'
import { InventoryReportDataService } from './services/inventory-report.service'
import { InventoryDetailsReportComponent } from './inventory-details-report/inventory-details-report.component'
import { InventoryValuationSummaryReportComponent } from './inventory-valuation-summary-report/inventory-valuation-summary-report.component'
import { PurchaseReportDataService } from './services/purchase-report.service'
import { PurchaseBySupplierReportComponent } from './purchase-by-supplier-report/purchase-by-supplier-report.component'
import { PurchaseByItemReportComponent } from './purchase-by-item-report/purchase-by-item-report.component'
import { SalesReportDataService } from './services/sales-report.service'
import { ValidatorService } from './services/validator.service'


@NgModule({
    declarations: [
        AppComponent,
        AppMenuComponent,
        AppSubMenu,
        AppSideBarComponent,
        AppSidebarTabContent,
        AppTopBar,
        AppFooter,

        DashboardComponent,
        InventoryListComponent,
        InventoryMaintComponent,
        InventoryViewComponent,
        ItemListComponent,
        ItemMaintComponent,
        ItemViewComponent,
        SupplierListComponent,
        SupplierMaintComponent,
        SupplierViewComponent,
        CustomerListComponent,
        CustomerMaintComponent,
        CustomerViewComponent,
        CategoryListComponent,
        CategoryMaintComponent,
        CategoryViewComponent,
        OrderListComponent,
        OrderMaintComponent,
        OrderViewComponent,
        MeasurementUnitListComponent,
        MeasurementUnitMaintComponent,
        MeasurementUnitViewComponent,
        MeasurementUnitSetupMaintComponent,
        BillListComponent,
        BillMaintComponent,
        BillViewComponent,
        WarehouseListComponent,
        WarehouseMaintComponent,
        WarehouseViewComponent,
        WarehouseItemMaintComponent,
        InvoiceListComponent,
        InvoiceMaintComponent,
        InvoiceViewComponent,
        InventoryDetailsReportComponent,
        InventoryValuationSummaryReportComponent,
        PurchaseBySupplierReportComponent,
        PurchaseByItemReportComponent,
        SalesByItemReportComponent,
        SalesByCustomerReportComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes),

        AccordionModule,
        AutoCompleteModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CarouselModule,
        ChartModule,
        CheckboxModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        SharedModule,
        ContextMenuModule,
        DataGridModule,
        DataListModule,
        DataScrollerModule,
        DataTableModule,
        DialogModule,
        DragDropModule,
        DropdownModule,
        FieldsetModule,
        FileUploadModule,
        GalleriaModule,
        GMapModule,
        GrowlModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessagesModule,
        MultiSelectModule,
        OrderListModule,
        OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        RatingModule,
        ScheduleModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        SplitButtonModule,
        StepsModule,
        TabMenuModule,
        TabViewModule,
        TerminalModule,
        TieredMenuModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        TriStateCheckboxModule,
        BlockUIModule
        //SidebarModule,
    ],
    providers: [
        DashboardDataService,
        CategoryDataService,
        ItemDataService,
        SupplierDataService,
        InventoryDataService,
        InventoryItemDataService,
        CustomerDataService,
        OrderDataService,
        OrderItemDataService,
        MeasurementUnitDataService,
        MeasurementUnitSetupDataService,
        BillDataService,
        WarehouseDataService,
        WarehouseItemDataService,
        InvoiceDataService,
        InvoiceItemDataService,
        InventoryReportDataService,
        PurchaseReportDataService,
        SalesReportDataService,
        ValidatorService
    ]
})
export class AppModuleShared {
}
