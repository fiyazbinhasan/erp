import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

import { IIntroInformation, IProductDetail, ITopSellingItem, IProfitMargin } from '../view-models/dashboard'
import { IItem } from '../view-models/item'
import { DashboardDataService } from '../services/dashboard-data.service'

import * as _ from "lodash";

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    errorMessage: string;
    introInformation: IIntroInformation = {
        inventoryCount: 0,
        orderCount: 0,
        pendingOrders: 0,
        partialDeliveredOrders: 0,
        invoiceCount: 0,
        billCount: 0,
        unpaidOrders: 0,
        partialPaidOrders: 0,
        warehouseCount: 0,
        supplierCount: 0,
        itemCount: 0,
        customerCount: 0
    };

    productDetails: IProductDetail = {
        lowStockItems: 0,
        itemCount: 0,
        categoryCount: 0
    };

    topSellingItems: ITopSellingItem[] = [];

    loading: boolean;
    profitMargins: any;
    soldQuantityByCumulativeQuantity: any;
    soldAmountByCumulativeAmount: any;
    purchasedQuantityByCumulativeQuantity: any;

    tripleSetOptions = {
        scales: {
            xAxes: [{
                barPercentage: 0.8
            }]
        }
    }

    doubleSetOptions = {
        scales: {
            xAxes: [{
                barPercentage: 0.8,
                categoryPercentage: 0.3
            }]
        }
    }


    constructor(private dashboardDataService: DashboardDataService) { }

    ngOnInit() {

        this.loadProfitMargins();
        this.loadSoldQuantityByMonth();
        this.loadSoldAmountByMonth();
        this.loadPurchasedQuantityByMonth();

        this.dashboardDataService.getIntroInformation()
            .subscribe((introInformation) => this.introInformation = introInformation,
            (error) => this.errorMessage = <any>error);

        this.dashboardDataService.getProductDetails()
            .subscribe((productDetails) => this.productDetails = productDetails,
            (error) => this.errorMessage = <any>error);

        this.dashboardDataService.getTopSellingItems()
            .subscribe((topSellingItems) => this.topSellingItems = topSellingItems,
            (error) => this.errorMessage = <any>error);

    }

    loadProfitMargins() {
        this.dashboardDataService.getProfitMargin()
            .subscribe((profitMargins) => {
                this.profitMargins = {
                    labels: _.map(profitMargins, 'month'),
                    datasets: [
                        {
                            label: 'Purchase',
                            backgroundColor: '#607D8B',
                            borderColor: '#607D8B',
                            data: _.map(profitMargins, 'purchasedAmount')
                        },
                        {
                            label: 'Sales',
                            backgroundColor: '#9E9E9E',
                            borderColor: '#9E9E9E',
                            data: _.map(profitMargins, 'soldAmount')
                        },
                        {
                            type: 'line',
                            label: 'Profit',
                            pointBackgroundColor: '#4DB6AC',
                            backgroundColor: '#4DB6AC',
                            borderColor: '#4DB6AC',
                            borderWidth: 2,
                            fill: false,
                            data: _.map(profitMargins, profitMargin => profitMargin.soldAmount - profitMargin.purchasedAmount)
                        }
                    ]
                }
            },
            (error) => this.errorMessage = <any>error);
    }

    loadSoldQuantityByMonth() {
        this.dashboardDataService.getSoldQuantityByMonth().subscribe((soldQuantityByMonths) => {
            this.soldQuantityByCumulativeQuantity = {
                labels: _.map(soldQuantityByMonths, 'month'),
                datasets: [{
                    type: 'bar',
                    label: 'Quantity',
                    borderWidth: 2,
                    backgroundColor: '#607D8B',
                    data: _.map(soldQuantityByMonths, 'quantity')
                },
                {
                    type: 'line',
                    label: 'Commulative Quantity',
                    pointBackgroundColor: '#4DB6AC',
                    backgroundColor: '#4DB6AC',
                    borderColor: '#4DB6AC',
                    borderWidth: 2,
                    fill: false,
                    data: this.CalclateCumulativeSoldQuantity(soldQuantityByMonths),
                }]
            }
        },
            (error) => this.errorMessage = <any>error);
    }

    CalclateCumulativeSoldQuantity(data: any[]) {
        let runningSum = 0;
        return _.map(data, (soldQuantityByMonth) => {
            return runningSum += soldQuantityByMonth.quantity;
        })
    }

    loadSoldAmountByMonth() {
        this.dashboardDataService.getSoldAmountByMonth().subscribe((soldAmountByMonths) => {
            this.soldAmountByCumulativeAmount = {
                labels: _.map(soldAmountByMonths, 'month'),
                datasets: [{
                    type: 'bar',
                    label: 'Amount',
                    borderWidth: 2,
                    backgroundColor: '#607D8B',
                    data: _.map(soldAmountByMonths, 'amount')
                },
                {
                    type: 'line',
                    label: 'Commulative Amount',
                    pointBackgroundColor: '#4DB6AC',
                    backgroundColor: '#4DB6AC',
                    borderColor: '#4DB6AC',
                    borderWidth: 2,
                    fill: false,
                    data: this.CalclateCumulativeSoldAmount(soldAmountByMonths)
                }]
            }
        },
            (error) => this.errorMessage = <any>error);
    }

    CalclateCumulativeSoldAmount(data: any[]) {
        let runningSum = 0;
        return _.map(data, (soldAmountByMonth) => {
            return runningSum += soldAmountByMonth.amount;
        });
    }

    loadPurchasedQuantityByMonth() {
        this.dashboardDataService.getPurchasedQuantityByMonth().subscribe((purchasedQuantityByMonths) => {
            this.purchasedQuantityByCumulativeQuantity = {
                labels: _.map(purchasedQuantityByMonths, 'month'),
                datasets: [{
                    type: 'bar',
                    label: 'Quantity',
                    borderWidth: 2,
                    backgroundColor: '#607D8B',
                    data: _.map(purchasedQuantityByMonths, 'quantity')
                },
                {
                    type: 'line',
                    label: 'Quantity Amount',
                    pointBackgroundColor: '#4DB6AC',
                    backgroundColor: '#4DB6AC',
                    borderColor: '#4DB6AC',
                    borderWidth: 2,
                    fill: false,
                    data: this.CalclateCumulativePurchasedQuantity(purchasedQuantityByMonths)
                }]
            }
        },
        (error) => this.errorMessage = <any>error);
    }

    CalclateCumulativePurchasedQuantity(data: any[]) {
        let runningSum = 0;
        return _.map(data, (purchasedQuantityByMonth) => {
            return runningSum += purchasedQuantityByMonth.quantity;
        });
    }
}