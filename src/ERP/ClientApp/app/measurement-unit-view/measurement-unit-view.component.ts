﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { IMeasurementUnitSetup } from '../view-models/measurement-unit-setup';
import { IMeasurementUnit} from '../view-models/measurement-unit';
import { MeasurementUnitSetupDataService } from '../services/measurement-unit-setup-data.service';
import { MeasurementUnitDataService } from '../services/measurement-unit-data.service';

import * as _ from 'lodash';
@Component({
    selector: 'measurement-unit-view',
    templateUrl: 'measurement-unit-view.component.html',
    styleUrls: ['./measurement-unit-view.component.css']
})

export class MeasurementUnitViewComponent implements OnInit {
    dtCols: any[];
    measurementunitName: string = "";
    errorMessage: string;
    measurementUnitSetups: Array<IMeasurementUnitSetup>;
    totalRecords: number = 0;

    constructor(private measurementUnitSetupDataService: MeasurementUnitSetupDataService,
        private measurementUnitDataService: MeasurementUnitDataService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'item.name', header: 'Name', filter: true },
            { field: 'item.category.name', header: 'Category' }
        ];

       
            this.route.params.subscribe(
                params => {
                    this.measurementUnitDataService.getMeasurementUnit(params['id'])
                        .subscribe((measurementUnit: IMeasurementUnit) => {
                            this.measurementunitName = measurementUnit.name;
                            this.measurementUnitSetupDataService.getItems(params['id']).subscribe(measurementUnitSetups => {
                                this.totalRecords = measurementUnitSetups.length;
                                this.measurementUnitSetups = measurementUnitSetups;
                            },
                                (error: any) => this.errorMessage = <any>error);
                        },
                        (error: any) => this.errorMessage = <any>error);
                },
                (error: any) => this.errorMessage = <any>error);
       
    }

}