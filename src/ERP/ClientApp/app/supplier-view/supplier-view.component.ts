﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SupplierDataService } from '../services/supplier-data.service';
import { InventoryDataService } from '../services/inventory-data.service';

import { IInventory } from '../view-models/inventory';

import { LazyLoadEvent } from 'primeng/primeng';

import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

@Component({
    moduleId: 'AppModule',
    selector: 'app-supplier-view',
    templateUrl: 'supplier-view.component.html',
    styleUrls: ['./supplier-view.component.css']
})

export class SupplierViewComponent implements OnInit {
    errorMessage: string;
    totalInventoryAmount: number = 0;
    totalInventoryItems: number = 0;
    phone = "";
    loading: boolean;

    supplier: any = {
        id: '',
        firstName: '',
        lastName: '',
        phone: '',
        email: '',
        address1: '',
        address2: '',
        city: '',
        country: ''
    }

    inventories: Array<IInventory>;
   
    dtInventoryCols: any[];
   
    lazyLoadEvent: LazyLoadEvent;
    loadingInventory: boolean;
    

    constructor(private supplierDataService: SupplierDataService,
        private inventoryDataService: InventoryDataService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.dtInventoryCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'purchaseDate', header: 'Purchase Date' },
            { field: 'description', header: 'Description' },
            { field: 'priorityLevel', header: 'Priority Level' }
        ];

        this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getSupplier(id);
            }
        );
    }

    getSupplier(supplierId: string): void {
        this.supplierDataService.getSupplier(supplierId)
            .subscribe(supplier => {
                this.supplier = supplier;
                this.supplierDataService.getSupplierViewData(supplierId)
                    .subscribe(result => {
                        this.totalInventoryAmount = result[0].totalInventoryAmount;
                        this.totalInventoryItems = result[0].totalInventoryItems;
                    });
            });
    }

    loadInventoriesLazy(event: LazyLoadEvent) {
        this.loading = true;
        this.inventoryDataService.getPagedInventories(event, this.supplier.phone)
            .subscribe(inventories => {
                this.inventories = inventories['body'];
                _.each(this.inventories, inventory => {
                    inventory.purchaseDate = new Date(inventory.purchaseDate).toDateString();
                });
                this.loading = false;
            }, (error: any) => this.errorMessage = <any>error);
    }
}