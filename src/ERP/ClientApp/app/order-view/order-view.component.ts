﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { OrderDataService } from '../services/order-data.service';
import { OrderItemDataService } from '../services/order-item-data.service';
import { InvoiceDataService } from '../services/invoice-data.service';
import { BillDataService } from '../services/bill-data.service';
import { IInvoice } from '../view-models/invoice';
import { IBill } from '../view-models/bill';

import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-order-view',
    templateUrl: 'order-view.component.html',
    styleUrls: ['./order-view.component.css']
})

export class OrderViewComponent implements OnInit {
    orderItems: Array<any>;
    invoices: Array<any>;
    bills: Array<any>;
    loading: boolean;
    order: any = {
        id: '',
        moniker: '',
        customer: {
            id: '',
            firstName: '',
            lastName: '',
            address1: '',
            phone: '',
            email: ''
        },
        expectedDeliveryDate: '',
        orderDate: '',
        orderStatus: "",
        paymentStatus: ""
    };
    dtCols: any[];
    dtInvoiceCols: any[];
    dtBillCols: any[];
    totalOrderAmount: number = 0;
    totalInvoices: number = 0;
    totalBills: number = 0;
    totalBillAmount: number = 0;

    constructor(private orderDataService: OrderDataService,
        private orderItemDataService: OrderItemDataService,
        private invoiceDataService: InvoiceDataService,
        private billDataService: BillDataService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'measurementUnitSetup.item.name', header: 'Item' },
            { field: 'quantity', header: 'Quantity' },
            { field: 'measurementUnitSetup.measurementUnit.name', header: 'M.Unit' },
            { field: 'unitPrice', header: 'U.Price' },
            { field: 'discount', header: 'Discount' },
            { field: 'amount', header: 'Amount' },
            { field: 'remarks', header: 'Remarks' }
        ];

        this.dtInvoiceCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'invoiceDate', header: 'Invoice Date' },
            { field: 'description', header: 'Description' }
        ];

        this.dtBillCols = [
            { field: 'moniker', header: 'Bill No', filter: true },
            { field: 'billDate', header: 'Bill Date' },
            { field: 'paidAmount', header: 'Amount' },
            { field: 'paymentMethod', header: 'Payment Method' }
        ];

        this.route.params.subscribe(
            params => {
                let customerId = params['customerId'];
                let id = params['id'];
                this.retrieveData(customerId, id);
            }
        );
    }

    retrieveData(customerId: string, orderId: string): void {
        Observable
            .forkJoin([
                this.orderDataService.getOrder(customerId, orderId),
                this.orderItemDataService.getItems(orderId),
                this.invoiceDataService.getOrderInvoices(customerId, orderId),
                this.billDataService.getOrderBills(orderId)
            ])
            .subscribe(results => {
                this.onOrderRetrieved(results[0]);
                this.onOrderItemsRetrieved(results[1]);
                this.onInvoicesRetrieved(results[2]);
                this.onBillsRetrieved(results[3]);
            });
    }

    onOrderRetrieved(order): void {
        this.order = order;
        this.order.expectedDeliveryDate = new Date(order.expectedDeliveryDate).toDateString();
        this.order.orderDate = new Date(order.orderDate).toDateString();
    }

    onOrderItemsRetrieved(orderItems): void {
        this.loading = true;
        this.orderItems = orderItems;
        _.each(orderItems, orderItem => {
            orderItem.amount = orderItem.quantity * (orderItem.unitPrice - orderItem.discount);
            this.totalOrderAmount += orderItem.amount;
        });
        this.loading = false;
    }

    onInvoicesRetrieved(invoices): void {
        this.loading = true;
        this.totalInvoices = invoices.length;
        this.invoices = invoices;
        _.each(this.invoices, invoice => {
            invoice.invoiceDate = new Date(invoice.invoiceDate).toDateString();
        });
        this.loading = false;
    }

    onBillsRetrieved(bills): void {
        this.loading = true;
        this.totalBills = bills.length;
        this.bills = bills;
        _.each(this.bills, bill => {
            bill.billDate = new Date(bill.billDate).toDateString();
            this.totalBillAmount += bill.paidAmount;
        });
        this.loading = false;
    }

    viewInvoice(invoice: IInvoice) {
        this.router.navigate(['/invoice-view', this.order.customer.id, this.order.id, invoice.id]);
    }

    viewBill(bill: IBill) {
        this.router.navigate(['/bill-view', this.order.customer.id, this.order.id, bill.id]);
    }

    editOrder() {
        this.router.navigate(['/order-maint', this.order.customer.id, this.order.id]);
    }

    generatePdf() {
        this.orderDataService.generatePdf(this.order.customer.id, this.order.id)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/pdf' });
                FileSaver.saveAs(blob, this.order.moniker + ".pdf");
            }, err => console.error(err));
    }

    addNewInvoice() {
        this.router.navigate(['/invoice-maint', this.order.moniker]);
    }

    addNewBill() {
        this.router.navigate(['/bill-maint', this.order.moniker]);
    }
}