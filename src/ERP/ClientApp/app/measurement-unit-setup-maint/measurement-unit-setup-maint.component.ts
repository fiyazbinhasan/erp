﻿import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, FormControlName } from '@angular/forms';
import { Message } from 'primeng/primeng';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';

import * as _ from "lodash";
import { IMeasurementUnit } from '../view-models/measurement-unit';
import { IItem } from '../view-models/item'
import { MeasurementUnitDataService } from '../services/measurement-unit-data.service';
import { IMeasurementUnitSetup } from '../view-models/measurement-unit-setup';
import { MeasurementUnitSetupDataService } from '../services/measurement-unit-setup-data.service';
import { GenericValidator } from '../shared/generic-validator';

@Component({
    selector: 'measurement-unit-setup-maint',
    templateUrl: 'measurement-unit-setup-maint.component.html',
    styleUrls: ['./measurement-unit-setup-maint.component.css']
})
export class MeasurementUnitSetupMaintComponent implements OnInit, AfterViewInit {
    measurementUnitSetupForm: FormGroup;
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    @Input('item') item: IItem;
    paragraph: string = 'Use this form for Measurement unit setup';
    displayMessage: { [key: string]: string } = {};
    messages: Message[];

    measurementUnits: IMeasurementUnit[] = [];
    measurementUnitSetupsCopy: IMeasurementUnitSetup[];
    previousBaseObject: IMeasurementUnitSetup;
    baseUnitName: string = '';

    get measurementUnitSetups(): FormArray {
        return <FormArray>this.measurementUnitSetupForm.get('measurementUnitSetups');
    }
    
    constructor(private fb: FormBuilder,
        private measurementUnitSetupDataService: MeasurementUnitSetupDataService,
        private measurementUnitDataService: MeasurementUnitDataService) { }

    ngOnInit(): void {
        this.measurementUnitSetupForm = this.fb.group({
            measurementUnitSetups: this.fb.array([])
        });

        this.validationMessages = {
            measurementUnit: {
                selection: 'Select a measurement Unit.'
            },
            conversionRatio: {
                selection: 'Provide a conversion ratio.'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);

        this.loadForm();
        
    }

    loadForm() {
        if (this.measurementUnitSetupForm) {
            this.measurementUnitSetupForm.setControl('measurementUnitSetups', this.fb.array([]));
            this.measurementUnitSetupForm.reset();
        }

        if (this.item.id !== undefined) {
            this.measurementUnitSetupDataService.getMeasurementUnits(this.item.id)
                .subscribe(measurementUnitSetups => {
                    this.onMeasurementUnitSetupsRetrieved(measurementUnitSetups);
                    this.measurementUnitDataService.getMeasurementUnits()
                        .subscribe(measurementUnits => {
                            this.onMeasurementUnitsRetrieved(measurementUnits);
                        });
                });
        } else {
            this.measurementUnitDataService.getMeasurementUnits()
                .subscribe(measurementUnits => {
                    this.onMeasurementUnitsRetrieved(measurementUnits);
                });
        }
    }

    onMeasurementUnitSetupsRetrieved(measurementUnitSetups: IMeasurementUnitSetup[]): void {
        this.measurementUnitSetupsCopy = measurementUnitSetups;

        const measurementUnitSetupsControls =
            this.measurementUnitSetupForm.controls['measurementUnitSetups'] as FormArray;

        _.each(measurementUnitSetups,
            (measurementUnitSetup => {
                if (measurementUnitSetup.isBase) {
                    this.previousBaseObject = measurementUnitSetup;
                    this.baseUnitName = measurementUnitSetup.measurementUnit.name;
                }
                measurementUnitSetupsControls.push(this.fb.group({
                    id: measurementUnitSetup.id,
                    isBase: measurementUnitSetup.isBase,
                    conversionRatio: measurementUnitSetup.isBase ? new FormControl({ value: measurementUnitSetup.conversionRatio, disabled: true }) : new FormControl(measurementUnitSetup.conversionRatio),
                    sellingPrice: measurementUnitSetup.sellingPrice,
                    discountPrice: measurementUnitSetup.allowDiscount ? new FormControl(measurementUnitSetup.discountPrice) : new FormControl({ value: 0, disabled: true }),
                    allowDiscount: measurementUnitSetup.allowDiscount,
                    isChecked: true,
                    item: new FormControl(measurementUnitSetup.item),
                    itemId: new FormControl(measurementUnitSetup.itemId),
                    measurementUnit: new FormControl(measurementUnitSetup.measurementUnit),
                    measurementUnitId: new FormControl(measurementUnitSetup.measurementUnit.id),
                    isUsed: new FormControl(measurementUnitSetup.isUsed)
                }));
            }));
    }

    onMeasurementUnitsRetrieved(measurementUnits: IMeasurementUnit[]) {
        const measurementUnitSetupsControls = this.measurementUnitSetupForm.get("measurementUnitSetups") as FormArray;
        let includedMeasurementUnits = _.map(this.measurementUnitSetupsCopy, 'measurementUnit');
        let excludedMeasurmentUnits = _.differenceWith(measurementUnits, includedMeasurementUnits, _.isEqual);
        _.each(excludedMeasurmentUnits,
            (measurementUnit => {
                measurementUnitSetupsControls.push(this.fb.group({
                    isBase: false,
                    conversionRatio: new FormControl({ value: 1, disabled: true }),
                    sellingPrice: new FormControl({ value: 0, disabled: true }),
                    discountPrice: new FormControl({ value: 0, disabled: true }),
                    allowDiscount: new FormControl({ value: false, disabled: true }),
                    isChecked: false,
                    item: new FormControl(this.item),
                    itemId: new FormControl(this.item.id),
                    measurementUnit: new FormControl(measurementUnit),
                    measurementUnitId: new FormControl(measurementUnit.id)
                }));
            }));
    }

    ngAfterViewInit(): void {
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        Observable.merge(this.measurementUnitSetupForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
                this.displayMessage = this.genericValidator.processMessages(this.measurementUnitSetupForm);
            });
    }

    updateBaseUnit(index: number) {
        var measurementUnitSetups = this.measurementUnitSetupForm.get('measurementUnitSetups') as FormArray;
        
        _.each(measurementUnitSetups.value,
            (measurementUnitSetup, i) => {
                if (i === index) {
                    this.baseUnitName = measurementUnitSetup.measurementUnit.name;
                    measurementUnitSetups.at(i).patchValue({ conversionRatio: 1, isBase: true, isChecked: true });
                    measurementUnitSetups.at(i).get('conversionRatio').disable();
                    measurementUnitSetups.at(i).get('sellingPrice').enable();
                    measurementUnitSetups.at(i).get('allowDiscount').enable();
                }
                else if (measurementUnitSetups.at(i).get('isChecked').value) {
                    measurementUnitSetups.at(i).patchValue({ isBase: false });
                    measurementUnitSetups.at(i).get('conversionRatio').enable();
                }
            });
    }

    resetDiscountFields(index: number) {
        var measurementUnitSetups = this.measurementUnitSetupForm.get('measurementUnitSetups') as FormArray;
        var allowDiscount: boolean = measurementUnitSetups.at(index).get('allowDiscount').value;

        if (allowDiscount) {
            measurementUnitSetups.at(index).get('discountPrice').enable();
        } else {
            measurementUnitSetups.at(index).get('discountPrice').disable();
            measurementUnitSetups.at(index).patchValue({discountPrice: 0 }); 
        }
        
    }

    updateSelection(index: number) {
        var measurementUnitSetups = this.measurementUnitSetupForm.get('measurementUnitSetups') as FormArray;
        var isChecked: boolean = measurementUnitSetups.at(index).get('isChecked').value;

        if (isChecked) {
            measurementUnitSetups.at(index).get('conversionRatio').enable();
            measurementUnitSetups.at(index).get('sellingPrice').enable();
            measurementUnitSetups.at(index).get('allowDiscount').enable();
        } else {

            let isUsed = measurementUnitSetups.at(index).get('isUsed').value;

            this.messages = [];
            if (isUsed) {
                measurementUnitSetups.at(index).patchValue({ isChecked: true });
                this.messages.push({ severity: 'error', summary: 'Error :', detail: 'This setup already in used.' });

            } else {
                measurementUnitSetups.at(index).patchValue({
                    isBase: false,
                    conversionRatio: 0,
                    sellingPrice: 0,
                    allowDiscount: false,
                    discountPrice: 0
                });
                measurementUnitSetups.at(index).get('conversionRatio').disable();
                measurementUnitSetups.at(index).get('sellingPrice').disable();
                measurementUnitSetups.at(index).get('allowDiscount').disable();
                measurementUnitSetups.at(index).get('discountPrice').disable();
            }
        }
    }

    saveMeasurementUnitSetup(): void {
        let measurementUnitSetupControls = this.measurementUnitSetupForm.get('measurementUnitSetups') as FormArray;

        if (this.measurementUnitSetupForm.dirty) {
            if (!_.some(measurementUnitSetupControls.getRawValue(), 'isBase')) {
                this.showMessage("error", "Error", "Select a base measurement unit.");
            } else {
                var currentBaseId: string = '';
                let observables = _.map(measurementUnitSetupControls.getRawValue(),
                    measurementUnitSetup => {
                        if (measurementUnitSetup.isChecked) {
                            if (measurementUnitSetup.isBase) {
                                measurementUnitSetup.id = measurementUnitSetup.id === undefined ? this.generateGuid() : measurementUnitSetup.id;
                                currentBaseId = measurementUnitSetup.id;
                            }
                            //if (measurementUnitSetup.id == this.previousBaseObject.id) this.previousBaseObject = measurementUnitSetup;

                            return this.measurementUnitSetupDataService.saveMeasurementUnitSetup(
                                <IMeasurementUnitSetup>{
                                    id: measurementUnitSetup.id,
                                    isBase: measurementUnitSetup.isBase,
                                    conversionRatio: measurementUnitSetup.conversionRatio,
                                    sellingPrice: measurementUnitSetup.sellingPrice,
                                    discountPrice: measurementUnitSetup.discountPrice,
                                    allowDiscount: measurementUnitSetup.allowDiscount,
                                    item: measurementUnitSetup.item,
                                    itemId: this.item.id,
                                    measurementUnit: measurementUnitSetup.measurementUnit,
                                    measurementUnitId: measurementUnitSetup.measurementUnitId
                                });
                        }
                        else
                            return this.measurementUnitSetupDataService.deleteMeasurementUnitSetup(measurementUnitSetup.id);
                    });

                Observable
                    .forkJoin(observables)
                    .subscribe((results) => {
                        this.onSaveComplete(results);
                        if (this.previousBaseObject !== undefined && currentBaseId != this.previousBaseObject.id)
                            this.baseWiseQuantityConversion(this.previousBaseObject, currentBaseId);
                    },
                    (error: any) => (error: any) => console.log(error));
            }
        } else if (!this.measurementUnitSetupForm.dirty) {
            this.onSaveComplete();
        }
    }

    baseWiseQuantityConversion(measurementUnitSetup: IMeasurementUnitSetup, currentBaseId: string): void {
        this.measurementUnitSetupDataService.baseWiseQuantityConversion(measurementUnitSetup.id, measurementUnitSetup.conversionRatio, currentBaseId)
            .subscribe();
    }
    
    onSaveComplete(results?: any[]): void {
        this.showMessage('success', 'Successful', 'Measurement units saved successfully');
        this.loadForm();
    }
    
    showMessage(severity, summary, details) {
        this.messages = [];
        this.messages.push({ severity: severity, summary: summary, detail: details });
    }

    generateGuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
}