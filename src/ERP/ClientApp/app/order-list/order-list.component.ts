﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IOrder } from '../view-models/order';

import { OrderDataService } from '../services/order-data.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng'

@Component({
    selector: 'order-list',
    templateUrl: 'order-list.component.html',
    styleUrls: ['./order-list.component.css'],
    providers: [ConfirmationService]
})

export class OrderListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    orders: Array<IOrder> = [];
    paymentStatusOptions: SelectItem[];
    orderStatusOptions: SelectItem[];
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;

    lazyLoadEvent: LazyLoadEvent;

    constructor(private orderDataService: OrderDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'orderDate', header: 'Order Date', filter: false},
            { field: 'expectedDeliveryDate', header: 'Expected Delivery Date', filter: false},
            { field: 'paymentStatus', header: 'Payment Status', filter: true },
            { field: 'orderStatus', header: 'Order Status', filter: true }    
        ];

        this.paymentStatusOptions = [
            { label: 'All', value: null },
            { label: 'Unpaid', value: 0 },
            { label: 'Partial Paid', value: 1},
            { label: 'Paid', value:2 }
        ];

        this.orderStatusOptions = [
            { label: 'All', value: null },
            { label: 'Pending', value: 0 },
            { label: 'Partial Delivered', value: 1},
            { label: 'Delivery Completed', value:2 }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols.slice(0, 5);
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadOrdersLazy(event: LazyLoadEvent) {
        this.lazyLoadEvent = event;
        this.loading = true;

        this.orderDataService.getPagedOrders(event)
            .subscribe(orders => {
                    this.totalRecords = orders['headers'].totalCount;
                    this.orders = orders['body'];
                    this.loading = false;
                },(error: any) => this.errorMessage = <any>error);
    }

    viewOrder(order: IOrder) {
        this.router.navigate(['/order-view', order.customer.id, order.id]);
    }

    editOrder(order: IOrder) {
        this.router.navigate(['/order-maint', order.customer.id, order.id]);
    }

    deleteOrder(order: IOrder): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.orderDataService.deleteOrder(order.customer.id, order.id)
                    .subscribe(
                        () => this.refreshDataTable(),
                        (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadOrdersLazy(this.lazyLoadEvent);
    }
    
    navigateToOrderAddPage() {
        this.router.navigate(['/order-maint']);
    }

    navigateToInvoiceAddPage(order: IOrder) {
        this.router.navigate(['/invoice-maint', order.moniker]);
    }

    navigateToBillAddPage(order: IOrder) {
        this.router.navigate(['/bill-maint', order.moniker]);
    }

}