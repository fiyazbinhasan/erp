import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IInventory } from '../view-models/inventory';
import { InventoryDataService } from '../services/inventory-data.service';
import { InventoryItemDataService } from '../services/inventory-item-data.service';
import { SelectItem } from '../shared/select-item';

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';

@Component({
    moduleId: 'AppModule',
    selector: 'app-inventory-list',
    templateUrl: 'inventory-list.component.html',
    styleUrls: ['./inventory-list.component.css'],
    providers: [ConfirmationService]
})

export class InventoryListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    inventories: Array<IInventory>;
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;

    lazyLoadEvent: LazyLoadEvent;

    constructor(private inventoryDataService: InventoryDataService,
        private inventoryItemDataService: InventoryItemDataService,
        private router: Router,
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'moniker', header: 'Code', filter: true },
            { field: 'purchaseDate', header: 'Purchase Date', filter: false },
            { field: 'description', header: 'Description', filter: false },
            { field: 'priorityLevel', header: 'Priority Level', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }

        this.cols = this.dtCols.slice(0, 3);
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadInventoriesLazy(event: LazyLoadEvent) {
        this.lazyLoadEvent = event;
        this.loading = true;

        this.inventoryDataService.getPagedInventories(event).subscribe(items => {
            this.totalRecords = items['headers'].totalCount;
            this.inventories = items['body'];
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editInventory(inventory: IInventory) {
        this.router.navigate(['/inventory-maint', inventory.supplier.id, inventory.id]);
    }

    navigateToInventoryAddPage() {
        this.router.navigate(['/inventory-maint']);
    }

    deleteInventory(inventory: IInventory): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.inventoryItemDataService.getItems(inventory.id)
                    .subscribe(inventoryItems => {
                        let observables = [];
                        observables = _.map(inventoryItems, inventoryItem => {
                            return this.inventoryItemDataService.deleteInventoryItem(inventoryItem.id);
                        });
                        observables.push(this.inventoryDataService.deleteInventory(inventory.supplier.id, inventory.id));
                        Observable.concat(...observables).subscribe(() => {
                            this.refreshDataTable();
                        }, (error: any) =>
                            { this.errorMessage = <any>error });

                    }, (error: any) => this.errorMessage = <any>error);
            }
        });
    }

    viewInventory(inventory: IInventory) {
        this.router.navigate(['/inventory-view', inventory.supplier.id, inventory.id]);
    }

    refreshDataTable() {
        this.loadInventoriesLazy(this.lazyLoadEvent);
    }
}