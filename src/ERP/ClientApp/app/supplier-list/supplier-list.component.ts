import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { ISupplier } from '../view-models/supplier'
import { SelectItem } from '../shared/select-item'
import { SupplierDataService } from '../services/supplier-data.service'

import { ConfirmationService } from 'primeng/primeng';
import { LazyLoadEvent } from 'primeng/primeng';

@Component({
    selector: 'supplier-list',
    templateUrl: 'supplier-list.component.html',
    styleUrls: ['./supplier-list.component.css'],
    providers: [ConfirmationService]
})

export class SupplierListComponent implements OnInit {
    errorMessage: string;

    cols: any[];
    dtCols: any[];
    columnOptions: SelectItem[];

    countries: SelectItem[];
    suppliers: Array<ISupplier>;
    totalRecords: number = 0;
    loading: boolean;
    stacked: boolean;

    lazyLoadEvent: LazyLoadEvent;

    constructor(private supplierDataService: SupplierDataService, 
        private router: Router, 
        private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.dtCols = [
            { header: 'First Name', field: 'firstName', filter: true },
            { header: 'Last Name', field: 'lastName', filter: true },
            { header: 'Company', field: 'companyName', filter: false },
            { header: 'Phone', field: 'phone', filter: true },
            { header: 'Primary Address', field: 'address1' },
            { header: 'City', field: 'city', filter: true },
            { header: 'Country', field: 'country', filter: true },
            { header: 'Postal Code', field: 'postalCode', filter: false },
            { header: 'Email', field: 'email', filter: true },
            { header: 'Website', field: 'website', filter: false }
        ];

        this.columnOptions = [];

        for (let i = 0; i < this.dtCols.length; i++) {
            this.columnOptions.push({ label: this.dtCols[i].header, value: this.dtCols[i] });
        }
        this.cols = this.dtCols.slice(0, 5);

        this.countries = this.supplierDataService.countries;
    }

    isVisible(dtCol) {
        for (let col of this.cols) {
            if (col.field === dtCol.field) {
                return true;
            }
        }

        return false;
    }

    toggleView() {
        this.stacked = !this.stacked;
    }

    loadSuppliersLazy(event: LazyLoadEvent) {
        this.loading = true;
        this.lazyLoadEvent = event;

        this.supplierDataService.getPagedSuppliers(event).subscribe(suppliers => {
            this.totalRecords = suppliers['headers'].totalCount;
            this.suppliers = suppliers['body']; 
            this.loading = false;
        }, (error: any) => this.errorMessage = <any>error);
    }

    editSupplier(supplier: ISupplier) {
        this.router.navigate(['/supplier-maint', supplier.id]);
    }
    viewSupplier(supplier: ISupplier) {
        this.router.navigate(['/supplier-view', supplier.id]);
    }

    navigateToSupplierAddPage() {
        this.router.navigate(['/supplier-maint']);
    }

    deleteSupplier(supplier: ISupplier): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                this.supplierDataService.deleteSupplier(supplier.id)
                    .subscribe(
                    () => this.refreshDataTable(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        });
    }

    refreshDataTable() {
        this.loadSuppliersLazy(this.lazyLoadEvent);
    }
}