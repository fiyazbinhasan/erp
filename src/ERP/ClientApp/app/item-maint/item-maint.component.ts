import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { SelectItem, Message } from 'primeng/primeng';
import { IItem } from '../view-models/item';
import { ItemDataService } from '../services/item-data.service';
import { CategoryDataService } from '../services/category-data.service';
import { GenericValidator } from '../shared/generic-validator';

@Component({
    selector: 'item-maint',
    templateUrl: 'item-maint.component.html',
    styleUrls: ['./item-maint.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class ItemMaintComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
    
    headline: string = 'Edit and configure Item';
    paragraph: string = 'Toggle between the tabs to edit the existing item and it\'s measurement unit(s)';
    messages: Message[];

    errorMessage: string;
    public itemForm: FormGroup;

    item: IItem;
    categories: Array<SelectItem> = [];
    
    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    private sub: Subscription;

    constructor(private fb: FormBuilder,
        private itemDataService: ItemDataService,
        private categoryDataService: CategoryDataService,
        private route: ActivatedRoute,
        private router: Router) {
        this.validationMessages = {
            name: {
                required: 'Name is required.',
                minlength: 'Name must be at least 3 characters.',
                maxlength: 'Name cannot exceed 50 characters.'
            },
            category: {
                required: 'Category is required'
            }
        };

        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit() {
        this.itemForm = this.fb.group({
            name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            description: [''],
            category: ['', [Validators.required]]
        });

        this.categoryDataService.getCategories().subscribe(
            (categories) => {
                categories.map((category) => {
                    this.categories.push(<SelectItem>{ label: category.name, value: category });
                });
            }
        );
        
        this.sub = this.route.params.subscribe(
            params => {
                let categoryId = params['categoryId'];
                let id = params['id'];
                this.getItem(categoryId, id);
            }
        );
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.itemForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.itemForm);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    saveItem(): void {
        if (this.itemForm.dirty && this.itemForm.valid) {
            let i = Object.assign({}, this.item, this.itemForm.value);

            this.itemDataService.saveItem(i)
                .subscribe((item) => {
                    this.item = item;
                    this.onSaveComplete();
                },
                (error: any) => {
                    this.showMessage('error', 'Error', `Could not save item. ${error}`);
                });

        } else if (!this.itemForm.dirty) {
            this.onSaveComplete();
        }
    }

    getItem(categoryId: string, id: string): void {
        if (id === undefined && categoryId === undefined) {
            this.headline = 'Add and configure Item';
            this.paragraph = 'Toggle between the tabs to add a new item and it\'s measurement unit(s)';
        } else {
            this.itemDataService.getItem(categoryId, id)
                .subscribe(
                (item: IItem) => this.onItemRetrieved(item),
                (error: any) => {
                    this.showMessage('error', 'Error', `Could not get items. ${error}`);
                });
        }
    }

    onItemRetrieved(item: IItem): void {
        if (this.itemForm) {
            this.itemForm.reset();
        }

        this.item = item;
        // Update the data on the form
        this.itemForm.patchValue({
            name: this.item.name,
            description: this.item.description,
            category: this.item.category
        });
    }

    onSaveComplete(): void {
        this.showMessage('success', 'Successful', 'Item saved successfully');
    }

    showMessage(severity, summary, details) {
        this.messages = [];
        this.messages.push({ severity: severity, summary: summary, detail: details });
    }
}