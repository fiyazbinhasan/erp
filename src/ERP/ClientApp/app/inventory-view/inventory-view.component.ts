﻿import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { InventoryDataService } from '../services/inventory-data.service';
import { InventoryItemDataService } from '../services/inventory-item-data.service';

import { IInventory } from '../view-models/inventory';
import { IInventoryItem } from '../view-models/inventory-item';

import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-inventory-view',
    templateUrl: 'inventory-view.component.html',
    styleUrls: ['./inventory-view.component.css']
})

export class InventoryViewComponent implements OnInit {

    dtCols: any[];
    inventory: any = {
        id: '',
        moniker: '',
        supplier: {
            id: '',
            firstName: '',
            lastName: '',
            address1: '',
            phone: '',
            email: '',
        },
        purchesDate: ''
    };

    totalInventoryAmount: number = 0;
    inventoryItems: Array<any>;
    loading: boolean;


    constructor(private inventoryDataService: InventoryDataService,
        private inventoryItemDataService: InventoryItemDataService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.dtCols = [
            { field: 'measurementUnitSetup.item.name', header: 'Item' },
            { field: 'warehouse.moniker', header: 'Warehouse' },
            { field: 'issuedQuantity', header: 'Quantity' },
            { field: 'measurementUnitSetup.measurementUnit.name', header: 'M.Unit' },
            { field: 'costPerUnit', header: 'U.Price' },
            { field: 'amount', header: 'Amount' },
            { field: 'remarks', header: 'Remarks' }
        ];

        this.route.params.subscribe(
            params => {
                let supplierId = params['supplierId'];
                let id = params['id'];
                this.retrieveData(supplierId, id);
            }
        );
    }

    retrieveData(supplierId: string, inventoryId: string): void {
        this.loading = true;
        Observable
            .forkJoin([
                this.inventoryDataService.getInventory(supplierId, inventoryId),
                this.inventoryItemDataService.getItems(inventoryId)
            ])
            .subscribe(results => {
                this.onInventoryRetrieved(results[0]);
                this.onInventoryItemsRetrieved(results[1])
            });
    }

    onInventoryRetrieved(inventory): void {
        this.inventory = inventory;
        this.inventory.purchaseDate = new Date(inventory.purchaseDate).toDateString();
    }

    onInventoryItemsRetrieved(inventoryItems): void {
        this.inventoryItems = inventoryItems;
        _.each(inventoryItems, inventoryItem => {
            inventoryItem.amount = inventoryItem.issuedQuantity * inventoryItem.costPerUnit;
            this.totalInventoryAmount += inventoryItem.amount;
        });
        this.loading = false;
    }

    generatePdf() {
        this.inventoryDataService.generatePdf(this.inventory.supplier.id, this.inventory.id)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/pdf' });
                FileSaver.saveAs(blob, this.inventory.moniker + ".pdf");
            },err => console.error(err));
    }

    editInventory() {
        this.router.navigate(['/inventory-maint', this.inventory.supplier.id, this.inventory.id]);
    }
}