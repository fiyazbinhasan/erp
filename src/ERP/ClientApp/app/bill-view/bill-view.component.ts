﻿import { Component, OnInit} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

import { BillDataService } from '../services/bill-data.service';
import { IBill } from '../view-models/bill';

import * as FileSaver from 'file-saver';

@Component({
    moduleId: 'AppModule',
    selector: 'app-bill-view',
    templateUrl: 'bill-view.component.html',
    styleUrls: ['./bill-view.component.css']
})

export class BillViewComponent implements OnInit {
    bill: any = {
        id:'',
        moniker: '',
        billDate: '',
        paidAmount: 0,
        paymentMethod: '',
        order: {
            id:'',
            moniker: '',
            customer: {
                firstName: '',
                lastName: '',
                address1: '',
                phone: '',
                email: ''
            }
        }
    };

    constructor(private billDataService: BillDataService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                let orderId = params['orderId'];
                let customerId = params['customerId'];
                let id = params['id'];

                this.getBill(customerId, orderId, id);
            }
        );
    }

    getBill(customerId: string, orderId: string, id: string): void {
        this.billDataService.getBill(customerId, orderId, id).subscribe(bill => {
            this.bill = bill;
        });
    }

    generatePdf() {
        this.billDataService.generatePdf(this.bill.order.customer.id, this.bill.order.id, this.bill.id)
            .subscribe(
            data => {
                var blob = new Blob([data], { type: 'application/pdf' });
                FileSaver.saveAs(blob, this.bill.moniker + ".pdf");
            }, err => console.error(err));
    }

    editBill(){
        this.router.navigate(['/bill-maint', this.bill.order.customer.id, this.bill.order.id, this.bill.id]);
    }

    viewOrder(){
        this.router.navigate(['/order-view', this.bill.order.customer.id, this.bill.order.id])
    }
}