﻿using System.Threading.Tasks;

namespace ERP.Services.Interfaces
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
