﻿namespace ERP.Services.Interfaces
{
    public interface ITypeHelperService
    {
        bool TypeHasProperties<T>(string fields);
        bool TypeHasProperty<T>(string orderBy);
    }
}
