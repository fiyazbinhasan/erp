﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Model.Common;
using static ERP.Model.Common.ApplicationEnums;
using EntityFrameworkCore.Triggers;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Model.Inventory
{
    public class Order
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        private string _moniker;
        public string Moniker
        {
            get => _moniker;
            set => _moniker = string.IsNullOrEmpty(value) ? GlobalMethods.GenerateMoniker("ORD") : value;
        }
        public DateTime OrderDate { get; set; } = DateTime.Now;
        public DateTime ExpectedDeliveryDate { get; set; }
        public PaymentStatus PaymentStatus { get; set; } = PaymentStatus.Unpaid;
        public OrderStatus OrderStatus { get; set; } = OrderStatus.Pending;
        public Customer Customer { get; set; }
        public Guid CustomerId { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }
        public IEnumerable<Bill> Bills { get; set; }
        public IEnumerable<Invoice> Invoices { get; set; }
        
        static Order()
        {
            Triggers<Bill>.Inserted += e =>
            {
                PaymentStatusChangeNotifierAsync(e);
            };

            Triggers<Bill>.Deleted += e =>
            {
                PaymentStatusChangeNotifierAsync(e);
            };

            Triggers<Bill>.Updated += e =>
            {
                PaymentStatusChangeNotifierAsync(e);
            };
        }

        private static void PaymentStatusChangeNotifierAsync(IEntry<Bill, Microsoft.EntityFrameworkCore.DbContext> e)
        {
            var orderedItems = e.Context.Entry(e.Entity.Order)
                                .Collection(o => o.OrderItems).Query()
                                .Where(wi => wi.OrderId == e.Entity.OrderId);

            var paidAmountSumation = e.Entity.Order.Bills.Sum(b => b.PaidAmount);

            var orderedItemAmountSumation = orderedItems.Sum(oi => oi.Quantity * (oi.UnitPrice - oi.Discount));

            if (paidAmountSumation == 0)
                e.Entity.Order.PaymentStatus = PaymentStatus.Unpaid;
            else if (paidAmountSumation == orderedItemAmountSumation)
                e.Entity.Order.PaymentStatus = PaymentStatus.Paid;
            else
                e.Entity.Order.PaymentStatus = PaymentStatus.PartialPaid;

            e.Context.SaveChanges();
        }
    }
}
