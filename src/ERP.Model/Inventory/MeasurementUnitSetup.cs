﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.Model.Inventory
{
    public class MeasurementUnitSetup
    {
        public Guid Id { get; set; }
        public Item Item { get; set; }
        public Guid ItemId { get; set; }
        public MeasurementUnit MeasurementUnit { get; set; }
        public Guid MeasurementUnitId { get; set; }
        public bool IsBase { get; set; }
        public decimal ConversionRatio { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountPrice { get; set; }
        public bool AllowDiscount { get; set; }

        [NotMapped]
        public bool IsUsed { get; set; }
       
        public ICollection<InventoryItem> InventoryItems { get; set; }
        public ICollection<InvoiceItem> InvoiceItems { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
        public ICollection<WarehouseItem> WarehouseItems { get; set; }

        public MeasurementUnitSetup Clone()
        {
            return (MeasurementUnitSetup)MemberwiseClone();
        }
    }
}
