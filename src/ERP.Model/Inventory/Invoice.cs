﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Model.Common;
using System.Collections.Generic;
using EntityFrameworkCore.Triggers;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ERP.Model.Inventory
{
    public class Invoice
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        private string _moniker;

        public string Moniker
        {
            get => _moniker;
            set => _moniker = string.IsNullOrEmpty(value) ? GlobalMethods.GenerateMoniker("INVC") : value;
        }

        public DateTime InvoiceDate { get; set; } = DateTime.Now;
        public string Description { get; set; }
        public Guid OrderId { get; set; }
        public Order Order { get; set; }
        public IEnumerable<InvoiceItem> InvoiceItems { get; set; }
    }
}
