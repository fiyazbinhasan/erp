﻿using EntityFrameworkCore.Triggers;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ERP.Model.Inventory
{
    public class InventoryItem
    {
        public Guid Id { get; set; }

        public Guid InventoryId { get; set; }
        public Inventory Inventory { get; set; }

        public int IssuedQuantity { get; set; }
        public decimal CostPerUnit { get; set; }
        public string Remarks { get; set; }

        public Guid WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }

        public Guid MeasurementUnitSetupId { get; set; }
        public MeasurementUnitSetup MeasurementUnitSetup { get; set; }

        [NotMapped]
        public int AdjustedQuantity { get; set; }

        public InventoryItem Clone()
        {
            return (InventoryItem)MemberwiseClone();
        }

        static InventoryItem()
        {
            Triggers<InventoryItem>.Deleted += e =>
            {
                decimal convertedQuantity = e.Entity.MeasurementUnitSetup.ConversionRatio * e.Entity.IssuedQuantity;

                var warehouseItem = e.Context.Entry(e.Entity.Warehouse)
                                        .Collection(o => o.WarehouseItems)
                                        .Query().SingleOrDefault(wi => wi.WarehouseId == e.Entity.WarehouseId && wi.MeasurementUnitSetup.ItemId == e.Entity.MeasurementUnitSetup.ItemId);

                if (warehouseItem.Quantity - convertedQuantity >= 0)
                    warehouseItem.Quantity -= convertedQuantity;

                e.Context.SaveChanges();
            };

            Triggers<InventoryItem>.Updated += e =>
            {
                var convertedQuantity = e.Entity.MeasurementUnitSetup.ConversionRatio * e.Entity.AdjustedQuantity;

                var warehouseItem = e.Context.Entry(e.Entity.Warehouse)
                            .Collection(o => o.WarehouseItems)
                            .Query().SingleOrDefault(wi => wi.MeasurementUnitSetup.ItemId == e.Entity.MeasurementUnitSetup.ItemId);

                if (warehouseItem.Quantity + convertedQuantity >= 0)
                    warehouseItem.Quantity += convertedQuantity;

                e.Context.SaveChanges();
            };
        }
    }
}
