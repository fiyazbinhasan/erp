﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkCore.Triggers;
using ERP.Model.Common;

namespace ERP.Model.Inventory
{
    public class InvoiceItem
    {
        public Guid Id { get; set; }

        public Invoice Invoice { get; set; }
        public Guid InvoiceId { get; set; }

        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }

        public Warehouse Warehouse { get; set; }
        public Guid WarehouseId { get; set; }

        public MeasurementUnitSetup MeasurementUnitSetup { get; set; }
        public Guid MeasurementUnitSetupId { get; set; }

        public InvoiceItem Clone()
        {
            return (InvoiceItem)MemberwiseClone();
        }

        static InvoiceItem()
        {
            Triggers<InvoiceItem>.Deleted += e =>
            {
                decimal convertedBaseQuantity = e.Entity.MeasurementUnitSetup.ConversionRatio * e.Entity.Quantity;
                var warehouseItem = e.Context.Entry(e.Entity.Warehouse).Collection(o => o.WarehouseItems)
                    .Query()
                    .SingleOrDefault(wi => wi.WarehouseId == e.Entity.WarehouseId &&
                                           wi.MeasurementUnitSetup.ItemId == e.Entity.MeasurementUnitSetup.ItemId);

                warehouseItem.Quantity += convertedBaseQuantity;

                var orderedItems = e.Context.Entry(e.Entity.Invoice.Order)
                    .Collection(o => o.OrderItems).Query();

                var orderItem = orderedItems.SingleOrDefault(oi => oi.MeasurementUnitSetup.ItemId == e.Entity.MeasurementUnitSetup.ItemId);

                if (orderItem.LeftQuantity + e.Entity.Quantity <= orderItem.Quantity)
                    orderItem.LeftQuantity += e.Entity.Quantity;

                if (All(orderedItems, oi => oi.Quantity == oi.LeftQuantity))
                    e.Entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.Pending;
                else if (All(orderedItems, oi => oi.LeftQuantity == 0))
                    e.Entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.DeliveryCompleted;
                else
                    e.Entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.PartialDelivered;

                e.Context.SaveChanges();
            };

            Triggers<InvoiceItem>.Inserted += e =>
            {
                    decimal convertedBaseQuantity = e.Entity.MeasurementUnitSetup.ConversionRatio * e.Entity.Quantity;

                    var warehouseItem = e.Context.Entry(e.Entity.Warehouse).Collection(o => o.WarehouseItems)
                        .Query()
                        .SingleOrDefault(wi => wi.WarehouseId == e.Entity.WarehouseId &&
                                               wi.MeasurementUnitSetup.ItemId == e.Entity.MeasurementUnitSetup.ItemId);

                    if (warehouseItem.Quantity - convertedBaseQuantity >= 0)
                        warehouseItem.Quantity -= convertedBaseQuantity;

                    var orderedItems = e.Context.Entry(e.Entity.Invoice.Order)
                        .Collection(o => o.OrderItems).Query();

                    var orderItem = orderedItems.SingleOrDefault(oi => oi.MeasurementUnitSetup.ItemId == e.Entity.MeasurementUnitSetup.ItemId);

                    if (orderItem.LeftQuantity - e.Entity.Quantity >= 0)
                        orderItem.LeftQuantity -= e.Entity.Quantity;

                    if (All(orderedItems, oi => oi.Quantity == oi.LeftQuantity))
                        e.Entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.Pending;
                    else if (All(orderedItems, oi => oi.LeftQuantity == 0))
                        e.Entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.DeliveryCompleted;
                    else
                        e.Entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.PartialDelivered;

                    e.Context.SaveChanges();
            };
        }
        public static bool All(IEnumerable<OrderItem> source, Func<OrderItem, bool> predicate)
        {
            return source.All(predicate);
        }
    }
}
