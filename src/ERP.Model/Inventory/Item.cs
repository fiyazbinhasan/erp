﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ERP.Model.Inventory
{
    public class Item
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string SerialNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public IEnumerable<WarehouseItem> WarehouseItems { get; set; }
        public IEnumerable<MeasurementUnitSetup> MeasurementUnitSetups { get; set; }
    }
}
