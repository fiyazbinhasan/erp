﻿using System;

namespace ERP.Model.Inventory
{
    public class WarehouseItem
    {
        public Guid Id { get; set; }

        public Warehouse Warehouse { get; set; }
        public Guid WarehouseId { get; set; }

        public decimal Quantity { get; set; }

        public MeasurementUnitSetup MeasurementUnitSetup { get; set; }

        public Guid MeasurementUnitSetupId { get; set; }
    }
}
