﻿using System;
using System.Collections.Generic;
using ERP.Model.Common;

namespace ERP.Model.Inventory
{
    public class Inventory
    {
        public Guid Id { get; set; }
        private string _moniker;

        public string Moniker
        {
            get => _moniker;
            set => _moniker = string.IsNullOrEmpty(value) ? GlobalMethods.GenerateMoniker("INVT") : value;
        }
       
        public int PriorityLevel { get; set; } = 1;
        public DateTime PurchaseDate { get; set; } = DateTime.Now;
        public string Description { get; set; }
        public Guid SupplierId { get; set; }
        public Supplier Supplier { get; set; }
        public IEnumerable<InventoryItem> InventoryItems { get; set; }
    }
}
