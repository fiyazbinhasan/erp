﻿using System;
using System.Collections.Generic;

namespace ERP.Model.Inventory
{
    public class Warehouse
    {
        public Guid Id { get; set; }
        public string Moniker { get; set; } 
        public string Address { get; set; }
        public IEnumerable<WarehouseItem> WarehouseItems { get; set; }
    }
}
