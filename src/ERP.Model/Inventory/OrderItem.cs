﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace ERP.Model.Inventory
{
    public class OrderItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Order Order { get; set; }
        public Guid OrderId { get; set; }

        public int Quantity { get; set; }
        public int LeftQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public string Remarks { get; set; }

        public Guid MeasurementUnitSetupId { get; set; }
        public MeasurementUnitSetup MeasurementUnitSetup { get; set; }
    }
}
