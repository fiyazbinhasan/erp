﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ERP.Model.Common;
using static ERP.Model.Common.ApplicationEnums;

namespace ERP.Model.Inventory
{
    public class Bill
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        private string _moniker;

        public string Moniker
        {
            get => _moniker;
            set => _moniker = string.IsNullOrEmpty(value) ? GlobalMethods.GenerateMoniker("BILL") : value;
        }
        public decimal PaidAmount { get; set; }
        public PaymentMethod PaymentMethod { get; set; } = PaymentMethod.Cash;
        public DateTime BillDate { get; set; } = DateTime.Now;

        public Order Order { get; set; }
        public Guid OrderId { get; set; }
    }
}
