﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Model.Inventory
{
    public class MeasurementUnit
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }
        public IEnumerable<MeasurementUnitSetup> MeasurementUnitSetups { get; set; }
    }
}
