﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Model.Common
{
    public class ApplicationEnums
    {
        public enum ResourceUriType
        {
            Previous,
            Next
        }

        public enum PaymentMethod
        {
            Cash = 0,
            Card = 1
        }

        public enum PaymentStatus
        {
            Unpaid = 0,
            PartialPaid = 1,
            Paid = 2
        }

        public enum OrderStatus
        {
            Pending = 0,
            PartialDelivered = 1,
            DeliveryCompleted = 2
        }
    }
}


