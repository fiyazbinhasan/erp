﻿using System;

namespace ERP.Model.Common
{
    public static class GlobalMethods
    {
        public static string GenerateMoniker(string prefix)
        {
            DateTime date = DateTime.Now;
            return (!string.IsNullOrEmpty(prefix) ? prefix + "-" : "") + date.ToString("yy")
                   + date.ToString("MM")
                   + date.ToString("dd")
                   + date.ToString("mm")
                   + date.ToString("ss")
                   + date.ToString("ff");
        }
    }
}
