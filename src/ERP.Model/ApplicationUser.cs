﻿using Microsoft.AspNetCore.Identity;

namespace ERP.Model
{
    public class ApplicationUser : IdentityUser
    {
    }
}
