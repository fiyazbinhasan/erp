﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data.Repository
{
    public class InvoiceRepository : GenericRepository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(ApplicationDbContext context) : base(context)
        {
        }

        public override Invoice Get(Guid invoiceId)
        {
            return DbSet
                .Include(o => o.Order)
                .Include(o => o.Order.Customer)
                .FirstOrDefault(o => o.Id == invoiceId);
        }

        public PagedList<Invoice> Get(InvoiceResourceParameters parameters, string customerPhone, string orderMoniker)
        {
            IQueryable<Invoice> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.Moniker))
                query = query.Where(s => s.Moniker.ToLowerInvariant()
                    .Equals(parameters.Moniker.Trim().ToLowerInvariant()));

            foreach (var includeProperty in parameters.Includes.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (customerPhone != "all")
                query = query.Where(i => i.Order.Customer.Phone.Equals(customerPhone));
            if (orderMoniker != "all")
                query = query.Where(i => i.Order.Moniker.Equals(orderMoniker));

            if (!string.IsNullOrEmpty(parameters.SearchQuery) && parameters.SearchQuery != "null")
                query = query.Where(i => i.Moniker.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.Description.ToLower().Contains(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Invoice>.Create(query, parameters.PageNumber,
                    parameters.PageSize);

            return PagedList<Invoice>.Create(query.OrderBy(parameters.OrderBy), parameters.PageNumber,
                parameters.PageSize);
        }

        public IQueryable<Invoice> GetOrderInvoices(Guid orderId)
        {
            return DbSet.Where(i => i.OrderId == orderId);
        }

        public Invoice Update(Invoice entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
