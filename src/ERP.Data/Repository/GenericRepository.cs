﻿using System;
using System.Threading.Tasks;
using ERP.Data.Helpers;
using ERP.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;
using IdentityModel.Client;
using Microsoft.Extensions.Caching.Memory;

namespace ERP.Data.Repository
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity: class
    {
        public ApplicationDbContext Context { get; }
        public DbSet<TEntity> DbSet { get; }
        public GenericRepository(ApplicationDbContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }
        public virtual IQueryable<TEntity> Get()
        {
            return DbSet;
        }
        public virtual TEntity Get(Guid id)
        {
            return DbSet.Find(id);
        }

        public virtual void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }
        public virtual void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }
        public async Task<bool> SaveChangesAsync()
        {
            try
            {
                return await Context.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string AddETag(TEntity entity, IMemoryCache cache)
        {
            var etag = Convert.ToBase64String(Context.Entry(entity).Property("Version").CurrentValue as byte[]);
            var id = Context.Entry(entity).Property("Id").CurrentValue;
            var entityType = entity.GetType().ToString().Split('.').Last();
            cache.Set($"{entityType}-{id}-{etag}", entity);
            return etag;
        }

        public byte[] GetVersion(TEntity entity)
        {
            return Context.Entry(entity).Property("Version").CurrentValue as byte[];
        }

    }
}
