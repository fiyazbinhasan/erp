﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data.Repository
{
    public class InvoiceItemRepository : GenericRepository<InvoiceItem>, IInvoiceItemRepository
    {
        ApplicationDbContext _context;
        public InvoiceItemRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public override IQueryable<InvoiceItem> Get()
        {
            return DbSet.Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Include(o => o.Invoice)
                .Include(o => o.Warehouse) ;
        }

        public override InvoiceItem Get(Guid id)
        {
            return DbSet.Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Include(o => o.Invoice)
                .Include(o => o.Invoice.Order)
                .Include(o => o.Warehouse)
                .SingleOrDefault(ii => ii.Id == id);
        }

        public IQueryable<InvoiceItem> GetInvoices(Guid itemId)
        {
            return DbSet.Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Invoice)
                .Include(s => s.Invoice.Order)
                .Include(o => o.Invoice.Order.Customer)
                .Include(o => o.Warehouse)
                .Where(i => i.MeasurementUnitSetup.Item.Id == itemId);
        }

        public IQueryable<InvoiceItem> GetItems(Guid invoiceId)
                
        {
            return DbSet.Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Invoice)
                .Include(o => o.Invoice.Order)
                .Include(oi => oi.Invoice.Order.OrderItems)
                .ThenInclude(m=>m.MeasurementUnitSetup)
                .Include(o => o.Invoice.Order.Customer)
                .Include(o => o.Warehouse)
                .Where(i => i.Invoice.Id == invoiceId);
        }

        public InvoiceItem Update(InvoiceItem entity, InvoiceItem prevEntity)
        {
            var convertedQuantity = prevEntity.MeasurementUnitSetup.ConversionRatio * prevEntity.Quantity;
            Context.Entry(prevEntity.Warehouse).Collection(b => b.WarehouseItems).Query().SingleOrDefault(wi => wi.MeasurementUnitSetup.ItemId == prevEntity.MeasurementUnitSetup.ItemId).Quantity += convertedQuantity;

            convertedQuantity = entity.MeasurementUnitSetup.ConversionRatio * entity.Quantity;
            Context.Entry(entity.Warehouse).Collection(b => b.WarehouseItems).Query().SingleOrDefault(wi => wi.MeasurementUnitSetup.ItemId == entity.MeasurementUnitSetup.ItemId).Quantity -= convertedQuantity;

            var prevOrderItem = Context.Entry(prevEntity.Invoice.Order).Collection(b => b.OrderItems).Query().SingleOrDefault(oi => oi.MeasurementUnitSetup.ItemId == prevEntity.MeasurementUnitSetup.ItemId);

            if (prevOrderItem.LeftQuantity + prevEntity.Quantity <= prevOrderItem.Quantity)
                 prevOrderItem.LeftQuantity += prevEntity.Quantity;

            var orderedItems = Context.Entry(entity.Invoice.Order).Collection(b => b.OrderItems).Query();
            var orderItem = orderedItems.SingleOrDefault(oi => oi.MeasurementUnitSetup.ItemId == entity.MeasurementUnitSetup.ItemId);

            if (orderItem.LeftQuantity - entity.Quantity >= 0)
                orderItem.LeftQuantity -= entity.Quantity;

            if (All(orderedItems, oi => oi.Quantity == oi.LeftQuantity))
                entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.Pending;
            else if (All(orderedItems, oi => oi.LeftQuantity == 0))
                entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.DeliveryCompleted;
            else
                entity.Invoice.Order.OrderStatus = ApplicationEnums.OrderStatus.PartialDelivered;

            return DbSet.Update(entity).Entity;
        }

        public bool All(IEnumerable<OrderItem> source, Func<OrderItem, bool> predicate)
        {
            return source.All(predicate);
        }
    }
}
