﻿using System;
using System.Linq;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace ERP.Data.Repository
{
    public class OrderItemRepository : GenericRepository<OrderItem>, IOrderItemRepository
    {
        ApplicationDbContext _context;
        public OrderItemRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public override OrderItem Get(Guid id)
        {
            return DbSet.Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Include(o => o.Order)
                .SingleOrDefault(ii => ii.Id == id);
        }

        public override IQueryable<OrderItem> Get()
        {
            return DbSet.Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Include(o => o.Order);
        }

        public IQueryable<OrderItem> GetItems(Guid orderId)
        {
            var orderItems = DbSet.Include(ii => ii.Order)
                .Include(s => s.Order.Customer)
                .Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Where(i => i.Order.Id == orderId);

            foreach (var orderItem in orderItems)
            {
                orderItem.MeasurementUnitSetup.IsUsed = true;
            }
            return orderItems;
        }

        public IQueryable<OrderItem> GetOrders(Guid itemId)
        {
            return DbSet.Include(ii => ii.Order)
                .Include(s => s.Order.Customer)
                .Include(m => m.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(i => i.MeasurementUnitSetup.Item.Category)
                .Where(i => i.MeasurementUnitSetup.Item.Id == itemId);
        }

        public OrderItem Update(OrderItem entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
