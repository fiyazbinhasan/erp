﻿using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace ERP.Data.Repository
{
    public class WarehouseRepository : GenericRepository<Warehouse>, IWarehouseRepository
    {
        public WarehouseRepository(ApplicationDbContext context) : base(context)
        {
        }

        public PagedList<Warehouse> Get(WarehouseResourceParameters parameters)
        {
            IQueryable<Warehouse> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.Moniker))
                query = query.Where(c => c.Moniker.ToLowerInvariant().Contains(parameters.Moniker.Trim().ToLowerInvariant()));
            
            foreach (var includeProperty in parameters.Includes.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (!string.IsNullOrEmpty(parameters.SearchQuery))
                query = query.Where(i => i.Moniker.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.Address.ToLower().Contains(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Warehouse>.Create(query, parameters.PageNumber,
                    parameters.PageSize);

            return PagedList<Warehouse>.Create(query.OrderBy(parameters.OrderBy), parameters.PageNumber, parameters.PageSize);
        }

        public Warehouse Update(Warehouse entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
