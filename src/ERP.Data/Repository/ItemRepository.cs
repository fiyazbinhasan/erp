﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data.Repository
{
    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ItemRepository(ApplicationDbContext context) : base(context)
        {
        }

        public override Item Get(Guid itemId)
        {
            return DbSet
                .Include(i => i.Category)
                .Include(m => m.MeasurementUnitSetups)
                .FirstOrDefault(s => s.Id == itemId);
        }

        public PagedList<Item> Get(ItemResourceParameters parameters, string categoryName)
        {
            IQueryable<Item> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.Name))
                query = query.Where(c => c.Name.ToLowerInvariant().Contains(parameters.Name.Trim().ToLowerInvariant()));

            
            foreach (var includeProperty in parameters.Includes.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (categoryName != "all")
                query = query.Where(i => i.Category.Name == categoryName);

            if (!string.IsNullOrEmpty(parameters.Category))
                query = query.Where(c => c.Category.Name.ToLowerInvariant().Contains(parameters.Category.Trim().ToLowerInvariant()));

            if (!string.IsNullOrEmpty(parameters.SearchQuery))
                query = query.Where(i => i.Name.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.Description.ToLower().Contains(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Item>.Create(query, parameters.PageNumber,
                    parameters.PageSize);

            return PagedList<Item>.Create(query.OrderBy(parameters.OrderBy), parameters.PageNumber,
                parameters.PageSize);
        }

        public Item Update(Item entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
