﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace ERP.Data.Repository
{
    public class MeasurementUnitSetupRepository : GenericRepository<MeasurementUnitSetup>, IMeasurementUnitSetupRepository
    {
        public MeasurementUnitSetupRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<MeasurementUnitSetup> GetMeasurementUnits(Guid itemId)
        {
            var measurementUnitSetups = DbSet.Include(m => m.MeasurementUnit)
                .Include(i => i.Item)
                .Include(ic => ic.Item.Category)
                .Include(ii => ii.InventoryItems)
                .Include(ii => ii.InvoiceItems)
                .Include(oi => oi.OrderItems)
                .Include(wi => wi.WarehouseItems)
                .Where(i => i.ItemId == itemId);

            foreach (var measurementUnitSetup in measurementUnitSetups)
            {
                if (measurementUnitSetup.InventoryItems.Count > 0
                    || measurementUnitSetup.InvoiceItems.Count > 0
                    || measurementUnitSetup.OrderItems.Count > 0
                    || measurementUnitSetup.WarehouseItems.Count > 0)
                {
                    measurementUnitSetup.IsUsed = true;
                }
                else
                    measurementUnitSetup.IsUsed = false;
            }
            return measurementUnitSetups;
        }

        public MeasurementUnitSetup Update(MeasurementUnitSetup entity)
        {
            return DbSet.Update(entity).Entity;
        }

        public MeasurementUnitSetup BaseWiseQuantityConversion(Guid previousBaseId, decimal conversionRatio, Guid currentBaseId)
        {
            var measurementUnitSetup = DbSet.Include(x => x.WarehouseItems)
                                             .SingleOrDefault(i => i.Id == previousBaseId);

            if (measurementUnitSetup != null)
            {
                measurementUnitSetup.WarehouseItems.ToList().ForEach(x =>
                {
                    x.Quantity *= conversionRatio;
                    x.MeasurementUnitSetupId = currentBaseId;
                });
                Context.SaveChanges();
            }
            return measurementUnitSetup;
        }

        public IEnumerable<MeasurementUnitSetup> GetItems(Guid measurementUnitId)
        {
            return DbSet.Include(m => m.MeasurementUnit)
                .Include(i => i.Item)
                .Include(ic => ic.Item.Category)
                .Where(i => i.MeasurementUnitId == measurementUnitId);
        }


    }
}
