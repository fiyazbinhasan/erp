﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {

        }

        public override Order Get(Guid orderId)
        {
            return DbSet
                .Include(o => o.Customer)
                .Include(o => o.Bills)
                .FirstOrDefault(o => o.Id == orderId);
        }
        public PagedList<Order> Get(OrderResourceParameters parameters, string customerPhone)
        {
            IQueryable<Order> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.Moniker))
                query = query.Where(c => c.Moniker.ToLowerInvariant().Equals(parameters.Moniker.Trim().ToLowerInvariant()));
            if(parameters.OrderStatus != null)
                query = query.Where(c => c.OrderStatus.Equals(parameters.OrderStatus));
            if (parameters.PaymentStatus != null)
                query = query.Where(c => c.PaymentStatus.Equals(parameters.PaymentStatus));

            foreach (var includeProperty in parameters.Includes.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (customerPhone != "all")
                query = query.Where(i => i.Customer.Phone.Equals(customerPhone));

            if (!string.IsNullOrEmpty(parameters.SearchQuery) && parameters.SearchQuery != "null")
                query = query.Where(i => i.Moniker.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.PaymentStatus.ToString().ToLower().Equals(parameters.SearchQuery.ToLower()) || i.OrderStatus.ToString().ToLower().Equals(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Order>.Create(query, parameters.PageNumber,
                    parameters.PageSize);

            return PagedList<Order>.Create(query.OrderBy(parameters.OrderBy), parameters.PageNumber,
                parameters.PageSize);
        }

        public Order SearchByMoniker(string moniker)
        {
            return DbSet
                .Include(o => o.Customer)
                .FirstOrDefault(o => o.Moniker.Equals(moniker));
        }

        public bool ValidateOrderStatus(Order entity)
        {
            if (entity.OrderStatus != ApplicationEnums.OrderStatus.Pending)
                return true;
            return false;
        }

        public bool ValidatePaymentStatus(Order entity)
        {
            if (entity.PaymentStatus != ApplicationEnums.PaymentStatus.Unpaid)
                return true;
            return false;
        }

        public Order Update(Order entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
