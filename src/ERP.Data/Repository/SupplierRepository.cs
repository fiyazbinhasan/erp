﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace ERP.Data.Repository
{
    public class SupplierRepository : GenericRepository<Supplier>, ISupplierRepository
    {
        public SupplierRepository(ApplicationDbContext context) : base(context)
        {

        }
        public PagedList<Supplier> Get(SupplierResourceParameters parameters)
        {
            IQueryable<Supplier> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.FirstName))
                query = query.Where(s => s.FirstName.ToLowerInvariant().Contains(parameters.FirstName.Trim().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(parameters.LastName))
                query = query.Where(s => s.LastName.ToLowerInvariant().Contains(parameters.LastName.Trim().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(parameters.Phone))
                query = query.Where(s => s.Phone.ToLowerInvariant().Equals(parameters.Phone.Trim().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(parameters.Email))
                query = query.Where(s => s.Email.ToLowerInvariant().Contains(parameters.Email.Trim().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(parameters.City))
                query = query.Where(s => s.City.ToLowerInvariant().Contains(parameters.City.Trim().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(parameters.Country))
                query = query.Where(s => parameters.Country.Split
                    (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Contains(s.Country));

            foreach (var includeProperty in parameters.Includes.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (!string.IsNullOrEmpty(parameters.SearchQuery))
                query = query.Where(i => i.FirstName.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.LastName.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.Address1.ToLower().Contains(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Supplier>.Create(query, parameters.PageNumber, parameters.PageSize);


            return PagedList<Supplier>.Create(query.OrderBy(parameters.OrderBy), parameters.PageNumber, parameters.PageSize);
        }
    }
}
