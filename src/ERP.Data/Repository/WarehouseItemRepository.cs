﻿using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ERP.Data.Repository
{
    public class WarehouseItemRepository : GenericRepository<WarehouseItem>, IWarehouseItemRepository
    {
        public WarehouseItemRepository(ApplicationDbContext context) : base(context)
        {
        }
        public override WarehouseItem Get(Guid id)
        {
            return DbSet.Include(mu => mu.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(c => c.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Warehouse)
                .SingleOrDefault(ii => ii.Id == id);
        }

        public override IQueryable<WarehouseItem> Get()
        {
            return DbSet.Include(mu => mu.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(c => c.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Warehouse);
        }

        public IQueryable<WarehouseItem> GetWarehouses(Guid itemId)
        {
            return DbSet.Include(mu => mu.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(c => c.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Warehouse)
                .Where(i => i.MeasurementUnitSetup.ItemId == itemId);
        }

        public IQueryable<WarehouseItem> GetItems(Guid warehouseId)
        {
            return DbSet.Include(mu => mu.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(c => c.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Warehouse)
                .Where(i => i.WarehouseId == warehouseId);
        }

        public WarehouseItem GetItem(Guid warehouseId, Guid itemId)
        {
            return DbSet.Include(mu => mu.MeasurementUnitSetup)
                .Include(i => i.MeasurementUnitSetup.MeasurementUnit)
                .Include(i => i.MeasurementUnitSetup.Item)
                .Include(c => c.MeasurementUnitSetup.Item.Category)
                .Include(ii => ii.Warehouse)
                .SingleOrDefault(i => i.WarehouseId == warehouseId && i.MeasurementUnitSetup.ItemId == itemId);
        }

        public WarehouseItem Update(WarehouseItem entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
