﻿using System;
using System.Linq;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace ERP.Data.Repository
{
    public class BillRepository : GenericRepository<Bill>, IBillRepository
    {
        public BillRepository(ApplicationDbContext context) : base(context)
        {

        }

        public override Bill Get(Guid billId)
        {
            return DbSet
                .Include(o => o.Order)
                .Include(o => o.Order.Customer)
                .FirstOrDefault(o => o.Id == billId);
        }

        public PagedList<Bill> Get(BillResourceParameters parameters, string customerPhone, string orderMoniker)
        {
            IQueryable<Bill> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.Moniker))
                query = query.Where(s => s.Moniker.Trim().ToLower().Contains(parameters.Moniker.Trim().ToLower()));

            if (!string.IsNullOrEmpty(parameters.PaidAmountRanges))
            {
                var ranges = parameters.PaidAmountRanges.Split
                    (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                decimal minValue = Convert.ToDecimal(ranges[0]);
                decimal maxValue = Convert.ToDecimal(ranges[1]);

                query = query.Where(x => x.PaidAmount >= minValue && x.PaidAmount <= maxValue);
            }
            if (parameters.PaymentMethod != null)
                query = query.Where(c => c.PaymentMethod.Equals(parameters.PaymentMethod));

            foreach (var includeProperty in parameters.Includes.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (customerPhone != "all")
                query = query.Where(i => i.Order.Customer.Phone == customerPhone);

            if (orderMoniker != "all")
                query = query.Where(i => i.Order.Moniker == orderMoniker);

            if (!string.IsNullOrEmpty(parameters.OrderNo))
                query = query.Where(s => s.Order.Moniker.Trim().ToLower().Equals(parameters.OrderNo.Trim().ToLower()));

            if (!string.IsNullOrEmpty(parameters.SearchQuery) && parameters.SearchQuery != "null")
                query = query.Where(i => i.Moniker.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.PaymentMethod.ToString().ToLower().Equals(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Bill>.Create(query, parameters.PageNumber,
                    parameters.PageSize);

            return PagedList<Bill>.Create(query.OrderBy(parameters.OrderBy).Include(o => o.Order.Customer), parameters.PageNumber,
                parameters.PageSize);
        }

        public IQueryable<Bill> GetAnOrderBills(Guid orderId)
        {
            return DbSet.Where(x => x.OrderId == orderId);
        }

        public Bill Update(Bill entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
