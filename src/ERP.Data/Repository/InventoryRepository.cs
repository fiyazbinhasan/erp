﻿using System;
using System.Linq;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace ERP.Data.Repository
{
    public class InventoryRepository : GenericRepository<Inventory>, IInventoryRepository
    {
       
        public InventoryRepository(ApplicationDbContext context) : base(context)
        {
        }

        public override Inventory Get(Guid inventoryId)
        {
            return DbSet.Include(ii => ii.Supplier)
                         .FirstOrDefault(i => i.Id == inventoryId);
        }

        public PagedList<Inventory> Get(InventoryResourceParameters parameters, string supplierPhone)
        {
            IQueryable<Inventory> query = DbSet;

            if (!string.IsNullOrEmpty(parameters.Moniker))
                query = query.Where(c => c.Moniker.ToLowerInvariant().Contains(parameters.Moniker.Trim().ToLowerInvariant()));

            foreach (var includeProperty in parameters.Includes.Split
                (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (supplierPhone != "all")
                query = query.Where(i => i.Supplier.Phone == supplierPhone);

            if (!string.IsNullOrEmpty(parameters.SearchQuery) && parameters.SearchQuery != "null")
                query = query.Where(i => i.Moniker.ToLower().Contains(parameters.SearchQuery.ToLower()) || i.Description.ToLower().Contains(parameters.SearchQuery.ToLower()));

            if (string.IsNullOrEmpty(parameters.OrderBy))
                return PagedList<Inventory>.Create(query, parameters.PageNumber,
                    parameters.PageSize);

            return PagedList<Inventory>.Create(query.OrderBy(parameters.OrderBy), parameters.PageNumber, parameters.PageSize);
        }

        public Inventory Update(Inventory entity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
