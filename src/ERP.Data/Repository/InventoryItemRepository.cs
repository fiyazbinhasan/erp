﻿using System;
using System.Linq;
using ERP.Data.Interface;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace ERP.Data.Repository
{
    public class InventoryItemRepository : GenericRepository<InventoryItem>, IInventoryItemRepository
    {
        ApplicationDbContext _context;
        public InventoryItemRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public override InventoryItem Get(Guid id)
        {
            return DbSet
                .Include(ii => ii.Inventory)
                .Include(s => s.Inventory.Supplier)
                .Include(m => m.MeasurementUnitSetup)
                .Include(m => m.MeasurementUnitSetup.Item)
                .Include(m => m.MeasurementUnitSetup.Item.Category)
                .Include(m => m.MeasurementUnitSetup.MeasurementUnit)
                .Include(w => w.Warehouse)
                .SingleOrDefault(ii => ii.Id == id);
        }

        public override IQueryable<InventoryItem> Get()
        {
            return DbSet
                .Include(ii => ii.Inventory)
                .Include(m => m.MeasurementUnitSetup)
                .Include(m => m.MeasurementUnitSetup.Item)
                .Include(m => m.MeasurementUnitSetup.Item.Category)
                .Include(m => m.MeasurementUnitSetup.MeasurementUnit)
                .Include(w => w.Warehouse)
                .Include(s => s.Inventory.Supplier);
        }

        public IQueryable<InventoryItem> GetInventories(Guid itemId)
        {
            return DbSet
                .Include(ii => ii.Inventory)
                .Include(s => s.Inventory.Supplier)
                .Include(m => m.MeasurementUnitSetup)
                .Include(m => m.MeasurementUnitSetup.Item)
                .Include(m => m.MeasurementUnitSetup.Item.Category)
                .Include(m => m.MeasurementUnitSetup.MeasurementUnit)
                .Include(w => w.Warehouse)
                .Where(i => i.MeasurementUnitSetup.ItemId == itemId);
        }

        public IQueryable<InventoryItem> GetItems(Guid inventoryId)
        {
            var inventoryItems = DbSet
                .Include(ii => ii.Inventory)
                .Include(s => s.Inventory.Supplier)
                .Include(m => m.MeasurementUnitSetup)
                .Include(m => m.MeasurementUnitSetup.Item)
                .Include(m => m.MeasurementUnitSetup.Item.Category)
                .Include(m => m.MeasurementUnitSetup.MeasurementUnit)
                .Include(w => w.Warehouse)
                .Where(i => i.Inventory.Id == inventoryId);
            foreach (var inventoryItem in inventoryItems)
            {
                inventoryItem.MeasurementUnitSetup.IsUsed = true;
            }
            return inventoryItems;
        }

        public override void Add(InventoryItem entity)
        {
            base.Add(entity);

            var warehouseItem = Context.WarehouseItem
                    .FirstOrDefault(wi => wi.WarehouseId == entity.WarehouseId && wi.MeasurementUnitSetup.ItemId == entity.MeasurementUnitSetup.ItemId);

            var convertedQuantity = entity.MeasurementUnitSetup.ConversionRatio * entity.IssuedQuantity;

            if (warehouseItem == null)
            {
                var baseMeasurementUnit =
                    Context.MeasurementUnitSetup.SingleOrDefault(
                        i => i.ItemId == entity.MeasurementUnitSetup.ItemId && i.IsBase);

                warehouseItem = new WarehouseItem
                {
                    WarehouseId = entity.WarehouseId,
                    MeasurementUnitSetupId = baseMeasurementUnit.Id,
                    Quantity = convertedQuantity
                };
                Context.WarehouseItem.Add(warehouseItem);
            }
            else
            {
                Context.Entry(entity.Warehouse)
                        .Collection(b => b.WarehouseItems).Query()
                        .SingleOrDefault(wi => wi.MeasurementUnitSetup.ItemId == entity.MeasurementUnitSetup.ItemId).Quantity += convertedQuantity;
            }
        }

        public InventoryItem Update(InventoryItem entity, InventoryItem prevEntity)
        {
            return DbSet.Update(entity).Entity;
        }
    }
}
