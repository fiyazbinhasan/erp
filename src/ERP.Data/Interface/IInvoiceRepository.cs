﻿using System;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;
using System.Linq;

namespace ERP.Data.Interface
{
    public interface IInvoiceRepository : IRepository<Invoice>, IUpdateEntity<Invoice>
    {
        PagedList<Invoice> Get(InvoiceResourceParameters invoiceResourceParameters, string customerPhone, string moniker);
        IQueryable<Invoice> GetOrderInvoices(Guid orderId);
    }
}
