﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ERP.Data.Helpers;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace ERP.Data.Interface
{
    public interface IRepository<TEntity> where TEntity : class
    {
        DbSet<TEntity> DbSet { get; }
        void Add(TEntity entity);
        IQueryable<TEntity> Get();
        TEntity Get(Guid id);
        void Delete(TEntity entity);
        Task<bool> SaveChangesAsync();
        string AddETag(TEntity entity, IMemoryCache cache);
        byte[] GetVersion(TEntity entity);
    }
}
