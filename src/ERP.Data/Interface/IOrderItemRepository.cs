﻿using System;
using System.Linq;
using ERP.Model.Inventory;
using System.Collections.Generic;

namespace ERP.Data.Interface
{
    public interface IOrderItemRepository : IRepository<OrderItem>, IUpdateEntity<OrderItem>
    {
        IQueryable<OrderItem> GetItems(Guid orderId);
        IQueryable<OrderItem> GetOrders(Guid itemId);
    }
}
