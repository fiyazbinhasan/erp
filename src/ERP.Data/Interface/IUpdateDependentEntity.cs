﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Data.Interface
{
    public interface IUpdateDependentEntity<TEntity> where TEntity : class
    {
        TEntity Update(TEntity entity, TEntity prevEntity);
    }
}
