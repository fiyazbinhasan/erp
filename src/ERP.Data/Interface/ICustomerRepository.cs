﻿using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;
using System.Linq;

namespace ERP.Data.Interface
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        PagedList<Customer> Get(CustomerResourceParameters parameters);
    }
}
