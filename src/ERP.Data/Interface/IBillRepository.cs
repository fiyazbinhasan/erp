﻿using System;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;
using System.Linq;

namespace ERP.Data.Interface
{
    public interface IBillRepository : IRepository<Bill>, IUpdateEntity<Bill>
    {
        PagedList<Bill> Get(BillResourceParameters billResourceParameters, string customerPhone, string orderMoniker);
        IQueryable<Bill> GetAnOrderBills(Guid orderId);
    }
}
