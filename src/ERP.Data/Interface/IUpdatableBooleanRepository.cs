﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Data.Interface
{
    public interface IUpdatableBooleanRepository<TEntity> where TEntity : class
    {
        bool UpdateEntity(TEntity entity);

    }
}
