﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Model.Inventory;

namespace ERP.Data.Interface
{
    public interface IInvoiceItemRepository : IRepository<InvoiceItem>, IUpdateDependentEntity<InvoiceItem>
    {
        IQueryable<InvoiceItem> GetItems(Guid invoiceId);
        IQueryable<InvoiceItem> GetInvoices(Guid itemId);
    }
}
