﻿using System;
using System.Linq;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;

namespace ERP.Data.Interface
{
    public interface IOrderRepository : IRepository<Order>, IUpdateEntity<Order>
    {
        PagedList<Order> Get(OrderResourceParameters parameters, string customerPhone);
        Order SearchByMoniker(string moniker);
        bool ValidatePaymentStatus(Order entity);
        bool ValidateOrderStatus(Order entity);
    }
}
