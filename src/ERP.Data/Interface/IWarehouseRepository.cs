﻿using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;

namespace ERP.Data.Interface
{
    public interface IWarehouseRepository : IRepository<Warehouse>, IUpdateEntity<Warehouse>
    {
        PagedList<Warehouse> Get(WarehouseResourceParameters parameters);
    }
}
