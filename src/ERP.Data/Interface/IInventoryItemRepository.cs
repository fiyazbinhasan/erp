﻿using ERP.Model.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Data.Interface
{
    public interface IInventoryItemRepository : IRepository<InventoryItem>, IUpdateDependentEntity<InventoryItem>
    {
        IQueryable<InventoryItem> GetItems(Guid inventoryId);
        IQueryable<InventoryItem> GetInventories(Guid itemId);
    }
}
