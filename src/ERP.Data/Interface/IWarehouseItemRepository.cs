﻿using ERP.Model.Inventory;
using System;
using System.Linq;

namespace ERP.Data.Interface
{
    public interface IWarehouseItemRepository : IRepository<WarehouseItem>, IUpdateEntity<WarehouseItem>
    {
        IQueryable<WarehouseItem> GetItems(Guid warehouseId);
        WarehouseItem GetItem(Guid warehouseId, Guid itemId);
        IQueryable<WarehouseItem> GetWarehouses(Guid itemId);
    }
}
