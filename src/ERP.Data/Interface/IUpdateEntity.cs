﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace ERP.Data.Interface
{
    public interface IUpdateEntity<TEntity> where TEntity : class
    {
        TEntity Update(TEntity entity);
       
    }
}
