﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;

namespace ERP.Data.Interface
{
    public interface ISupplierRepository : IRepository<Supplier>
    {
        PagedList<Supplier> Get(SupplierResourceParameters parameters);
    }
}
