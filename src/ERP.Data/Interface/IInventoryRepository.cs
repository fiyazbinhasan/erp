﻿using System;
using System.Linq;
using ERP.Data.Helpers.ResourceParameter;
using ERP.Model.Inventory;

namespace ERP.Data.Interface
{
    public interface IInventoryRepository : IRepository<Inventory>, IUpdateEntity<Inventory>
    {
        PagedList<Inventory> Get(InventoryResourceParameters parameters, string supplierPhone);
    }
}
