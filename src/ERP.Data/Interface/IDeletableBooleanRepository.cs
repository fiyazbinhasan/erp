﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Data.Interface
{
    public interface IDeletableBooleanRepository<TEntity> where TEntity : class
    {
        bool DeleteEntity(TEntity entity);

    }
}
