﻿using ERP.Model.Inventory;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ERP.Data.Interface
{
    public interface IMeasurementUnitSetupRepository : IRepository<MeasurementUnitSetup>, IUpdateEntity<MeasurementUnitSetup>
    {
        IEnumerable<MeasurementUnitSetup> GetMeasurementUnits(Guid itemId);
        IEnumerable<MeasurementUnitSetup> GetItems(Guid measurementUnitId);
        MeasurementUnitSetup BaseWiseQuantityConversion(Guid previousBaseId, decimal conversionRatio, Guid currentBaseId);
    }
}
