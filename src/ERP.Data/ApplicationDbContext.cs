﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ERP.Data.Configurations;
using ERP.Model;
using ERP.Model.Inventory;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using EntityFrameworkCore.Triggers;
using static ERP.Model.Common.ApplicationEnums;

namespace ERP.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<InventoryItem> InventoryItem { get; set; }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Bill> Bills { get; set; }

        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<Order> Orders { get; set; }

        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceItem> InvoiceItem { get; set; }

        public DbSet<MeasurementUnit> MeasurementUnit { get; set; }
        public DbSet<MeasurementUnitSetup> MeasurementUnitSetup { get; set; }

        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<WarehouseItem> WarehouseItem { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new CategoryConfiguration());
            modelBuilder.AddConfiguration(new ItemConfiguration());
            modelBuilder.AddConfiguration(new SupplierConfiguration());
            modelBuilder.AddConfiguration(new InventoryConfiguration());
            modelBuilder.AddConfiguration(new InventoryItemConfiguration());

            modelBuilder.AddConfiguration(new CustomerConfiguration());
            modelBuilder.AddConfiguration(new BillConfiguration());
            modelBuilder.AddConfiguration(new OrderConfiguration());
            modelBuilder.AddConfiguration(new OrderItemConfiguration());

            modelBuilder.AddConfiguration(new InvoiceConfiguration());
            modelBuilder.AddConfiguration(new InvoiceItemConfiguration());
            modelBuilder.AddConfiguration(new MeasurementUnitConfiguration());

            modelBuilder.AddConfiguration(new WarehouseConfiguration());
            modelBuilder.AddConfiguration(new WarehouseItemConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            foreach (var entry in modifiedEntries)
            {
                var entityType = entry.Context.Model.FindEntityType(entry.Entity.GetType());

                var modifiedProperty = entityType.FindProperty("Modified");
                var createdProperty = entityType.FindProperty("Created");

                if (entry.State == EntityState.Modified && modifiedProperty != null)
                {
                    entry.Property("Modified").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Added && createdProperty != null)
                {
                    entry.Property("Created").CurrentValue = DateTime.Now;
                }
            }

            return this.SaveChangesWithTriggersAsync(base.SaveChangesAsync, acceptAllChangesOnSuccess: true, cancellationToken: cancellationToken);
        }
    }
}