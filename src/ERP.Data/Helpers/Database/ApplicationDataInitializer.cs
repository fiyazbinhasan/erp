﻿using System;
using System.Threading.Tasks;
using ERP.Model;
using ERP.Model.Common;
using ERP.Model.Inventory;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.Data.Helpers.Database
{
    public class ApplicationDataInitializer : IApplicationDatabaseInitializer
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ApplicationDataInitializer(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;

        }
        public async Task SeedAsync(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var applicationDbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                applicationDbContext.Database.Migrate();

                var user = await _userManager.FindByEmailAsync("fiyazhasan@yahoo.com");
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        UserName = "fiyazhasan@yahoo.com",
                        Email = "fiyazhasan@yahoo.com"
                    };
                    await _userManager.CreateAsync(user, "@Fizz1234");
                }
                var role = await _roleManager.FindByNameAsync("admin");

                if (role == null)
                {
                    role = new IdentityRole()
                    {
                        Name = "admin"
                    };

                    await _roleManager.CreateAsync(role);
                }

                await _userManager.AddToRoleAsync(user, "admin");

                var category = await applicationDbContext.Categories.AddAsync(new Category
                {
                    Name = "Electronics",
                    Description = "All electronics products goes here"
                });

                var item = await applicationDbContext.Items.AddAsync(new Item
                {
                    Name = "iPhone8",
                    Description = "Best smart phone from Apple.",
                    Category = category.Entity
                });

                var measurementUnit = await applicationDbContext.MeasurementUnit.AddAsync(new MeasurementUnit
                {
                    Name = "KG",
                    Remarks = "Kilogram"
                });

                var measurementUnitSetup = await applicationDbContext.MeasurementUnitSetup.AddAsync(new MeasurementUnitSetup
                {
                    Item = item.Entity,
                    MeasurementUnit = measurementUnit.Entity,
                    IsBase = true,
                    ConversionRatio = 1
                });

                var supplier = await applicationDbContext.Suppliers.AddAsync(new Supplier
                {
                    FirstName = "Imrez",
                    LastName = "Ratin",
                    Address1 = "24/B Aparajita Apartment, West Malibagh",
                    Address2 = "17/11 Paribagh, Sonargoan Street",
                    Country = "Bangladesh",
                    City = "Dhaka",
                    PostalCode = "1217",
                    Phone = "8801911993320",
                    AlternativePhone = "+8801515602838",
                    Email = "imrezratin@gmail.com",
                    Website = "imrezratin.blogspot.com"
                });

                var inventory = await applicationDbContext.Inventories.AddAsync(new Inventory
                {
                    Moniker = "INVENTORY-0001",
                    Description = "IPhone inventory",
                    PriorityLevel = 1,
                    PurchaseDate = DateTime.Now,
                    Supplier = supplier.Entity
                });

                await applicationDbContext.InventoryItem.AddAsync(new InventoryItem
                {
                    Inventory = inventory.Entity,
                    MeasurementUnitSetup = measurementUnitSetup.Entity,
                    CostPerUnit = 1_15_000,
                    IssuedQuantity = 100,
                    Remarks = "Sell it in wholesale price"
                });

                var customer = await applicationDbContext.Customers.AddAsync(new Customer
                {
                    FirstName = "Fiyaz",
                    LastName = "Hasan",
                    Phone = "8801719152599",
                    AlternativePhone = "N/A",
                    Email = "fiyazhasan@gmail.com",
                    CompanyName = "Geeky Hours",
                    Address1 = "17/11 Paribagh, Sonargaon Street",
                    Address2 = "N/A",
                    City = "Dhaka",
                    Country = "Bangladesh",
                    PostalCode = "1000",
                    Website = "www.fiyazhasan.me"
                });

                var order = await applicationDbContext.Orders.AddAsync(new Order
                {
                    Moniker = "ORDER-0001",
                    OrderDate = DateTime.Now,
                    ExpectedDeliveryDate = DateTime.Now.AddDays(1),
                    PaymentStatus = ApplicationEnums.PaymentStatus.Paid,
                    OrderStatus = ApplicationEnums.OrderStatus.Pending,
                    Customer = customer.Entity
                });

                var bill = await applicationDbContext.Bills.AddAsync(new Bill
                {
                    Moniker = "BILL-0001",
                    BillDate = DateTime.Now,
                    PaidAmount = 1000,
                    PaymentMethod = ApplicationEnums.PaymentMethod.Cash,
                    Order = order.Entity
                });

                await applicationDbContext.OrderItem.AddAsync(new OrderItem
                {
                    Order = order.Entity,
                    MeasurementUnitSetup = measurementUnitSetup.Entity,
                    Discount = 2300,
                    Quantity = 1,
                    UnitPrice = 1_15_000,
                    Remarks = "Item ordered in summer sale"

                });

                var invoice = await applicationDbContext.Invoices.AddAsync(new Invoice
                {
                    Moniker = "INVC-0001",
                    InvoiceDate = DateTime.Now,
                    Description="Invoice",
                    Order = order.Entity
                });

                await applicationDbContext.SaveChangesAsync();
            }
        }
    }
}
