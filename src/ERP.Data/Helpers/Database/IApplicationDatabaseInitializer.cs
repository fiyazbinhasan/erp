﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace ERP.Data.Helpers.Database
{
    public interface IApplicationDatabaseInitializer
    {
        Task SeedAsync(IApplicationBuilder app);
    }
}
