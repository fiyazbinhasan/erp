﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace ERP.Data.Helpers.Database
{
    public interface IIdentityServerDatabaseInitializer
    {
        Task SeedAsync(IApplicationBuilder app);
    }
}
