﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Model.Inventory;

namespace ERP.Data.Helpers.ResourceParameter
{
    public class SupplierResourceParameters : PaginationResourceParameters
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } ="FirstName";
        public string Fields { get; set; }
        public string Includes { get; set; } = string.Empty;
    }
}
