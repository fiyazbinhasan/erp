﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Model.Inventory;

namespace ERP.Data.Helpers.ResourceParameter
{
    public class InventoryResourceParameters : PaginationResourceParameters
    {
        public string Moniker { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } = "Moniker";
        public string Fields { get; set; }
        public string Includes { get; set; } = "Supplier";
    }
}
