﻿namespace ERP.Data.Helpers.ResourceParameter
{
    public class InvoiceResourceParameters : PaginationResourceParameters
    {
        public string Moniker { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } = "Moniker";
        public string Fields { get; set; }
        public string Includes { get; set; } = "Order,Order.Customer";
    }
}
