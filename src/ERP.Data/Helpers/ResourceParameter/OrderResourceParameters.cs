﻿using System;
using System.Collections.Generic;
using System.Text;
using static ERP.Model.Common.ApplicationEnums;

namespace ERP.Data.Helpers.ResourceParameter
{
    public class OrderResourceParameters : PaginationResourceParameters
    {
        public string Moniker { get; set; }
        public PaymentStatus? PaymentStatus { get; set; }
        public OrderStatus? OrderStatus { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } = "Moniker";
        public string Fields { get; set; }
        public string Includes { get; set; } = "Customer";
        public Guid CustomerId { get; set; }
    }
}
