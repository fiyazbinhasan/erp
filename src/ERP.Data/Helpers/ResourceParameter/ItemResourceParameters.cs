﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Model.Inventory;

namespace ERP.Data.Helpers.ResourceParameter
{
    public class ItemResourceParameters : PaginationResourceParameters
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } = "Name";
        public string Fields { get; set; }
        public string Includes { get; set; } = "Category,MeasurementUnitSetups,MeasurementUnitSetups.MeasurementUnit";
    }
}
