﻿namespace ERP.Data.Helpers.ResourceParameter
{
    public class WarehouseResourceParameters : PaginationResourceParameters
    {
        public string Moniker { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } = "Moniker";
        public string Fields { get; set; }
        public string Includes { get; set; } = string.Empty;
    }
}
