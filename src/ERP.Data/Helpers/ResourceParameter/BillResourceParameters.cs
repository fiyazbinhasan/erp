﻿using System;
using System.Collections.Generic;
using System.Text;
using static ERP.Model.Common.ApplicationEnums;

namespace ERP.Data.Helpers.ResourceParameter
{
    public class BillResourceParameters : PaginationResourceParameters
    {
        public string Moniker { get; set; }
        public string PaidAmountRanges { get; set; }
        public PaymentMethod? PaymentMethod { get; set; }
        public string OrderNo { get; set; }
        public string SearchQuery { get; set; }
        public string OrderBy { get; set; } = "Moniker";
        public string Fields { get; set; }
        public string Includes { get; set; } = "Order,Order.Customer";
    }
}
