﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ERP.Data.Helpers.ExcelGenerators
{
    public class PurchaseBySupplierExcel
    {
        int rowIndex = 2;
        ExcelRange cell;
        ExcelFill fill;
        Border border;

        public byte[] GenerateExcel(DateTime startDate, DateTime endDate, string listType, IEnumerable<object> purchaseBySuppliers)
        {
           
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "3TierLab";
                excelPackage.Workbook.Properties.Title = "3 Tier Lab";
                var sheet = excelPackage.Workbook.Worksheets.Add("Purchase by Supplier");
                sheet.Name = "Purchase by Supplier";
                sheet.Column(2).Width = 10; //SL
                sheet.Column(3).Width = 30; //Supplier
                sheet.Column(4).Width = 20; //Bill

                int maxColumn = 4;

                #region Report Header
                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "My Company Name"; cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 20; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rowIndex = rowIndex + 1;

                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "Company Address"; cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rowIndex = rowIndex + 2;

                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "Date : " + startDate.ToString("dd-MMM-yyyy") + " to " + endDate.ToString("dd-MMM-yyyy"); cell.Style.Font.Bold = false;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowIndex = rowIndex + 1;

                #endregion

                #region Column Header

                cell = sheet.Cells[rowIndex, 2]; cell.Value = "#"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 3]; cell.Value = "Supplier"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 4]; cell.Value = "Bill"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex = rowIndex + 1;
                #endregion

                #region Column Body
                int serialNumber = 1;
                double totalBill = 0;

                if (purchaseBySuppliers != null)
                {
                    if (listType == "true")
                    {
                        purchaseBySuppliers = purchaseBySuppliers.Where(o => Convert.ToDouble(o?.GetType().GetProperty("Bill").GetValue(o, null)) == 0);
                    }
                    else if (listType == "false")
                    {
                        purchaseBySuppliers = purchaseBySuppliers.Where(o => Convert.ToDouble(o?.GetType().GetProperty("Bill").GetValue(o, null)) > 0);
                    }

                    foreach (object obj in purchaseBySuppliers)
                    {
                        cell = sheet.Cells[rowIndex, 2]; cell.Value = serialNumber++.ToString(); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3]; cell.Value = obj?.GetType().GetProperty("Name").GetValue(obj, null); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalBill += Convert.ToDouble(obj?.GetType().GetProperty("Bill").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 4]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("Bill").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        rowIndex = rowIndex + 1;
                    }
                }
                #endregion

                #region Total

                cell = sheet.Cells[rowIndex, 2]; cell.Value = ""; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 3]; cell.Value = "Total : "; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 4]; cell.Value = totalBill; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex = rowIndex + 1;
                #endregion

                return excelPackage.GetAsByteArray();
            }
        }
    }
}
