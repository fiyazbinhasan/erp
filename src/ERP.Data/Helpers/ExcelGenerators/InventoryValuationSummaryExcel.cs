﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ERP.Data.Helpers.ExcelGenerators
{
    public class InventoryValuationSummaryExcel
    {
        int rowIndex = 2;
        ExcelRange cell;
        ExcelFill fill;
        Border border;

        public byte[] GenerateExcel(DateTime startDate, DateTime endDate, string listType, IEnumerable<object> inventoryValuationSummary)
        {
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "3TierLab";
                excelPackage.Workbook.Properties.Title = "3 Tier Lab";
                var sheet = excelPackage.Workbook.Worksheets.Add("Inventory Valuation Summary");
                sheet.Name = "Inventory Valuation Summary";
                sheet.Column(2).Width = 10; //SL
                sheet.Column(3).Width = 30; //Name
                sheet.Column(4).Width = 20; //Quantity Available
                sheet.Column(5).Width = 20; //Inventory Asset Value

                int maxColumn = 5;

                #region Report Header
                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "My Company Name"; cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 20; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rowIndex = rowIndex + 1;

                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "Company Address"; cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rowIndex = rowIndex + 2;

                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "Date : " + startDate.ToString("dd-MMM-yyyy") + " to " + endDate.ToString("dd-MMM-yyyy"); cell.Style.Font.Bold = false;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowIndex = rowIndex + 1;

                #endregion

                #region Column Header

                cell = sheet.Cells[rowIndex, 2]; cell.Value = "#"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 3]; cell.Value = "Name"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 4]; cell.Value = "Quantity Available"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 5]; cell.Value = "Inventory Asset Value"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                rowIndex = rowIndex + 1;
                #endregion

                #region Column Body
                int serialNumber = 1;
                double totalQuantityAvailable = 0,
                       totalInventoryAssetValue = 0;

                if (inventoryValuationSummary != null)
                {
                    if (listType == "true")
                    {
                        inventoryValuationSummary = inventoryValuationSummary.Where(o => Convert.ToDouble(o?.GetType().GetProperty("InventoryAsset").GetValue(o, null)) == 0 &&
                                                                                         Convert.ToDouble(o?.GetType().GetProperty("WarehouseQuantity").GetValue(o, null)) == 0);
                    }
                    else if (listType == "false")
                    {
                        inventoryValuationSummary = inventoryValuationSummary.Where(o => Convert.ToDouble(o?.GetType().GetProperty("InventoryAsset").GetValue(o, null)) > 0 ||
                                                                                         Convert.ToDouble(o?.GetType().GetProperty("WarehouseQuantity").GetValue(o, null)) > 0);
                    }
                    foreach (object obj in inventoryValuationSummary)
                    {
                        cell = sheet.Cells[rowIndex, 2]; cell.Value = serialNumber++.ToString(); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3]; cell.Value = obj?.GetType().GetProperty("Name").GetValue(obj, null); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalQuantityAvailable += Convert.ToDouble(obj?.GetType().GetProperty("WarehouseQuantity").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 4]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("WarehouseQuantity").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalInventoryAssetValue += Convert.ToDouble(obj?.GetType().GetProperty("InventoryAsset").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 5]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("InventoryAsset").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                        rowIndex = rowIndex + 1;
                    }
                }
                #endregion

                #region Total

                cell = sheet.Cells[rowIndex, 2]; cell.Value = ""; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 3]; cell.Value = "Total : "; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 4]; cell.Value = totalQuantityAvailable; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 5]; cell.Value = totalInventoryAssetValue; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                rowIndex = rowIndex + 1;
                #endregion

                return excelPackage.GetAsByteArray();
            }
        }
    }
}
