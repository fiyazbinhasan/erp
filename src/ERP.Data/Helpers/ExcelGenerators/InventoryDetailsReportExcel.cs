﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ERP.Data.Helpers.ExcelGenerators
{
    public class InventoryDetailsReportExcel
    {
        int rowIndex = 2;
        ExcelRange cell;
        ExcelFill fill;
        Border border;

        public byte[] GenerateExcel(DateTime startDate, DateTime endDate, string listType, IEnumerable<object> inventoryDetails)
        {
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "3TierLab";
                excelPackage.Workbook.Properties.Title = "3 Tier Lab";
                var sheet = excelPackage.Workbook.Worksheets.Add("Inventory Details Report");
                sheet.Name = "Inventory Details Report";
                sheet.Column(2).Width = 10; //SL
                sheet.Column(3).Width = 30; //Name
                sheet.Column(4).Width = 20; //Ordered Quantity
                sheet.Column(5).Width = 20; //Quantity In
                sheet.Column(6).Width = 20; //Quantity Out
                sheet.Column(7).Width = 20; //Quantity Available

                int maxColumn = 7;

                #region Report Header
                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "My Company Name"; cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 20; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rowIndex = rowIndex + 1;

                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "Company Address"; cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rowIndex = rowIndex + 2;

                sheet.Cells[rowIndex, 2, rowIndex, maxColumn].Merge = true;
                cell = sheet.Cells[rowIndex, 2]; cell.Value = "Date : " + startDate.ToString("dd-MMM-yyyy") + " to " + endDate.ToString("dd-MMM-yyyy"); cell.Style.Font.Bold = false;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowIndex = rowIndex + 1;

                #endregion

                #region Column Header

                cell = sheet.Cells[rowIndex, 2, rowIndex + 1, 2]; cell.Value = "#"; cell.Merge = true; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 3, rowIndex + 1, 3]; cell.Value = "Name"; cell.Merge = true; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 4, rowIndex + 1, 4]; cell.Value = "Ordered Quantity"; cell.Merge = true; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 5, rowIndex, 7]; cell.Value = "Stock Detail"; cell.Merge = true; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex + 1, 5]; cell.Value = "Quantity In"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex + 1, 6]; cell.Value = "Quantity Out"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex + 1, 7]; cell.Value = "Quantity Available"; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex = rowIndex + 2;
                #endregion

                #region Column Body
                int serialNumber = 1;
                double totalOrderedQuantity = 0,
                       totalInvoicedQuantity = 0,
                       totalInventoryQuantity = 0,
                       totalWarehouseQuantity = 0;

                if (inventoryDetails != null)
                {
                    if (listType == "true")
                    {
                        inventoryDetails = inventoryDetails.Where(o => Convert.ToDouble(o?.GetType().GetProperty("OrderedQuantity").GetValue(o, null)) == 0 &&
                                                                       Convert.ToDouble(o?.GetType().GetProperty("InvoicedQuantity").GetValue(o, null)) == 0 &&
                                                                       Convert.ToDouble(o?.GetType().GetProperty("InventoryQuantity").GetValue(o, null)) == 0);
                    }
                    else if (listType == "false")
                    {
                        inventoryDetails = inventoryDetails.Where(o => Convert.ToDouble(o?.GetType().GetProperty("OrderedQuantity").GetValue(o, null)) > 0 ||
                                                                       Convert.ToDouble(o?.GetType().GetProperty("InvoicedQuantity").GetValue(o, null)) > 0 ||
                                                                       Convert.ToDouble(o?.GetType().GetProperty("InventoryQuantity").GetValue(o, null)) > 0);
                    }
                    foreach (object obj in inventoryDetails)
                    {
                        cell = sheet.Cells[rowIndex, 2]; cell.Value = serialNumber++.ToString(); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3]; cell.Value = obj?.GetType().GetProperty("Name").GetValue(obj, null); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalOrderedQuantity += Convert.ToDouble(obj?.GetType().GetProperty("OrderedQuantity").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 4]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("OrderedQuantity").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalInvoicedQuantity += Convert.ToDouble(obj?.GetType().GetProperty("InvoicedQuantity").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 5]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("InvoicedQuantity").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalInventoryQuantity += Convert.ToDouble(obj?.GetType().GetProperty("InventoryQuantity").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 6]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("InventoryQuantity").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        totalWarehouseQuantity += Convert.ToDouble(obj?.GetType().GetProperty("WarehouseQuantity").GetValue(obj, null));
                        cell = sheet.Cells[rowIndex, 7]; cell.Value = Convert.ToDouble(obj?.GetType().GetProperty("WarehouseQuantity").GetValue(obj, null)); cell.Style.Font.Bold = false; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.White);
                        border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        rowIndex = rowIndex + 1;
                    }
                }
                #endregion

                #region Total

                cell = sheet.Cells[rowIndex, 2]; cell.Value = ""; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 3]; cell.Value = "Total : "; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 4]; cell.Value = totalOrderedQuantity; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 5]; cell.Value = totalInvoicedQuantity; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 6]; cell.Value = totalInventoryQuantity; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                cell = sheet.Cells[rowIndex, 7]; cell.Value = totalWarehouseQuantity; cell.Style.Font.Bold = true; cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                fill = cell.Style.Fill; fill.PatternType = ExcelFillStyle.Solid; fill.BackgroundColor.SetColor(Color.LightGray);
                border = cell.Style.Border; border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex = rowIndex + 1;
                #endregion

                return excelPackage.GetAsByteArray();
            }
        }
    }
}
