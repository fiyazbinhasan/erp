﻿using ERP.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ERP.Data.Helpers.RepositoryExtensions
{
    public static class PurchaseReportExtensions
    {
        public static dynamic PurchaseBySupplierReport(this ISupplierRepository repository, DateTime startDate, DateTime endDate)
        {
            var suppliers = repository.DbSet
                .Include("Inventories")
                .Include("Inventories.InventoryItems")
                .Include("Inventories.InventoryItems.MeasurementUnitSetup")
                .ToList();

            return suppliers
                  .Select(s => new
                  {
                      Name = s.FirstName + " " + s.LastName,
                      Bill = s.Inventories.Sum(i => i.InventoryItems
                          .Where(ii => ii.Inventory.PurchaseDate >= startDate && ii.Inventory.PurchaseDate <= endDate)
                          .Sum(ii => ii.MeasurementUnitSetup.ConversionRatio * ii.IssuedQuantity * ii.CostPerUnit))
                  }).OrderBy(s => s.Name);
        }

        public static dynamic PurchaseByItemReport(this IItemRepository repository, DateTime startDate, DateTime endDate)
        {
            var items = repository.DbSet
                 .Include("MeasurementUnitSetups")
                 .Include("MeasurementUnitSetups.InventoryItems")
                 .Include("MeasurementUnitSetups.InventoryItems.Inventory")
                 .ToList();

            return items
                   .Select(i => new
                   {
                       i.Name,
                       PurchasedQuantity = i.MeasurementUnitSetups.Sum(mu => mu.InventoryItems
                           .Where(ii => ii.Inventory.PurchaseDate >= startDate && ii.Inventory.PurchaseDate <= endDate)
                           .Sum(ii => ii.IssuedQuantity * mu.ConversionRatio)),
                       Amount = i.MeasurementUnitSetups.Sum(mu => mu.InventoryItems
                           .Where(ii => ii.Inventory.PurchaseDate >= startDate && ii.Inventory.PurchaseDate <= endDate)
                           .Sum(ii => ii.IssuedQuantity * mu.ConversionRatio * ii.CostPerUnit))
                   }).OrderBy(i => i.Name);
        }
    }
}
