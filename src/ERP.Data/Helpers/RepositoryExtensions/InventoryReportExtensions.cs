﻿using ERP.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ERP.Data.Helpers.RepositoryExtensions
{
    public static class InventoryReportExtensions
    {
        public static dynamic InventoryDetails(this IItemRepository repository, DateTime startDate, DateTime endDate)
        {
            var items = repository.DbSet
                .Include("MeasurementUnitSetups")
                .Include("MeasurementUnitSetups.OrderItems")
                .Include("MeasurementUnitSetups.OrderItems.Order")
                .Include("MeasurementUnitSetups.InvoiceItems")
                .Include("MeasurementUnitSetups.InvoiceItems.Invoice")
                .Include("MeasurementUnitSetups.InventoryItems")
                .Include("MeasurementUnitSetups.InventoryItems.Inventory")
                .Include("MeasurementUnitSetups.WarehouseItems")
                .ToList();

            return items
                   .Select(i => new
                   {
                       i.Name,
                       OrderedQuantity = i.MeasurementUnitSetups.Sum(mu => mu.OrderItems
                           .Where(oi => oi.Order.OrderDate >= startDate && oi.Order.OrderDate <= endDate)
                           .Sum(oi => oi.Quantity * mu.ConversionRatio)),
                       InvoicedQuantity = i.MeasurementUnitSetups.Sum(mu => mu.InvoiceItems
                           .Where(ii => ii.Invoice.InvoiceDate >= startDate && ii.Invoice.InvoiceDate <= endDate)
                           .Sum(ii => ii.Quantity * mu.ConversionRatio)),
                       InventoryQuantity = i.MeasurementUnitSetups.Sum(mu => mu.InventoryItems
                           .Where(ii => ii.Inventory.PurchaseDate >= startDate && ii.Inventory.PurchaseDate <= endDate)
                           .Sum(ii => ii.IssuedQuantity * mu.ConversionRatio)),
                       WarehouseQuantity = i.MeasurementUnitSetups.Sum(mu => mu.WarehouseItems.Where(wi => wi.MeasurementUnitSetup.ItemId == i.Id)
                           .Sum(o => o.Quantity))
                   }).OrderBy(i => i.Name);
        }

        public static dynamic InventoryValuationSummaryReport(this IItemRepository repository, DateTime startDate, DateTime endDate)
        {
            var items = repository.DbSet
                .Include("MeasurementUnitSetups")
                .Include("MeasurementUnitSetups.InventoryItems")
                .Include("MeasurementUnitSetups.InventoryItems.Inventory")
                .Include("MeasurementUnitSetups.WarehouseItems")
                .ToList();

            return items
                   .Select(i => new
                   {
                       i.Name,
                       WarehouseQuantity = i.MeasurementUnitSetups.Sum(mu => mu.WarehouseItems.Where(wi => wi.MeasurementUnitSetup.ItemId == i.Id)
                          .Sum(o => o.Quantity)),
                       InventoryAsset = i.MeasurementUnitSetups.Sum(mu => mu.InventoryItems
                           .Where(ii => ii.Inventory.PurchaseDate >= startDate && ii.Inventory.PurchaseDate <= endDate)
                           .Sum(ii => ii.IssuedQuantity * mu.ConversionRatio * ii.CostPerUnit)),
                   }).OrderBy(i => i.Name);
        }
    }
}
