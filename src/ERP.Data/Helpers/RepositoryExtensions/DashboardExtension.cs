﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ERP.Data.Interface;
using ERP.Model.Common;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;

namespace ERP.Data.Helpers.RepositoryExtensions
{
    public static class DashboardExtension
    {
        public static int Count<TEntity>(this IRepository<TEntity> repository) where TEntity : class
        {
            return repository.DbSet.Count();
        }

        public static int PendingOrders(this IOrderRepository repository)
        {
            return repository.DbSet.Count(o => o.OrderStatus == ApplicationEnums.OrderStatus.Pending);
        }

        public static int PartialDeliveredOrders(this IOrderRepository repository)
        {
            return repository.DbSet.Count(o => o.OrderStatus == ApplicationEnums.OrderStatus.PartialDelivered);
        }

        public static int UnpaidOrders(this IOrderRepository repository)
        {
            return repository.DbSet.Count(o => o.PaymentStatus == ApplicationEnums.PaymentStatus.Unpaid);
        }

        public static int PartialPaidOrders(this IOrderRepository repository)
        {
            return repository.DbSet.Count(o => o.PaymentStatus == ApplicationEnums.PaymentStatus.PartialPaid);
        }

        public static dynamic TopSellingItems(this IOrderItemRepository repository)
        {
            var orderedItems = repository.DbSet
                .Include("MeasurementUnitSetup.Item.MeasurementUnitSetups")
                .Include("MeasurementUnitSetup.Item.MeasurementUnitSetups.MeasurementUnit")
                .Include("MeasurementUnitSetup.Item.Category")
                .Include("MeasurementUnitSetup.MeasurementUnit")
                .ToList();

            return orderedItems.GroupBy(oi => oi.MeasurementUnitSetup.Item)
                .OrderByDescending(gp => gp.Count())
                .Take(3)
                .Select(i => new
                {
                    Item = i.Key,
                    TotalOrdered = orderedItems
                        .Where(oi => oi.MeasurementUnitSetup.Item == i.Key)
                        .Sum(oi => oi.Quantity * oi.MeasurementUnitSetup.ConversionRatio),
                    BaseUnit = i.Key.MeasurementUnitSetups.Single(ms => ms.IsBase).MeasurementUnit
                });
        }

        public static int LowStockItems(this IWarehouseItemRepository repository)
        {
            return repository.DbSet
                .Count(wi => wi.Quantity < 10);
        }

        public static IQueryable<PurchasedAmountByMonth> PurchasedAmountByMonth(this IInventoryItemRepository repository)
        {
            return repository.DbSet
                .GroupBy(g => g.Inventory.PurchaseDate.Month)
                .Select(i => new PurchasedAmountByMonth
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i.Key),
                    Amount = i.Sum(ii => ii.CostPerUnit * ii.IssuedQuantity)
                });
        }

        public static IQueryable<PurchasedQuantityByMonth> PurchasedQuantityByMonth(this IInventoryItemRepository repository)
        {
            return repository.DbSet
                .GroupBy(g => g.Inventory.PurchaseDate.Month)
                .Select(i => new PurchasedQuantityByMonth
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i.Key),
                    Quantity = i.Sum(ii => ii.IssuedQuantity)
                });
        }

        public static IQueryable<SoldAmountByMonth> SoldAmountByMonth(this IBillRepository repository)
        {
           return repository.DbSet
                .GroupBy(g => g.BillDate.Month)
                .Select(i => new SoldAmountByMonth
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i.Key),
                    Amount = i.Sum(ii => ii.PaidAmount)
                });
        }

        public static IQueryable<InvoicedQuantityByMonth> InvoicedQuantityByMonth(this IInvoiceItemRepository repository)
        {
            return repository.DbSet
                .GroupBy(g => g.Invoice.InvoiceDate.Month)
                .Select(i => new InvoicedQuantityByMonth
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i.Key),
                    Quantity = i.Sum(ii => ii.Quantity * ii.MeasurementUnitSetup.ConversionRatio)
                });
        }

        //public static IQueryable<InvoicedAmountByMonth> InvoicedAmountByMonth(this IInvoiceItemRepository repository)
        //{
        //    return repository.DbSet
        //        .GroupBy(g => g.Invoice.InvoiceDate.Month)
        //        .Select(i => new InvoicedAmountByMonth
        //        {
        //            Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i.Key),
        //            Amount = i.Sum(ii => ii.MeasurementUnitSetup.ConversionRatio * ii.MeasurementUnitSetup.SellingPrice)
        //        });
        //}
    }

    public class SoldAmountByMonth
    {
        public string Month { get; set; }
        public decimal Amount { get; set; }
    }

    public class PurchasedAmountByMonth
    {
        public string Month { get; set; }
        public decimal Amount { get; set; }
    }

    public class PurchasedQuantityByMonth
    {
        public string Month { get; set; }
        public decimal Quantity { get; set; }
    }

    public class InvoicedQuantityByMonth
    {
        public string Month { get; set; }
        public decimal Quantity { get; set; }
    }

    public class InvoicedAmountByMonth
    {
        public string Month { get; set; }
        public decimal Amount { get; set; }
    }
}
