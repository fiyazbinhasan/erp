﻿using ERP.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ERP.Data.Helpers.RepositoryExtensions
{
    public static class CustomerViewExtensions
    {
        public static dynamic CustomerViewDatas(this ICustomerRepository repository, Guid customerId)
        {
            var customers = repository.DbSet
                .Include("Orders")
                .Include("Orders.Invoices")
                .Include("Orders.Invoices.InvoiceItems")
                .Include("Orders.Invoices.InvoiceItems.MeasurementUnitSetup")
                .Include("Orders.OrderItems")
                .Include("Orders.OrderItems.MeasurementUnitSetup")
                .Include("Orders.Bills")
                .Where(c => c.Id == customerId).ToList();

            return customers
                  .Select(c => new
                  {
                      TotalBillAmount = c.Orders.Sum(o => o.Bills.Sum(b => b.PaidAmount)),
                      TotalOrderAmount = c.Orders.Sum(o => o.OrderItems.Sum(oi => oi.Quantity * (oi.UnitPrice - oi.Discount))),
                      TotalOrderedItems = c.Orders.Sum(o => o.OrderItems.Sum(oi => oi.MeasurementUnitSetup.ConversionRatio * oi.Quantity)),
                      TotalInvoicedItems = c.Orders.Sum(o => o.Invoices.Sum(oi => oi.InvoiceItems.Sum(ii => ii.MeasurementUnitSetup.ConversionRatio * ii.Quantity)))
                  });
        }
    }
}
