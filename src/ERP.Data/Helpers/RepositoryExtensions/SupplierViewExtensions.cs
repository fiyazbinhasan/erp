﻿using ERP.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ERP.Data.Helpers.RepositoryExtensions
{
    public static class SupplierViewExtensions
    {
        public static dynamic SupplierViewDatas(this ISupplierRepository repository, Guid supplierId)
        {
            var suppliers = repository.DbSet
                .Include("Inventories")
                .Include("Inventories.InventoryItems")
                .Include("Inventories.InventoryItems.MeasurementUnitSetup")
                .Where(c => c.Id == supplierId).ToList();

            return suppliers
                  .Select(c => new
                  {
                      TotalInventoryAmount = c.Inventories.Sum(o => o.InventoryItems.Sum(oi => oi.IssuedQuantity * oi.CostPerUnit)),
                      TotalInventoryItems = c.Inventories.Sum(o => o.InventoryItems.Sum(oi => oi.MeasurementUnitSetup.ConversionRatio * oi.IssuedQuantity))
                  });
        }
    }
}
