﻿using ERP.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace ERP.Data.Helpers.RepositoryExtensions
{
    public static class SalesReportExtensions
    {
        public static dynamic SalesByItem(this IItemRepository repository, DateTime startDate, DateTime endDate)
        {
            var items = repository.DbSet
                .Include("MeasurementUnitSetups")
                .Include("MeasurementUnitSetups.InvoiceItems")
                .Include("MeasurementUnitSetups.InvoiceItems.Invoice")
                .ToList();

            return items

                  .Select(i => new
                  {
                      Name = i.Name,

                      SoldQuantity = i.MeasurementUnitSetups.Sum(mu => mu.InvoiceItems
                             .Where(oi => oi.Invoice.InvoiceDate >= startDate && oi.Invoice.InvoiceDate <= endDate)
                            .Sum(oi => oi.Quantity * mu.ConversionRatio)),

                      TotalSoldPrice = i.MeasurementUnitSetups.Sum(mu => mu.InvoiceItems
                             .Where(ii => ii.Invoice.InvoiceDate >= startDate && ii.Invoice.InvoiceDate <= endDate)
                             .Sum(ii => ii.Quantity * mu.ConversionRatio * ii.UnitPrice)),

                  }).OrderBy(i => i.Name);

        }

        public static dynamic SalesByCustomer(this ICustomerRepository repository, DateTime startDate, DateTime endDate)
        {
            var Customers = repository.DbSet
                .Include("Orders")
                .Include("Orders.Invoices")
                .Include("Orders.Invoices.InvoiceItems")
                .Include("Orders.Invoices.InvoiceItems.MeasurementUnitSetup")
                .ToList();

            return Customers
                  .Select(c => new
                  {
                      Name = c.FirstName + " " + c.LastName,
                      TotalInvoice = c.Orders.Select(o => o.Invoices
                        .Where(i => i.InvoiceDate >= startDate && i.InvoiceDate <= endDate)).Count(),
                      TotalInvoicedAmount = c.Orders.Sum(o => o.Invoices
                        .Where(i => i.InvoiceDate >= startDate && i.InvoiceDate <= endDate)
                        .Sum(i => i.InvoiceItems.Sum(ii => ii.MeasurementUnitSetup.ConversionRatio * ii.Quantity * ii.UnitPrice))),
                  }).OrderBy(c => c.Name);
        }


        public static dynamic ItemDemandGraph(this IItemRepository repository, Guid itemId)
        {
            var item = repository.DbSet
               .Include("MeasurementUnitSetups")
               .Include("MeasurementUnitSetups.InventoryItems")
               .Include("MeasurementUnitSetups.OrderItems")
               .Include("MeasurementUnitSetups.InvoiceItems")
               .Include("MeasurementUnitSetups.InvoiceItems.Invoice")
               .Include("MeasurementUnitSetups.InventoryItems.Inventory")
               .Include("MeasurementUnitSetups.OrderItems.Order")
               .Where(i => i.Id == itemId)
               .ToList();

            var dates = GetDates();
            List<object> itemDemandGrapValues = new List<object>();
            foreach (DateTime date in dates)
            {
                itemDemandGrapValues.Add(new
                {
                    Month = date.ToString("MMM"),
                    OrderedQuantity = Convert.ToDecimal(item.Sum(mus => mus.MeasurementUnitSetups.Sum(mu => mu.OrderItems.Where(oi => oi.Order.OrderDate.Month == date.Month).Sum(ss => ss.Quantity * mu.ConversionRatio)))),
                    PurchaseQuantity = Convert.ToDecimal(item.Sum(mus => mus.MeasurementUnitSetups.Sum(mu => mu.InventoryItems.Where(oi => oi.Inventory.PurchaseDate.Month == date.Month).Sum(ss => ss.IssuedQuantity * mu.ConversionRatio)))),
                    SoldQuantity = Convert.ToDecimal(item.Sum(mus => mus.MeasurementUnitSetups.Sum(mu => mu.InvoiceItems.Where(oi => oi.Invoice.InvoiceDate.Month == date.Month).Sum(ss => ss.Quantity * mu.ConversionRatio))))
                });
            }
            return itemDemandGrapValues;
        }

        public static List<DateTime> GetDates()
        {
            return Enumerable.Range(1, DateTime.Today.Month)
                .Select(m => new DateTime(DateTime.Today.Year, m, 1)).ToList();
        }
    }
}
