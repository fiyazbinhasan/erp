﻿using ERP.Model.Inventory;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace ERP.Data.Helpers.PdfGenerators
{
    public class BillPdfGenerator
    {
        #region Declaration
        int _totalColumn = 2;
        Document _document;
        Font _fontStyle;
        PdfPTable _pdfTable = new PdfPTable(2);
        PdfPCell _pdfPCell;
        MemoryStream _memoryStream = new MemoryStream();
        Bill _bill = new Bill();
        #endregion

        public byte[] GeneratePdf(Bill bill)
        {
            _bill = bill;

            #region
            _document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            _document.SetMargins(20f, 20f, 20f, 20f);
            _pdfTable.WidthPercentage = 100;
            _pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfWriter.GetInstance(_document, _memoryStream);
            _document.Open();
            _pdfTable.SetWidths(new float[] { 100f, 100f });
            #endregion

            GetPdfHeader();
            GetPdfBody();
            _document.Add(_pdfTable);
            _document.Close();
            return _memoryStream.ToArray();
        }

        private void GetPdfHeader()
        {
            _fontStyle = FontFactory.GetFont("Tahoma", 18f, 1);
            _pdfPCell = new PdfPCell(new Phrase("Company Name", _fontStyle));
            _pdfPCell.Border = 0;
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            _fontStyle = FontFactory.GetFont("Tahoma", 12f, 0);
            _pdfPCell = new PdfPCell(new Phrase("24/B Hatirpool, Dhaka, Bangladesh", _fontStyle));
            _pdfPCell.Border = 0;
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.ExtraParagraphSpace = 20;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            _fontStyle = FontFactory.GetFont("Tahoma", 14f, 1);
            _pdfPCell = new PdfPCell(new Phrase("Bill", _fontStyle));
            _pdfPCell.Border = 0;
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.ExtraParagraphSpace = 20;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
        }

        private void GetPdfBody()
        {
            _fontStyle = FontFactory.GetFont("Tahoma", 12f, 0);
            _pdfPCell = new PdfPCell(GetBillInfo());
            _pdfPCell.Border = 0;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(BillingAndContactInfo());
            _pdfPCell.Border = 0;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
        }

        private PdfPTable GetBillInfo()
        {
            PdfPTable oPdfPTable = new PdfPTable(2);
            oPdfPTable.SetWidths(new float[] { 100f, 100f });

            _pdfPCell = new PdfPCell(new Phrase("Bill No : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_bill.Moniker, _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Bill Date : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_bill.BillDate.ToString("dd-MMM-yyyy"), _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Paid Amount : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_bill.PaidAmount.ToString(), _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Payment Method : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_bill.PaymentMethod.ToString(), _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Order : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_bill.Order.Moniker, _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();
            return oPdfPTable;
        }

        private PdfPTable BillingAndContactInfo()
        {
            var fontStyle = FontFactory.GetFont("Tahoma", 12f, 1);

            PdfPTable oPdfPTable = new PdfPTable(2);
            oPdfPTable.SetWidths(new float[] { 100f, 100f });

            #region Billing Address
            _pdfPCell = new PdfPCell(new Phrase("Billing Address", fontStyle));
            _pdfPCell.Colspan = 2;
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Name : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_bill.Order.Customer.FirstName + " " + _bill.Order.Customer.LastName, _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            if (!string.IsNullOrEmpty(_bill.Order.Customer.Address1))
            {
                _pdfPCell = new PdfPCell(new Phrase("Address : ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_bill.Order.Customer.Address1, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }

            if (!string.IsNullOrEmpty(_bill.Order.Customer.Address2))
            {
                _pdfPCell = new PdfPCell(new Phrase("Address 2: ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_bill.Order.Customer.Address2, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }
            #endregion

            #region Contact
            _pdfPCell = new PdfPCell(new Phrase("Contact Info", fontStyle));
            _pdfPCell.Colspan = 2;
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            if (!string.IsNullOrEmpty(_bill.Order.Customer.Phone))
            {
                _pdfPCell = new PdfPCell(new Phrase("Phone : ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_bill.Order.Customer.Phone, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }

            if (!string.IsNullOrEmpty(_bill.Order.Customer.Email))
            {
                _pdfPCell = new PdfPCell(new Phrase("Email: ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_bill.Order.Customer.Email, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }
            #endregion
            return oPdfPTable;
        }
    }
}
