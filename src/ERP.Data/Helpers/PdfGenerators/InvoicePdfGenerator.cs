﻿using ERP.Model.Inventory;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Linq;

namespace ERP.Data.Helpers.PdfGenerators
{
    public class InvoicePdfGenerator
    {
        #region Declaration
        int _totalColumn = 8;
        Document _document;
        Font _fontStyle;
        Font _fontStyleBold;
        PdfPTable _pdfTable = new PdfPTable(8);
        PdfPCell _pdfPCell;
        MemoryStream _memoryStream = new MemoryStream();
        Invoice _invoice = new Invoice();
        #endregion

        public byte[] GeneratePdf(Invoice invoice)
        {
            _invoice = invoice;
            #region
            _document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            _document.SetMargins(20f, 20f, 20f, 20f);
            _pdfTable.WidthPercentage = 100;
            _pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfWriter.GetInstance(_document, _memoryStream);
            _document.Open();
            _pdfTable.SetWidths(new float[] { 55f, 200f, 100f, 100f, 100f, 100f, 200f, 100f });
            #endregion

            GetPdfHeader();
            GetPdfBody();
            _document.Add(_pdfTable);
            _document.Close();
            return _memoryStream.ToArray();
        }

        private void GetPdfHeader()
        {
            _fontStyle = FontFactory.GetFont("Tahoma", 18f, 1);
            _pdfPCell = new PdfPCell(new Phrase("Company Name", _fontStyle));
            _pdfPCell.Border = 0;
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            _fontStyle = FontFactory.GetFont("Tahoma", 12f, 0);
            _pdfPCell = new PdfPCell(new Phrase("24/B Hatirpool, Dhaka, Bangladesh", _fontStyle));
            _pdfPCell.Border = 0;
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.ExtraParagraphSpace = 20;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            _fontStyle = FontFactory.GetFont("Tahoma", 14f, 1);
            _pdfPCell = new PdfPCell(new Phrase("Invoice", _fontStyle));
            _pdfPCell.Border = 0;
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.ExtraParagraphSpace = 20;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
        }

        private void GetPdfBody()
        {
            _fontStyle = FontFactory.GetFont("Tahoma", 12f, 0);
            _pdfPCell = new PdfPCell(GetInvoiceInfo());
            _pdfPCell.Colspan = 4;
            _pdfPCell.Border = 0;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(BillingAndContactInfo());
            _pdfPCell.Colspan = 4;
            _pdfPCell.Border = 0;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            this.Blank(10);

            _pdfPCell = new PdfPCell(new Phrase("Items Description", _fontStyle));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.Border = 0;
            _pdfPCell.ExtraParagraphSpace = 15;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();

            #region Table header
            _fontStyle = FontFactory.GetFont("Tahoma", 12f, 1);

            _pdfPCell = new PdfPCell(new Phrase("SLn", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Name", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Quantity", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("M.Unit", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Unit Price", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Discount", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Amount", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("Remarks", _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _pdfPCell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
            #endregion

            #region Table Body

            _fontStyle = FontFactory.GetFont("Tahoma", 10f, 0);

            int serialNumber = 1;
            foreach (var invoiceItem in _invoice.InvoiceItems)
            {
                _pdfPCell = new PdfPCell(new Phrase(serialNumber++.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(invoiceItem.MeasurementUnitSetup.Item.Name, _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(invoiceItem.Quantity.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(invoiceItem.MeasurementUnitSetup.MeasurementUnit.Name, _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(invoiceItem.UnitPrice.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(invoiceItem.Discount.ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase((invoiceItem.Quantity * (invoiceItem.UnitPrice - invoiceItem.Discount)).ToString(), _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(invoiceItem.Remarks, _fontStyle));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                _pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _pdfPCell.BackgroundColor = BaseColor.WHITE;
                _pdfTable.AddCell(_pdfPCell);
                _pdfTable.CompleteRow();
            }
            #endregion

            #region Total
            _fontStyle = FontFactory.GetFont("Tahoma", 12f, 1);

            _pdfPCell = new PdfPCell(new Phrase("Total : ", _fontStyle));
            _pdfPCell.Colspan = _totalColumn - 2;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_invoice.InvoiceItems.Sum(oi => oi.Quantity * (oi.UnitPrice - oi.Discount)).ToString(), _fontStyle));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            _pdfTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase("", _fontStyle));
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
            #endregion
        }

        private PdfPTable GetInvoiceInfo()
        {
            PdfPTable oPdfPTable = new PdfPTable(2);
            oPdfPTable.SetWidths(new float[] { 100f, 100f });

            _pdfPCell = new PdfPCell(new Phrase("Invoice No : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_invoice.Moniker, _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Invoice Date : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_invoice.InvoiceDate.ToString("dd-MMM-yyyy"), _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Order : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_invoice.Order.Moniker, _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            return oPdfPTable;
        }

        private PdfPTable BillingAndContactInfo()
        {
            var fontStyle = FontFactory.GetFont("Tahoma", 12f, 1);

            PdfPTable oPdfPTable = new PdfPTable(2);
            oPdfPTable.SetWidths(new float[] { 100f, 100f });

            #region Billing Address
            _pdfPCell = new PdfPCell(new Phrase("Billing Address", fontStyle));
            _pdfPCell.Colspan = 2;
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            _pdfPCell = new PdfPCell(new Phrase("Name : ", _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);

            _pdfPCell = new PdfPCell(new Phrase(_invoice.Order.Customer.FirstName + " " + _invoice.Order.Customer.LastName, _fontStyle));
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            if (!string.IsNullOrEmpty(_invoice.Order.Customer.Address1))
            {
                _pdfPCell = new PdfPCell(new Phrase("Address : ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_invoice.Order.Customer.Address1, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }

            if (!string.IsNullOrEmpty(_invoice.Order.Customer.Address2))
            {
                _pdfPCell = new PdfPCell(new Phrase("Address 2: ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_invoice.Order.Customer.Address2, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }
            #endregion

            #region Contact
            _pdfPCell = new PdfPCell(new Phrase("Contact Info", fontStyle));
            _pdfPCell.Colspan = 2;
            _pdfPCell.Border = 0;
            oPdfPTable.AddCell(_pdfPCell);
            oPdfPTable.CompleteRow();

            if (!string.IsNullOrEmpty(_invoice.Order.Customer.Phone))
            {
                _pdfPCell = new PdfPCell(new Phrase("Phone : ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_invoice.Order.Customer.Phone, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }

            if (!string.IsNullOrEmpty(_invoice.Order.Customer.Email))
            {
                _pdfPCell = new PdfPCell(new Phrase("Email: ", _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);

                _pdfPCell = new PdfPCell(new Phrase(_invoice.Order.Customer.Email, _fontStyle));
                _pdfPCell.Border = 0;
                oPdfPTable.AddCell(_pdfPCell);
                oPdfPTable.CompleteRow();
            }
            #endregion
            return oPdfPTable;
        }

        private void Blank(int nFixedHeight)
        {
            _pdfPCell = new PdfPCell(new Phrase("", _fontStyle));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.Border = 0;
            _pdfPCell.FixedHeight = nFixedHeight;
            _pdfTable.AddCell(_pdfPCell);
            _pdfTable.CompleteRow();
        }
    }
}
