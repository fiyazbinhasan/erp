﻿using System;
using System.Collections.Generic;
using System.Text;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;

namespace ERP.Data.Helpers
{
    public class IdentityServerConfiguration
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            var role = new IdentityResource("role", "User Role", new[] { "admin", "manager" });

            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                role
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource
                {
                    Name="inventory",
                    Scopes =
                    {
                        new Scope
                        {
                            Name = "inventoy.full_access",
                            DisplayName = "Full Access to Inventory"
                        },
                        new Scope
                        {
                            Name = "inventoy.readonly",
                            DisplayName = "Read Only Access to Inventory"
                        }
                    },
                    UserClaims =
                    {
                        JwtClaimTypes.Profile,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.Role
                    }
                }
            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {
                // OpenID Connect hybrid flow and client credentials client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    ClientUri = "http://localhost:5000/",

                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    AllowOfflineAccess = true,

                    ClientSecrets = { new Secret("secret".Sha256()) },
                    //RequireConsent = true,

                    RedirectUris = { "http://localhost:5002/signin-oidc" },
                    PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "role",
                        "inventory",
                        "inventoy.full_access",
                        "inventoy.readonly"
                    } 
                }
            };
        }
    }
}
