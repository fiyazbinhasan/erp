﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class InventoryItemConfiguration : EntityTypeConfiguration<InventoryItem>
    {
        public override void Map(EntityTypeBuilder<InventoryItem> builder)
        {
            builder.HasIndex(ii => new { ii.InventoryId, ii.MeasurementUnitSetupId, ii.WarehouseId })
                .IsUnique();
            CommonAuditFieldsConfiguration<InventoryItem>.AddShadowAuditFields(builder);
        }
    }
}
