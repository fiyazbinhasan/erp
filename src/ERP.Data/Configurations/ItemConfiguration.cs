﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class ItemConfiguration : EntityTypeConfiguration<Item>
    {
        public override void Map(EntityTypeBuilder<Item> builder)
        {
            builder.HasIndex(i => i.Name).IsUnique();

            CommonAuditFieldsConfiguration<Item>.AddShadowAuditFields(builder);
        }
    }
}
