﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class SupplierConfiguration : EntityTypeConfiguration<Supplier>
    {
        public override void Map(EntityTypeBuilder<Supplier> builder)
        {
            builder.HasIndex(s => s.Phone).IsUnique();
            CommonAuditFieldsConfiguration<Supplier>.AddShadowAuditFields(builder);
        }
    }
}
