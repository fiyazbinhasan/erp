﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class InventoryConfiguration : EntityTypeConfiguration<Inventory>
    {
        public override void Map(EntityTypeBuilder<Inventory> builder)
        {
            CommonAuditFieldsConfiguration<Inventory>.AddShadowAuditFields(builder);
            builder.HasIndex(a => a.Moniker).IsUnique();
        }

    }
}
