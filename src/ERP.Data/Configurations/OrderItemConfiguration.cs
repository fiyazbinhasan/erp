﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class OrderItemConfiguration : EntityTypeConfiguration<OrderItem>
    {
        public override void Map(EntityTypeBuilder<OrderItem> builder)
        {
            builder.HasIndex(ii => new { ii.OrderId, ii.MeasurementUnitSetupId })
                .IsUnique();

            CommonAuditFieldsConfiguration<OrderItem>.AddShadowAuditFields(builder);
        }
    }
}
