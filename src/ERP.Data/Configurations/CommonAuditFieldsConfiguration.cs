﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public static class CommonAuditFieldsConfiguration<TEntity> where TEntity : class
    {
        public static void AddShadowAuditFields(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property<byte[]>("Version")
                .IsConcurrencyToken()
                .IsRowVersion();

            builder.Property<DateTime?>("Created");

            builder.Property<DateTime?>("Modified");
        }
    }
}
