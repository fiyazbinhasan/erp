﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    class InvoiceItemConfiguration : EntityTypeConfiguration<InvoiceItem>
    {
        public override void Map(EntityTypeBuilder<InvoiceItem> builder)
        {
            builder.HasIndex(ii => new {ii.InvoiceId, ii.WarehouseId, ii.MeasurementUnitSetupId})
                .IsUnique();

            CommonAuditFieldsConfiguration<InvoiceItem>.AddShadowAuditFields(builder);
        }
    }
}
