﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class WarehouseConfiguration : EntityTypeConfiguration<Warehouse>
    {
        public override void Map(EntityTypeBuilder<Warehouse> builder)
        {
            builder.HasIndex(i => i.Moniker).IsUnique();
            CommonAuditFieldsConfiguration<Warehouse>.AddShadowAuditFields(builder);
        }
    }
}
