﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public override void Map(EntityTypeBuilder<Order> builder)
        {
            CommonAuditFieldsConfiguration<Order>.AddShadowAuditFields(builder);
            builder.HasIndex(a => a.Moniker).IsUnique();
        }
    }
}
