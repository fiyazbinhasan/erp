﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class MeasurementUnitConfiguration : EntityTypeConfiguration<MeasurementUnit>
    {
        public override void Map(EntityTypeBuilder<MeasurementUnit> builder)
        {
            builder.HasIndex(i => i.Name).IsUnique();

            CommonAuditFieldsConfiguration<MeasurementUnit>.AddShadowAuditFields(builder);
        }
    }
}
