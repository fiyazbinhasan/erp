﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class InvoiceConfiguration : EntityTypeConfiguration<Invoice>
    {
        public override void Map(EntityTypeBuilder<Invoice> builder)
        {
            CommonAuditFieldsConfiguration<Invoice>.AddShadowAuditFields(builder);
            builder.HasIndex(a => a.Moniker).IsUnique();
        }
    }
}
