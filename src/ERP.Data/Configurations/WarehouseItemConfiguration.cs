﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class WarehouseItemConfiguration : EntityTypeConfiguration<WarehouseItem>
    {
        public override void Map(EntityTypeBuilder<WarehouseItem> builder)
        {
            builder.HasIndex(ii => new { ii.WarehouseId, ii.MeasurementUnitSetupId }).IsUnique();
            CommonAuditFieldsConfiguration<WarehouseItem>.AddShadowAuditFields(builder);
        }
    }
}
