﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public override void Map(EntityTypeBuilder<Customer> builder)
        {
            CommonAuditFieldsConfiguration<Customer>.AddShadowAuditFields(builder);
            builder.HasIndex(a => a.Phone).IsUnique();
        }
    }
}
