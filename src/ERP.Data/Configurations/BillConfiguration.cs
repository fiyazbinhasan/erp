﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    public class BillConfiguration : EntityTypeConfiguration<Bill>
    {
        public override void Map(EntityTypeBuilder<Bill> builder)
        {
            CommonAuditFieldsConfiguration<Bill>.AddShadowAuditFields(builder);
            builder.HasIndex(a => a.Moniker).IsUnique();
        }
    }
}
