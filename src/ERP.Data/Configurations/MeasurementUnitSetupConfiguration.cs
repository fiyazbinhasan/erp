﻿using ERP.Model.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Data.Configurations
{
    class MeasurementUnitSetupConfiguration : EntityTypeConfiguration<MeasurementUnitSetup>
    {
        public override void Map(EntityTypeBuilder<MeasurementUnitSetup> builder)
        {
            builder.HasIndex(ii => new { ii.ItemId, ii.MeasurementUnitId }).IsUnique();
            CommonAuditFieldsConfiguration<MeasurementUnitSetup>.AddShadowAuditFields(builder);
        }
    }
}
